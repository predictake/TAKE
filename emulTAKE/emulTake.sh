#!/usr/local/bin/bash


#usage
# to deploy and run a simulation: sudo ./emulTake.sh run
# to collect the data after a simulation


USERNAME="take"
PASSWORD="take"

COUNTER=0
COUNTER2=0

DEPLOY=0
START_JAVA=0
START_MESSENGING=1

export SSHPASS=${PASSWORD}

#Load HOSTS
mapfile -t HOSTS < <(grep '^[^#;]' hosts.csv)

if [ $1 == 'run' ] ; then

	if [ $DEPLOY == 1 ] ; then
		echo ""
		echo "Deploying files on Hosts"
		for HOSTNAME in ${HOSTS[@]} ; do
			echo "--   HOST <$HOSTNAME> --"
			sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'pkill java'
			sshpass -p take scp -o StrictHostKeyChecking=no node_settings.csv ${USERNAME}@${HOSTNAME}:.
			sshpass -p take scp -o StrictHostKeyChecking=no config.properties.node${COUNTER} ${USERNAME}@${HOSTNAME}:./config.properties
			sshpass -p take scp -o StrictHostKeyChecking=no TakeMessengerDistrib.json ${USERNAME}@${HOSTNAME}:.
			sshpass -p take scp -o StrictHostKeyChecking=no takeMessenger.py ${USERNAME}@${HOSTNAME}:.
			sshpass -p take scp -o StrictHostKeyChecking=no startTakeApp.sh ${USERNAME}@${HOSTNAME}:.
			sshpass -p take scp -o StrictHostKeyChecking=no startTakeMessenger.sh ${USERNAME}@${HOSTNAME}:.
			sshpass -p take scp -o StrictHostKeyChecking=no take.jar ${USERNAME}@${HOSTNAME}:.
			sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm logs/take.log'
			COUNTER=$((COUNTER+1))
		done
	fi

	if [ $START_JAVA == 1 ] ; then
		echo ""
		echo "Starting TakeApp application on Hosts"
		for HOSTNAME in ${HOSTS[@]} ; do
			echo "--  HOST <$HOSTNAME> --"
			#sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'pkill java'
			sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'nohup ./startTakeApp.sh  > /dev/null 2>&1 &'
			#sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'curl http://localhost:8080/take/app/reset'
		done
		sleep 10
		echo ""
		echo "Reset TakeApp application on Hosts"
		for HOSTNAME in ${HOSTS[@]} ; do
			echo "--  HOST <$HOSTNAME> --"
			#sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'pkill java'
			#sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'nohup ./startTakeApp.sh  > /dev/null 2>&1 &'
			sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'curl http://localhost:8080/take/app/reset'
		done
	fi

	if [ $START_MESSENGING == 1 ] ; then
		echo ""
		echo "Starting Messenging application on Hosts"
		for HOSTNAME in ${HOSTS[@]} ; do
			echo "--  HOST <$HOSTNAME> --"
			sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} "nohup ./startTakeMessenger.sh $COUNTER2 > /dev/null 2>&1 &"
			COUNTER2=$((COUNTER2+1))
		done
	fi

elif [ $1 == 'collect' ] ; then
	echo 'Sending messages to generate Statistics:'
	current_datetime="`date +%Y-%m-%d_%H-%M`"
	#echo $current_datetime

	for HOSTNAME in ${HOSTS[@]} ; do
		echo "--  HOST <$HOSTNAME> --"
		#TOTEST - Send the curl command to generate stats
		sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'curl --connect-timeout 3 --max-time 3 http://localhost:8080/take/app/savetransmissionstats' 
	done

	sleep 0

	echo 'Collecting data from nodes:'
	i=0
	mkdir -p Stats/${current_datetime}/Collected/
	for HOSTNAME in ${HOSTS[@]} ; do
		echo "--  HOST <$HOSTNAME> --"
		#TOTEST - Send the curl command to generate stats
		#sshpass -e ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'curl --connect-timeout 3 --max-time 3 http://localhost:8080/take/app/savetransmissionstats' 
		sshpass -p take scp -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME}:./data/transmission_stats.json ./Stats/${current_datetime}/Collected/node-${i}.json
		i=$((i+1))
	done
	echo "Generating statistics from collected data:"
	python takeStats.py ./Stats/${current_datetime}
fi
	







