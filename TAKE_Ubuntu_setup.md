VM Ubuntu TAKE
username:take
password:take

# 1 Change MAC in virtualbox before to start the VM
Adapter 1 and 2 in settings, change their MAC to avoid having same MAC on different machines.

# 2 Change hostname in /etc/hostname
sudo vim /etc/hostname

# 3 Change IP(s)
Change IP according to your own network for the interfaces wireless and ethernet (enp0s3 enp0s8).

sudo vim /etc/networks/interfaces
