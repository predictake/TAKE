function [] = create_traffic(src,dst,pktsize,start_time,speed)
%Flows occur
%src: source node
%dst: destination node
%pktsize: Byte
%speed:Kb/s
%start_time: s
global_statement;
np = floor((1000*speed)/(8*pktsize));
interval = 1/np;
n = floor(np*(STOPTIME-start_time));
instant_temp = start_time;


% clear tempe;
% tempe = create_event('send_app',src,start_time,0,0);
% tempe.app.atype = 'send_data';
% pkt = create_packet(src,'DATA');
% pkt.src = src;
% pkt.dst = dst;
% pkt.size = pktsize + LEN_IP + LEN_MAC;
% tempe.packet = pkt;
% EventList =[EventList; tempe];

for i=1:1:n
    clear tempe;
    tempe = create_event('send_app',src,instant_temp,0,0);
    tempe.app.atype = 'send_data';
    if(i==n)
        pkt = create_packet(src,'DATA',1);
    else
        pkt = create_packet(src,'DATA',0);
    end        
    pkt.src = src;
    pkt.dst = dst;
    pkt.size = pktsize +  LEN_IP + LEN_MAC;
    tempe.packet = pkt;
    EventList =[EventList; tempe];
    instant_temp = instant_temp + interval;
end
end

