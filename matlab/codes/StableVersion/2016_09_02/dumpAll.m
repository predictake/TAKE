function [] = dumpAll(time)
    %DUMPALL : dump all rt from nodes and their position at current time
    %   Detailed explanation goes here
    global_statement;
    if (time < 1), return; end
    for i=1:1:N
        export(export_index).time = time;
        export(export_index).node(i).rt = NodeList(i).rt;
        export(export_index).node(i).pos = NodeList(i).pos;
        export(export_index).node(i).emitSpeed = NodeList(i).emitSpeed;
        export(export_index).node(i).id = NodeList(i).id;
    end
    export_index = export_index + 1;

end

