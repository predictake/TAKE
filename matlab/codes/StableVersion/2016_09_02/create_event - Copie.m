function [event] = create_event(type,nid,instant)
%Create standardized event, Eliminate inconsistencies
    event.instant = instant;
    event.etype = type;
    event.nodeid = nid;
    event.app.atype = [];
    event.packet.id = [];
end