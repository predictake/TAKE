function [event] = create_event(type,nid,instant,src,dst)
%Create standardized event, Eliminate inconsistencies
    event.instant = instant;
    event.etype = type;
    event.nodeid = nid;
    event.app.atype = [];
    event.packet.id = [];
    %specific msg part
    %means nothing for other events
    event.src = src;
    event.dst = dst;
end
