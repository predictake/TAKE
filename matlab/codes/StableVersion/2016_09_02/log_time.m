function [  ] = log_time(id)
    % Write a time log file to know how much time does it take to
    % send a message and get an ACK
    %   Detailed explanation goes here
    global_statement;
    %str_id = sprintf
    str_from = sprintf('from : %d\t', msgLog(id).src);
    str_to = sprintf('to : %d\t', msgLog(id).dst);
    str_startTime = sprintf('start : %.4f\t', msgLog(id).startTime);
    str_endTime = sprintf('end : %.4f\t',msgLog(id).endTime);
    totalTime = msgLog(id).endTime - msgLog(id).startTime;
    str_totalTime = sprintf('total time : %.4f\t',totalTime);
    str = sprintf(' %s %s %s %s ', str_from, str_to, str_startTime, str_endTime, str_totalTime);
    
    fid = fopen(TIMELOG_FILENAME, 'a');
    if fid == -1, error('Cannot open log file'); end
    fprintf(fid,'%s\n',str);
    fclose(fid);
    

end

