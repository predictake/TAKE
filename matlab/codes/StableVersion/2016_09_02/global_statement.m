global N TOPO_W TOPO_H STOPTIME MOB_INTERVAL;
global HELLO_INTERVAL NEIGHB_HOLD_TIME TC_HOLD_TIME TC_INTERVAL;
global PROP_DISTANCE;
global RX_QUE_TIME TX_QUE_TIME RX_Queue TX_Queue SQUE_LEN RQUE_LEN PROC_TIME BW DATA_PKT_SIZE;
global LEN_IP LEN_MAC LEN_OLSR;
global packet_id node_id;
global NodeList EventList;
global current_time;
global adebug ldebug IDEBUG;
global LOG_FILENAME LOG_FLAG;
global TTL_PKT;
global Monitor;

global ACTIVATE_TRAFFIC_IN_LOG_ONLY;
global TRAFFIC_LOG_FILENAME;
global FROM TO;
global START_MSG_TIME;
global MSG_SIZE MSG_ACK_SIZE;

global MTU;

global TIMELOG_FILENAME;

global msgLog timelog_index;
global NBR_MSG;

global pairSrcDst;
global DEFAULT_EMIT_SPEED; %in kb/s
global WAITING_BEFORE_SEND_ACK;
global ROUTING_TREATMENT_TIME;

global FLOODING;
global SEND_MSG;
global SEND_RANDOM_MSG;

global TEST_STABLE_NETWORK;
global nodesToReach timeToReachNodes isFirstNodeReached isSecondNodeReached isThirdNodeReached TEST_NETWORK_FILENAME;

global export OLSR_ON;
global DUMP_RT_INTERVAL export_index;
global APPEAR_TEST;

global GRAPH_T_AXIS_INCREMENT;
global CALCULATE_HOP_MEAN;
global graphLogMean graphLogMean_index;

global CALCULATE_NETWORK_DEBIT;
global GRAPH_T_AXIS_INCREMENT_DEBIT;

global datasGraphDebit;
global datasGraphDebit_index;
global graphDebit;


