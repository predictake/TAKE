function [ ] = create_olsr( )
%Event -related settings with OLSR
global_statement;
%HELLO message events
instant_temp = rand(N,1)*3; %Random start , because to prevent simultaneous transmission queue is full OLSR control messages lead to loss
for k=1:1:N
    nrepeat=floor((STOPTIME-instant_temp(k))/HELLO_INTERVAL);
    for h = 1:1:nrepeat+1
        clear tempe;
        tempe = create_event('send_app',k,instant_temp(k),0,0);
        tempe.app.atype = 'send_hello';
        EventList =[EventList; tempe];
        instant_temp(k) = instant_temp(k) + HELLO_INTERVAL;
    end
end

%TC message events
instant_temp = rand(N,1)*3; %Random start , because to prevent simultaneous transmission queue is full OLSR control messages lead to loss

for k=1:1:N
    nrepeat=floor((STOPTIME-instant_temp(k))/TC_INTERVAL);
    for h = 1:1:nrepeat+1
        clear tempe;
        tempe = create_event('send_app',k,instant_temp(k),0,0);
        tempe.app.atype = 'send_tc';
        EventList =[EventList; tempe];
        instant_temp(k) = instant_temp(k) + TC_INTERVAL;
    end
end
end

