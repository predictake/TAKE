function [] = create_msg_traffic(src,dst,pktsize,start_time, type)
    %src: source node
    %dst: destination node
    %pktsize: Byte
    %start_time: s
    %msg type: 0=MSG, 1=ACK
    global_statement;
    pktPartId = 0;
    mustPktBeSepared = 0;
    if((pktsize + LEN_IP + LEN_MAC)> MTU)
        mustPktBeSepared = 1;
    end
    if(mustPktBeSepared)
        temporisation = 0;
        sendTime = start_time + temporisation;
        currentPktLen = MTU-LEN_IP-LEN_MAC;
        while(pktsize > currentPktLen)
            create_event_to_send_msg_traffic(src,dst,currentPktLen,sendTime,type,0,pktPartId);
            temporisation = temporisation + PROC_TIME;
            sendTime = start_time + temporisation;
            pktsize = pktsize - currentPktLen;
            pktPartId = pktPartId + 1;
        end
        create_event_to_send_msg_traffic(src,dst,pktsize,sendTime,type,1,pktPartId);
    else
      create_event_to_send_msg_traffic(src,dst,pktsize,start_time,type,1,pktPartId);
    end
end

function [] = create_event_to_send_msg_traffic(src,dst,pktsize,start_time, type, isLastPkt, pktPartId)
    global_statement;
    clear tempe;
    tempe = create_event('send_app',src,start_time,src,dst);
    tempe.app.atype = 'send_data';

        

    if(type==0)
        pkt = create_packet(src,'MSG',isLastPkt);
    else 
        pkt = create_packet(src,'MSG_ACK',isLastPkt);
    end
    pkt.src = src;
    pkt.dst = dst;
    pkt.size = pktsize + LEN_IP + LEN_MAC;
    pkt.partId = pktPartId;
    tempe.packet = pkt;
    if(pktPartId==0 && type==0)
        index = findNextTimelogMatrixIndex();
        msgLog(index).src = src;
        msgLog(index).dst = dst;
        msgLog(index).isTaken = 1;
    end
    
    EventList =[EventList; tempe];
end

