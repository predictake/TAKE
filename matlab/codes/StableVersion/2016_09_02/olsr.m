clear;
clc;
close all;
global_statement;
parameters;

%--------SIMULATION PARAMETERS--------
OLSR_ON = 1;                                %activate OLSR traffic
DUMP_RT_INTERVAL = 0.05;                    %interval for dumping rt over time or for reading rt over time
APPEAR_TEST = 0;                            %activate APPEAR test of a node
TEST_STABLE_NETWORK = 0;                    %activate testing network stability by sending message from beginning of simulation to response of the dst node
FLOODING = 0;                               %activate flooding between 2 nodes
SEND_MSG = 0;                               %activate a one shot message from one/two node(s) to one/two node(s)
SEND_RANDOM_MSG = 1;                        %activate generation of msg from random node to random node at random time
NBR_MSG = 100;                              %set number of random message
ACTIVATE_TRAFFIC_IN_LOG_ONLY = 1;           %activate traffic log (log only physical layer)
CALCULATE_HOP_MEAN = 0;                     %activate meanHops calculation
CALCULATE_NETWORK_DEBIT =1;                 %activate network traffic over time calculation
GRAPH_T_AXIS_INCREMENT_DEBIT = 0.001;       
GRAPH_T_AXIS_INCREMENT = DUMP_RT_INTERVAL;
%-------------------------------------


if (OLSR_ON)
    create_topo();
    plot_topo();
    create_olsr();
else
    rng('shuffle');
    pos_temp=rand(N,2);
    for i=1:1:N
        pos_temp(i,1)=pos_temp(i,1)*TOPO_W;
        pos_temp(i,2)=pos_temp(i,2)*TOPO_H;
    end
    export(1).node(1).pos = [0 0];
    export(1).node(1).rt = [];
    export(1).node(1).id = 0;
    export(1).node(1).emitSpeed = 0;
    load('export.mat');
    for i=1:1:N
       NodeList(i).rt = export(1).node(i).rt;
       NodeList(i).tqueue = [];
       NodeList(i).rqueue = [];
       NodeList(i).emitSpeed = export(1).node(i).emitSpeed;
       NodeList(i).id = export(1).node(i).id;
       NodeList(i).ds = [];
       NodeList(i).pos = pos_temp(i,:);
    end
    plot_topo();    
end 

drawnow;
init_timelogMatrix();

% create event for dumping all nodes or for reading values
if(OLSR_ON)
    for i=0:DUMP_RT_INTERVAL:STOPTIME
        clear tempe;
        tempe = create_event('dump_all',0,i,0,0);
        at(i,tempe);
    end
else
    for i=0:DUMP_RT_INTERVAL:STOPTIME
        clear tempe;
        tempe = create_event('read_all_rt',0,i,0,0);
        at(i,tempe);
    end
end

% Move at instant a specific node (principle of appear and disapear
if(APPEAR_TEST)
    DISAPEAR_START = 2;
    DISAPEAR_STOP = 4;
    DISAPEARING_NODE = 1;
    tempe = create_event('disapear',DISAPEARING_NODE, DISAPEAR_START,0,0);
    at(pairSrcDst(1).start,tempe);
end

% Generate traffic between two nodes  distant from 2 or 3 hops(flooding from given time to end of
% simulation)
% see case 'traffic_stable_network' in action.m and create_trafic_stable_network.m
% to have more details
if(TEST_STABLE_NETWORK)
    pairSrcDst(1).src = 1;
    pairSrcDst(1).dst = 4;
    pairSrcDst(1).start = 0;
    tempe = create_event('traffic_stable_network',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
    at(pairSrcDst(1).start,tempe);
end

% Generate traffic between two nodes (flooding from given time to end of
% simulation)
% see case 'traffic' in action.m and create_trafic.m
% to have more details
if(FLOODING)
    pairSrcDst(1).src = 1;
    pairSrcDst(1).dst = 4;
    pairSrcDst(1).start = 3;
    tempe = create_event('traffic',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
    at(pairSrcDst(1).start,tempe);
end
%send a msg from one node to another at given time

%Generate 2 message during smiluation (1 to 4 and 6 to 3 at time 11 and 12)
if(SEND_MSG)
%     pairSrcDst(1).src = 1;
%     pairSrcDst(1).dst = 4;
%     pairSrcDst(1).start = 4;
%     tempe = create_event('msg',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
%     at(pairSrcDst(1).start,tempe);
    pairSrcDst(2).src = 5;
    pairSrcDst(2).dst = 2;
    pairSrcDst(2).start = 5;
    tempe = create_event('msg',pairSrcDst(2).src, pairSrcDst(2).start,pairSrcDst(2).src,pairSrcDst(2).dst);
    at(pairSrcDst(2).start,tempe);
end


%Generate NBR_MSG from random src to random dst at different random times
if(SEND_RANDOM_MSG)
    rng('shuffle'); %cause different values on every program start 
    possibleNodes = 1:N;
    possibleTime = 4:0.0001:STOPTIME-3;
    for i=1:1:NBR_MSG
        src = randi([1 N],1,1);
        dst = randi([1 N],1,1);
        while(dst == src)
            dst = randi([1 N],1,1);
        end
        t = randi([1 length(possibleTime)],1,1);
        if(isCoupleSrcDstPresent(src,dst,possibleTime(t)))
            i = i-1;
        else
            pairSrcDst(i).src = src;
            pairSrcDst(i).dst = dst;
            pairSrcDst(i).time = possibleTime(t);
        end

    end
    for y=1:1:NBR_MSG
        clear tempe;
        tempe = create_event('msg',pairSrcDst(y).src, pairSrcDst(y).time,pairSrcDst(y).src,pairSrcDst(y).dst);
        at(pairSrcDst(y).time,tempe);
    end
end

if(CALCULATE_HOP_MEAN)
    for i=1:GRAPH_T_AXIS_INCREMENT:STOPTIME
        clear tempe;
        tempe = create_event('calculate_hop_mean',0,i,0,0);
        at(i,tempe);
    end
    
end







delete(TRAFFIC_LOG_FILENAME);
delete(TEST_NETWORK_FILENAME);
delete(TIMELOG_FILENAME);
delete(LOG_FILENAME);
run(STOPTIME);
if(OLSR_ON)
    save('NodeList.mat','NodeList');
    save('export.mat','export');
end
str = sprintf(' Mean Number of Hop in the network : %d',logMeanHop(0));
fid = fopen(TIMELOG_FILENAME, 'a');
if fid == -1, error('Cannot open log file'); end
fprintf(fid,'%s\n',str);
fclose(fid);
d = date;
str1 = strcat(date,'_Topology');
if(OLSR_ON)
    str2 = strcat(str1,'_withOLSR.fig');
else
    str2 = strcat(str1,'_withoutOLSR.fig');
end

savefig(str2);
close all;
if(OLSR_ON && TEST_STABLE_NETWORK)
    str = sprintf('Time to reach one hop node = %4.4f, time to reach 2-hop node = %4.4f, time to reach 3-hop node = %4.4f',timeToReachNodes(1),timeToReachNodes(2),timeToReachNodes(3));
    fid = fopen(TEST_NETWORK_FILENAME, 'a');
    if fid == -1, error('Cannot open log file'); end
    fprintf(fid,'%s\n',str);
    fclose(fid);
end
if(CALCULATE_HOP_MEAN)
    X = [graphLogMean.time];
    Y = [graphLogMean.value];
    plot(X,Y);
    if(OLSR_ON)
        title('Mean hops over time with OLSR') 
    else
        title('Mean hops over time without OLSR')
    end
    xlabel('time in seconds') % x-axis label
    ylabel('Mean number of hops') % y-axis label
    stableValueIndex = 0;
    stableValue = 0;
    for i=1:1:size(graphLogMean,2)
          if(graphLogMean(i).value ~= stableValue)
              stableValue = graphLogMean(i).value;
              stableValueIndex = i;
          end
%         if(graphLogMean(i)~=0)
%             if(i>=2)
%                 if(graphLogMean(i) ~= graphLogMean(i-1))
%                     text(i,graphLogMean(i),num2str(graphLogMean(i)));
%                 end
%             else
%                 text(i,graphLogMean(i),num2str(graphLogMean(i)));
%             end
%         end
    end
    disp(num2str(graphLogMean(stableValueIndex).time));
    disp(num2str(graphLogMean(stableValueIndex).value));
    text(graphLogMean(stableValueIndex).time, graphLogMean(stableValueIndex).value, strcat('Stable mean at (',num2str(graphLogMean(stableValueIndex).time),',',num2str(graphLogMean(stableValueIndex).value),')'));
    d=date;
    if(OLSR_ON)
        savefig(strcat(d,'_MeanHopsOverTimeWithOLSR.fig'));  
    else
        savefig(strcat(d,'_MeanHopsOverTimeWithoutOLSR.fig'));
    end
end
close all;

if(CALCULATE_NETWORK_DEBIT && ACTIVATE_TRAFFIC_IN_LOG_ONLY)
    extractInfosFromTraffic_log;
end
disp('end');
