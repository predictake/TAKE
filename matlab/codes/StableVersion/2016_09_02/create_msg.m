function [ ] = create_msg( src, start_time )
    %Create the message file with the src and its start time
    %   Detailed explanation goes here
    str = sprintf('FROM : %d AT : %4.4f\nMSG:\nTake the target',src,start_time);
    fileName = sprintf('messageFrom%d.txt',src);
    fileID = fopen(fileName,'w');
    fprintf(fileID,'%s\n',str);
    fclose(fileID);
end

