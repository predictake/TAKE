function [newid] = new_id(type)
% return a new id for node i

global  packet_id node_id;
newid = 0;
switch type
    case 'packet'
        packet_id = packet_id + 1;
        newid = packet_id;
    case 'node'
        node_id = node_id +1;
        newid = node_id;
end

return;
