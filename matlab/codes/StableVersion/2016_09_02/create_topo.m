function [ ] = create_topo()
    %Create topology : Map of distribution of nodes mobility management
    global_statement;

    isOneOrMoreNodesMoving = 0;


    pos_temp=zeros(N,2);
    %n1
    pos_temp(1,1)=35;
    pos_temp(1,2)=50;
    %n2
    pos_temp(2,1)= 25;
    pos_temp(2,2)= 60;
    %n3
    pos_temp(3,1)= 20;
    pos_temp(3,2) = 50;
    %n4
    pos_temp(4,1)= 15;
    pos_temp(4,2) = 40;
    %n5
    pos_temp(5,1)= 70;
    pos_temp(5,2) = 50;
%     %n6
%     pos_temp(6,1)= 50;
%     pos_temp(6,2) = 80;
%     %n7
%     pos_temp(7,1)= 35;
%     pos_temp(7,2) = 60;
%     %n8
%     pos_temp(8,1)= 60;
%     pos_temp(8,2) = 60;
%     %n9
%     pos_temp(9,1)= 45;
%     pos_temp(9,2) = 70;
%     %n10
%     pos_temp(10,1)= 20;
%     pos_temp(10,2) = 80;


    %Create a random map
    % rand('state', 7);
    % pos_temp=rand(N,2);
    % for i=1:1:N
    %     pos_temp(i,1)=pos_temp(i,1)*TOPO_W;
    %     pos_temp(i,2)=pos_temp(i,2)*TOPO_H;
    % end

    %Create random destinations for all nodes
    dst_temp=rand(N,2);
    for i=1:1:N
        dst_temp(i,1)=dst_temp(i,1)*TOPO_W;
        dst_temp(i,2)=dst_temp(i,2)*TOPO_H;
    end
    %set defined destination for node 10
    % dst_temp(10,1)= 50;
    % dst_temp(10,2)= 30;


    %Create n moving node
    % for i=1:1:N
    %    speed = 3.5;
    %    create_node(pos_temp(i,:),dst_temp(i,:),DEFAULT_EMIT_SPEED,speed);
    % end
    % %Or create n node but only a single node moves here node 10
    for i=1:1:N
        speed = 3.5;
        %     if(i==10)
        %         create_node(pos_temp(i,:),dst_temp(i,:),DEFAULT_EMIT_SPEED,speed);
        %     else
        create_node(pos_temp(i,:),pos_temp(i,:),DEFAULT_EMIT_SPEED,speed);
        %     end
    end
    %NodeList(IDEBUG).dst_pos(1,:) = dst_temp(IDEBUG,:);
    if(isOneOrMoreNodesMoving)
        % %------------------------------------------------
        %Mobility Management
        %Time assignment !!!!!!!!!! event should be careful to correctly
        instant_temp = current_time;
        for j=1:1:floor((STOPTIME-current_time)/MOB_INTERVAL)
            %Mobile incident frequency
            instant_temp = instant_temp + MOB_INTERVAL;
            for i=1:1:N
                clear tempe;
                tempe = create_event('move',i,instant_temp,0,0);
                EventList =[EventList; tempe];
            end
        end
    end

end

