global N TOPO_W TOPO_H STOPTIME MOB_INTERVAL;
global ACTIVATE_TRAFFIC_IN_LOG_ONLY;
global TRAFFIC_LOG_FILENAME;
global HELLO_INTERVAL NEIGHB_HOLD_TIME TC_HOLD_TIME TC_INTERVAL;
global PROP_DISTANCE;
global RX_QUE_TIME TX_QUE_TIME RX_Queue TX_Queue SQUE_LEN RQUE_LEN PROC_TIME BW DATA_PKT_SIZE;
global LEN_IP LEN_MAC LEN_OLSR;
global packet_id node_id;
global NodeList EventList;
global current_time;
global adebug ldebug IDEBUG;
global  LOG_FILENAME LOG_FLAG;
global TTL_PKT;

global FROM TO;
global START_MSG_TIME;
global MSG_SIZE MSG_ACK_SIZE;

global MTU;

global TIMELOG_FILENAME;

global msgLog;
global timelog_index;

global SEND_RANDOM_MSG;
global NBR_RANDOM_MSG;
global START_TIME_RANDOM_MSG;

global DEFAULT_EMIT_SPEED; %in kb/s
global WAITING_BEFORE_SEND_ACK; %in seconds
global ROUTING_TREATMENT_TIME; %in seconds

global pairSrcDst;

global FLOODING;
global SEND_MSG;

global TEST_STABLE_NETWORK;
global nodesToReach timeToReachNodes isFirstNodeReached isSecondNodeReached isThirdNodeReached;
global TEST_NETWORK_FILENAME;

global export OLSR_ON;
global DUMP_RT_INTERVAL export_index;
global APPEAR_TEST;

global GRAPH_T_AXIS_INCREMENT;
global CALCULATE_HOP_MEAN ;
global graphLogMean graphLogMean_index;
global CALCULATE_NETWORK_DEBIT;
global GRAPH_T_AXIS_INCREMENT_DEBIT;
global graphDebit;
global datasGraphDebit;
global datasGraphDebit_index;

global isOneOrMoreNodesMoving;
global generalMovingSpeed;
global SEND_MESSAGE_WHILE_MOVING IS_MOVING_TIME_SET MOVING_TIME_SET;
global CREATE_RANDOM_NODES;

START_TIME_RANDOM_MSG = 4;
CREATE_RANDOM_NODES = 0;
generalMovingSpeed = 3.5;
isOneOrMoreNodesMoving = 0;
SEND_MESSAGE_WHILE_MOVING = 0;
IS_MOVING_TIME_SET = 0;
MOVING_TIME_SET = 5;

export_index = 1;
DUMP_RT_INTERVAL = 1;
datasGraphDebit = [];
datasGraphDebit_index = 1;

GRAPH_T_AXIS_INCREMENT = 1;
CALCULATE_HOP_MEAN = 0;
graphLogMean = [];
graphLogMean_index = 1;

CALCULATE_NETWORK_DEBIT = 1;
GRAPH_T_AXIS_INCREMENT_DEBIT = 0.05;
graphDebit = [];
ACTIVATE_TRAFFIC_IN_LOG_ONLY = 0;
TRAFFIC_LOG_FILENAME = 'traffic_log.txt';

export = [];
OLSR_ON = 1;
APPEAR_TEST = 0;

FLOODING = 0;
SEND_MSG = 0;
SEND_RANDOM_MSG = 0;

TEST_STABLE_NETWORK = 0;
nodesToReach = [6 5 4];
timeToReachNodes = [0 0 0];
isFirstNodeReached = 0;
isSecondNodeReached = 0;
isThirdNodeReached = 0;
TEST_NETWORK_FILENAME = 'test_stable_network.txt';

pairSrcDst = [];
DEFAULT_EMIT_SPEED = 150;
TIMELOG_FILENAME = 'timelog.txt';
timelog_index = 0;
NBR_RANDOM_MSG = 20;
WAITING_BEFORE_SEND_ACK = 0.5;
ROUTING_TREATMENT_TIME = 0.005;
MTU = 1000; %Unit : Byte

MSG_SIZE = 1000; %Unit : Byte
MSG_ACK_SIZE = 100; %Unit : Byte
%Simulation Topology related parameters
N =5;   %Nodes
TOPO_W = 150;   %Topology width unit: m
TOPO_H = 150;   %Topology height unit: m
STOPTIME = 10;   %Simulation time
PROP_DISTANCE = 40; %Unit: m
current_time = 0;
MOB_INTERVAL = 1; %Mobile Frequency
%Logbook
LOG_FILENAME = 'log.txt';
LOG_FLAG = 1;
%debug Related
adebug = 0;
ldebug = 0;
IDEBUG = 5;

%Related groups
TTL_PKT = 255;      %General packet TTL
%OLSR protocol-related
NEIGHB_HOLD_TIME = 6;   %Neighbor record keeping time : second
TC_HOLD_TIME = 15;   %Topology record keeping time : second
HELLO_INTERVAL = 2.00; %Unit : second
TC_INTERVAL = 5.00; %Unit : second ///if new ns send new TC (verif spec)
NodeList = [];
EventList = [];
msgLog = [];

%Physical Layer


%queue
SQUE_LEN = 10;   %Queue Length
RQUE_LEN = 4;   %Queue Length
RX_QUE_TIME = 0.5e-3;  %Single packet queue time second
TX_QUE_TIME = 0.5e-3;
PROC_TIME = 1e-3;   %CPU processing time second
BW = 0.5e1;    %Bandwidth : Mbps
DATA_PKT_SIZE = 500;    %Unit : Byte
RX_Queue = []; %queue
TX_Queue = [];

%Packet header length
LEN_IP = 20;    %Unit : Byte
LEN_MAC = 22;   %Unit : Byte
LEN_OLSR = 4;   %Unit : Byte

packet_id = 0;
node_id = 0;