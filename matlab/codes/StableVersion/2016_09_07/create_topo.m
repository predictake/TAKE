function [ ] = create_topo()
    %Create topology : Map of distribution of nodes mobility management
    global_statement;
    pos_temp=zeros(N,2);
    if(CREATE_RANDOM_NODES)
        %Create a random map
        rand('state', 7);
        pos_temp=rand(N,2);
        for i=1:1:N
            pos_temp(i,1)=pos_temp(i,1)*TOPO_W;
            pos_temp(i,2)=pos_temp(i,2)*TOPO_H;
        end
    else
        %n1
        pos_temp(1,1)=35;
        pos_temp(1,2)=50;
        %n2
        pos_temp(2,1)= 25;
        pos_temp(2,2)= 60;
        %n3
        pos_temp(3,1)= 20;
        pos_temp(3,2) = 50;
        %n4
        pos_temp(4,1)= 15;
        pos_temp(4,2) = 40;
        %n5
        pos_temp(5,1)= 70;
        pos_temp(5,2) = 50;
        %n6
        pos_temp(6,1)= 90;
        pos_temp(6,2) = 50;
%     %n7
%     pos_temp(7,1)= 35;
%     pos_temp(7,2) = 60;
%     %n8
%     pos_temp(8,1)= 60;
%     pos_temp(8,2) = 60;
%     %n9
%     pos_temp(9,1)= 45;
%     pos_temp(9,2) = 70;
%     %n10
%     pos_temp(10,1)= 20;
%     pos_temp(10,2) = 80;
    end
   



   

    %Create random destinations for all nodes
    dst_temp=rand(N,2);
    for i=1:1:N
        dst_temp(i,1)=dst_temp(i,1)*TOPO_W;
        dst_temp(i,2)=dst_temp(i,2)*TOPO_H;
    end
    %set defined destination for node 10
%     dst_temp(4,1)= 70;
%     dst_temp(4,2)= 25;


    %Create n moving node
    % for i=1:1:N
    %    speed = 3.5;
    %    create_node(pos_temp(i,:),dst_temp(i,:),DEFAULT_EMIT_SPEED,speed);
    % end
    % %Or create n node but only a single node moves here node 4
    for i=1:1:N
        speed = generalMovingSpeed;
%         if(i==4)
%             create_node(pos_temp(i,:),dst_temp(i,:),DEFAULT_EMIT_SPEED,speed);
%         else
            create_node(pos_temp(i,:),pos_temp(i,:),DEFAULT_EMIT_SPEED,speed);
%         end
    end
    %NodeList(IDEBUG).dst_pos(1,:) = dst_temp(IDEBUG,:);
    if(isOneOrMoreNodesMoving)
        % %------------------------------------------------
        %Mobility Management
        %Time assignment !!!!!!!!!! event should be careful to correctly
        instant_temp = current_time;
        if(IS_MOVING_TIME_SET)
            tempe = create_event('move',4,MOVING_TIME_SET,0,0);
            EventList =[EventList; tempe];
            clear tempe;
            if(SEND_MESSAGE_WHILE_MOVING)
                pairSrcDst(1).src = 4;
                pairSrcDst(1).dst = 2;
                pairSrcDst(1).start = 4;
                tempe = create_event('msg',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
                at(pairSrcDst(1).start,tempe);
                EventList =[EventList; tempe];
                clear tempe;
                pairSrcDst(1).src = 4;
                pairSrcDst(1).dst = 2;
                pairSrcDst(1).start = MOVING_TIME_SET+10;
                tempe = create_event('msg',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
                at(pairSrcDst(1).start,tempe);
                EventList =[EventList; tempe];
            end
                
        else
            for j=1:1:floor((STOPTIME-current_time)/MOB_INTERVAL)
                %Mobile incident frequency
                instant_temp = instant_temp + MOB_INTERVAL;
                for i=1:1:N
                    clear tempe;
                    tempe = create_event('move',i,instant_temp,0,0);

                    EventList =[EventList; tempe];
    %                 if(SEND_MESSAGE_WHILE_MOVING && (j==1 || j==floor((STOPTIME-current_time)/MOB_INTERVAL)))
    %                     pairSrcDst(1).src = 4;
    %                     pairSrcDst(1).dst = 2;
    %                     pairSrcDst(1).start = i+0.05;
    %                     tempe2 = create_event('msg',pairSrcDst(1).src, pairSrcDst(1).start,pairSrcDst(1).src,pairSrcDst(1).dst);
    %                     at(pairSrcDst(1).start,tempe2);
    %                     EventList =[EventList; tempe2];
    %                 end

                end
            end
        end
    end

end

