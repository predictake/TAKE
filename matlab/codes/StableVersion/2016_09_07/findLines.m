fid=fopen('olsr.m');
lineno=0;
while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
       tline=tline+1;
       % clip % and after %
       tline=strtok(tline,'%')
       % semicolons and elipsis
       % = sign before number or variable
       if isempty(regexp(tline, ...
          '(;|\.\.\.)[\s\n]*$')) & ...
          ~isempty(regexp( ...
          '=\s*[''(){}\[\]\d\w._]+[\s\n*$'...
           ))
           fprintf('Possible hit on %d\n',...
              lineno);
        end;
end; % while
fclose(fid); 