function [] = disapear( nodeid, time )
    global_statement;
    NodeList(nodeid).tc_seq = 1;
    NodeList(nodeid).ns = [];   %OLSR: neighbor list
    NodeList(nodeid).ns2 = [];  %OLSR: 2 -hop neighbor list
    NodeList(nodeid).ts = [];   %OLSR: Topology
    NodeList(nodeid).mprs = []; %OLSR:MPRS
    NodeList(nodeid).mss = [];  %OLSR:MSS
    NodeList(nodeid).ds = [];   %dumplicate set, the record received pktid
    NodeList(nodeid).rt = [];   %Routing Table
end