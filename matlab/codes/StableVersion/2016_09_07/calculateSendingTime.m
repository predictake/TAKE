function [ time ] = calculateSendingTime( emitSpeed, pktSize )
    %Calculate the requiered time for sending a packet on the network
    % depending on the node emit speed
    % pktSize :     Byte
    % emitSpeed :   kb/s
    % time :        seconds
    pktSizeInBits = 8*pktSize;
    time = pktSizeInBits/(1000*emitSpeed);
end

