function [p] = create_packet(nodeid,ptype,isLastPkt)
    %Create a message packet
    global_statement;
    % p.type = [];
    p.id = new_id('packet');
    p.src = [];
    p.dst = [];
    p.size = 0; %Unit : Byte
    p.ttl = TTL_PKT;    %TTL
    p.lastp = isLastPkt;
    switch ptype
        case 'HELLO'
            p.ptype = 'HELLO';
            n0 = NodeList(nodeid);
            p.n_item = [];
            p.mpr_item = [];
            %neighbor node
            if ~isempty(n0.ns)
                for i=1:1:length(n0.ns(:,1))
                    p.n_item = [p.n_item; n0.ns(i,1)];
                    p.size = p.size + length(p.n_item)*4;
                end
            end
            %MPR node
            if ~isempty(n0.mprs)
                for i=1:1:length(n0.mprs(:,1))
                    p.mpr_item = [p.mpr_item; n0.mprs(i,1)];
                    p.size = p.size + length(p.mpr_item)*4;
                end
            end
            %Calculated packet length
            p.size = p.size  + LEN_IP + LEN_MAC + LEN_OLSR;
            %Setting validity
            p.hello_vt = NEIGHB_HOLD_TIME;
        case 'TC'
            p.ptype = 'TC';
            n0 = NodeList(nodeid);
            p.mss_item = [];
            %mss node
            if ~isempty(n0.mss)
                p.mss_item = n0.mss(:,1);
                p.size = p.size + length(p.mss_item)*4;
            end
            %Calculated packet length
            p.size = p.size  + LEN_IP + LEN_MAC + LEN_OLSR;
            %Setting validity
            p.tc_vt = TC_HOLD_TIME;
            p.ansn = n0.tc_seq; %TC serial number, to prevent the old message
        case 'DATA'
            p.ptype = 'DATA';
        case 'STABLE'
            p.ptype = 'STABLE';
        case 'MSG'
            p.ptype = 'MSG';
        case 'MSG_ACK'
            p.ptype = 'MSG_ACK';
    end
end