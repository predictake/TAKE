function [ ] = plot_topo(varargin)
%Drawing Topology
%nargin = 0; draw all node locations
%nargin = 1; parameter is nodeid, draws only a single node
global_statement;
cdebug = 1;
switch nargin
    case 0
        for i=1:1:N
%             hold on;
            plot( NodeList(i).pos(1), NodeList(i).pos(2),'--rs','LineWidth',2,...
                'MarkerEdgeColor','k',...
                'MarkerFaceColor','g',...
                'MarkerSize',10,...
                'Tag','hi')
            text(NodeList(i).pos(1)-2, NodeList(i).pos(2)+4,strcat('n',num2str(i)));
            if cdebug
                %Circle
                alpha=0:pi/20:2*pi;%Angle [0,2 * pi]
                x=PROP_DISTANCE*cos(alpha);
                y=PROP_DISTANCE*sin(alpha);
                plot(x+NodeList(i).pos(1),y+NodeList(i).pos(2),'-')
            end
%             hold on;
        end
        grid on;
%         hold on;
    case 1
        nid = varargin{1};
        plot( NodeList(nid).pos(1), NodeList(nid).pos(2),'--rs','LineWidth',2,...
            'MarkerEdgeColor','k',...
            'MarkerFaceColor','y',...
            'MarkerSize',10,...
            'Tag','hi')
        text(NodeList(nid).pos(1)-2, NodeList(nid).pos(2)+4,strcat('n',num2str(nid)));
        if cdebug
            %Circle
            alpha=0:pi/20:2*pi;%Angle [0,2 * pi]
            x=PROP_DISTANCE*cos(alpha);
            y=PROP_DISTANCE*sin(alpha);
            plot(x+NodeList(nid).pos(1),y+NodeList(nid).pos(2),'-')
        end
        drawnow;
%         hold on;
end
end

