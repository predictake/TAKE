function [ mean ] = logMeanHop(globalMean) %0=global else = nodeid
%LOGMEANHOP Return the mean of the number op hop in the network
% could be for one node or for the complete network
    global_statement;
    counter = 0;
    if(globalMean==0)
        mean = 0;
        for i=1:1:length(NodeList)
            for y=1:1:size(NodeList(i).rt,1)
               mean = mean + NodeList(i).rt(y,3);
               counter = counter + 1;
            end
        end
    else
        mean = 0;
        for y=1:1:size(NodeList(globalMean).rt,1)
            mean = mean + NodeList(globalMean).rt(y,3);
            counter = counter + 1;    
        end
    end
    if(mean>0)
        mean = mean/counter;
    else
        mean = 0;
    end
end

