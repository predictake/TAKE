function [  ] = event_log(event)
    % function [  ] = event_log(logtype, acttype, nodeid, level,pktid, ptype, psize, psrc, pdst)
    % disp([num2str(current_time), ' ', actype, ' ', num2str(nodeid), ' ', level, ' ', str2num(pktid), ' ', ptype, ' ', num2str(psize), ' ',num2str(psrc), ' ', num2str(pdst)]);
    %event_log Summary of this function goes here
    %   Detailed explanation goes here
    global_statement;
    switch event.etype
        case 'move'
        otherwise
            if(~isempty(event.packet.id))
                log_pkt(event);
            else
                return;
            end
    end

    end
function [  ] = log_pkt(event)
    %Associated with the packet log
    global_statement;
    isTrafficLogOn = ACTIVATE_TRAFFIC_IN_LOG_ONLY;
    n0 = NodeList(event.nodeid);
    str_ctime = sprintf('%.4f\t',current_time);
    str_nid = sprintf('%d', event.nodeid);
    if(OLSR_ON),str_pos = sprintf('[%.2f %.2f]',n0.pos(1,1),n0.pos(1,2));end
    if(~isempty(event.packet.id))
        str_pktid = sprintf('%d', event.packet.id);
        str_ptype = sprintf('%s', event.packet.ptype);
        str_psize = sprintf('%d\t', event.packet.size);
        str_psrc = sprintf('%d\t', event.packet.src);
        str_pdst = sprintf('%d\t', event.packet.dst);
    end
    switch event.etype
        case 'msg'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','m');
            str_level = sprintf('%s\t','APP');
        case 'msg_ack'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','m');
            str_level = sprintf('%s\t','APP');
        case 'rcv_phy'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','r');
            str_level = sprintf('%s\t','PHY');
        case 'en_rqueue'
            isTrafficLogOn = 1;
            str_act = sprintf('%c\t','+');
            str_level = sprintf('%s\t','RQUE');
        case 'de_rqueue'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','-');
            str_level = sprintf('%s\t','RQUE');
        case 'rcv_net'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','r');
            str_level = sprintf('%s\t','NET');
        case 'rcv_app'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','r');
            str_level = sprintf('%s\t','APP');
        case 'send_phy'
            str_act = sprintf('%c\t','s');
            str_level = sprintf('%s\t','PHY');
        case 'de_tqueue'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','-');
            str_level = sprintf('%s\t','TQUE');
        case 'en_tqueue'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','+');
            str_level = sprintf('%s\t','TQUE');
        case 'send_net'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','s');
            str_level = sprintf('%s\t','NET');
        case 'send_app'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','s');
            str_level = sprintf('%s\t','APP');
        case 'drop'
            isTrafficLogOn = 0;
            str_act = sprintf('%c\t','d');
            str_level = sprintf('%s\t','QDP');  %Loss
    end

    if(OLSR_ON)
        str = sprintf('%s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s',str_ctime,str_act,str_nid,str_level,str_pktid,str_ptype,str_psize,str_psrc,str_pdst,str_pos);
    else
        str = sprintf('%s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s',str_ctime,str_act,str_nid,str_level,str_pktid,str_ptype,str_psize,str_psrc,str_pdst);
    end
    %Debug display
    if(ldebug), disp(str);   end
    %Written to the log
    fid = fopen(LOG_FILENAME, 'a');
    if fid == -1, error('Cannot open log file'); end
    fprintf(fid,'%s\n',str);
    fclose(fid);
    
    if(isTrafficLogOn)
        fid = fopen(TRAFFIC_LOG_FILENAME, 'a');
        if fid == -1, error('Cannot open log file'); end
        fprintf(fid,'%s\n',str);
        fclose(fid);
        datasGraphDebit(datasGraphDebit_index).time = current_time;
        datasGraphDebit(datasGraphDebit_index).act = str_act;
        datasGraphDebit(datasGraphDebit_index).nid = event.nodeid;
        datasGraphDebit(datasGraphDebit_index).level = str_level;
        datasGraphDebit(datasGraphDebit_index).pktid = event.packet.id;
        datasGraphDebit(datasGraphDebit_index).ptype = event.packet.ptype;
        datasGraphDebit(datasGraphDebit_index).pktsize = event.packet.size;
        datasGraphDebit(datasGraphDebit_index).pdst = event.packet.dst;
        datasGraphDebit_index = datasGraphDebit_index + 1;
        

    end
        
end
