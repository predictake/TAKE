function [ node ] = create_node(pos_i,dest_i,emitSpeed,speed_i)
%Generating node
global_statement
node.id = new_id('node');
node.tc_seq = 1;
node.pos = pos_i(1,:);  %current location
node.dst_pos = dest_i(1,:); %target location
node.speed = speed_i;   %Moving speed
node.emitSpeed = emitSpeed; %Transmit speed in KByte/sec
node.rqueue = RX_Queue; %Receive Queue
node.tqueue = TX_Queue; %Send Queue
node.ns = [];   %OLSR: neighbor list
node.ns2 = [];  %OLSR: 2 -hop neighbor list
node.ts = [];   %OLSR: Topology
node.mprs = []; %OLSR:MPRS
node.mss = [];  %OLSR:MSS
node.ds = [];   %dumplicate set, the record received pktid
node.rt = [];   %Routing Table
NodeList=[NodeList node];
end

