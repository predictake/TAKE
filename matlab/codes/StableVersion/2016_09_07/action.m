function [NewEvents] = action(event)
global_statement;

NewEvents = [];
if LOG_FLAG,    event_log(event);  end
switch event.etype
    case 'calculate_hop_mean'
        graphLogMean(graphLogMean_index).time = event.instant;
        graphLogMean(graphLogMean_index).value = logMeanHop(0);
        graphLogMean_index = graphLogMean_index + 1;
    case 'disapear'
        disapear(event.nodeid,event.instant);
    case 'dump_all'
        dumpAll(event.instant);
    case 'read_all_rt'
        readAllRt(event.instant);
    case 'msg'
        create_msg(event.src,event.instant);
        create_msg_traffic(event.src, event.dst, MSG_SIZE, event.instant,0);
    case 'msg_ack'
        create_msg_traffic(event.nodeid, event.packet.dst, MSG_ACK_SIZE, event.instant+WAITING_BEFORE_SEND_ACK, 1);
    case 'traffic'
        create_traffic(event.src,event.dst,DATA_PKT_SIZE,event.instant,NodeList(event.nodeid).emitSpeed);
    case 'traffic_stable_network'
        create_traffic_stable_network(event.src,event.dst,DATA_PKT_SIZE,event.instant,NodeList(event.nodeid).emitSpeed);

    case 'move'
        n0 = NodeList(event.nodeid);
        %         if(n0.id==6&&current_time >= 20);
        %             1111;
        %         end
        if(~isequal(n0.pos,n0.dst_pos))
            %It hasn't reached the destination
            dist = sqrt((n0.dst_pos(1,1)-n0.pos(1,1))^2+(n0.dst_pos(1,2)-n0.pos(1,2))^2);
            cosv = (n0.dst_pos(1,1)-n0.pos(1,1))/dist; %Speed ??decomposition : cos angle
            sinv = (n0.dst_pos(1,2)-n0.pos(1,2))/dist;  %Speed ??decomposition : sin angle
            x = n0.speed*MOB_INTERVAL*cosv; %Lateral moving distance, may be negative
            y = n0.speed*MOB_INTERVAL*sinv; %Longitudinal moving distance, can be negative
            n0.pos(1,1) = n0.pos(1,1) + x;  %Update the current distance
            n0.pos(1,2) = n0.pos(1,2) + y;
            if(cosv<0)
                %Move to the left
                if(n0.pos(1,1)<=n0.dst_pos(1,1)),   n0.pos(1,:) = n0.dst_pos(1,:); end  %We have arrived
            else
                %Move right
                if(n0.pos(1,1)>=n0.dst_pos(1,1)),   n0.pos(1,:) = n0.dst_pos(1,:); end  %We have arrived
            end
            NodeList(event.nodeid) = n0;
            plot_topo(n0.id);  %end
        end
    case 'dump'
        dump(event.nodeid);
    case 'time_out'
        n0 = NodeList(event.nodeid);
        switch event.app.atype
            case 'ns_expire'
                %Extended neighbor list
                j = find(n0.ns(:,1)==event.app.index);
                if(isempty(j))
                    error('Error: Cannot find NS item');
                end
                if(n0.ns(j,2)>current_time)
                    %No extended : they received a HELLO, update the expiration time
                    return;
                else
                    n0.ns(j,:) = [];
                    NodeList(event.nodeid) = n0;
                    n0.mprs = MPR_select(event);
                    n0.rt = route_cal(event);
                    NodeList(event.nodeid) = n0;
                end
            case 'ns2_expire'
                j = find(ismember(n0.ns2(:,1:2),[event.app.ns_index,event.app.ns2_index],'rows'));
                if(isempty(j))
                    %When updating NS may be deleted NS2, it will not have a logical problem
                    %                    error('Error: Cannot find NS2 item');
                    return;
                end
                if(n0.ns2(j,3)>current_time)
                    %No extended : they received a HELLO, update the expiration time
                    return;
                else
                    n0.ns2(j,:) = [];
                    NodeList(event.nodeid) = n0;
                    n0.mprs = MPR_select(event);
                    n0.rt = route_cal(event);
                    NodeList(event.nodeid) = n0;
                end
            case 'mss_expire'
                %MSS list extended
                j = find(n0.mss(:,1)==event.app.index);
                if(isempty(j))
                    error('Error: Cannot find MSS item');
                end
                if(n0.mss(j,2)>current_time)
                    %No extended : they received a HELLO, update the expiration time
                    return;
                else
                    n0.mss(j,:) = [];
                    NodeList(event.nodeid) = n0;
                    %                     n0.mprs = MPR_select(event);
                    %                     NodeList(event.nodeid) = n0;
                end
            case 'ts_expire'
                j = find(ismember(n0.ts(:,1:2),[event.app.dst_index,event.app.last_index],'rows'));
                if(isempty(j))
                    %When updating ts may be deleted ts, so there will be any problems logic
                    %                    error('Error: Cannot find TS item');
                    return;
                end
                if(n0.ts(j,4)>current_time)
                    %No extended : middle of TC received a new message , update the expiration time
                    return;
                else
                    n0.ts(j,:) = [];
                    NodeList(event.nodeid) = n0;
                    n0.rt = route_cal(event);
                    NodeList(event.nodeid) = n0;
                end
        end
    case 'rcv_app'        
        switch event.app.atype
            case 'rcv_hello'
                if(adebug)
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' received hello from ',num2str(event.packet.src)]);
                end
                %                NewEvents = update_ns(event);
                NewEvents = [NewEvents; update_ns(event)];   %Update NS
                NewEvents = [NewEvents; update_ns2(event)];  %Update NS2
                NewEvents = [NewEvents; update_mss(event)];  %Update MSS
            case 'rcv_tc'
                if event.nodeid==1&event.packet.src==5
                    1111;
                end
                %                 disp([num2str(current_time), '  ',num2str(event.nodeid), ' received tc from ',num2str(event.packet.src)]);
                
                n0 = NodeList(event.nodeid);
                pkt = event.packet;
                k = find(n0.ns==pkt.prev_addr);
                if isempty(k)
%                     error('rcv_tc: tc message is not from neighbor');
%                     Because HELLO queue is full due to packet loss , it may not establish neighbor relations
                    return;
                end
                NewEvents = [NewEvents; update_ts1(event)];   %Update TS
                m = find(n0.mss==pkt.prev_addr);
                if isempty(m),  return; end     %Instead of sending / MPR TC message forwarding node , not forward
                %TTL processing in rcv_net
                if (pkt.ttl>1)
                    pkt.ttl = pkt.ttl - 1;
                else
                    %                  disp(['drop']);
                end
                pkt.prev_addr = n0.id;
                newevent = event;
                newevent.packet = pkt;
                newevent.etype = 'send_app';
                newevent.app.atype = 'fwd_tc';
                NewEvents = [NewEvents; newevent]; clear newevent;
            case 'rcv_data'
                disp([num2str(current_time), '  ',num2str(event.nodeid), ' received data from ',num2str(event.packet.src)]);
            case 'rcv_msg'
                if(event.packet.lastp)
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' received end part of msg from ',num2str(event.packet.src)]);
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' will send MSG_ACK']);
                    readTextMessage(event.packet);
                    newevent = event;
                    firstSrc = newevent.packet.src;
                    newevent.packet.dst = firstSrc;
                    newevent.packet.src = event.nodeid;
                    newevent.etype = 'msg_ack';
                    newevent.app.atype = 'send_data';
                    NewEvents = [NewEvents; newevent]; clear newevent;
                else
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' received part ',num2str(event.packet.partId),' of msg from ',num2str(event.packet.src)]);                    
                end
%                 newevent = event;
%                 firstSrc = newevent.packet.src;
%                 newevent.packet.dst = firstSrc;
%                 newevent.packet.src = event.nodeid;
%                 newevent.etype = 'msg_ack';
%                 newevent.app.atype = 'send_data';
%                 NewEvents = [NewEvents; newevent]; clear newevent;
            case 'rcv_msg_ack'
                if(event.packet.lastp)
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' received end part of msg_ack from ',num2str(event.packet.src)]);
                    index = findIndexForConversation(event.nodeid, event.packet.src);
                    msgLog(index).endTime = current_time;
                    msgLog(index).isTransactionEnded = 1;
                    log_time(index);
                else
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' received part ',num2str(event.packet.partId),' of msg_ack part  from ',num2str(event.packet.src)]);
                end
                
        end
    case 'rcv_phy'
        %Physical layer receives , directly into the receive queue
        newevent = event;
        newevent.etype = 'en_rqueue';
        NewEvents = [NewEvents; newevent]; clear newevent;
    case 'en_rqueue'
        %Into the receive queue
        n0 = NodeList(event.nodeid);
        if(isequal(event.packet.ptype,'MSG')), disp(['__',num2str(event.nodeid),' entering en_rqueue for MSG packet']); end
        if(isequal(event.packet.ptype,'MSG_ACK')), disp(['__',num2str(event.nodeid),' entering en_rqueue for MSG_ACK packet']); end
        pkt = event.packet;
        if(isempty(event.packet.id)), error('rcv_phy:no packet in event');  end
        if(n0.id==event.packet.src),    return; end     %Receive what they have sent a packet forwarding neighbor
        if (pkt.ttl < 1),  return;  end %Beyond the lifetime of the paquet
        k = find(n0.ds==event.packet.id);
        if(~isempty(k))
            %He has handled this packet
            %             disp(['drop']);
            return;
        end
        NodeList(event.nodeid).ds = unique([NodeList(event.nodeid).ds; event.packet.id]); %Record ever received pkt_id, to prevent the repetition processing or forwarding packets
        if length(n0.rqueue)>=RQUE_LEN;
            %Loss
            %             disp(['drop']);
            event_temp = event;
            event_temp.etype = 'drop';
            if LOG_FLAG,    event_log(event_temp);  end
            clear event_temp;
            return;
        end
        %Packet into the queue
        if(event.packet.id~=0)
            n0.rqueue = [n0.rqueue event.packet.id];
        else
            if adebug, error('event.packet.id==0'); end
        end
        NodeList(event.nodeid) = n0;
        newevent = event;
        newevent.etype = 'de_rqueue';
        if(strcmp(event.packet.ptype,'STABLE'))
            if(isFirstNodeReached)
                if(isSecondNodeReached)
                    if(isThirdNodeReached == 0)
                        if(event.nodeid == nodesToReach(3))
                            isThirdNodeReached = 1;
                            timeToReachNodes(3) = current_time;
                            disp('------------First packet to reach 3-hop node');
                        end
                    end
                else
                    if(event.nodeid == nodesToReach(2))
                        isSecondNodeReached = 1;
                        timeToReachNodes(2) = current_time;
                        disp('------------First packet to reach 2-hop node');
                    end
                end    
            else
                if(event.nodeid == nodesToReach(1))
                    isFirstNodeReached = 1;
                    timeToReachNodes(1) = current_time;
                    disp('------------First packet to reach 1-hop node');
                end
            end
        end
        if(strcmp(event.packet.ptype,'MSG') || strcmp(event.packet.ptype,'DATA')|| strcmp(event.packet.ptype,'MSG_ACK'))
            if(strcmp(event.packet.ptype,'MSG')|| strcmp(event.packet.ptype,'MSG_ACK'))
                newevent.instant = newevent.instant + (TX_QUE_TIME+PROC_TIME)*length(n0.rqueue);
            else
                newevent.instant = newevent.instant + (TX_QUE_TIME+PROC_TIME)*length(n0.rqueue);%(length(n0.rqueue)*DATA_PKT_SIZE*8)/BW;
            end
        else
            newevent.instant = newevent.instant + (TX_QUE_TIME+PROC_TIME)*length(n0.rqueue);
        end        
        NewEvents = [NewEvents; newevent]; clear newevent;
        disp([num2str(current_time), '  ',num2str(event.nodeid), '''s rqueue''s length is: ', num2str(length(n0.rqueue))]);
    case 'de_rqueue'
        %The receive queue
        n0 = NodeList(event.nodeid);
        j = find(n0.rqueue==event.packet.id);
        n0.rqueue(j) = [];
        NodeList(event.nodeid) = n0;
        newevent = event;
        newevent.etype = 'rcv_net';
        NewEvents = [NewEvents; newevent]; clear newevent;
    case 'rcv_net'
        if(isempty(event.packet.id)), error('rcv_net:no packet in event');  end
         NodeList(event.nodeid).ds = unique([NodeList(event.nodeid).ds; event.packet.id]);    %Record ever received pkt_id, to prevent the repetition processing or forwarding packets
        if ~isempty(event.app.atype)
            %Determine whether app is used
            switch event.app.atype
                %Received by the app
                case 'send_hello'
                    newevent = event;
                    newevent.etype = 'rcv_app';
                    newevent.app.atype = 'rcv_hello';
                    NewEvents = [NewEvents; newevent]; clear newevent;
                case 'send_tc'
                    newevent = event;
                    newevent.etype = 'rcv_app';
                    newevent.app.atype = 'rcv_tc';
                    NewEvents = [NewEvents; newevent]; clear newevent;
                case 'fwd_tc'
                    newevent = event;
                    newevent.etype = 'rcv_app';
                    newevent.app.atype = 'rcv_tc';
                    NewEvents = [NewEvents; newevent]; clear newevent;
                case 'send_data'
                    n0 = NodeList(event.nodeid);
                    pkt = event.packet;
                    if n0.id==pkt.dst
                        %We have reached the destination
                        newevent = event;
                        newevent.etype = 'rcv_app';
                        if(isequal(newevent.packet.ptype,'MSG'))
                            newevent.app.atype = 'rcv_msg';
                        elseif(isequal(newevent.packet.ptype,'MSG_ACK'))
                            newevent.app.atype = 'rcv_msg_ack';
                        else
                            disp('hello');
                            newevent.app.atype = 'rcv_data';
                        end
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    else
                        newevent = event;
                        newevent.etype = 'send_net';
                        newevent.app.atype = 'fwd_data';
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    end
                case 'fwd_data'
                    n0 = NodeList(event.nodeid);
                    pkt = event.packet;
                    if n0.id==pkt.dst
                        %We have reached the destination
                        newevent = event;
                        newevent.etype = 'rcv_app';
                        if(isequal(newevent.packet.ptype,'MSG'))
                            newevent.app.atype = 'rcv_msg';
                        elseif(isequal(newevent.packet.ptype,'MSG_ACK'))
                            newevent.app.atype = 'rcv_msg_ack';
                        else
                            newevent.app.atype = 'rcv_data';
                        end
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    else
                        newevent = event;
                        newevent.etype = 'send_net';
                        newevent.app.atype = 'fwd_data';
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    end
            end
        end
        
    case 'send_phy'
        n0 = NodeList(event.nodeid);
        pkt = event.packet;
        timeToSend = calculateSendingTime(n0.emitSpeed,pkt.size);
        switch event.packet.ptype
            case 'HELLO'
                for i=1:1:N
                    n1 = NodeList(i);
                    if(n0.id==n1.id),   continue;   end    %Not sent to their own
                    if(topo_dist(n0,n1)<=PROP_DISTANCE)
                        newevent = event;
                        %                         newevent.etype = 'rcv_phy';
                        newevent.instant = event.instant + timeToSend;% 1.67e-7;
                        newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                        newevent.nodeid = n1.id;
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    end
                end
            case 'TC'
                %TC messages are also broadcasted
                for i=1:1:N
                    n1 = NodeList(i);
                    if(n0.id==n1.id),   continue;   end     %Not sent to their own
                    if(event.packet.src==n1.id), continue;   end    %Not to issue a source node
                    if(topo_dist(n0,n1)<=PROP_DISTANCE)
                        newevent = event;
                        %                         newevent.etype = 'rcv_phy';
                        newevent.instant = event.instant + timeToSend;% 1.67e-7;
                        newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                        newevent.nodeid = n1.id;
                        NewEvents = [NewEvents; newevent]; clear newevent;
                    end
                end
            case 'DATA'
                %DATA message is unicast
                if isempty(n0.rt)
                    %No routing table , unable to forward
                    disp(['No routing table on node ',num2str(n0.id),' -> drop']);
                    return;
                end
                posInRt = find(n0.rt(:,1) == pkt.dst);
                if(n0.id==10)
                    disp(['posInRt = ',num2str(posInRt)]);
                    disp('RT(10) = ');
                    dump(10);
                    disp(['Next nodeid : ',num2str(n0.rt(posInRt,2))]);
                    pause;
                end
                if isempty(posInRt)
                    %No route found , unable to forward
                    %             disp(['drop']);
                    return;
                else
                    createMsgTrace(n0,NodeList(n0.rt(posInRt,2)),'b');
                    newevent = event;
                    %newevent.etype = 'rcv_phy';
                    newevent.instant = event.instant + timeToSend;%1.67e-7;
                    newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                    newevent.nodeid = n0.rt(posInRt,2);
                    NewEvents = [NewEvents; newevent]; clear newevent;
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' forward data ', num2str(event.packet.id), ' from ',num2str(event.packet.src),' to ',num2str(n0.rt(posInRt,2))]);
                end
            case 'STABLE'
                %STABLE message is unicast
                if isempty(n0.rt)
                    %No routing table , unable to forward
                    disp(['No routing table on node ',num2str(n0.id),' -> drop']);
                    return;
                end
                posInRt = find(n0.rt(:,1) == pkt.dst);
                if(n0.id==10)
                    disp(['posInRt = ',num2str(posInRt)]);
                    disp('RT(10) = ');
                    dump(10);
                    disp(['Next nodeid : ',num2str(n0.rt(posInRt,2))]);
                    pause;
                end
                if isempty(posInRt)
                    %No route found , unable to forward
                    %             disp(['drop']);
                    return;
                else
                    createMsgTrace(n0,NodeList(n0.rt(posInRt,2)),'b');
                    newevent = event;
                    %newevent.etype = 'rcv_phy';
                    newevent.instant = event.instant + timeToSend;%1.67e-7;
                    newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                    newevent.nodeid = n0.rt(posInRt,2);
                    NewEvents = [NewEvents; newevent]; clear newevent;
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' forward data ', num2str(event.packet.id), ' from ',num2str(event.packet.src),' to ',num2str(n0.rt(posInRt,2))]);
                end
            case 'MSG'
                %MSG message is unicast
                if isempty(n0.rt)
                    %No routing table , unable to forward
                    disp(['No routing table on node ',num2str(n0.id),' -> drop']);
                    return;
                end
                posInRt = find(n0.rt(:,1) == pkt.dst);
%                 if(n0.id==10)
%                     disp(['posInRt = ',num2str(posInRt)]);
%                     disp('RT(10) = ');
%                     dump(10);
%                     disp(['Next nodeid : ',num2str(n0.rt(posInRt,2))]);
%                     pause;
%                 end
                if isempty(posInRt)
                    %No route found , unable to forward
                    disp(['drop cause no route found']);
                    return;
                else
                    createMsgTrace(n0,NodeList(n0.rt(posInRt,2)),'g');
                    newevent = event;
                    %newevent.etype = 'rcv_phy';
                    newevent.instant = event.instant + timeToSend+ROUTING_TREATMENT_TIME;%1.67e-7;
                    newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                    newevent.nodeid = n0.rt(posInRt,2);
                    if(newevent.packet.partId==0) %first message
                        index = findIndexForConversation(newevent.packet.src,newevent.packet.dst);
                        msgLog(index).startTime = current_time;
                    end
                    NewEvents = [NewEvents; newevent]; clear newevent;
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' forward MSG ', num2str(event.packet.id), ' from ',num2str(event.packet.src),' to ',num2str(n0.rt(posInRt,2))]);
                    
                end
            case 'MSG_ACK'
                %MSG message is unicast
                if isempty(n0.rt)
                    %No routing table , unable to forward
                    disp(['No routing table on node ',num2str(n0.id),' -> drop']);
                    return;
                end
                posInRt = find(n0.rt(:,1) == pkt.dst);
%                 if(n0.id==10)
%                     disp(['posInRt = ',num2str(posInRt)]);
%                     disp('RT(10) = ');
%                     dump(10);
%                     disp(['Next nodeid : ',num2str(n0.rt(posInRt,2))]);
%                     pause;
%                 end
                if isempty(posInRt)
                    %No route found , unable to forward
                                disp(['drop cuase no route found']);
                    return;
                else
                    createMsgTrace(n0,NodeList(n0.rt(posInRt,2)),'r');
                    newevent = event;
                    %newevent.etype = 'rcv_phy';
                    newevent.instant = event.instant + timeToSend+ROUTING_TREATMENT_TIME;%1.67e-7;
                    newevent.etype = 'en_rqueue';   %To improve efficiency , skip rcv_phy
                    newevent.nodeid = n0.rt(posInRt,2);
                    NewEvents = [NewEvents; newevent]; clear newevent;
                    disp([num2str(current_time), '  ',num2str(event.nodeid), ' forward MSG_ACK ', num2str(event.packet.id), ' from ',num2str(event.packet.src),' to ',num2str(n0.rt(posInRt,2))]);
                    
                end
        end
        
    case 'de_tqueue'
        %A transmission queue
        n0 = NodeList(event.nodeid);
        if(isequal(event.packet.ptype,'MSG')),disp(['__',num2str(event.nodeid),' packet MSG is in transmission queue --> event type becomes send_phy']); end
        if(isequal(event.packet.ptype,'MSG_ACK')),disp(['__',num2str(event.nodeid),' packet MSG_ACK is in transmission queue --> event type becomes send_phy']); end
        newevent = event;
        newevent.etype = 'send_phy';
        NewEvents = [NewEvents; newevent]; clear newevent;
        j = find(n0.tqueue==event.packet.id);
        n0.tqueue(j) = [];
        NodeList(event.nodeid) = n0;
    case 'en_tqueue'
        %Into the transmit queue
        n0 = NodeList(event.nodeid);    %Node events
        if length(n0.tqueue)>=SQUE_LEN;
            %Loss
            %             disp(['drop']);
            return;
        end
        if(event.packet.id~=0)
            if(isequal(event.packet.ptype,'MSG')),disp(['__',num2str(event.nodeid),' push msg packet into transmit queue']); end
            if(isequal(event.packet.ptype,'MSG_ACK')),disp(['__',num2str(event.nodeid),' push msg_ack packet into transmit queue']); end
            n0.tqueue = [n0.tqueue event.packet.id];    %Into the queue
        else
            if adebug, error('event.packet.id==0'); end
        end
        NodeList(event.nodeid) = n0;
        if(isequal(event.packet.ptype,'MSG')),disp(['__',num2str(event.nodeid),' want to send packet MSG --> event type becomes de_tqueue']);end
        if(isequal(event.packet.ptype,'MSG_ACK')),disp(['__',num2str(event.nodeid),' want to send packet MSG_ACK --> event type becomes de_tqueue']);end
        newevent = event;
        newevent.etype = 'de_tqueue';
        newevent.instant = newevent.instant + (TX_QUE_TIME+PROC_TIME)*length(n0.tqueue);
        NewEvents = [NewEvents; newevent]; clear newevent;
    case 'send_net'
        newevent = event;
        newevent.etype = 'en_tqueue';
        NewEvents = [NewEvents; newevent]; clear newevent ;
    case 'send_app'
        switch event.app.atype
            case 'send_hello'
                pkt = create_packet(event.nodeid,'HELLO',1);
                pkt.src = event.nodeid;
                pkt.dst = 0;
                newevent = event;
                newevent.packet = pkt;
                %                 newevent.etype = 'send_net';
                newevent.etype = 'en_tqueue';   %To improve efficiency , skip send_net
                event_temp = newevent;
                event_temp.etype = 'send_app';
                if LOG_FLAG,    event_log(event_temp);  end
                NewEvents = [NewEvents; newevent]; clear newevent event_temp;                
            case 'send_tc'
                n0 = NodeList(event.nodeid);
                if(isempty(n0.mss))
                    %Only MPR nodes are eligible to generate TC messages ; no MPR nodes , but also sent to the neighbor TC
                    return;
                end
                pkt = create_packet(event.nodeid,'TC',1);
                pkt.src = event.nodeid;
                pkt.prev_addr = event.nodeid;    %Forwarding TC messages on the hop
                pkt.dst = 0;
                newevent = event;
                newevent.packet = pkt;
                newevent.etype = 'en_tqueue';   %To improve efficiency , skip send_net
                event_temp = newevent;
                event_temp.etype = 'send_app';
                if LOG_FLAG,    event_log(event_temp);  end
                NewEvents = [NewEvents; newevent]; clear newevent event_temp;
            case 'fwd_tc'
                %                 disp([num2str(current_time), '  ',num2str(event.nodeid), ' forward tc from ',num2str(event.packet.src)]);
                newevent = event;
                newevent.etype = 'en_tqueue';   %To improve efficiency , skip send_net
                event_temp = newevent;
                event_temp.etype = 'send_app';
                if LOG_FLAG,    event_log(event_temp);  end
                NewEvents = [NewEvents; newevent]; clear newevent event_temp;
            case 'send_data'
                if(isequal(event.packet.ptype,'MSG')),disp(['__',num2str(event.nodeid),' want to send MSG --> event type becomes en_tqueue']);end
                if(isequal(event.packet.ptype,'MSG_ACK')),disp(['__',num2str(event.nodeid),' want to send MSG_ACK --> event type becomes en_tqueue']);end
                newevent = event;
                newevent.etype = 'en_tqueue';   %To improve efficiency , skip send_net
                NewEvents = [NewEvents; newevent]; clear newevent;
        end
end
end
%--------------------------------------------------------------------------
function [NewEvents] = update_ns(event)
%Update NS
NewEvents = [];
global_statement;
n0 = NodeList(event.nodeid);
if ~isempty(n0.ns)
    ns_old = n0.ns(:,1);
else
    ns_old = [];
end
pkt = event.packet;
expire_time = current_time + pkt.hello_vt;    %Extended time
if ~isempty(n0.ns)
    k = find(n0.ns(:,1)==pkt.src);
    if ~isempty(k)
        %already exists
        n0.ns(k,2) = expire_time;
        NodeList(event.nodeid) = n0;
    else
        %New neighbor
        n0.ns = [n0.ns; [pkt.src expire_time]];
        NodeList(event.nodeid) = n0;
        %         n0.mprs = MPR_select(event);
    end
else
    %The neighbor list is empty
    n0.ns = [n0.ns; [pkt.src expire_time]];
    NodeList(event.nodeid) = n0;
    %     n0.mprs = MPR_select(event);
end
if ~isempty(n0.ns)
    ns_new = n0.ns(:,1);
else
    ns_new = [];
end
if ~isequal(ns_old,ns_new)
    %Neighbor list changes , updates and rt mprs
    n0.mprs = MPR_select(event);
    n0.rt = route_cal(event);
end
%Delete neighbor entries in the list to jump 2
if ~isempty(n0.ns2)
    m = find(n0.ns2(:,2)==pkt.src);
    if ~isempty(m)
        delrows = [];
        for j=1:1:length(m)
            delrows = [delrows m(j)];
        end
        n0.ns2(delrows,:) = [];
        NodeList(event.nodeid) = n0;
        n0.mprs = MPR_select(event);
        n0.rt = route_cal(event);
    end
end
NodeList(event.nodeid) = n0;
%Extended event indication app extended tables and table indexes extended
newevent = create_event('time_out',event.nodeid,expire_time,0,0);
newevent.app.atype = 'ns_expire';
newevent.app.index = pkt.src;
NewEvents = [NewEvents; newevent]; clear newevent;
end
%--------------------------------------------------------------------------
function [NewEvents] = update_ns2(event)
%Update NS2
NewEvents = [];
global_statement;

n0 = NodeList(event.nodeid);
if ~isempty(n0.ns2)
    ns2_old = n0.ns2(:,1:2);
else
    ns2_old = [];
end
pkt = event.packet;
j = find((n0.ns(:,1))==pkt.src);
if(isempty(j)), return; end  %Not from a neighbor hello message received
expire_time = current_time + pkt.hello_vt;    %Extended time
addr_list = [pkt.n_item; pkt.mpr_item];
if(~isempty(addr_list))
    for i=1:1:length(addr_list)
        addr = addr_list(i);
        if(addr==event.nodeid), continue; end %Node itself can not be its own 2 -hop neighbor
        if(find(n0.ns(:,1)==addr)),  continue; end %Neighbor 2 Neighbor jump
        if ~isempty(n0.ns2)
            %NS2 is not empty
            j = find(ismember(n0.ns2(:,1:2),[pkt.src,addr],'rows'));
            if isempty(j)
                %The new 2 -hop neighbor
                n0.ns2 = [n0.ns2; [pkt.src addr expire_time]];
                NodeList(event.nodeid) = n0;
                %                 n0.mprs = MPR_select(event);
            else
                %Existing 2 -hop neighbor
                n0.ns2(j,3) =  expire_time;
                NodeList(event.nodeid) = n0;
            end
        else
            %NS2 is empty
            n0.ns2 = [n0.ns2; [pkt.src addr expire_time]];
            NodeList(event.nodeid) = n0;
            %             n0.mprs = MPR_select(event);
        end
        %Extended event indication app extended tables and table indexes extended
        newevent = create_event('time_out',event.nodeid,expire_time,0,0);
        newevent.app.atype = 'ns2_expire';
        newevent.app.ns2_index = addr;
        newevent.app.ns_index = pkt.src;
        NewEvents = [NewEvents; newevent]; clear newevent;
    end
    if ~isempty(n0.ns2)
        ns2_new = n0.ns2(:,1:2);
    else
        ns2_new = [];
    end
    if ~isequal(ns2_old,ns2_new)
        %2 -hop neighbor list changes , updates and rt mprs
        n0.mprs = MPR_select(event);
        n0.rt = route_cal(event);
    end
    NodeList(event.nodeid) = n0;
end

end
%--------------------------------------------------------------------------
function [NewEvents] = update_mss(event)
%Update MSS
%mpr_item  n x 1 [mpr_addr]
NewEvents = [];
global_statement;
n0 = NodeList(event.nodeid);
pkt = event.packet;
mss_origin = n0.mss;    %mss before update
j = find((n0.ns(:,1))==pkt.src);
if(isempty(j)), return; end  %Not from a neighbor hello message received
if(isempty(pkt.mpr_item)), return; end  %Empty mpr_item
addr_list = pkt.mpr_item;
expire_time = current_time + pkt.hello_vt;    %Extended time
if ~isempty(find(addr_list==n0.id))
    if ~isempty(n0.mss)
        %MSS list is not empty
        k = find(n0.mss(:,1)==pkt.src);
        if ~isempty(k)
            n0.mss(k,2) = expire_time;
        else
            n0.mss = [n0.mss; [pkt.src expire_time]];
        end
    else
        %MSS list is empty
        n0.mss = [n0.mss; [pkt.src expire_time]];
    end
else
    return;
end
mss_update = n0.mss;    %mss updated
if ~isempty(mss_origin)&~isempty(mss_update)&~isequal(mss_origin(:,1),mss_update(:,1))
    %MSS changes, update tc_seq
    n0.tc_seq = n0.tc_seq + 1;
end
if isempty(mss_origin)&&~isempty(mss_update)
    %MSS changes, update tc_seq
    n0.tc_seq = n0.tc_seq + 1;
end
if ~isempty(mss_origin)&&isempty(mss_update)
    %MSS changes, update tc_seq
    n0.tc_seq = n0.tc_seq + 1;
end
NodeList(event.nodeid) = n0;
%Extended event indication app extended tables and table indexes extended
newevent = create_event('time_out',event.nodeid,expire_time,0,0);
newevent.app.atype = 'mss_expire';
newevent.app.index = pkt.src;
NewEvents = [NewEvents; newevent]; clear newevent;
end
%--------------------------------------------------------------------------
function [NewEvents] = update_ts(event)
%Update TS
NewEvents = [];
global_statement;
n0 = NodeList(event.nodeid);
pkt = event.packet;
expire_time = current_time + pkt.tc_vt;    %Extended time
if (isempty(pkt.mss_item)), error('update_ts:pkt.mss_item is empty');    end %mss_item is empty there is no need to update the TS
ts_candi = pkt.mss_item;
ansn_current = pkt.ansn;
for i=1:1:length(ts_candi)
    ts_candi(i,2) = pkt.src;
    ts_candi(i,3) = ansn_current;
    ts_candi(i,4) = expire_time;
end
if isempty(n0.ts)
    %ts is empty
    n0.ts = ts_candi;
    NodeList(event.nodeid) = n0;
    for i=1:1:length(ts_candi(:,1))
        %Extended time
        newevent = create_event('time_out',event.nodeid,expire_time,0,0);
        newevent.app.atype = 'ts_expire';
        newevent.app.dst_index = ts_candi(i,1);
        newevent.app.last_index = ts_candi(i,2);
        NewEvents = [NewEvents; newevent]; clear newevent;
    end
    return;
end
ts_temp = n0.ts;
for i=1:1:length(ts_temp(:,1))
    if(ts_temp(i,2)==pkt.src)&&(ts_temp(i,3)>ansn_current)
        %Old TC message
        return;
    end
end

%Remove T_seq < ansn entry
delrows_ts = [];
for i=1:1:length(ts_temp(:,1))
    if (ts_temp(i,2) == pkt.src)
        if ts_temp(i,3)<ansn_current
            delrows_ts = [delrows_ts; i];
        end
    end
end
ts_temp(unique(delrows_ts),:) = [];
%Update T_seq = ansn entry expiretime domain ; add a new entry
for i=1:1:length(ts_candi(:,1))
    k = find(ismember(ts_temp(:,1:3),ts_candi(i,1:3),'rows'));
    if ~isempty(k)
        %Entry that already exists , only the domain update expiretime
        ts_temp(k,4) = expire_time;
    else
        ts_temp = [ts_temp; ts_candi(i,:)];
    end
    newevent = create_event('time_out',event.nodeid,expire_time,0,0);
    newevent.app.atype = 'ts_expire';
    newevent.app.dst_index = ts_candi(i,1);
    newevent.app.last_index = ts_candi(i,2);
    NewEvents = [NewEvents; newevent]; clear newevent;
end
n0.ts = ts_temp;
NodeList(event.nodeid) = n0;
end
%--------------------------------------------------------------------------
function [NewEvents] = update_ts1(event)
%Update TS, He removed himself , TS neighbor nodes , two-hop neighbor nodes
NewEvents = [];
global_statement;
n0 = NodeList(event.nodeid);
if ~isempty(n0.ts)
    ts_old = n0.ts(:,1:2);
else
    ts_old = [];
end
pkt = event.packet;
expire_time = current_time + pkt.tc_vt;    %Extended time
if (isempty(pkt.mss_item)), error('update_ts:pkt.mss_item is empty');    end %mss_item is empty there is no need to update the TS
ts_candi = pkt.mss_item;
ansn_current = pkt.ansn;
for i=1:1:length(ts_candi)
    ts_candi(i,2) = pkt.src;
    ts_candi(i,3) = ansn_current;
    ts_candi(i,4) = expire_time;
end
ts_temp = n0.ts;
if isempty(n0.ts)
    %ts is empty
    %      n0.ts = ts_candi;
    for i=1:1:length(ts_candi(:,1))
        %Extended time
        if n0.id==ts_candi(i,1),  continue;   end %Own address
        if ~isempty(n0.ns)
            if ~isempty(find(n0.ns(:,1)==ts_candi(i,1))),  continue;   end %Already exists in the neighbor list.
        end
        if ~isempty(n0.ns2)
            if ~isempty(find(n0.ns2(:,2)==ts_candi(i,1))),  continue;   end %Already present in the 2 -hop neighbor list
        end
        ts_temp = [ts_temp;ts_candi(i,:)];
        newevent = create_event('time_out',event.nodeid,expire_time,0,0);
        newevent.app.atype = 'ts_expire';
        newevent.app.dst_index = ts_candi(i,1);
        newevent.app.last_index = ts_candi(i,2);
        NewEvents = [NewEvents; newevent]; clear newevent;
    end
    n0.ts = ts_temp;
    NodeList(event.nodeid) = n0;
    if ~isempty(n0.ts)
        ts_new = n0.ts(:,1:2);
    else
        ts_new = [];
    end
    if ~isequal(ts_old,ts_new)
        %ts is changed , updating rt
        n0.rt = route_cal(event);
    end
    NodeList(event.nodeid) = n0;
    return;
end
for i=1:1:length(ts_temp(:,1))
    if(ts_temp(i,2)==pkt.src)&&(ts_temp(i,3)>ansn_current)
        %Old TC message
        return;
    end
end

%Remove T_seq < ansn entry
delrows_ts = [];
for i=1:1:length(ts_temp(:,1))
    if (ts_temp(i,2) == pkt.src)
        if ts_temp(i,3)<ansn_current
            delrows_ts = [delrows_ts; i];
        end
    end
end
ts_temp(unique(delrows_ts),:) = [];
%Update T_seq = ansn entry expiretime domain ; add a new entry
for i=1:1:length(ts_candi(:,1))
    if n0.id==ts_candi(i,1),  continue;   end %Own address
    if ~isempty(n0.ns)
        if ~isempty(find(n0.ns(:,1)==ts_candi(i,1))),  continue;   end %Already exists in the neighbor list.
    end
    if ~isempty(n0.ns2)
        if ~isempty(find(n0.ns2(:,2)==ts_candi(i,1))),  continue;   end %Already present in the 2 -hop neighbor list
    end
    k = find(ismember(ts_temp(:,1:3),ts_candi(i,1:3),'rows'));
    if ~isempty(k)
        %Entry that already exists , only the domain update expiretime
        ts_temp(k,4) = expire_time;
    else
        ts_temp = [ts_temp; ts_candi(i,:)];
    end
    newevent = create_event('time_out',event.nodeid,expire_time,0,0);
    newevent.app.atype = 'ts_expire';
    newevent.app.dst_index = ts_candi(i,1);
    newevent.app.last_index = ts_candi(i,2);
    NewEvents = [NewEvents; newevent]; clear newevent;
end
n0.ts = ts_temp;
NodeList(event.nodeid) = n0;
if ~isempty(n0.ts)
    ts_new = n0.ts(:,1:2);
else
    ts_new = [];
end
if ~isequal(ts_old,ts_new)
    %ts is changed , updating rt
    n0.rt = route_cal(event);
end
NodeList(event.nodeid) = n0;
end
%--------------------------------------------------------------------------
function [mprs] = MPR_select(event)
%Calculate MPR Set
%ns     n x 2 [n_addr, degree(n_addr)];
%ns2    n x 3 [n_addr, 2hop_addr, degree(n_addr)]
%n2     n x 1 [2hop_addr]       It has not yet reached the 2 -hop neighbor
global_statement;
node = NodeList(event.nodeid);
if isempty(node.ns)||isempty(node.ns2)
    mprs = [];
    return;
end
node.mprs = [];
ns = node.ns(:,1);
ns2 = node.ns2(:,1:2);
n2 = unique(ns2(:,2));  %strict 2-hop neighbor: n x 1
% for i=1:1:length(ns(:,1))
%     Calculated neighbor nodes Degree (strict 2-hop neighbor number can reach )
%     ns(i,2) = length(find(ns2(:,1)==ns(i,1)));
% end
for i=1:1:length(ns2(:,1))
    %Calculated neighbor nodes Degree (strict 2-hop neighbor number can reach )
    ns2(i,3) = length(find(ns2(:,1)==ns2(i,1)));
end

delrows_ns2 = [];
delrows_n2 = [];
for i=1:1:length(n2)
    %Explicit Node
    n2_temp = n2(i);
    k = find(ns2(:,2)==n2_temp);
    if(length(k)==1)
        %Only one neighbor node i arrive
        n_temp = ns2(k,1);
        if ~isempty(node.mprs)
            if isempty(find(node.mprs(:,1)==n_temp))
                node.mprs = [node.mprs; n_temp];
            end
        else
            node.mprs = [node.mprs; n_temp];
        end
        %N2 delete entries in the loop obtained directly from�
        delrows_n2 = [delrows_n2, i];
        %Delete the entry ns2 associated n_temp
        m = find(ns2(:,1)==n_temp);
        delrows_ns2 = [delrows_ns2 m'];
        %Delete Delete entry because the association n_temp resulting in the corresponding node n2
        if ~isempty(m)
            for i=1:1:length(m)
                nn2_temp = ns2(m(i),2);
                x = find(n2(:,1)==nn2_temp);
                delrows_n2 = [delrows_n2, x];
            end
        end
    end
end
ns2(unique(delrows_ns2),:) = [];
n2(unique(delrows_n2),:) = [];
%Cleaning ns2
delrows_ns2 = [];
for i=1:1:length(ns2(:,1))
    if(isempty(find(n2(:,1)==ns2(i,2))))
        %ns2 the existing entry has no meaning , because the 2 -hop entry has been marked up
        delrows_ns2 = [delrows_ns2 i];
    end
end
ns2(unique(delrows_ns2),:) = [];

while ~isempty(n2)
    k = find(ns2(:,3)==max(ns2(:,3)));
    if ~isempty(k)
        k = k(1,1);
        n_temp = ns2(k,1);
        n2_temp = ns2(k,2);
        if ~isempty(node.mprs)
            if isempty(find(node.mprs(:,1)==n_temp))
                node.mprs = [node.mprs; n_temp];
            end
        else
            node.mprs = [node.mprs; n_temp];
        end
        
        %n2 ns2 in joint also deleted
        delrows_ns2 = [];
        delrows_n2 = [];
        
        %Remove the n2
        for j=1:1:length(ns2(:,1))
            if(ns2(j,1)==n_temp)
                m = find(n2(:,1)==ns2(j,2));
                delrows_n2 = [delrows_n2, m];
            end
        end
        n2(unique(delrows_n2),:) = [];
        
        %Cleaning ns2
        delrows_ns2 = [];
        for i=1:1:length(ns2(:,1))
            if(isempty(find(n2(:,1)==ns2(i,2))))
                %ns2 the existing entry has no meaning , because the 2 -hop entry has been marked up
                delrows_ns2 = [delrows_ns2 i];
            end
        end
        ns2(unique(delrows_ns2),:) = [];
        %         Delete the entry ns2 associated n_temp
        %         m = find(ns2(:,1)==n_temp);
        %         delrows_ns2 = [delrows_ns2, m'];
        %         Delete the entry ns2 associated n2_temp
        %         m = find(ns2(:,2)==n2_temp);
        %         delrows_ns2 = [delrows_ns2, m'];
        %         ns2(unique(delrows_ns2),:) = [];
    end
end
mprs = node.mprs;
end
%--------------------------------------------------------------------------
function [rt] = route_cal(event)
%Calculate routing table
global_statement;
n0 = NodeList(event.nodeid);
rt = [];
%The neighbor list added to the routing table
if isempty(n0.ns),  return; end     %No neighbors , there will be no need to calculate routes
for i=1:1:length(n0.ns(:,1))
    item_temp = [];
    if(n0.ns(i,2)>current_time)
        item_temp = [n0.ns(i,1),n0.ns(i,1),1];
    end
    rt = [rt; item_temp];
    n0.rt = rt;
end
%The two-hop neighbor list added to the routing table
if isempty(n0.ns2),  return; end     %No 2 -hop neighbors, stop
for i=1:1:length(n0.ns2(:,1))
    item_temp = [];
    if(n0.id==8)
        1111;
    end
    if ~isempty(find(n0.rt(:,1)==n0.ns2(i,2))), continue;   end %Already exists in the routing table , indicating that there are multiple paths to the destination node
    if (n0.ns2(i,3)>current_time)
        item_temp = [n0.ns2(i,2),n0.ns2(i,1),2];
        rt = [rt; item_temp];
        n0.rt = rt;
    end
end
%The TS list added to the routing table
if isempty(n0.ts),  return; end     %ts is empty, stop
h = 2;
flag = 1; %Control flag 
while(flag)
    flag = 0;
    for i=1:1:length(n0.ts(:,1))
        item_temp = [];
        if ~isempty(find(n0.rt(:,1)==n0.ts(i,1))), continue;   end %Already exists in the routing table , indicating that there are multiple paths to the destination node
        k = find(n0.rt(:,1)==n0.ts(i,2));   %Found on the hop
        if (~isempty(k))&(n0.rt(k,3)==h)
            item_temp = [n0.ts(i,1),n0.rt(k,2),h+1];
            rt = [rt; item_temp];
            n0.rt = rt;
            flag = 1;
        end
    end
    h = h+1;
end
end