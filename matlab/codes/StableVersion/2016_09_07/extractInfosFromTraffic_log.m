global_statement;
%     fid = fopen(TRAFFIC_LOG_FILENAME,'r');
%     if (fid ~= -1)
%         nbrLines = length(textread(TRAFFIC_LOG_FILENAME,'%1c%*[^\n]'));
%         if(ischar(nbrLines)),disp('char');end
%         if(isinteger(int32(str2num(nbrLines)))),disp('int');end
timePlanIndex = 1;
timePlan = [];
currentPktIndex = 1;
currentDatasIndex = 1;
currentSize = 0;
endTime = 0;
maxPktId = 0;
for i=1:1:size(datasGraphDebit,2)
    if(maxPktId < datasGraphDebit(i).pktid), maxPktId = datasGraphDebit(i).pktid;end
end
UsedIndex = [];
for i=1:1:maxPktId
    UsedIndex(i).id = i;
    UsedIndex(i).isUsed = 0;
end
while(1)
    currentType = datasGraphDebit(currentDatasIndex).ptype;
    currentPktId = datasGraphDebit(currentDatasIndex).pktid;
    startTime = datasGraphDebit(currentDatasIndex).time;
    currentSize = datasGraphDebit(currentDatasIndex).pktsize;
    if(UsedIndex(currentPktId).isUsed)
        currentDatasIndex = currentDatasIndex + 1;
        if(currentDatasIndex > size(datasGraphDebit,2)),break;end
        continue;
    end
    UsedIndex(currentPktId).isUsed = 1;
    for i=currentDatasIndex+1:1:size(datasGraphDebit,2)
        if(isequal(currentType,datasGraphDebit(i).ptype) && (currentPktId==datasGraphDebit(i).pktid))
            endTime = datasGraphDebit(i).time;
        end
    end
    timePlan(timePlanIndex).start = startTime;
    timePlan(timePlanIndex).end = endTime;
    timePlan(timePlanIndex).size = currentSize;
    timePlan(timePlanIndex).pktid = currentPktId;
    timePlanIndex = timePlanIndex + 1;
    
    currentPktIndex = currentPktIndex + 1;
    currentDatasIndex = currentDatasIndex + 1;
    if(currentDatasIndex > size(datasGraphDebit,2)),break;end
end
points = [];
pointsIndex = 1;
for i=1:1:(STOPTIME/0.0001)
    points(i).t = 0.0001*(i-1);
    points(i).c = 0;
end
% for i=1:1:size(datasGraphDebit,2)
%     points(i).t = datasGraphDebit(i).time;
%     points(i).c = 0;
% end
for i=1:1:size(timePlan,2)
    start = timePlan(i).start;
    endTime = timePlan(i).end;
    data = timePlan(i).size;
    for y=1:1:size(points,2)
        if((points(y).t >= start)&&(points(y).t<=endTime))
            points(y).c = points(y).c + data;
        end
    end
end
X = [points.t];
Y = [points.c];
figure;
hold on;
maxYvalue = max(Y);
meanY = mean(Y);
v = meanY*ones(1,size(Y,2));
if(OLSR_ON)
    title('Network traffic over time with OLSR')
else
    title('Network traffic over time without OLSR')
end
xlabel('time in seconds') % x-axis label
ylabel('traffic load in Byte') % y-axis label
plot(X,Y);
% for i=1:1:size(points,2)
%     if((points(i).c<=maxYvalue)&&(points(i).c>=maxYvalue-10))
%         text(points(i).t,points(i).c,num2str(points(i).c));
%     end
% end
plot(X,v);
[Peak, PeakIdx] = findpeaks(Y);
idX = 1;
maxV = max(Peak);
for i=1:1:size(points,2)
    if(points(i).c == maxV)
        idX = i;
        break;
    end
end
text(X(idX), max(Peak), sprintf('Max = %6.3f Bytes', max(Peak)));
legend('traffic load over time',strcat('mean load of the simulation = ',num2str(meanY),'Bytes'),'Location','northeast')
legend('boxoff')
hold off;
d=date;
if(OLSR_ON)
    savefig(strcat(d,'_NetworkTrafficOverTimeWithOLSR.fig'));
else
    savefig(strcat(d,'_NetworkTrafficOverTimeWithoutOLSR.fig'));
end
