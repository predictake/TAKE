function [d] = topo_dist(node0, node1)
% return distance between two nodes i and j

global_statement;

d = 0;
x0 = node0.pos(1);
y0 = node0.pos(2);
x1=node1.pos(1);
y1=node1.pos(2);
if x0<=0 || x0>TOPO_W, return; end
if x1<=0 || x1>TOPO_W, return; end
if y0<=0 || y0>TOPO_H, return; end
if y1<=0 || y1>TOPO_H, return; end

d = sqrt((x0 - x1)^2 + (y0 - y1)^2);

return;
