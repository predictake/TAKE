"""
@Title: takeStats.py
@Description: Parse the transmission_stats.json file from all nodes and generate stats
@Comments:
 usage :
@Author: Simon Ruffieux (HES-SO//FR)
@Project:  TAKE
"""

import json
import sys, os
import datetime as dt
import matplotlib.pyplot as plt
from collections import defaultdict
import math

DATETIME_FMT = '%Y/%m/%d %H:%M:%S.%f' #Time format in json is 2016/12/19 08:37:12.125

#FILE_PATH = "D:/Devel/TAKE/Data/stats/"  # Windows
FILE_PATH = "/home/take4/TAKE/tapTake/TAPTAKE/dockemu_TAKE_JavaREST/take/data/"  # Linux

my_stats = [] #nodeName - RawData

def generate_stats_perNode():
    my_stats = load_stat_file(FILE_PATH + '/mergedStats.json')
    parsedData = defaultdict(lambda: 'None')

    #Compute the desired values from the loaded data and append it to the dictionary
    for node in my_stats:
        nodeName = str(node['Current Node'])
        parsedData[nodeName] = {}

        parsedData[nodeName]['mean_rtt'] = node['Mean Round-Trip-Time']
        parsedData[nodeName]['message_completion_rate'] = node['Message Completion Rate (ACK received by sender from recipients)'] * 100
        parsedData[nodeName]['successful_transmissions'] = node['Successful Sent Messages (ACK received from dest. nodes)']
        parsedData[nodeName]['failed_transmissions'] = node['Failed Sent Messages (NO ACK received from dest. nodes)']
        parsedData[nodeName]['total_transmissions'] = node['Successful Sent Messages (ACK received from dest. nodes)'] + node['Failed Sent Messages (NO ACK received from dest. nodes)']

    # return a dictionary with a list of pairs {node:'nodename' , [[param_name, value],[param_name, value], ...]}
    return parsedData

def load_stat_file(filepath):
    #print 'Parsing ' + str(filepath)
    with open(filepath) as data_file:
        jdata = json.load(data_file)
    return jdata

def mergeFiles():
    try:
        os.remove(FILE_PATH + 'mergedStats.json')
    except OSError:
        pass
    print 'Merging files...'
    my_stats_files = [f for f in os.listdir(FILE_PATH +'/Collected') if os.path.isfile(os.path.join(FILE_PATH+'/Collected', f)) and f.endswith(".json")]
    for file in my_stats_files:
        print 'Parsing file ' + str(file)
        my_stats.append(load_stat_file(os.path.join(FILE_PATH+'/Collected', file)))
    with open(FILE_PATH + '/mergedStats.json', 'w') as f:
        print 'Merged the stats into a single file: ' + str(FILE_PATH + '/mergedStats.json')
        json.dump(my_stats, f)

def plot_subplot_all(parsedData, show = False, suptitle=""):
    print 'Generate <AllPlot> graph'

    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        # Get all values
        node_names = []
        rtt_values = []
        cr_values = []
        st_values = []
        ft_values = []
        tt_values = []

        for node in sorted(parsedData):
            node_names.append(node)
            rtt_values.append(parsedData[node]['mean_rtt'])
            cr_values.append(parsedData[node]['message_completion_rate'])
            st_values.append(parsedData[node]['successful_transmissions'])
            ft_values.append(parsedData[node]['failed_transmissions'])
            tt_values.append(parsedData[node]['total_transmissions'])

        fig, ((ax_rtt, ax_cr), (ax_st, ax_ft), (ax_tt, ax_6)) = plt.subplots(3, 2)

        #SUBPLOT 1,1 - RTT (row, column)
        max_rtt = max(rtt_values)
        avg_rtt = sum(rtt_values) / len(rtt_values)
        # for p in node_names: print p
        barIndex = range(len(rtt_values))
        ax_rtt.bar(barIndex, rtt_values, color='black', align='center')
        ax_rtt.axhline(y=avg_rtt, color='red', linestyle='dashed', linewidth=1.5)
        ax_rtt.text(0.3, avg_rtt, 'Average', rotation=0, color='red')
        plt.sca(ax_rtt)
        plt.xticks(barIndex, node_names, rotation=90)
        ax_rtt.set_ylabel('Mean RTT [ms]')
        ax_rtt.set_xlabel('Nodes')
        ax_rtt.yaxis.grid(True)
        ax_rtt.set_ylim([0, max_rtt + 5])

        #SUBPLOT 1,2 - CompletionRate
        max_cr = max(cr_values)
        avg_cr = sum(cr_values) / len(cr_values)
        # for p in node_names: print p
        barIndex = range(len(cr_values))
        ax_cr.bar(barIndex, cr_values, color='black', align='center')
        ax_cr.axhline(y=avg_cr, color='red', linestyle='dashed', linewidth=1.5)
        ax_cr.text(0.3, avg_cr, 'Average', rotation=0, color='red')
        plt.sca(ax_cr)
        plt.xticks(barIndex, node_names, rotation=90)
        ax_cr.set_ylabel('Completion Rate [%]')
        ax_cr.set_xlabel('Nodes')
        ax_cr.yaxis.grid(True)
        ax_cr.set_ylim([0, 100])

        #SUBPLOT 2,1 - Successful transmissions
        max_st = max(st_values)
        avg_st = sum(st_values) / len(st_values)
        # for p in node_names: print p
        barIndex = range(len(st_values))
        ax_st.bar(barIndex, st_values, color='black', align='center')
        ax_st.axhline(y=avg_st, color='red', linestyle='dashed', linewidth=1.5)
        ax_st.text(0.3, avg_st, 'Average', rotation=0, color='red')
        plt.sca(ax_st)
        plt.xticks(barIndex, node_names, rotation=90)
        ax_st.set_ylabel('Successful Transmissions [#]')
        ax_st.set_xlabel('Nodes')
        ax_st.yaxis.grid(True)
        ax_st.set_ylim([0, max_st+5])

        #SUBPLOT 2,2 - Successful transmissions
        max_ft = max(ft_values)
        avg_ft = sum(ft_values) / len(ft_values)
        # for p in node_names: print p
        barIndex = range(len(ft_values))
        ax_ft.bar(barIndex, ft_values, color='black', align='center')
        ax_ft.axhline(y=avg_ft, color='red', linestyle='dashed', linewidth=1.5)
        ax_ft.text(0.3, avg_ft, 'Average', rotation=0, color='red')
        plt.sca(ax_ft)
        plt.xticks(barIndex, node_names, rotation=90)
        ax_ft.set_ylabel('Failed Transmissions [#]')
        ax_ft.set_xlabel('Nodes')
        ax_ft.yaxis.grid(True)
        ax_ft.set_ylim([0, max_ft+5])

        #SUBPLOT 3,1 - Total transmissions
        max_tt = max(tt_values)
        avg_tt = sum(tt_values) / len(ft_values)
        # for p in node_names: print p
        barIndex = range(len(tt_values))
        ax_tt.bar(barIndex, tt_values, color='black', align='center')
        ax_tt.axhline(y=avg_tt, color='red', linestyle='dashed', linewidth=1.5)
        ax_tt.text(0.3, avg_tt, 'Average', rotation=0, color='red')
        plt.sca(ax_tt)
        plt.xticks(barIndex, node_names, rotation=90)
        ax_tt.set_ylabel('Total Transmissions [#]')
        ax_tt.set_xlabel('Nodes')
        ax_tt.yaxis.grid(True)
        ax_tt.set_ylim([0, max_tt+5])

        fig.set_size_inches(32,18)
        if suptitle=="":
            plt.suptitle("Algorithm: ?\nTopology: ?\nDistribution: ?")
        else:
            plt.suptitle(suptitle)
        plt.savefig(FILE_PATH + str('/allPlots') + '.png', format='png', dpi=100)
        if show: plt.show()
        print_stats_summary(rtt_values, cr_values)
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def bar_plot_rtt(parsedData, show = False):
    print 'Generate <Mean RTT> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        #  Get RTT
        node_names = []
        rtt_values = []
        for node in sorted(parsedData):
            node_names.append(node)
            rtt_values.append(parsedData[node]['mean_rtt'])


        max_rtt = max(rtt_values)
        avg_rtt = sum(rtt_values) / len(rtt_values)
        fig, ax = plt.subplots()
        #for p in node_names: print p
        barIndex = range(len(rtt_values))
        rects1 = ax.bar(barIndex, rtt_values, color='black', align='center')
        plt.axhline(y=avg_rtt, color='red', linestyle='dashed', linewidth=1.5)
        plt.text(0.3, avg_rtt + 1, 'Average', rotation=0, color='red')
        plt.xticks(barIndex, node_names, rotation=90)
        ax.set_ylabel('Mean RTT [ms]')
        ax.set_xlabel('Nodes')
        ax.yaxis.grid(True)
        ax.set_ylim([0, max_rtt + 5])
        plt.savefig(FILE_PATH + str('_rttPlot') + '.png', bbox_inches='tight', format='png', dpi=300)
        if show:plt.show()
        plt.clf()
        plt.close()
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def bar_plot_completionRate(parsedData, show=False):
    print 'Generate <Completion Rate> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        #  Get RTT
        node_names = []
        completionRate = []
        for node in sorted(parsedData):
            node_names.append(node)
            completionRate.append(parsedData[node]['message_completion_rate'])


        max_cr = max(completionRate)
        avg_cr = sum(completionRate) / len(completionRate)
        fig, ax = plt.subplots()
        #for p in node_names: print p
        barIndex = range(len(completionRate))
        rects1 = ax.bar(barIndex, completionRate, color='black', align='center')
        plt.axhline(y=avg_cr, color='red', linestyle='dashed', linewidth=1.5)
        plt.text(0.3, avg_cr + 1, 'Average', rotation=0, color='red')
        plt.xticks(barIndex, node_names, rotation=90)
        ax.set_ylabel('Completion Rate [%]')
        ax.set_xlabel('Nodes')
        ax.yaxis.grid(True)
        ax.set_ylim([0, 100])
        plt.savefig(FILE_PATH + str('_messCompletionRate') + '.png', bbox_inches='tight', format='png', dpi=300)
        if show: plt.show()
        plt.clf()
        plt.close()
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def bar_plot_successfulTransmission(parsedData, show=False):
    print 'Generate <Successful Transmissions> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        #  Get RTT
        node_names = []
        values = []
        for node in sorted(parsedData):
            node_names.append(node)
            values.append(parsedData[node]['successful_transmissions'])

        max_values = max(values)
        avg_values = sum(values) / len(values)
        fig, ax = plt.subplots()
        #for p in node_names: print p
        barIndex = range(len(values))
        rects1 = ax.bar(barIndex, values, color='black', align='center')
        plt.axhline(y=avg_values, color='red', linestyle='dashed', linewidth=1.5)
        plt.text(0.3, avg_values + 1, 'Average', rotation=0, color='red')
        plt.xticks(barIndex, node_names, rotation=90)
        ax.set_ylabel('Successful Transmissions (#)')
        ax.set_xlabel('Nodes')
        ax.yaxis.grid(True)
        ax.set_ylim([0, max_values + 5])
        plt.savefig(FILE_PATH + str('_successfulTransmissions') + '.png', bbox_inches='tight', format='png', dpi=300)
        if show: plt.show()
        plt.clf()
        plt.close()
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def bar_plot_failedTransmission(parsedData, show=False):
    print 'Generate <Failed Transmissions> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        #  Get RTT
        node_names = []
        values = []
        for node in sorted(parsedData):
            node_names.append(node)
            values.append(parsedData[node]['failed_transmissions'])

        max_values = max(values)
        avg_values = sum(values) / len(values)
        fig, ax = plt.subplots()
        #for p in node_names: print p
        barIndex = range(len(values))
        rects1 = ax.bar(barIndex, values, color='black', align='center')
        plt.axhline(y=avg_values, color='red', linestyle='dashed', linewidth=1.5)
        plt.text(0.3, avg_values + 1, 'Average', rotation=0, color='red')
        plt.xticks(barIndex, node_names, rotation=90)
        ax.set_ylabel('Failed Transmissions (#)')
        ax.set_xlabel('Nodes')
        ax.yaxis.grid(True)
        ax.set_ylim([0, max_values + 5])
        plt.savefig(FILE_PATH + str('_failedTransmissions') + '.png', bbox_inches='tight', format='png', dpi=300)
        if show: plt.show()
        plt.clf()
        plt.close()
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def bar_plot_totalTransmission(parsedData, show=False):
    print 'Generate <Total Transmissions> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        #  Get RTT
        node_names = []
        values = []
        for node in sorted(parsedData):
            node_names.append(node)
            values.append(parsedData[node]['total_transmissions'])

        max_values = max(values)
        avg_values = sum(values) / len(values)
        fig, ax = plt.subplots()
        #for p in node_names: print p
        barIndex = range(len(values))
        rects1 = ax.bar(barIndex, values, color='black', align='center')
        plt.axhline(y=avg_values, color='red', linestyle='dashed', linewidth=1.5)
        plt.text(0.3, avg_values + 1, 'Average', rotation=0, color='red')
        plt.xticks(barIndex, node_names, rotation=90)
        ax.set_ylabel('Total Transmissions (#)')
        ax.set_xlabel('Nodes')
        ax.yaxis.grid(True)
        ax.set_ylim([0, max_values + 5])
        plt.savefig(FILE_PATH + str('_totalTransmissions') + '.png', bbox_inches='tight', format='png', dpi=300)
        if show: plt.show()
        plt.clf()
        plt.close()
    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)

def print_RTT_perNode():
    for node_data in my_stats:
        msg_counter_total = 0
        msg_counter_success = 0
        node_RTT = []
        #print 'Node: ' + str(node_data['Current Node'])
        print '\tMeanRTT:' + str(node_data['Mean Round-Trip-Time']) + '\t#Retransmissions:' + str(node_data['Message Retransmissions'])
        for key in node_data:
            if(str(key).startswith('Transmission Data')):
                if node_data[key]['MSG Departure Node'] == node_data['Current Node']:
                    msg_counter_total+=1
                    all_keys = node_data[str(key)].keys()
                    if('ACK Arrival Date' in all_keys):
                        msg_counter_success += 1
                        departure_date = dt.datetime.strptime(node_data[str(key)]['MSG Departure Date'], DATETIME_FMT)
                        arrival_date = dt.datetime.strptime(node_data[str(key)]['ACK Arrival Date'], DATETIME_FMT)
                        rtt = (arrival_date - departure_date).total_seconds() * 1000
                        node_RTT.append(rtt)

        print '\tComputed MeanRTT:' + "{:.2f}".format(sum(node_RTT)/len(node_RTT)) + '\t#SuccessfulMessages:[' + str(msg_counter_success) + '/' + str(msg_counter_total) + ']'

def print_stats_summary(rtt_values, cr_values):
    print ''
    print 'STATS:'
    print 'RTT: (' + str(' ,').join(map(str, rtt_values)) + ')  ' + 'CompletionRate: (' + str(' ,').join(map(str, cr_values)) + ')'
    print ''

def get_first_message_time(merged_data):
    first_message_time = dt.datetime.max
    for node in merged_data:
        list = node.keys()
        list = [x for x in list if x.startswith('Transmission')]
        for key in list:
            msg_time = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
            if(msg_time < first_message_time):
                first_message_time = msg_time
    return first_message_time

def get_last_message_time(merged_data):
    last_message_time = dt.datetime.min
    for node in merged_data:
        list = node.keys()
        list = [x for x in list if x.startswith('Transmission')]
        for key in list:
            msg_time = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
            if (msg_time > last_message_time):
                last_message_time = msg_time
    return last_message_time

def get_node_name(my_data):
    try:
        return my_data['Current Node']
    except KeyError:
        return 0

def plot_bytesReceivedVersusTime(mergedData_filepath, show=False):
    print 'Generate <Incoming Bytes per second of all received messages for this node> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        with open(mergedData_filepath) as data_file:
            mergedData = json.load(data_file)


        start_time = get_first_message_time(mergedData).replace(microsecond=0)
        end_time = get_last_message_time(mergedData).replace(microsecond=0) + dt.timedelta(seconds=1)

        time_range_inSeconds = int((end_time-start_time).total_seconds())
        node_counter = 0

        #TODO change if more nodes
        total_ligns = int(math.ceil(len(mergedData)/2.0))
        fig, axes = plt.subplots(total_ligns, 2)
        fig.suptitle('Bytes per second of all received messages for this node')


        mergedData = sorted(mergedData, key=lambda k: k['Current Node'])
        for node in mergedData:
            bytes_perSecond = [0] * time_range_inSeconds
            lign_id = node_counter / 2
            col_id = node_counter % 2
            node_counter+=1

            messages_rtt = []
            current_node = node['Current Node']
            list = node.keys()
            list = [x for x in list if x.startswith('Transmission')]
            for key in list:
                if node[key]['MSG Type'] == 'Received':
                    #time_ack = dt.datetime.strptime(node[key]['ACK Arrival Date'], "%Y/%m/%d %H:%M:%S.%f")
                    #deltaT = (time_ack-time_dep).total_seconds() *1000
                    time_dep = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
                    index = int((time_dep - start_time).total_seconds())
                    msg_size = node[key]['MSG Size']
                    bytes_perSecond[index] = bytes_perSecond[index] + msg_size

            # for p in node_names: print p
            max__val = max(bytes_perSecond)
            avg_val = sum(bytes_perSecond) / len(bytes_perSecond)
            barIndex = range(len(bytes_perSecond))
            axes[lign_id, col_id].bar(barIndex, bytes_perSecond, color='black', align='center')
            axes[lign_id, col_id].axhline(y=avg_val, color='red', linestyle='dashed', linewidth=1.5)
            axes[lign_id, col_id].text(0.3, avg_val, 'Average', rotation=0, color='red')
            axes[lign_id, col_id].set_title(current_node)
            plt.sca(axes[lign_id, col_id])
            #plt.xticks(barIndex, range(time_range_inSeconds), rotation=90)
            axes[lign_id, col_id].set_ylabel('Bytes')
            axes[lign_id, col_id].set_xlabel('Time [seconds]')
            axes[lign_id, col_id].yaxis.grid(True)
            axes[lign_id, col_id].set_ylim([0, max__val + 5])
            axes[lign_id, col_id].set_xlim([0, time_range_inSeconds])
        fig.set_size_inches(32, 18)
        fig.savefig(FILE_PATH + str('/in_bytesPerSecond') + '.png', format='png', dpi=100)
        if show: plt.show()

    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)
        sys.exit(1)

def plot_bytesSentVersusTime(mergedData_filepath, show=False):
    print 'Generate <Outgoing Bytes per second of all messages for this node (acked and not acked messages)> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        with open(mergedData_filepath) as data_file:
            mergedData = json.load(data_file)


        start_time = get_first_message_time(mergedData).replace(microsecond=0)
        end_time = get_last_message_time(mergedData).replace(microsecond=0) + dt.timedelta(seconds=1)
        time_range_inSeconds = int((end_time-start_time).total_seconds())

        node_counter = 0

        total_ligns = int(math.ceil(len(mergedData)/2.0))
        fig, axes = plt.subplots(total_ligns, 2)

        mergedData =  sorted(mergedData, key=lambda k: k['Current Node'])
        for node in mergedData:
            bytes_perSecond = [0] * time_range_inSeconds
            lign_id = node_counter / 2
            col_id = node_counter % 2
            node_counter+=1

            current_node = node['Current Node']
            list = node.keys()
            list = [x for x in list if x.startswith('Transmission')]
            for key in list:
                if node[key]['MSG Type'] == 'SentAck' or node[key]['MSG Type'] == 'SentNoAck':
                    #time_ack = dt.datetime.strptime(node[key]['ACK Arrival Date'], "%Y/%m/%d %H:%M:%S.%f")
                    #deltaT = (time_ack-time_dep).total_seconds() *1000
                    time_dep = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
                    index = int((time_dep - start_time).total_seconds())
                    msg_size = node[key]['MSG Size']
                    bytes_perSecond[index] = bytes_perSecond[index] + msg_size

            # for p in node_names: print p
            max__val = max(bytes_perSecond)
            avg_val = sum(bytes_perSecond) / len(bytes_perSecond)
            barIndex = range(len(bytes_perSecond))
            axes[lign_id, col_id].bar(barIndex, bytes_perSecond, color='black', align='center')
            axes[lign_id, col_id].axhline(y=avg_val, color='red', linestyle='dashed', linewidth=1.5)
            axes[lign_id, col_id].text(0.3, avg_val, 'Average', rotation=0, color='red')
            plt.sca(axes[lign_id, col_id])
            #plt.xticks(barIndex, range(time_range_inSeconds), rotation=90)
            axes[lign_id, col_id].set_ylabel('Bytes')
            axes[lign_id, col_id].set_xlabel('Time [seconds]')
            axes[lign_id, col_id].set_title(current_node)
            axes[lign_id, col_id].yaxis.grid(True)
            axes[lign_id, col_id].set_ylim([0, max__val + 5])
            axes[lign_id, col_id].set_xlim([0, time_range_inSeconds])
        fig.suptitle('Outgoing Bytes per second of all messages (acked and not acked)')
        fig.set_size_inches(32, 18)
        plt.savefig(FILE_PATH + str('/out_bytesPerSecond') + '.png', format='png', dpi=100)
        if show: plt.show()

    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)
        sys.exit(1)

def plot_avgRTTPerSecond(mergedData_filepath, show=False):
    print 'Generate <Average RTT per Second> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        with open(mergedData_filepath) as data_file:
            mergedData = json.load(data_file)

        start_time = get_first_message_time(mergedData).replace(microsecond=0)
        end_time = get_last_message_time(mergedData).replace(microsecond=0) + dt.timedelta(seconds=1)
        time_range_inSeconds = int((end_time - start_time).total_seconds())

        node_counter = 0

        total_ligns = int(math.ceil(len(mergedData)/2.0))
        fig, axes = plt.subplots(total_ligns, 2)

        mergedData = sorted(mergedData, key=lambda k: k['Current Node'])
        for node in mergedData:
            rttSum_perSecond = [0] * time_range_inSeconds
            rttSum_counter = [0] * time_range_inSeconds

            lign_id = node_counter / 2
            col_id = node_counter % 2
            node_counter += 1
            current_node = node['Current Node']
            list = node.keys()
            list = [x for x in list if x.startswith('Transmission')]
            for key in list:
                if node[key]['MSG Type'] == 'SentAck':
                    time_ack = dt.datetime.strptime(node[key]['ACK Arrival Date'], "%Y/%m/%d %H:%M:%S.%f")
                    time_dep = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
                    rtt = (time_ack - time_dep).total_seconds() * 1000
                    index = int((time_dep - start_time).total_seconds())
                    rttSum_perSecond[index] = rttSum_perSecond[index] + rtt
                    rttSum_counter[index] = rttSum_counter[index] + 1

            for i in range(len(rttSum_perSecond)):
                if(rttSum_counter[i] != 0):
                    if rttSum_counter[i] == 0:
                        rttSum_perSecond[i] = 0
                    else:    
                        rttSum_perSecond[i] = rttSum_perSecond[i]/float(rttSum_counter[i])


            # for p in node_names: print p
            max__val = max(rttSum_perSecond)
            avg_val = sum(rttSum_perSecond) / len(rttSum_perSecond)
            barIndex = range(len(rttSum_perSecond))
            axes[lign_id, col_id].bar(barIndex, rttSum_perSecond, color='black', align='center')
            axes[lign_id, col_id].axhline(y=avg_val, color='red', linestyle='dashed', linewidth=1.5)
            axes[lign_id, col_id].text(0.3, avg_val, 'Average', rotation=0, color='red')
            plt.sca(axes[lign_id, col_id])
            # plt.xticks(barIndex, range(time_range_inSeconds), rotation=90)
            axes[lign_id, col_id].set_ylabel('RTT [ms]')
            axes[lign_id, col_id].set_xlabel('Time [seconds]')
            axes[lign_id, col_id].set_title(current_node)
            axes[lign_id, col_id].yaxis.grid(True)
            axes[lign_id, col_id].set_ylim([0, max__val + 5])
            axes[lign_id, col_id].set_xlim([0, time_range_inSeconds])
        fig.suptitle('Average RTT per Second')
        fig.set_size_inches(32, 18)
        plt.savefig(FILE_PATH + str('/avgRTTPerSecond') + '.png', format='png', dpi=100)
        if show: plt.show()

    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)
        sys.exit(1)

def plot_msgSentVersusTime(mergedData_filepath, show=False):
    print 'Generate <Outgoing messages per second for each node (acked and not acked messages)> graph'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        with open(mergedData_filepath) as data_file:
            mergedData = json.load(data_file)


        start_time = get_first_message_time(mergedData).replace(microsecond=0)
        end_time = get_last_message_time(mergedData).replace(microsecond=0) + dt.timedelta(seconds=1)
        time_range_inSeconds = int((end_time-start_time).total_seconds())

        node_counter = 0

        total_ligns = int(math.ceil(len(mergedData)/2.0))
        fig, axes = plt.subplots(total_ligns, 2)

        mergedData =  sorted(mergedData, key=lambda k: k['Current Node'])
        for node in mergedData:
            msg_perSecond = [0] * time_range_inSeconds
            lign_id = node_counter / 2
            col_id = node_counter % 2
            node_counter+=1

            current_node = node['Current Node']
            list = node.keys()
            list = [x for x in list if x.startswith('Transmission')]
            for key in list:
                if node[key]['MSG Type'] == 'SentAck' or node[key]['MSG Type'] == 'SentNoAck':
                    #time_ack = dt.datetime.strptime(node[key]['ACK Arrival Date'], "%Y/%m/%d %H:%M:%S.%f")
                    #deltaT = (time_ack-time_dep).total_seconds() *1000
                    time_dep = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
                    index = int((time_dep - start_time).total_seconds())
                    msg_perSecond[index] = msg_perSecond[index] + 1

            # for p in node_names: print p
            max__val = max(msg_perSecond)
            avg_val = sum(msg_perSecond) / len(msg_perSecond)
            barIndex = range(len(msg_perSecond))
            axes[lign_id, col_id].bar(barIndex, msg_perSecond, color='black', align='center')
            axes[lign_id, col_id].axhline(y=avg_val, color='red', linestyle='dashed', linewidth=1.5)
            axes[lign_id, col_id].text(0.3, avg_val, 'Average', rotation=0, color='red')
            plt.sca(axes[lign_id, col_id])
            #plt.xticks(barIndex, range(time_range_inSeconds), rotation=90)
            axes[lign_id, col_id].set_ylabel('Messages [#]')
            axes[lign_id, col_id].set_xlabel('Time [seconds]')
            axes[lign_id, col_id].set_title(current_node)
            axes[lign_id, col_id].yaxis.grid(True)
            axes[lign_id, col_id].set_ylim([0, max__val + 5])
            axes[lign_id, col_id].set_xlim([0, time_range_inSeconds])
        fig.suptitle('Message sent per second (acked and not acked messages)')
        fig.set_size_inches(32, 18)
        plt.savefig(FILE_PATH + str('/out_msgPerSecond') + '.png', format='png', dpi=100)
        if show: plt.show()

    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)
        sys.exit(1)

def plot_msgReceivedVersusTime(mergedData_filepath, show=False):
    print 'Generate <Incoming messages per second for each node> graphs'
    try:
        # graph
        SIZE = 18
        plt.rc('font', size=SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=SIZE)  # fontsize of the x any y labels
        plt.rc('xtick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)  # legend fontsize

        with open(mergedData_filepath) as data_file:
            mergedData = json.load(data_file)


        start_time = get_first_message_time(mergedData).replace(microsecond=0)
        end_time = get_last_message_time(mergedData).replace(microsecond=0) + dt.timedelta(seconds=1)
        time_range_inSeconds = int((end_time-start_time).total_seconds())

        node_counter = 0

        total_ligns = int(math.ceil(len(mergedData)/2.0))
        fig, axes = plt.subplots(total_ligns, 2)

        mergedData =  sorted(mergedData, key=lambda k: k['Current Node'])
        for node in mergedData:
            msg_perSecond = [0] * time_range_inSeconds
            lign_id = node_counter / 2
            col_id = node_counter % 2
            node_counter+=1

            current_node = node['Current Node']
            list = node.keys()
            list = [x for x in list if x.startswith('Transmission')]
            for key in list:
                if node[key]['MSG Type'] == 'Received':
                    #time_ack = dt.datetime.strptime(node[key]['ACK Arrival Date'], "%Y/%m/%d %H:%M:%S.%f")
                    #deltaT = (time_ack-time_dep).total_seconds() *1000
                    time_dep = dt.datetime.strptime(node[key]['MSG Departure Date'], "%Y/%m/%d %H:%M:%S.%f")
                    index = int((time_dep - start_time).total_seconds())
                    msg_perSecond[index] = msg_perSecond[index] + 1

            # for p in node_names: print p
            max__val = max(msg_perSecond)
            avg_val = sum(msg_perSecond) / len(msg_perSecond)
            barIndex = range(len(msg_perSecond))
            axes[lign_id, col_id].bar(barIndex, msg_perSecond, color='black', align='center')
            axes[lign_id, col_id].axhline(y=avg_val, color='red', linestyle='dashed', linewidth=1.5)
            axes[lign_id, col_id].text(0.3, avg_val, 'Average', rotation=0, color='red')
            plt.sca(axes[lign_id, col_id])
            #plt.xticks(barIndex, range(time_range_inSeconds), rotation=90)
            axes[lign_id, col_id].set_ylabel('Messages [#]')
            axes[lign_id, col_id].set_xlabel('Time [seconds]')
            axes[lign_id, col_id].set_title(current_node)
            axes[lign_id, col_id].yaxis.grid(True)
            axes[lign_id, col_id].set_ylim([0, max__val + 5])
            axes[lign_id, col_id].set_xlim([0, time_range_inSeconds])
        fig.suptitle('Message received per second (all messages [does not include acks])')
        fig.set_size_inches(32, 18)
        plt.savefig(FILE_PATH + str('/in_msgPerSecond') + '.png', format='png', dpi=100)
        if show: plt.show()

    except Exception, e:
        print "Problem when generating the plot !"
        print "Error Message:"
        print str(e)
        sys.exit(1)

def main(argv):
    global FILE_PATH 

    graphTitle=''

    if len(argv) == 1:
        FILE_PATH = argv[0]
        #print 'Received argument ' + str(FILE_PATH)
    elif len(argv) == 2:
        FILE_PATH = argv[0]
        graphTitle = argv[1]

    mergeFiles()
    my_data = generate_stats_perNode()
    #print my_data
    #bar_plot_rtt(my_data, show=False)
    #bar_plot_completionRate(my_data, show=False)
    #bar_plot_successfulTransmission(my_data, show=False)
    #bar_plot_failedTransmission(my_data, show=False)
    #bar_plot_totalTransmission(my_data, show=False)

    plot_subplot_all(my_data, show=False, suptitle=graphTitle)
    plot_avgRTTPerSecond(FILE_PATH + '/mergedStats.json', show=False)
    plot_bytesSentVersusTime(FILE_PATH + '/mergedStats.json', show=False)
    plot_bytesReceivedVersusTime(FILE_PATH + '/mergedStats.json', show=False)
    plot_msgSentVersusTime(FILE_PATH + '/mergedStats.json', show=False)
    plot_msgReceivedVersusTime(FILE_PATH + '/mergedStats.json', show=False)
    


if __name__ == "__main__":
    main(sys.argv[1:])