"""
@Title: take.py
@Description: Main script that handle take system
@Comments:
 usage :
@Author: Simon Ruffieux (HES-SO//FR)
@Project:  TAKE
"""

import sys
import getopt
import subprocess
import time
import os
import signal
import datetime

from tempfile import mkstemp
from shutil import move
from os import remove, close, path
from collections import defaultdict
import json
import csv

#./take init  (start NS3 and VMs) - param: <int> topology (toplogies are hardcoded in NS-3 file)
#./take config (copy necessary files to VMS) - param: <string> distribution_filename.json (topologies are json files)
#./take start (start messenging systems in VMs)
#./take collect (collect stats VMs)
#./take all config file

# Simulation MODE is NS-3 and "Real" MODE is with real computers for the nodes
MODE = "Real"

#BASE_DIR = "/Users/ruffieuxsimon/Take/take/"    # base_dir for simon's Mac
BASE_DIR = "/home/take3/TAKE/take/"         # base_dir for take computer
#BASE_DIR = "/Users/gislerc/Documents/HEIA-FR/Projets/TAKE/Git/take/" #base dir for christophe
DISTRIBUTION_DIR = BASE_DIR + "take/distributions/"
SCENARIO_DIR = BASE_DIR + "take/scenarios/"
STATS_DIR = BASE_DIR + 'Stats/' + str(MODE) + "/"

# Configuration for <Real> Mode
HOST_FILENAME = "take/real/hosts.csv"  # location of the host filename

# username for the remote hosts (they all have the same)
HOST_USERNAME = "olsr"
# password for the remote hosts (they all have the same)
HOST_PASSWORD = "root"


# The time allowed for the network to covnerge after changing the
# parameters before running an experiment
NETWORK_CONVERGENCE_DELAY = 30  # default 30
# The delay between two experiments, default = 60
BETWEEN_EXPERIMENT_DELAY = 10
# The delay time before collecting the data (to ensure that all messages
# were sent and everything finished smoothly)
BEFOREDATACLLECTION_DELAY = 45  # default = 60
NS3_RUNNING_TIME = 1000000000  # 'infinite' time for NS-3 simulation

topology_names = ['4Nodes_Custom', '4Nodes_FullyMeshed', '10Nodes_Custom', '10Nodes_FullyMeshed',
    '60Nodes_Custom', '60Nodes_FullyMeshed', '5Nodes_Custom', '5Nodes_Custom']

method = 'usage'

# TODO load the params from a config file

# Params:
#--------
hosts = ""
loaded_scenario = ""
current_experiment = 0
run_multiple_experiment = False
current_scenario = ""
# #nodeNumber (for takeStartMessaging) - the number of nodes that will be started
# topo (for takeConfig, used for setIpTables.sh) - the topologie(s) that will be used (can be multiple spearated by ';', ex 0;1)
# message_distributions (for takeConfig) - the messages distribution(s) to use (can be multiple separated by ';', ex: blabla.json;blabla2.json)
# networkSettings (for takeConfig) - the settings for each node
# (latency,drop,throughput)
current_networkSettings = ""
current_topology = ""
current_distribution = ""
current_algo = ""
current_algoTimeout = ""

current_distribution_folder = 'unknown-distribution'
current_topology_folder = 'custom-topo'

current_distribution_messageNumber = 'unknown'
current_distribution_messageSize = 'unknown'
current_distribution_duration = 'unknown'


class Scenario():
    name = ""
    descriptions = ""
    nodeNumber = ""
    experiments = []

    def __init__(self, filename):
        with open(SCENARIO_DIR + filename) as data_file:
            jdata = json.load(data_file)

            self.name = scenario_name = jdata['scenario']
            self.nodeNumber = jdata['nodes']
            self.description = jdata['description']

            for experiment in jdata['experiments']:
                topo_id = getTopologyId(
                    self.nodeNumber, experiment['topology'])
                exp = Experiment(topology=topo_id, distribution=experiment['distribution'], algorithm_strategy=experiment['algorithm_strategy'], algorithm_timeout=experiment[
                                 'algorithm_timeout'], network_settings=[experiment['network_settings']['throughput'], experiment['network_settings']['latency'], experiment['network_settings']['drop_rate']])
                if "rft_update_period" in experiment:
                    exp.rft_update_period = experiment['rft_update_period']
                if "bft_update_period" in experiment:
                    exp.bft_update_period = experiment['bft_update_period']
                self.experiments.append(exp)


class Experiment():
    topology = ""                   # FullyMeshed or Custom
    algorithm_strategy = ""         # Direct or GetCloser
    # the timeout when sending message [sec], default = 30
    algorithm_timeout = ""
    # The message distribution for the experiment
    distribution = ""
    # the network settings (throuhput, latency, drop_rate)
    network_settings = ["", "", ""]
    bft_update_period = 0
    rft_update_period = 0

    def __init__(self, topology, distribution, algorithm_strategy, algorithm_timeout, network_settings, bft_update_period=0, rft_update_period=0):
        self.topology = topology
        self.distribution = distribution
        self.algorithm_strategy = algorithm_strategy
        self.algorithm_timeout = algorithm_timeout
        self.network_settings = network_settings
        self.bft_update_period = bft_update_period
        self.rft_update_period = rft_update_period


class Hosts:

    """
    A class containing the real hosts ips (4G ips)
    """
    ips = []

    def __init__(self, filename):
    	self.ips = []
        with open(BASE_DIR + '/' + filename) as f:
            lines = f.readlines()
            for line in lines:
                if not line.startswith('#'):
                    self.ips.append(line.strip())


class Message:
    source = 0
    recipients = list()
    size = 100
    time = 0

    def __init__(self, time, source, recipients, size):
        self.source = source
        self.recipients = recipients
        self.size = size
        self.time = time

    def __str__(self):
        return "Message = [" + str(self.time) + ", " + str(self.source) + ", " + str(self.recipients) + ", " + str(self.size) + "]"


class Distribution:
    nodes = 0
    message_second = 0
    duration = 0
    nb_messages = 0
    messages = list()
    message_sizes = [100]
    multi_recipients = False

    def __init__(self, nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        # The number of nodes in the simulation
        self.nodes = nodes
        # The number of messages per second (from each node)
        self.message_second = message_second
        # The duration of the simulation
        self.duration = duration
        # The total number of messages sent during the simulation
        self.nb_messages = duration * nodes * message_second
        # The list of messages to be sent
        self.messages = messages
        # The potential sizes of messages. If more than one size are provided,
        # each mesage will use a randomly selected size
        self.message_sizes = message_sizes
        # Use multi-recipients ? (the recipient(s) are randomly selected)
        self.multi_recipients = multi_recipients

    def get_messages(self, node_id):
        node_messages = []
        for msg in self.messages:
            if str(msg.source) == str(node_id):
                node_messages.append(msg)
        return node_messages

    @staticmethod
    def create(nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        distribution = Distribution(
            nodes, duration, message_second, messages, message_sizes, multi_recipients)
        distribution.generate()
        return distribution

    @staticmethod
    def load(filename):
        # Load from file
        # Set values and load messages
        with open(filename, 'r') as outfile:
            data = json.load(outfile)
            msg_sizes = [i for i in data['message_sizes']]
            myDistrib = Distribution(nodes=data['nodes'],  message_second=data['message_second'], duration=data[
                                     'duration'], message_sizes=msg_sizes, multi_recipients=data['multi_recipients'])
            for message in data['messages']:
                recipients = [i for i in message['recipients']]
                myDistrib.messages.append(Message(time=message['time'], source=message[
                                          'source'], recipients=recipients, size=message['size']))
            return myDistrib

    def save(self, filename=""):
        if filename is "":
            filename = "distribution-deterministic_{0}-Nodes_{1}s_{2}-messages_{3}-bytes.json".format(self.nodes, self.duration, self.nb_messages, str(self.message_sizes))
        with open(filename, 'w') as outfile:
            json.dump(self, fp=outfile, default=self.jdefault)

    def generate(self):
        self.messages = []
        random.seed(42) #use a fixed random seed
        total_messages = 0
        message_per_node = int(self.nb_messages / self.nodes)
        for source in range(self.nodes):
            total_source_message = 0
            for i in range(message_per_node):
                potential_recipients = range(0, self.nodes)
                potential_recipients.remove(source)     # Avoid sending to himself
                if self.multi_recipients is True:
                    nb_recipients = random.randint(1, self.nodes-1)
                    if total_source_message + nb_recipients > message_per_node:   # avoid generating more messages than desired in multi-recipients
                        nb_recipients = message_per_node - total_source_message
                else:
                    nb_recipients = 1
                if len(self.message_sizes) > 1:
                    size = random.choice(self.message_sizes)
                else:
                    size = self.message_sizes[0]

                time = random.randint(1, self.duration)
                recipients = [potential_recipients[i] for i in sorted(random.sample(xrange(len(potential_recipients)), nb_recipients))]
                msg = Message(time=time, source=source, recipients=recipients, size=size)
                # print msg
                self.messages.append(msg)

                total_source_message += nb_recipients
                total_messages += nb_recipients
                if total_source_message >= message_per_node:
                    break
            if total_messages >= self.nb_messages:
                break
        self.messages.sort(key=operator.attrgetter('time'))
        print "The distribution contains a total of " + str(total_messages) + " messages"

    @staticmethod
    def jdefault(o):
        return o.__dict__



def getTopologyId(node, topology_str):
    if(node == 4):
        if(topology_str == 'Custom'):
            return 0
        else:
            return 1
    elif(node == 10):
        if(topology_str == 'Custom'):
            return 2
        else:
            return 3
    elif(node == 60):
        if(topology_str == 'Custom'):
            return 4
        else:
            return 5
    elif(node == 5):
        if(topology_str == 'Custom'):
            return 6
        else:
            return 7
    else:
        if MODE is 'Simulation':
            print 'Unknown topology ! exiting !'
            exit(1)
        else:
            return 0


def load_scenario(filename):
    """
    Load a scenario file ands tore it and update the current parameters to the run 0 of the scenario
    """
    global scenario

    scenario = Scenario(filename)

    updateCurrentParameters(0)


def updateCurrentParameters(current_experiment_counter=0):
    """
    Update the current parameters with the parameters from the run <current_experiment_counter> from the Scenario
    """
    global current_networkSettings
    global current_topology
    global current_distribution
    global current_distribution_duration
    global current_distribution_messageNumber
    global current_distribution_messageSize
    global current_algo
    global current_algoTimeout

    global current_distribution_folder
    global current_topology_folder
    global message_distribution_duration

    current_networkSettings = scenario.experiments[current_experiment_counter].network_settings
    current_topology = scenario.experiments[current_experiment_counter].topology
    current_distribution = scenario.experiments[current_experiment_counter].distribution
    current_algo = scenario.experiments[current_experiment_counter].algorithm_strategy
    current_algoTimeout = scenario.experiments[current_experiment_counter].algorithm_timeout
    distribution = Distribution.load(DISTRIBUTION_DIR + current_distribution)
    current_distribution_duration = distribution.duration
    current_distribution_messageNumber = distribution.nb_messages
    current_distribution_messageSize = str(distribution.message_sizes)


    # infos = get_stringInfos_fromDistributionFilename(current_distribution)
    # current_distribution_duration = int((infos[1])[:-1])
    # current_distribution_messageNumber = (infos[2].split(':')[0]).split('-')[0]
    # current_distribution_messageSize = infos[3].split('-')[0]

    current_topology_folder = topology_names[current_topology]
    file_name = os.path.basename(current_distribution)
    current_distribution_folder = file_name.split('.')[0]


def check_allnodes_ready():
    """
    Check that all nodes are ready by trying to communicate with their respective REST interface (of JavaTakeApp)
    """
    proc = subprocess.Popen(["./simulation/takeCheckRestSrv"], stdout=subprocess.PIPE)
    out, err = proc.communicate()  # wait for completion
    # print out
    processes = out.splitlines()[:-1]
    processes_started = 0
    for line in processes:
        if line.startswith('Take'):
            processes_started += 1

    # check if all REST java app reply to a getversion call (indicating
    # that they are up and running)
    if out.splitlines()[-1] == 'true':
        return True
    else:
        print 'Nodes ready: ' + str(processes_started) + '/' + str(len(processes))
        return False


def get_stringInfos_fromDistributionFilename(file_path):
    # parse its name, copy it to TakeMessengerDistrib.json update config files
    # accordingly
    file_name = os.path.basename(file_path)
    # ex. distribution_4-nodes_180s_1000-messages_100-bytes.json
    infos = file_name.split('_')

    message_distribution_duration = int((infos[2])[:-1])

    # nodes, duration, message, size
    return [infos[1], infos[2], infos[3], infos[4]]


def update_config_file(running_time=NS3_RUNNING_TIME, node_containers=4):
    """
    Update the configuration file of NS-3 to indicate the correct number of nodes
    """
    #  Open the configuration file and update its values
    file_path = BASE_DIR + "/conf/dockemu.conf.template"
    out_path = BASE_DIR + "/conf/dockemu.conf"
    with open(out_path, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if line.startswith("node_containers="):
                    new_file.write("node_containers=" +
                                   str(node_containers) + "\n")
                elif line.startswith("runningTime="):
                    new_file.write("runningTime=" +
                                   str(running_time) + "\n")
                else:
                    new_file.write(line)


def initNS3():
    """
    Init NS-3 back-end to create multiple linked VM (starts NS-3 with N nodes/VMs)
    """
    if MODE is 'Simulation':
        p = subprocess.Popen(["gnome-terminal", '-x', 'sh', '-c',
                              "cd /home/take4/TAKE/tapTake/TAPTAKE/dockemu_TAKE_JavaREST/;sleep 1;./dockemu start conf/dockemu.conf; echo 'Terminal will be closing in 10 seconds'; sleep 10"])
        # p = subprocess.Popen(["gnome-terminal", '-x', 'sh', '-c', " ls -l; exec bash"])
        # working paritally p = subprocess.Popen(["sh", '-c', './dockemu start /home/take4/TAKE/tapTake/TAPTAKE/dockemu_TAKE_JavaREST/conf/dockemu.conf'])
        # subprocess.call()
        # print 'PID is' + str(p.pid)
        print 'Waiting ' + str(int(0.5 * scenario.nodeNumber + 5)) + ' seconds for system to start'
        time.sleep(int(0.5 * scenario.nodeNumber + 5))
        print "*** Dockers and NS-3 should now be running in a distinct terminal***"
    else:
        # TODO
        print "NOTHING"


def config(distribution, topology, network_settings, algo_policy, algo_timeout):
    """
    Send a configuration message to the REST interface of each node in order to use the desired settings
    """
    if MODE is 'Simulation':
        print "[" +str(distribution) + ", " + str(topology) + ", " + str(network_settings) + ", " + str(algo_policy) + " ," + str(algo_timeout) + "]"
        print "RFT = " +str(scenario.experiments[current_experiment].rft_update_period)
        print "BFT = " +str(scenario.experiments[current_experiment].bft_update_period)

        proc = subprocess.Popen(["./simulation/takeConfig", BASE_DIR, distribution, str(topology), network_settings[
                                0], network_settings[1], network_settings[2], algo_policy, str(algo_timeout), str(scenario.experiments[current_experiment].bft_update_period), str(scenario.experiments[current_experiment].rft_update_period)])
        proc.communicate()  # wait for compeltion
    else:
        # Set up the desired configration on each node of the system"
        #print "Number of hosts.ips={0}".format(str(len(hosts.ips)))
        for host_ip in hosts.ips:
        	print "-- CONFIGURATION OF {0} --".format(str(host_ip))
        	proc = subprocess.Popen(["./real/takeConfig_real.sh", HOST_USERNAME, HOST_PASSWORD, host_ip, BASE_DIR, distribution, algo_policy, str(algo_timeout), str(scenario.experiments[current_experiment].bft_update_period), str(scenario.experiments[current_experiment].rft_update_period)])
        	proc.communicate()  # wait for completion


def startJavaTake(username="", password="", host_ip=""):
    """
    Start the JavaTakeApp on each node
    """
    if MODE is 'Simulation':
        proc = subprocess.Popen(["./simulation/takeJavaStart", BASE_DIR])
        proc.communicate()  # wait for completion
    else:
        # Start javaTake on desired node
        proc = subprocess.Popen(
            ["./real/takeJavaStart_real.sh", username, password, host_ip])
        (stdout, stderr) = proc.communicate()  # wait for completion


def startMessaging():
    """
    Start the Messaging agent on each node, node a 5 seconds delay is automatically added to ensure that they all start at the same time
    """
    if MODE is 'Simulation':
        proc = subprocess.Popen(["./simulation/takeStartMessaging"])
        proc.communicate()  # wait for completion
    else:
        # start messaging applications on all nodes"
        for counter, host_ip in enumerate(hosts.ips):
            proc = subprocess.Popen(
                ["./real/takeStartMessaging_real.sh", HOST_USERNAME, HOST_PASSWORD, host_ip, str(counter)])
            proc.communicate()  # wait for completion


def collect(data_path):
    """
    Collect the data from every node and store it in a specific location
    """
    if MODE is 'Simulation':
        proc = subprocess.Popen(["./simulation/takeCollect", data_path])
        proc.communicate()  # wait for compeltion
    else:
        # TODO
        print "Collect data from all nodes:"
        print "Sending signal to generate data"
        for host_ip in hosts.ips:
            proc = subprocess.Popen(
                ["./real/takeGenerateStats_real.sh", HOST_USERNAME, HOST_PASSWORD, host_ip])
            proc.communicate()  # wait for completion

        print "Sending signal to collect generate data"
        for counter, host_ip in enumerate(hosts.ips):
            proc = subprocess.Popen(
                ["./real/takeCollect_real.sh", HOST_USERNAME, HOST_PASSWORD, host_ip, data_path, "node-" + str(counter)])
            proc.communicate()  # wait for completion


def generateStats(data_path, suptitle=""):
    """
    Generate the statistics (plots and graphs) at the right place (datapath)
    """
    if MODE is 'Simulation':
        proc = subprocess.Popen(
            ["python", 'takeStats.py', data_path, suptitle])
        proc.communicate()  # wait for completion
    else:
        proc = subprocess.Popen(
        ["python", 'takeStats.py', data_path, suptitle])
        proc.communicate()  # wait for completion   


def cleanVMs():
    """
    Send a signal to the REST interface of the JavaTakeApp of each node to do a RESET
    """
    if MODE is 'Simulation':
        proc = subprocess.Popen(["./simulation/takeClean"])
        (stdout, stderr) = proc.communicate()  # wait for compeltion
    else:
        for host_ip in hosts.ips:
            proc = subprocess.Popen(
                ["./real/takeClean_real.sh", HOST_USERNAME, HOST_PASSWORD, host_ip])
            (stdout, stderr) = proc.communicate()  # wait for completion


def computeStats(merged_filepath, nodes, topology, algorithm, msg_size, throughput, latency, drop, distribution, datetime):
    # WE DO IT IN TWO STEPS IN ORDER TO SORT THE DICTIONARY BY NODES NAMES
    with open(merged_filepath + '/mergedStats.json') as data_file:
        my_stats = json.load(data_file)

    parsedData = defaultdict(lambda: 'None')

    # Compute the desired values from the loaded data and append it to the
    # dictionary
    for node in my_stats:
        nodeName = str(node['Current Node'])
        parsedData[nodeName] = {}

        parsedData[nodeName]['mean_rtt'] = node['Mean Round-Trip-Time']
        parsedData[nodeName]['message_completion_rate'] = node[
            'Message Completion Rate (ACK received by sender from recipients)'] * 100
        parsedData[nodeName]['successful_transmissions'] = node[
            'Successful Sent Messages (ACK received from dest. nodes)']
        parsedData[nodeName]['failed_transmissions'] = node[
            'Failed Sent Messages (NO ACK received from dest. nodes)']
        parsedData[nodeName]['total_transmissions'] = node[
            'Successful Sent Messages (ACK received from dest. nodes)'] + node['Failed Sent Messages (NO ACK received from dest. nodes)']

    rtt_values = []
    cr_values = []

    for node in sorted(parsedData):
        rtt_values.append(parsedData[node]['mean_rtt'])
        cr_values.append(parsedData[node]['message_completion_rate'])

    avg_rtt = sum(rtt_values) / len(rtt_values)
    avg_cr = sum(cr_values) / len(cr_values)

    row = [datetime, nodes, topology, algorithm, msg_size, throughput,
           latency, drop, distribution, avg_rtt, avg_cr, rtt_values, cr_values]
    with open(STATS_DIR + '/results_summary.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(row)


def printOptions():
    """
    Print the current options
    """
    print 'Starting with options:'
    print '----------------------'
    print '\tMode ' + str(MODE)
    print '\tNumberOfNodes: ' + str(scenario.nodeNumber)
    print '\tAlgorithm: ' + str(current_algo)
    print '\tMsgDistribution: ' + str(current_distribution)
    print '\tTopology: ' + str(current_topology) + ' (' + str(current_topology_folder) + str(')')
    print '\tMessageDistributionDuration: ' + str(current_distribution_duration)
    print '\tNetworkSettings: [' + ', '.join(current_networkSettings) + ']'

    # print '\tSimulationDuration: ' + str(simulation_duration)
    print ''


def generate_node_config_properties(node_id):
    with open(BASE_DIR + "take/real/config.properties.node" + str(node_id), 'w') as node_file:
        with open(BASE_DIR + 'take/real/config.properties') as template_file:
            for line in template_file:
                node_file.write(line.replace("node_name = LAP02", "node_name = node-" + str(node_id)))


def procedure_init_backbone():
    """
    init the whole backbone to be able to run experiments
    This will be very different if in Simulation MODE or Real MODE
    """
    global hosts

    if MODE is 'Simulation':
        print 'Starting Backbone (NS-3 and containers)'
        # Only parameter should be NumberOfNodes
        # start the backbone with desired number of nodes and "infinite running
        # time"
        update_config_file(node_containers=scenario.nodeNumber,
                           running_time=NS3_RUNNING_TIME)
        initNS3()
        startJavaTake()
        print 'Backbone started with ' + str(scenario.nodeNumber) + ' nodes'
    else:
        # TODO
        #"Load host file and check that #host = #node"
        hosts = Hosts(HOST_FILENAME)
        print "Copy necessary files to each node"
        for counter, host_ip in enumerate(hosts.ips):
            print "-- Deploying file to host <" + str(host_ip) + "> --"
            print "-- user <" + str(HOST_USERNAME+str(counter+1)) + "> --"
            #Generate the config.properties for this specific node
            generate_node_config_properties(counter)
            # execute script to copy the files
            try:
                proc = subprocess.Popen(
                    ["./real/takeDeploy_real.sh", HOST_USERNAME+str(counter+1), HOST_PASSWORD, host_ip, str(counter)])
                (stdout, stderr) = proc.communicate()  # wait for completion
            except Exception as e:
                print "  ERROR - Could not execute the process"
                print "  Exception " + str(e)
        for i, host_ip in enumerate(hosts.ips):
            print "Starting JavaTakeApp on host <" + str(host_ip) + ">"
            # execute script to copy the files
            try:
                proc = subprocess.Popen(
                    ["./real/takeJavaStart_real.sh", HOST_USERNAME+str(i+1), HOST_PASSWORD, host_ip])
                (stdout, stderr) = proc.communicate()  # wait for completion
            except Exception as e:
                print "  ERROR - Could not execute the process"
                print "  Exception: " + str(e)


def procedure_run_experiment(i):
    current_date = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M')
    global hosts
    # Update parameters for current experiment
    updateCurrentParameters(i)

    print ""
    printOptions()
    print ""

    if MODE is "Simulation":
        # Start experiment
        print 'Waiting for all nodes to be ready:'
        print '------------------------------'
        # TODO Get time to indicate get an idea of the time needed to start all
        # nodes

        start_time = time.time()
        while check_allnodes_ready() is not True:
            # print 'Nodes are not ready yet'
            time.sleep(10)
        elapsed = time.time() - start_time
        print '--> Ok, all nodes are ready! It took ' + str(elapsed) + ' seconds\n'
    else:
        hosts = Hosts(HOST_FILENAME)
    # Clean (if necessary)
    print 'Cleaning VMs'
    print '------------'
    cleanVMs()
    print '--> OK\n'
    # TODO

    # Get & set options (distribution, toplogy, network_settings)
    print 'Setting up configuration for all VMs'
    print '------------------------------------'

    config(current_distribution, current_topology,
           current_networkSettings, current_algo, current_algoTimeout)
    print '--> OK\n'

    print 'Waiting ' + str(NETWORK_CONVERGENCE_DELAY) + ' seconds to let the system converge'
    print '----------------------------------------------------'
    time.sleep(NETWORK_CONVERGENCE_DELAY)
    print '--> OK\n'

    # TODO Get time to indicate get an idea of the time needed to start all
    # nodes
    print 'Starting messaging app'
    print '----------------------'
    startMessaging()
    print '--> OK\n'

    print 'Waiting for all messages to be sent (duration of msg distribution + ' + str(BEFOREDATACLLECTION_DELAY) + ' seconds) (' + str(current_distribution_duration + BEFOREDATACLLECTION_DELAY) + ')'
    print '--------------------------------------'
    time.sleep(current_distribution_duration + BEFOREDATACLLECTION_DELAY)
    print '--> OK\n'

    print 'Collecting stats & logs'
    print '---------------'
    statsPath = STATS_DIR + '/' + current_topology_folder + '/' + \
        current_distribution_folder + '/' + \
        current_algo + '/' + current_date + '/Collected'
    if not os.path.exists(statsPath):
        os.makedirs(statsPath)
    collect(statsPath)
    print '--> OK\n'

    print 'Generating statistics'
    print '--------------------'
    try:
        # Generate plots from collected data
        statsPath = STATS_DIR + '/' + current_topology_folder + '/' + \
            current_distribution_folder + '/' + \
            current_algo + '/' + current_date
        # TODO send the title of the graph:
        graphTitle = 'Algorithm: ' + str(current_algo) + '\n' + 'Topology: ' + str(topology_names[current_topology]) + ' ' + str(current_networkSettings) + '\n' + 'Distribution: ' + str(
            current_distribution_messageNumber) + '-messages' + ', ' + str(current_distribution_duration) + '-seconds, ' + str(current_distribution_messageSize) + '-bytes'
        generateStats(statsPath, suptitle=graphTitle)
        # TODO - fetch the avgRTT and avgCR and append them to a file.csv
        # [nodes,topology, algorithm, msg size, throughput, latency, drop,
        # avgRTT, avgCR, perNodeRTT, perNodeCR, datetime]
        computeStats(statsPath, scenario.nodeNumber, (str(topology_names[current_topology])).split('_')[1], current_algo, current_distribution_messageSize, current_networkSettings[
                     0], current_networkSettings[1], current_networkSettings[2], current_distribution, current_date)
    except Exception as e:
        print 'Problem when generating statistics'
        print "  Exception: " + str(e)


def main(argv):
    """Main access point. 

        Keyword arguments:
            -m method: the desired method to use (the sub-method to start)
            -t topology (the id [int] of the topology)
            -d distribution (the name of the message distribution file)
        """
    global nodeNumber
    global current_distribution
    global current_topology
    global current_algo

    global current_date

    global current_experiment
    global run_multiple_experiment
    global loaded_scenario

    current_date = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M')

    # Default
    method = "run"  # default method to run
    current_experiment = 0  # start with the first experiment
    run_multiple_experiment = True  # perform all experiment of the scenario

    try:
        opts, args = getopt.getopt(
            argv, "hs:m:e:", ["scenario", "method=", "experiment="])
        if len(opts) is 0:
            print 'Warning - No argument received!'
            print 'you must at least specify the scenario you want to run '
    except getopt.GetoptError:
        print 'take.py -s <scenario> -m <method> -e <experiment>'
        print 'Example: ' + str("sudo python take.py -s 4Nodes_Test.scenario -e 0")
        print '--> this will run the experiment 0 of the scenario <4Nodes_Test.scenario >'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'take.py -s <scenario> -m <method> -e <experiment>'
            print 'Example: ' + str("sudo python take.py -s 4Nodes_Test.scenario -e 0")
            sys.exit(2)
        elif opt in ("-m", "--method"):
            method = str(arg)
        elif opt in ("-s", "--scenario"):
            loaded_scenario = arg
            load_scenario(loaded_scenario)
        elif opt in ("-e", "--experiment"):
            if(arg != 'all'):
                current_experiment = int(arg)
                run_multiple_experiment = False
            else:
                current_experiment = 0
                run_multiple_experiment = True

            updateCurrentParameters(current_experiment)

    if method == 'usage':
        print 'usage'
        print 'take.py -m <method> -t <topology> -d <distribution>'
        print 'method = [init, config, start, collect, execute]'
        print 'topology = [0, 1, 2, 3, 4, 5, 6, 7]'
        print 'distribution = message distribution full_file_path.json'

    elif method == 'test':
        for i in range(len(scenario.experiments)):
            print 'Experiment ' + str(i)
            updateCurrentParameters(i)
            printOptions()
    elif method == 'collect':
        print 'collect'
        statsPath = STATS_DIR + '/' + current_distribution_folder + '/' + \
            current_distribution_folder + '/' + \
            current_algo + '/' + current_date + '/Collected'
        collect(statsPath)
    elif method == 'plot':
        statsPath = STATS_DIR + '/' + current_distribution_folder + '/' + \
            current_distribution_folder + '/' + \
            current_algo + '/' + current_date
        # TODO send the title of the graph:
        generateStats(statsPath)
    elif method == 'init':
        procedure_init_backbone()
    elif method == 'run':
        start_time = datetime.datetime.now()
        if run_multiple_experiment:
            print 'Found ' + str(len(scenario.experiments)) + ' experiments to run'
            for i in range(len(scenario.experiments)):
                current_experiment = i
                print ""
                print "--------------------------------"
                print '    Running experiment <' + str(i) + '>'
                print "--------------------------------"
                print ""

                procedure_run_experiment(i)

                if(i != len(scenario.experiments) - 1):
                    print "Waiting " + str(BETWEEN_EXPERIMENT_DELAY) + " seconds before starting next experiment"
                    time.sleep(BETWEEN_EXPERIMENT_DELAY)
                    print ''
        else:
            print ""
            print "--------------------------------"
            print '    Running a single experiment <' + str(current_experiment) + '>'
            print "--------------------------------"
            print ""
            procedure_run_experiment(current_experiment)
        end_time = datetime.datetime.now()
        print "The scenario took {0} seconds".format(end_time-start_time) 
if __name__ == "__main__":
    main(sys.argv[1:])
