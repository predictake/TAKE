"""
@Title: distribution.py
@Description: Script to generate distributions
@Comments:
 usage : see samples in Main
@Author: Simon Ruffieux (HES-SO//FR)
@Project:  TAKE
"""

import random
import sys
import json
import operator


class Message:
    source = 0
    recipients = list()
    size = 100
    time = 0

    def __init__(self, time, source, recipients, size):
        self.source = source
        self.recipients = recipients
        self.size = size
        self.time = time

    def __str__(self):
        return "Message = [" + str(self.time) + ", " + str(self.source) + ", " + str(self.recipients) + ", " + str(self.size) + "]"


class Distribution:
    nodes = 0
    message_second = 0
    duration = 0
    nb_messages = 0
    messages = list()
    message_sizes = [100]
    multi_recipients = False

    def __init__(self, nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        # The number of nodes in the simulation
        self.nodes = nodes
        # The number of messages per second (from each node)
        self.message_second = message_second
        # The duration of the simulation
        self.duration = duration
        # The total number of messages sent during the simulation
        self.nb_messages = duration * nodes * message_second
        # The list of messages to be sent
        self.messages = messages
        # The potential sizes of messages. If more than one size are provided,
        # each mesage will use a randomly selected size
        self.message_sizes = message_sizes
        # Use multi-recipients ? (the recipient(s) are randomly selected)
        self.multi_recipients = multi_recipients

    def get_messages(self, node_id):
        node_messages = []
        for msg in self.messages:
            if str(msg.source) == str(node_id):
                node_messages.append(msg)
        return node_messages

    @staticmethod
    def create(nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        distribution = Distribution(
            nodes, duration, message_second, messages, message_sizes, multi_recipients)
        distribution.generate()
        return distribution

    @staticmethod
    def load(filename):
        # Load from file
        # Set values and load messages
        with open(filename, 'r') as outfile:
            data = json.load(outfile)
            msg_sizes = [i for i in data['message_sizes']]
            myDistrib = Distribution(nodes=data['nodes'],  message_second=data['message_second'], duration=data[
                                     'duration'], message_sizes=msg_sizes, multi_recipients=data['multi_recipients'])
            for message in data['messages']:
                recipients = [i for i in message['recipients']]
                myDistrib.messages.append(Message(time=message['time'], source=message[
                                          'source'], recipients=recipients, size=message['size']))
            return myDistrib

    def save(self, filename=""):
        if filename is "":
        	if self.multi_recipients:
        		recipient_string = "multi-recipients"
        	else:
        		recipient_string = "mono-recipients"
    		filename = "distribution-deterministic_{0}-Nodes_{1}s_{2}-messages_{3}-bytes_{4}.json".format(self.nodes, self.duration, self.nb_messages, "[" + ",".join(map(str,self.message_sizes))+ "]" , recipient_string)
        with open(filename, 'w') as outfile:
            json.dump(
self, fp=outfile, default=self.jdefault)
            print "Distribution saved to <{0}>".format(filename)

    def generate(self):
    	self.messages = []
        random.seed(42)  # use a fixed random seed
        total_messages = 0
        message_per_node = int(self.nb_messages / self.nodes)
        for source in range(self.nodes):
            total_source_message = 0
            for i in range(message_per_node):
                potential_recipients = range(0, self.nodes)
                potential_recipients.remove(source)     # Avoid sending to himself
                if self.multi_recipients is True:
                    nb_recipients = random.randint(1, self.nodes-1)
                    if total_source_message + nb_recipients > message_per_node:   # avoid generating more messages than desired in multi-recipients
                        nb_recipients = message_per_node - total_source_message
                else:
                    nb_recipients = 1
                if len(self.message_sizes) > 1:
                    size = random.choice(self.message_sizes)
                else:
                    size = self.message_sizes[0]

                time = random.randint(1, self.duration)
                recipients = [potential_recipients[i] for i in sorted(random.sample(xrange(len(potential_recipients)), nb_recipients))]
                msg = Message(time=time, source=source, recipients=recipients, size=size)
                # print msg
                self.messages.append(msg)

                total_source_message += nb_recipients
                total_messages += nb_recipients
                if total_source_message >= message_per_node:
                    break
            if total_messages >= self.nb_messages:
                break
        self.messages.sort(key=operator.attrgetter('time'))
        print "The generated distribution contains a total of {0} messages".format(total_messages)


    @staticmethod
    def jdefault(o):
        return o.__dict__


def main(argv):

	## SAMPLES ####################
	#
	# 1 Save and load a distribution with a fixed name
    # myDistrib = Distribution.create(nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save("sample_distribution.json")
    # myLoadedDistrib = Distribution.load('sample_distribution.json')
    # print myLoadedDistrib.messages[0]
    #
    # 2 Save a distribution with automatic naming
    # myDistrib = Distribution.create(nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    # myDistrib.save()
    ##############################
    
    myDistrib = Distribution.create(nodes=4, duration=50, message_second=1, multi_recipients=False, message_sizes=[100])
    myDistrib.save()

    myDistrib = Distribution.create(nodes=4, duration=50, message_second=1, multi_recipients=True, message_sizes=[100])
    myDistrib.save()
    
    myDistrib = Distribution.create(nodes=4, duration=50, message_second=0, multi_recipients=False, message_sizes=[100])
    myDistrib.save()

    myDistrib = Distribution.create(nodes=2, duration=50, message_second=1, multi_recipients=False, message_sizes=[100])
    myDistrib.save()

if __name__ == "__main__":
    main(sys.argv[1:])
