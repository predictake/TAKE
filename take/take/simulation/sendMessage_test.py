
import requests  # enable calls to REST
import json # enable json parsing for message distribution

url = "http://localhost:8080/take/msg/sendmessage"
data = {'type': 1, 'title': 'my title', 'content': 'the content of the message', 'nodeNames': 'node-2'}
headers = {'Content-type': "application/x-www-form-urlencoded", "Accept": "application/json"}
r = requests.post(url, params=data, headers=headers)
#r = requests.post(url, params=data)

print 'URL:' + r.url
print r.content
