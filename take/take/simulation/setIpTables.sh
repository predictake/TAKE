#!/bin/bash

# first param = current node  +1 ($1) EX: 3 for node-2
# second parm = topology type 
#  		custom  	meshed    	nbNode
#		0		1		4
#		2		3		10
#		4		5		60
#   6   7   5
if [ $2 = "1" ] || [ $2 = "3" ] || [ $2 = "5" ] || [ $2 = "7" ]; then
	echo "+ Full meshed topology (clear iptables)"
	iptables --flush
elif [ $2 = "0" ]; then
	echo "+ 4 nodes Arrow-linked"
	echo " n0"
	echo "  \\"
	echo "   n2---n3"
	echo "   /"
	echo " n1"
	iptables --flush
	if [ $1 = "1" ]; then  # param for node 0
		echo "++ Current node is 0 => Suppress olsr traffic from n3 and n1"
		iptables -I INPUT --source 10.0.0.4 -p udp --dport 698 -j DROP
		iptables -I INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP
	elif [ $1 = "2" ]; then # param for node 1
		echo "++ Current node is 1 => Suppress olsr traffic from n3 and n0"
		iptables -I INPUT --source 10.0.0.4 -p udp --dport 698 -j DROP	
		iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP	
	elif [ $1 = "3" ]; then # param for node 2
		echo "++ Current node is 2 => Suppress no olsr traffic"
	elif [ $1 = "4" ]; then # param for node 3
		echo "++ Current node is 3 => Suppress olsr traffic from n1 and n0"
		iptables -I INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP	
		iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP		
	else
		echo "-- Bad node id for the scenario => Flush Iptables on host"
		iptables --flush
	fi
elif [ $2 = "2" ]; then
	echo "+ 10 nodes custom"
	echo "     n0--------------------n5"
	echo "    /| \\                  /  \\"
	echo "   / |  \\                /    \\"
	echo " n4  |   n1------------n6-----n9"
	echo "   \\ |   /              \\      /"
	echo "    \\|  /                \\    /"
	echo "    n3-n2                n7--n8"
	iptables --flush
	if [ $1 -le 5 ] && [ $1 -ge 1 ]; then
		echo "++ Current node is on the left nodes group => Suppress olsr traffic from right group"
		for i in {6..10}
		do
			iptables -I INPUT --source 10.0.0.$i -p udp --dport 698 -j DROP
		done
		if [ $1 = "1" ]; then
			echo "+++ Current node is 0 => Suppress olsr traffic from n2"
			iptables -I INPUT --source 10.0.0.3 -p udp --dport 698 -j DROP
			echo "+++ reactivate olsr from n5"
			iptables -D INPUT --source 10.0.0.6 -p udp --dport 698 -j DROP
		elif [ $1 = "2" ]; then
			echo "+++ Current node is 1 => Suppress olsr traffic from n3 and n4"
			iptables -I INPUT --source 10.0.0.4 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.5 -p udp --dport 698 -j DROP
			echo "+++ reactivate olsr from n6"
			iptables -D INPUT --source 10.0.0.7 -p udp --dport 698 -j DROP
		elif [ $1 = "3" ]; then
			echo "+++ Current node is 2 => Suppress olsr traffic from n0 and n4"
			iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.5 -p udp --dport 698 -j DROP
		elif [ $1 = "4" ]; then
			echo "+++ Current node is 3 => Suppress olsr traffic from n1"
			iptables -I INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP
		else
			echo "+++ Current node is 4 => Suppress olsr traffic from n1 and n2"
			iptables -I INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.3 -p udp --dport 698 -j DROP
		fi
	elif [ $1 -le 10 ] && [ $1 -ge 6 ]; then
		echo "++ Current node is on the right nodes group => Suppress olsr traffic from left group"
		for i in {1..5}
		do
			iptables -I INPUT --source 10.0.0.$i -p udp --dport 698 -j DROP
		done
		if [ $1 = "6" ]; then
			echo "+++ Current node is 5 => Suppress olsr traffic from n7 and n8"
			iptables -I INPUT --source 10.0.0.8 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.9 -p udp --dport 698 -j DROP
			echo "+++ reactivate olsr from n0"
			iptables -D INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP
		elif [ $1 = "7" ]; then
			echo "+++ Current node is 6 => Suppress olsr traffic from n8"
			iptables -I INPUT --source 10.0.0.9 -p udp --dport 698 -j DROP
			echo "+++ reactivate olsr from n1"
			iptables -D INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP
		elif [ $1 = "8" ]; then
			echo "+++ Current node is 7 => Suppress olsr traffic from n5 and n9"
			iptables -I INPUT --source 10.0.0.6 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.10 -p udp --dport 698 -j DROP
		elif [ $1 = "9" ]; then
			echo "+++ Current node is 8 => Suppress olsr traffic from n5 and n6"
			iptables -I INPUT --source 10.0.0.6 -p udp --dport 698 -j DROP
			iptables -I INPUT --source 10.0.0.7 -p udp --dport 698 -j DROP
		else
			echo "+++ Current node is 9 => Suppress olsr traffic from n7"
			iptables -I INPUT --source 10.0.0.8 -p udp --dport 698 -j DROP
		fi
	else
		echo "-- Bad node id for the scenario => Flush Iptables on host"
		iptables --flush
	fi
elif [ $2 = "6" ]; then
	echo "+ 5 nodes custom"
	echo "        n2       "
	echo "       /  \      "
	echo " n0---n1--n3---n4"
	iptables --flush
	if [ $1 = "1" ]; then  # param for node 0
		echo "++ Current node is 0 => Suppress olsr traffic from n2 to n4"
		iptables -I INPUT --source 10.0.0.3 -p udp --dport 698 -j DROP
		iptables -I INPUT --source 10.0.0.4 -p udp --dport 698 -j DROP
		iptables -I INPUT --source 10.0.0.5 -p udp --dport 698 -j DROP
	elif [ $1 = "2" ]; then # param for node 1
		echo "++ Current node is 1 => Suppress olsr traffic from n4"
		iptables -I INPUT --source 10.0.0.5 -p udp --dport 698 -j DROP	
	elif [ $1 = "3" ]; then # param for node 2
		echo "++ Current node is 2 => Suppress olsr traffic from n0 and n4"
				iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP
				iptables -I INPUT --source 10.0.0.5 -p udp --dport 698 -j DROP
	elif [ $1 = "4" ]; then # param for node 3
		echo "++ Current node is 3 => Suppress olsr traffic from n0"
		iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP		
	elif [ $1 = "5" ]; then # param for node 4
		echo "++ Current node is 3 => Suppress olsr traffic from n0 to n2"
		iptables -I INPUT --source 10.0.0.1 -p udp --dport 698 -j DROP
		iptables -I INPUT --source 10.0.0.2 -p udp --dport 698 -j DROP		
		iptables -I INPUT --source 10.0.0.3 -p udp --dport 698 -j DROP				
	else
		echo "-- Bad node id for the scenario => Flush Iptables on host"
		iptables --flush
	fi
else
	echo "- Bad scenario number => Flush Iptables on host"
	iptables --flush
fi
