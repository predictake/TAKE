#!/bin/bash
# ---------------------------------------------------------------

if [[ -z "$1"  ]]
  then
     echo "Usage: tc <rate in kbit> <delay in ms> <drop in %>"
     echo "Example : ./tc.sh 300kbit 10ms 10%"
     exit
fi

echo "TC Command: ./tc.sh $1 $2 $3"

# purge the old queue
tc qdisc del dev eth0 root

# create the rules for rate, delay and drop
tc qdisc add dev eth0 root handle 1: tbf rate $1 burst $1 latency 180s
tc qdisc add dev eth0 parent 1: handle 2: netem delay $2
tc qdisc add dev eth0 parent 2: handle 3: netem loss $3

# display current config
tc qdisc show dev eth0

