#!/bin/bash
#NAME: takeDeploy_real.sh
#AUTHOR: Simon Ruffieux  2017
#CONTACT INFO: simon.ruffieux@hefr.ch
#VERSION: 0.1
#DESCRIPTION: A script that will deploy the files on a single machine


USERNAME=$1		# The username on the remote machine
PASSWORD=$2		# The password for the username
HOSTNAME=$3		# the remote machine ip or dns
DEST_DIR=$4
DEST_FILENAME=$5

echo "-->CMD: sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME}:./data/transmission_stats.json ${DEST_DIR}/${DEST_FILENAME}.json "
sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME}:./data/transmission_stats.json ${DEST_DIR}/${DEST_FILENAME}.json