#!/bin/bash
#NAME: takeConfig_real.sh
#AUTHOR: Simon Ruffieux  2017
#CONTACT INFO: simon.ruffieux@hefr.ch
#VERSION: 0.1
#DESCRIPTION: A sample script that copies Config files to each container (VM). The config file is adapted for each VM.
#NOTE Do not forget to chmod +x the file after copying it to the destination VM



USERNAME=$1		# The username on the remote machine
PASSWORD=$2		# The password for the username
HOSTNAME=$3		# the remote machine ip or dns
BASEDIR=$4
MESSAGE_DISTRIB=$5
ALGORITHM=$6
ALGORITHM_TIMEOUT=$7
BFT_UPDATEPERIOD=$8
RFT_UPDATEPERIOD=$9


sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no $BASEDIR/take/distributions/$MESSAGE_DISTRIB ${USERNAME}@${HOSTNAME}:./TakeMessengerDistrib.json
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} "curl -X POST http://localhost:8080/take/app/setsendingstrategy -d strategy=${ALGORITHM} -d timeout=${ALGORITHM_TIMEOUT}"
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} "curl -X POST http://localhost:8080/take/bft/setmessagesendingperiod -d period=${BFT_UPDATEPERIOD}"
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} "curl -X POST http://localhost:8080/take/bft/setmessagesendingperiod -d period=${RFT_UPDATEPERIOD}"