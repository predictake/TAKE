#!/bin/bash
#NAME: takeDeploy_real.sh
#AUTHOR: Simon Ruffieux  2017
#CONTACT INFO: simon.ruffieux@hefr.ch
#VERSION: 0.1
#DESCRIPTION: A script that will deploy the files on a single machine


USERNAME=$1		# The username on the remote machine
PASSWORD=$2		# The password for the username
HOSTNAME=$3		# the remote machine ip or dns
COUNTER=$4

sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} "nohup ./startTakeMessenger.sh $COUNTER > /dev/null 2>&1 &"