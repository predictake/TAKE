#!/bin/bash
#NAME: 			emultakeDeployAllParallel.sh
#AUTHOR: 		Christophe Gisler	(2017)
#DESCRIPTION: 	A script that will deploy EmulTake and Take on the machines (nodes) listed in the file given in argument 

HOSTS_FILE=$1		# "hosts_thun.txt" 	# File containing the list of IPs/DNS of the remote machines (format: one IP/DNS per line)
USERNAME=$2			# "root"			# Username to log on the remote machines
PASSWORD=$3			# "wtk"				# Password to log on the remote machines
REMOTEBASEDIR=$4	# "/root"			# Absolute path of the base directory in which install the files on the remote machines
ERROROUTDIR=./emultakeDeployErrors

clear

echo "Deploy EmulTake and Take Java applications on hosts (nodes) listed in '$HOSTS_FILE':"


echo -e "\n0. Install Linux requirements (unzip application)"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'sudo apt install unzip'

echo -e "\n1. Kill Java processes"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'pkill -9 java'

echo -e "\n2. Remove old version of EmulTake"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'rm -r ./EmulTake'

echo -e "\n3. Remove old version of Take"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'rm -r ./Take'

echo -e "\n4. Copy new version of EmulTake"
sshpass -p ${PASSWORD} pscp -h ${HOSTS_FILE} -A -l ${USERNAME} -v -e ${ERROROUTDIR} -rO StrictHostKeyChecking=no ./EmulTake ${REMOTEBASEDIR}/

echo -e "\n5. Copy new version of Take"
sshpass -p ${PASSWORD} pscp -h ${HOSTS_FILE} -A -l ${USERNAME} -v -e ${ERROROUTDIR} -rO StrictHostKeyChecking=no ./Take ${REMOTEBASEDIR}/

echo -e "\n6. Copy start script of EmulTake"
sshpass -p ${PASSWORD} pscp -h ${HOSTS_FILE} -A -l ${USERNAME} -v -e ${ERROROUTDIR} -O StrictHostKeyChecking=no ./startEmulTakeApp.sh ${REMOTEBASEDIR}/

echo -e "\n7. Copy service to launch EmulTake at start"
sshpass -p ${PASSWORD} pscp -h ${HOSTS_FILE} -A -l ${USERNAME} -v -e ${ERROROUTDIR} -O StrictHostKeyChecking=no ./emultake_service ${REMOTEBASEDIR}/emultake_service
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'chmod +x ./emultake_service'

echo -e "\n8. Copy script to install EmulTake launch service"
sshpass -p ${PASSWORD} pscp -h ${HOSTS_FILE} -A -l ${USERNAME} -v -e ${ERROROUTDIR} -O StrictHostKeyChecking=no ./set_launch_at_startup.sh ${REMOTEBASEDIR}/set_launch_at_startup.sh
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'chmod +x ./set_launch_at_startup.sh'

echo -e "\n9. Run script to install EmulTake launch service"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no ${REMOTEBASEDIR}/set_launch_at_startup.sh

#echo -e "\n10. > Remove script to install EmulTake launch service"
#sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'rm ./emultake_service'

#echo -e "\n11. > Remove service to launch EmulTake at start"
#sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'rm /set_launch_at_startup.sh

echo -e "\n10. Start EmulTake"
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'chmod +x ./startEmulTakeApp.sh'
sshpass -p ${PASSWORD} pssh -h ${HOSTS_FILE} -A -l ${USERNAME} -v -i -O StrictHostKeyChecking=no 'nohup ./startEmulTakeApp.sh > /dev/null 2>&1 &'
    
echo -e "\nDone for all hosts (nodes)"
