#!/bin/bash
#NAME: takeDeploy_real.sh
#AUTHOR: Simon Ruffieux  2017
#CONTACT INFO: simon.ruffieux@hefr.ch
#VERSION: 0.1
#DESCRIPTION: A script that will deploy the files on a single machine


USERNAME=$1		# The username on the remote machine
PASSWORD=$2		# The password for the username
HOSTNAME=$3		# the remote machine ip or dns
COUNTER=$4


sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'pkill java'
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm -r EmulTake'
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm -r Take'
sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no real/EmulTake ${USERNAME}@${HOSTNAME}:.
sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no real/Take ${USERNAME}@${HOSTNAME}:.
sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/startEmulTakeApp.sh ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/node_settings.csv ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/config.properties.node${COUNTER} ${USERNAME}@${HOSTNAME}:./config.properties
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/TakeMessengerDistrib.json ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/takeMessenger.py ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/startTakeApp.sh ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/startTakeMessenger.sh ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no real/take.jar ${USERNAME}@${HOSTNAME}:.
#sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm logs/take.log'
