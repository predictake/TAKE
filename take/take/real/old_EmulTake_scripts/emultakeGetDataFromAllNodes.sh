#!/bin/bash
#NAME: emultakeGetDataFromAllNodes.sh
#AUTHOR: Christophe Gisler  2017
#DESCRIPTION: A script that will get the data created by EmulTake and Take on the machines (nodes) 1..4 by armasuisse in Thun

DESTPATH=~/Desktop/EmulTake_Node_Data 	#${1}
USERNAME="root"								# The username on the remote machine
PASSWORD="wtk"								# The password for the username

mkdir ${DESTPATH}
for i in {1..4}
do
    HOSTNAME="take.n$i.wtk.tka.home"		# the remote machine ip or dns
        
    echo "Getting EmulTake logs and data files on:" ${HOSTNAME}
    sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME}:EmulTake/data ${DESTPATH}/Node_${i}
    sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME}:EmulTake/logs ${DESTPATH}/Node_${i}
    
    echo "Done"
done