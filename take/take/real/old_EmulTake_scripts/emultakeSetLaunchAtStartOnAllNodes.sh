#!/bin/bash
#NAME: 		emultakeDeployOnAllNodes.sh
#AUTHOR: 	Christophe Gisler  2017
#DESCRIPTION: A script that will deploy EmulTake and Take on the machines (nodes) 1..4 by armasuisse in Thun


USERNAME="root"		# The username on the remote machine
PASSWORD="wtk"		# The password for the username

for i in {1..4}
do
    HOSTNAME="take.n$i.wtk.tka.home"		# the remote machine ip or dns

    echo "Set launch EmulTake java application at start on:" ${HOSTNAME}
    sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no EmulTake/emultake_service ${USERNAME}@${HOSTNAME}:./emultake_service
    sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no EmulTake/set_launch_at_startup.sh ${USERNAME}@${HOSTNAME}:./set_launch_at_startup.sh
    sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'nohup ./set_launch_at_startup.sh'
    sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm ./emultake_service'
    sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOSTNAME} 'rm ./set_launch_at_startup.sh'

    echo "Done"
done
