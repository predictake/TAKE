# Script to launch the EmulTake (takemul) application at startup

sudo cp emultake_service /etc/init.d/emultake_service
sudo chmod +x /etc/init.d/emultake_service
sudo update-rc.d emultake_service defaults
#sudo update-rc.d emultake_service disable 

# N.B.: To verify that EmulTake can work as a service nohup java -jar takemul.jar & 