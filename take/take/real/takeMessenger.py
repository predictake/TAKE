"""
@Title: TakeMessenger.py
@Description: Scheduling system to send messages to TakeApp at specific times
@Comments:
 usage :
@Author: Simon Ruffieux (HES-SO//FR)
@Project:  TAKE
"""

#!/usr/bin/env python

import sys, getopt
import time
import os
import datetime
from apscheduler.schedulers.background import BackgroundScheduler

import requests  # enable calls to REST
import json # enable json parsing for message distribution

import logging
logging.basicConfig()

import random # for distributions
import operator




logger = logging.getLogger('TakeMessenger')
hdlr = logging.FileHandler('logs/messenger.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

DELAY_EXIT = 5  # the time before the programs shuts down when there is no more message to send
BASE_REST_URL = 'http://localhost:8080/take/'
MSG_REST_URL = BASE_REST_URL + 'msg/'
DISTRIBUTION_FILENAME = 'TakeMessengerDistrib.json'
#DISTRIBUTION_FILENAME = 'distribution.json'
DATETIME_FORMAT = "%Y-%m-%d_%H:%M:%S"
distribution = ""


class Message:
    source = 0
    recipients = list()
    size = 100
    time = 0

    def __init__(self, time, source, recipients, size):
        self.source = source
        self.recipients = recipients
        self.size = size
        self.time = time

    def __str__(self):
        return "Message = [" + str(self.time) + ", " + str(self.source) + ", " + str(self.recipients) + ", " + str(self.size) + "]"


class Distribution:
    nodes = 0
    message_second = 0
    duration = 0
    nb_messages = 0
    messages = list()
    message_sizes = [100]
    multi_recipients = False

    def __init__(self, nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        self.nodes = nodes                                      # The number of nodes in the simulation
        self.message_second = message_second                    # The number of messages per second (from each node)
        self.duration = duration                                # The duration of the simulation
        self.nb_messages = duration * nodes * message_second    # The total number of messages sent during the simulation
        self.messages = messages                                # The list of messages to be sent
        self.message_sizes = message_sizes                      # The potential sizes of messages. If more than one size are provided, each mesage will use a randomly selected size
        self.multi_recipients = multi_recipients                # Use multi-recipients ? (the recipient(s) are randomly selected)

    def get_messages(self, node_id):
        node_messages = []
        for msg in self.messages:
            if str(msg.source) == str(node_id):
                node_messages.append(msg)
        return node_messages

    @staticmethod
    def create(nodes, duration, message_second, messages=[], message_sizes=[100], multi_recipients=False):
        distribution = Distribution(nodes, duration, message_second, messages, message_sizes, multi_recipients)
        distribution.generate()
        return distribution

    @staticmethod
    def load(filename):
        #Load from file
        # Set values and load messages
        with open(filename, 'r') as outfile:
            data = json.load(outfile)
            msg_sizes = [i for i in data['message_sizes']]
            myDistrib = Distribution(nodes=data['nodes'],  message_second=data['message_second'], duration=data['duration'], message_sizes=msg_sizes,multi_recipients=data['multi_recipients'] )
            for message in data['messages']:
                recipients = [i for i in message['recipients']]
                myDistrib.messages.append(Message(time=message['time'], source=message['source'], recipients=recipients,size=message['size']))
            return myDistrib

    def save(self, filename):
        with open(filename, 'w') as outfile:
            json.dump(self, fp=outfile, default=self.jdefault)

    def generate(self):
        self.messages = []
        random.seed(42) #use a fixed random seed
        total_messages = 0
        message_per_node = int(self.nb_messages / self.nodes)
        for source in range(self.nodes):
            total_source_message = 0
            for i in range(message_per_node):
                potential_recipients = range(0, self.nodes)
                potential_recipients.remove(source)     # Avoid sending to himself
                if self.multi_recipients is True:
                    nb_recipients = random.randint(1, self.nodes-1)
                    if total_source_message + nb_recipients > message_per_node:   # avoid generating more messages than desired in multi-recipients
                        nb_recipients = message_per_node - total_source_message
                else:
                    nb_recipients = 1
                if len(self.message_sizes) > 1:
                    size = random.choice(self.message_sizes)
                else:
                    size = self.message_sizes[0]

                time = random.randint(1, self.duration)
                recipients = [potential_recipients[i] for i in sorted(random.sample(xrange(len(potential_recipients)), nb_recipients))]
                msg = Message(time=time, source=source, recipients=recipients, size=size)
                #print msg
                self.messages.append(msg)

                total_source_message += nb_recipients
                total_messages += nb_recipients
                if total_source_message >= message_per_node:
                    break
            if total_messages >= self.nb_messages:
                break
        self.messages.sort(key=operator.attrgetter('time'))
        print "The distribution contains a total of " + str(total_messages) + " messages"

    @staticmethod
    def jdefault(o):
        return o.__dict__


def print_message_test(text):
    """[TestMethod] Print the message to send on the console"""
    logger.info(str(datetime.datetime.now()) + ' - ' + text)

def send_message_test(dest_nodename):
    """Send a test message to through the TakeApp to the dest_nodename
    Keyword arguments:
        dest_nodename -- the name of the destination node
    """
    logger.info('Sending test message to ' + dest_nodename)
    #  Example: curl http://localhost:8080/take/msg/sendmsgtest/node-0/HELLO
    custom_url = MSG_REST_URL + 'sendmsgtest/' + dest_nodename + '/HELLO'
    try:
        myResponse = requests.get(url=custom_url)
    except requests.ConnectionError:
        logger.info('Connection error')
        myResponse = 'error'
    logger.info('REST Response: ' + str(myResponse))


def send_message(recipients, text_size):
    """Send a  message through the TakeApp to the desired recipients (can be one or many)
    Keyword arguments:
        recipients -- the name(s) of the recipients. Corresponds to the node name sperated by ';'' ex: dockemu-olsrd-0;dockemu-olsrd-1
                        Note that multiple reciepients are handled by the java program
    """
    msg_body = 'Default size'
    if text_size == 100:
        #message body = 100 characters
        msg_body = 'OAwSjZAIMt dBzFAapuxY CMrActjgTM IxZcqIEekV NpXwhfcAtQ TwdBaGllwn XADIPdZjMA WhDGzIzlJq HsedIfXvaR VzayCHsDuh'
    if text_size == 1000:
        #message body = 1000 characters
        msg_body = 'OAwSjZAIMt dBzFAapuxY CMrActjgTM IxZcqIEekV NpXwhfcAtQ TwdBaGllwn XADIPdZjMA WhDGzIzlJq HsedIfXvaR VzayCHsDuh '
        msg_body = msg_body + 'ZzsrqxkXSU WFmfrFWxPA BgeRAeEGWq EmKiVtxSlM wcOYFNfbnE tkztDqjyIr RdmIUPAOyg SNOvlIIpPz NKGLGhfBRc YZljGilGwq '
        msg_body = msg_body + 'CBcEBwMKKY HbgCwmfzNZ UgWrqCjxUN IrIsCmzzdJ CMFGlltdZI yXuBJDyKiU srfgvZKAoI whwFBbicii jWjWXELwtC ZjhoySDFqb '
        msg_body = msg_body + 'MSetOFtTKp wffoYjmiuT ddquLuHAIM fUjXGrQvex WknzsXRWSw fEvCTiMKwO fzOfiuAKKg XtlkhQegAu CjsyOwDzlM MYnSaHguDv '
        msg_body = msg_body + 'GtRoCvrqGW VubIvxrraO brTQAQysWx CYfVLfebOD alQndSFmLp PlFVaNYiBw TsnGskUHyt phihqRlKBL beUHRTrqPs rXrsoXcWae '
        msg_body = msg_body + 'rhDXvaXKcy uuzkQHMjVt jIrMbejiDN dRrqjbVmwe mJSxCKZWCU xfgZLSAvkG BlGjERQgHq sGavSDkYiC vkNaOJdAzL wkxHgNyUKs '
        msg_body = msg_body + 'iCuPVZpbUy EISNqUASQD aYDkcCqAww mdapXWWgcQ vWOYPaltJV kzTmeKLmzy DoGwHFVscU JDWbhQXlEM fHogGjiiYs mOkgvlnZso '
        msg_body = msg_body + 'KmhUTPNtrH TRFZwncDFK BirSEcCmhI lMJqEpvkrR WePDtyVKqe odDRbzKMzf YRgnvEnnqb nZpaDgxWtC rIiBtbMjCw LmoYIjfjNH '
        msg_body = msg_body + 'EEmUKDVciN JNrAmQIlhn LvSsIkiodp RhBXrygUYX FXuafSVPHj RkbHwqMFnt bsFmuFBckL HKiVQEdyvV kkFYtdvIpO IalBGJsGqa '
        msg_body = msg_body + 'GRCtRAbcbF fKzypnHFPi VzEDGedLAQ FEVfLWJGVf huCAViUKvV zNCBBHiMBm cjgiOIpXRV XLtnOUIfgs MqVPwxnnPD TgUUhFTGLT '

    url = MSG_REST_URL + 'sendmessage'
    post_fields = {'type': 1, 'title': 'my title', 'content': msg_body, 'nodenames': recipients }
    headers = {'Content-type': "application/x-www-form-urlencoded", "Accept": "application/json"}
    try:
        myResponse = requests.post(url=url, params=post_fields, headers=headers)
    except requests.ConnectionError:
        logger.info('Connection error')
        myResponse = 'Connection error'
    #logger.info('Sent: ' + str(myResponse.url))
    logger.info('Sending Message to ' + str(recipients) + ' with ' + str(text_size) + ' bytes - REST Response: ' + str(myResponse))




def load_my_jobs(nodeId):
    """Load the jobs (or messages) from the json distribution file and keep only the message sent from this node.
    Keyword arguments:
        nodeId -- the nodeId of the current node (it is an int)
    """
    logger.info('Loading jobs')
    loaded_jobs = []
    # Load my messaging jobs from json
    with open(DISTRIBUTION_FILENAME) as data_file:
        jdata = json.load(data_file)
        for msg in jdata['messages']:
            if str(msg['src']) is nodeId:
                loaded_jobs.append(msg)

    # TODO Get the final message time
    return loaded_jobs

def add_jobs(scheduler, jobs, start_time):
    """Add jobs to the scheduler. The jos are added respectively to a start_time.
    Keyword arguments:
        scheduler -- the scheduler to add the jobs to
        jobs -- the list of jobs to add
        start_time -- the initial start time, each job will be added x seconds after this datetime
    """
    logger.info(str(len(jobs)) + ' jobs added to the scheduler')
    # TODO Add jobs to scheduler
    for msg in jobs:
        job_timedelta = msg.time # TODO get the job time
        job_time = datetime.datetime.strptime(start_time, DATETIME_FORMAT) + datetime.timedelta(0, job_timedelta)
        recipient = ','.join('node-{0}'.format(w) for w in msg.recipients)
        msg_size = msg.size
        # TODO Add the msg size and use send_message instead of send_message_test
        scheduler.add_job(send_message, trigger='date', run_date=job_time, args=[recipient, msg_size])

def main(argv):
    """Main access point. Load the jobs, adds them and starts the scheduling procedure.
        Message will be sent to the TakeApp REST service of the current node.

        Keyword arguments:
            argv[1] -- the start time (the start_time of schedulers on every node should be the same ...)
            nodeID -- the id of the current node (int)
        """
    # Default values
    start_time = datetime.datetime.now().strftime(DATETIME_FORMAT)  # default start_time is now
    nodeID = '0'  # default node id is '0'
    try:
        opts, args = getopt.getopt(argv, "hs:n:", ["start_time=", "node_id="])
        if len(opts) is 0:
            logger.info('Warning - No argument received, using default parameters')
    except getopt.GetoptError:
        logger.info('takeMessenger.py -s <start_time> -n <node_id>')
        logger.info('Example: ' + str("python takeMessenger.py -s \"2016-12-15 15:21:00\" -n 3"))
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            logger.info('takeMessenger.py -s <start_time> -n <node_id>')
            logger.info('Example: ' + str("python takeMessenger.py -s \"2016-12-15 15:21:00\" -n 3"))
            sys.exit()
        elif opt in ("-s", "--stime"):
            start_time = arg
        elif opt in ("-n", "--nid"):
            nodeID = arg

    logger.info('Starting: nodeID=' + str(nodeID) + '  startTime=' + str(start_time))
    scheduler = BackgroundScheduler()
    #jobs = load_my_jobs(nodeID)  # Load the jobs from json file
    distribution = Distribution.load(DISTRIBUTION_FILENAME)
    jobs = distribution.get_messages(nodeID)

    add_jobs(scheduler=scheduler, jobs=jobs, start_time=start_time)  # Add the jobs


    ##Start the scheduler
    scheduler.start()  # start the scheduler
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        # This is here to simulate application activity (which keeps the main thread alive)
        exit_delay = 0
        while True:
            time.sleep(1)
            if len(scheduler.get_jobs()) is 0:
                if exit_delay is 0:
                    logger.info('No more message to send - Exiting in ' + str(DELAY_EXIT) + ' seconds')
                exit_delay += 1
                if exit_delay > DELAY_EXIT:
                    scheduler.shutdown()
                    logger.info('Exiting messenger application')
                    break # exit loop and program
    except (KeyboardInterrupt, SystemExit):
        # Not strictly necessary if daemonic mode is enabled but should be done if possible
        scheduler.shutdown()


if __name__ == "__main__":
    main(sys.argv[1:])








