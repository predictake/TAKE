TAKE Application Readme File
----------------------------

To run the application:
1. make sure that: 
	- Java 8 or higher is installed
	- the config.properties file and the node_settings.csv are correctly set

3. execute the script run.sh
4. go to localhost:8080/take/<api> to use the web app (according the API)

All created data will be available under <data_dir_path> set in the config.properties file.