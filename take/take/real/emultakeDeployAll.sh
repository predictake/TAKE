#!/bin/bash
#NAME: 			emultakeDeployAll.sh
#AUTHOR: 		Christophe Gisler	(2017)
#DESCRIPTION: 	A script that will deploy EmulTake and Take on the machines (nodes) listed in the file given in argument 

HOSTS_FILE=$1 	# "hosts_thun.txt" 	# File containing the list of IPs/DNS of the remote machines (format: one IP/DNS per line)
USERNAME=$2 	# "root"			# Username to log on the remote machines
PASSWORD=$3 	# "wtk"				# Password to log on the remote machines

clear

echo "Deploying Take and EmulTake on hosts (nodes) given in file '$HOSTS_FILE'"

while read HOST || [ -n "$HOST" ]; do
	./emultakeDeployOne.sh ${HOST} ${USERNAME} ${PASSWORD}
done < ${HOSTS_FILE}

echo "Done for all hosts (nodes)"