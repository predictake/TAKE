#!/bin/bash
#NAME: 			emultakeDeployOne.sh
#AUTHOR: 		Christophe Gisler	(2017)
#DESCRIPTION: 	A script that will deploy EmulTake and Take on a single machine (node) given in argument 

HOST=$1 		# IP/DNS of the remote machine (node) 
USERNAME=$2		# Username to log on the remote machine
PASSWORD=$3		# Password to log on the remote machine

echo " - ${HOST} > Installing Linux requirements (unzip application)"
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'sudo apt install unzip'

echo " - ${HOST} > Deploying Take and EmulTake"
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'sudo apt install unzip'
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'pkill -9 java'
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'rm -r EmulTake'
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'rm -r Take'
sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no EmulTake ${USERNAME}@${HOST}:.
sshpass -p ${PASSWORD} scp -ro StrictHostKeyChecking=no Take ${USERNAME}@${HOST}:.
sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no startEmulTakeApp.sh ${USERNAME}@${HOST}:.
    
echo " - ${HOST} > Set launch EmulTake java application at start"
sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no emultake_service ${USERNAME}@${HOST}:./emultake_service
sshpass -p ${PASSWORD} scp -o StrictHostKeyChecking=no set_launch_at_startup.sh ${USERNAME}@${HOST}:./set_launch_at_startup.sh
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} './set_launch_at_startup.sh'
#sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'rm ./emultake_service'
#sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'rm ./set_launch_at_startup.sh'

echo " - ${HOST} > Starting EmulTake Java application"
sshpass -p ${PASSWORD} ssh -o StrictHostKeyChecking=no ${USERNAME}@${HOST} 'nohup ./startEmulTakeApp.sh > /dev/null 2>&1 &'
    
echo " - ${HOST} > Done"
