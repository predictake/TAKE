#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2016 Mathieu Devaud
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" This module fetch the list of ip from the routing table and format it for
    the network api """

# ------------------------------------------------------------------------------
import json
# ------------------------------------------------------------------------------

if __name__ == '__main__':

    try:
        print("Fetching olsrd topology ...")

        # get the json file from the olsrd daemon
        with open('api-routing-table.json') as data_file:
            data = json.load(data_file)

        # ip list
        topology = {
            'topology': {
                'nodeid': '',
                'nodeip': '',
                'time': '',
                'ips': []
            }
        }

        # control if there is link on this node
        if data['rt']:
            topology['topology']['nodeid'] = data['rt']['nodeid']  # get nodeid
            topology['topology']['nodeip'] = data['rt']['nodeip']  # get nodeip
            topology['topology']['time'] = data['rt']['time']  # get timestamp

            # control if there is routes on this node
            if data['rt']['entry']:
                # get all the routes entry
                for i in range(0, len(data['rt']['entry'])):
                    ips = {'ip': (data['rt']['entry'][i]['dst'])
                           }
                    topology['topology']['ips'].append(ips)

                # Save the dictionary into this file
                # (the 'indent=4' is optional, but makes it more readable)
                out_file = open("api-ips.json", "w")
                json.dump(topology, out_file, indent=4)

                print('List of ip was written on "api-ips.json"')

            else:
                print("No routes were found...")
        else:
            print("No links on this node")

    except:
        print("Error while trying to fetch the routing table")

__author__ = 'Mathieu Devaud'
__since__ = '2016-09-13'
__date__ = '2016-09-13'
__version__ = '1.0'
__email__ = 'mathieu.devaud@hefr.ch'
