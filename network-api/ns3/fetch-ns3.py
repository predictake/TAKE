#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2016 Mathieu Devaud
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" This module fetch the routing table of the ns3 file and format it for
    the network api """

# ------------------------------------------------------------------------------
import json
import re
from xml.dom import minidom
# ------------------------------------------------------------------------------

if __name__ == '__main__':

    try:
        print("Fetching ns3 routing table ...")
        xmldoc = minidom.parse('olsr-wifi-routingtable.xml')  # read xml file

        node = "1"
        time = "5"

        # routing table
        routing = {
            'rt': {
                'nodeid': '',
                'nodeip': '',
                'time': '',
                'entry': []
            }
        }

        routing['rt']['nodeid'] = node  # write node id in JSON structure
        routing['rt']['time'] = time  # write time in JSON structure

        itemlist = xmldoc.getElementsByTagName('rt')

        for s in itemlist:
            if s.attributes['id'].value == node and s.attributes['t'].value \
                    == time:
                info = s.attributes['info'].value

                ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', info)
                number = [int(s) for s in info.split() if s.isdigit()]
                metric = number[1:]
                routes = []
                i = 0
                while i < len(ip) - 1:
                    routes.append([ip[i], ip[i+1]])
                    i += 2

                j = 0
                i = 0
                while i < len(metric):
                    entry = {   'dst' : routes[j][0],
                                'nexthop' : routes[j][1],
                                'hopcnt' : metric[i+1]
                            }
                    routing['rt']['entry'].append(entry)
                    i += 2
                    j += 1

        routing['rt']['nodeid'] = itemlist[0].attributes['id'].value
        routing['rt']['nodeip'] = itemlist[0].attributes['id'].value

        # Save the dictionary into this file
        # (the 'indent=4' is optional, but makes it more readable)
        out_file = open("api-routing-table.json", "w")
        json.dump(routing, out_file, indent=4)

        print('Routing table was written on "api-routing-table.json"')
        os.system("rm ns3-routing-table.json")


    except:
        print("Error while trying to fetch the routing table")

__author__ = 'Mathieu Devaud'
__since__ = '2016-09-13'
__date__ = '2016-09-13'
__version__ = '1.0'
__email__ = 'mathieu.devaud@hefr.ch'
