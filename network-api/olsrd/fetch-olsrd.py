#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2016 Mathieu Devaud
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" This module fetch the routing table of the daemon olsrd and format it for
    the network api """

# ------------------------------------------------------------------------------
import os
import json
# ------------------------------------------------------------------------------

if __name__ == '__main__':

    try:
        print ("Fetching olsrd routing table ...")

        os.system("curl http://localhost:9090/all > olsrd-routing-table.json")

        # get the json file from the olsrd daemon
        with open('olsrd-routing-table.json') as data_file:
            data = json.load(data_file)

        # routing table
        routing = {
            'rt': {
                'nodeid': '',
                'nodeip': '',
                'time': '',
                'entry': []
            }
        }

        # control if there is link on this node
        if data['links']:
            routing['rt']['nodeid'] = data['links'][0]['localIP']  # get nodeid
            routing['rt']['nodeip'] = data['links'][0]['localIP']  # get nodeip
            routing['rt']['time'] = data['systemTime']  # get timestamp

            # control if there is routes on this node
            if data['routes']:
                # get all the routes entry
                for i in range(0, len(data['routes'])):
                    entry = {'dst': (data['routes'][i]['destination']),
                             'nexthop': (data['routes'][i]['gateway']),
                             'hopcnt': (data['routes'][i]['metric'])
                             }
                    routing['rt']['entry'].append(entry)

                # Save the dictionary into this file
                # (the 'indent=4' is optional, but makes it more readable)
                out_file = open("api-routing-table.json", "w")
                json.dump(routing, out_file, indent=4)

                print('Routing table was written on "api-routing-table.json"')
                os.system("rm olsrd-routing-table.json")

            else:
                print("No routes were found...")
        else:
            print("No links on this node")

    except:
        print("Error while trying to fetch the routing table")

__author__ = 'Mathieu Devaud'
__since__ = '2016-09-13'
__date__ = '2016-09-13'
__version__ = '1.0'
__email__ = 'mathieu.devaud@hefr.ch'
