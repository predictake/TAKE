/**********************************************************************/

package take.server.ws;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**********************************************************************/
private static String readFile(String filename) {
    String result = "";
    try {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            line = br.readLine();
        }
        result = sb.toString();
    } catch(Exception e) {
        e.printStackTrace();
    }
    return result;
}

/**********************************************************************/
/**
 * Network REST API
 * @author Devaud Mathieu
 */
/**********************************************************************/

/**
 * Get the routing table at current time of either :
 * 	- all nodes (no parameter);
 * 	- node(s) with the given ID(s) (parameter 'nodeids');
 * @param nodeIDs: (optional) comma-separated list of node IDs: "id0,id1,..."
 * @return JSON response containing the routing table of the specific node
 */

public Response getroutingtable(int[] nodeIDs) {

	String result = "";

	Process p = Runtime.getRuntime().exec("python olsrd/fetch-olsrd.py");
    //Process p = Runtime.getRuntime().exec("python ns3/fetch-ns3.py");

	if (nodeID == null) {
		LOGGER.info("Get routing table of all the nodes");

		String jsonData = readFile("api-routing-table.json");
    JSONObject jsonObject = new JSONObject(jsonData);
		result = "" + jsonObject;

	}
	else {
		LOGGER.info("Get routing table of node(s) {}", nodeIDs);

        String jsonData = readFile("olsrd/api-routing-table.json");
		//String jsonData = readFile("ns3/api-routing-table.json");

		JSONObject jsonObject = new JSONObject(jsonData);
		result = "" + jsonObject;
	}

	return Response.status(200).entity(result).build();
}
