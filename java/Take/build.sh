# Script to compile, package and run the Take application

# Created on 2016.12.13 by Christophe Gisler

clear
rm -rf ./data
rm -rf ./logs
rm -rf ./tomcat.*
mvn clean compile
mvn package
cp ./target/take.jar .
rm -rf ./target 