# To deploy the artifact into this local Maven repository (lib):
# (See https://devcenter.heroku.com/articles/local-maven-dependencies)
cd ..
mvn deploy:deploy-file -Durl=file:///absolute/path/to/this/directory/lib -Dfile=lib/routingLibrary-0.0.6-SNAPSHOT.jar -DgroupId=ch.ar.wtk -DartifactId=routing -Dpackaging=jar -Dversion=0.0.6
mvn install:install-file -Dfile=lib/routingLibrary-0.0.6-SNAPSHOT.jar -DgroupId=ch.ar.wtk -DartifactId=routing -Dpackaging=jar -Dversion=0.0.6