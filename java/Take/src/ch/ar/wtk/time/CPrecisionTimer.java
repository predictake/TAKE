package ch.ar.wtk.time;

import com.sun.jna.Library;

public interface CPrecisionTimer extends Library {

	public void Init();

	public int SetMode(int newMode, int syncIp, int portNb, int datarate);

	public double GetTime();

	public void EndGPSDMonitoring();

}
