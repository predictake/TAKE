package main.java.take.controller.network;

import java.util.Date;

import main.java.take.model.Geolocation;
import main.java.take.model.RoutingTable;

/**
 * 
 * Network interface to retrieve current required network and node information.
 * 
 * @author Gisler Christophe
 *
 */
public interface RadioGPSInterface {

	public RoutingTable getCurrentOLSRRoutingTable() throws Exception;

	public Geolocation getCurrentGeolocation() throws Exception;

	public long getCurrentTimeMillis();

	public Date getCurrentDate();

}
