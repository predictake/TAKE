package main.java.take.controller.network;

import com.sun.jna.Native;

import java.io.IOException;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.ar.wtk.routing.RoutingReader;
import ch.ar.wtk.routing.interfaces.RoutingListener;
//import ch.ar.wtk.routing.model.Band;   	// For routingLibrary-0.0.1-SNAPSHOT.jar
import ch.ar.wtk.routing.model.Channel;		// For routingLibrary-0.0.2-SNAPSHOT.jar
import ch.ar.wtk.routing.model.RouteEntry;
import ch.ar.wtk.routing.model.RoutingTables;
import ch.ar.wtk.routing.model.Supplier;
import ch.ar.wtk.time.CPrecisionTimer;
import de.taimos.gpsd4java.api.ObjectListener;
import de.taimos.gpsd4java.backend.GPSdEndpoint;
import de.taimos.gpsd4java.backend.ResultParser;
import de.taimos.gpsd4java.types.TPVObject;
import main.java.take.controller.Preferences;
import main.java.take.controller.TakeController;
import main.java.take.model.Geolocation;
import main.java.take.model.Node;
import main.java.take.model.RoutingTable;

/**
 * 
 * @see provided README.md file and Tester.java example in the downloaded
 *      Gpsd4Java project.
 * 
 * @author Gisler Christophe
 *
 */
public class RadioGPSInterfaceReal implements RadioGPSInterface {

	public static final String NAME = "Real";

	private static final boolean USE_ARMY_PRECISION_TIMER = true;
	private static final int GPS_MODE = 2; // Use GPS for sync
	private static final DecimalFormat DF = new DecimalFormat("#.####");

	private boolean gpsdReady = false, precisionTimerReady = false, radioInterfaceReady = false;
	private RoutingTable currentRoutingTable;
	private GPSdEndpoint gpsdEndpoint = null;
	private CPrecisionTimer timer;
	private double currentLongitude, currentLatitude;
	private long currentTimeMillisDelta;

	private static final Logger LOGGER = LoggerFactory.getLogger(RadioGPSInterfaceReal.class);

	private static final org.apache.logging.log4j.Logger LOGGER2 = org.apache.logging.log4j.LogManager
			.getLogger(RadioGPSInterfaceReal.class);

	/**
	 * Init current GPS location polling system using GPSd library
	 */
	public void initGPSd() throws UnknownHostException, IOException {
		DF.setRoundingMode(RoundingMode.DOWN);
		Preferences pm = Preferences.getInstance();
		String gpsdHostAddress = InetAddress.getByName(pm.getGPSdHost()).getHostAddress();
		int gpsdPort = pm.getGPSdPort();

		gpsdEndpoint = new GPSdEndpoint(gpsdHostAddress, gpsdPort, new ResultParser());
		gpsdEndpoint.addListener(new ObjectListener() {
			@Override
			public void handleTPV(TPVObject tpv) {
				if (!USE_ARMY_PRECISION_TIMER)
					currentTimeMillisDelta = (long) tpv.getTimestamp() - System.currentTimeMillis();
				// Round number to 4 decimals
				currentLongitude = Double.parseDouble(DF.format(tpv.getLongitude()));
				currentLatitude = Double.parseDouble(DF.format(tpv.getLatitude()));
			}
		});
		gpsdEndpoint.start();
		gpsdEndpoint.watch(true, true);
		this.gpsdReady = true;
		LOGGER.info("GPSd interface initialized (GPSd host address: {}, GPSd port: {})", gpsdHostAddress, gpsdPort);
	}

	public boolean isGPSdReady() {
		return gpsdReady;
	}

	/**
	 * Init army precision timer
	 * 
	 * @throws UnknownHostException
	 */
	public void initPrecisionTimer() throws UnknownHostException {
		Preferences pm = Preferences.getInstance();
		InetAddress gpsdHost = InetAddress.getByName(pm.getGPSdHost());
		// Load and init native library
		timer = (CPrecisionTimer) Native.loadLibrary("CPrecisionTimer", CPrecisionTimer.class);
		timer.Init();
		// Set mode (convert IP to int)
		timer.SetMode(GPS_MODE, ByteBuffer.wrap(gpsdHost.getAddress()).getInt(), pm.getGPSdPort(), 0);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Stop native threading
				timer.EndGPSDMonitoring();
				LOGGER.debug("Armasuisse precision timer ended");
			}
		});
		this.precisionTimerReady = true;
		LOGGER.info("Armasuisse precision timer initialized (GPSd host address: {})", gpsdHost.getHostAddress());
	}

	public boolean isPrecisionTimerReady() {
		return precisionTimerReady;
	}

	/**
	 * Init radio interface for polling routing tables using Armasuisse library
	 * 
	 * @throws UnknownHostException
	 */
	public void initRadioInterface() throws UnknownHostException {
		Preferences pm = Preferences.getInstance();
		Supplier radioSupplier = pm.getRadioSupplier().equals("Elbit") ? Supplier.ELBIT : Supplier.ROHDE_SCHWARZ;
		Node currentNode = pm.getCurrentNode();
		String radioID = currentNode.getRadioID();
		InetAddress radioMgtIP = InetAddress.getByName(currentNode.getRadioIP());
		Channel radioChannel;
		switch (pm.getRadioChannel()) {
		case "CH1":
			radioChannel = Channel.CH1;
			break;
		case "CH0":
		default:
			radioChannel = Channel.CH0;
			break;
		}
		int radioRoutingTablePollingInterval = pm.getRadioRoutingTablePollingInterval();
		RoutingListener routingListener = new RoutingListener() {
			@Override
			public void tablesAvailable(String radioID, RoutingTables routingTables) {
				try {
					setCurrentRoutingTable(routingTables.getRouteTable());
				} catch (Exception e) {
					LOGGER.error("ERROR during setCurrentRoutingTable:", e);
				}
				// Pour info: LinkEntry le = new LinkEntry(node, rssi, snr, linkQuality);
			}
		};
		try {
			RoutingReader.getInstance(routingListener, radioMgtIP, radioSupplier, radioRoutingTablePollingInterval, radioID, radioChannel, LOGGER2);
		} catch (Exception e) {
			LOGGER.error("ERROR in routingReader.getInstance:", e);
		}
		this.radioInterfaceReady = true;
		LOGGER.info(
				"Armasuisse radio routing interface initialized (radio supplier: {}, radio ID: {}, polling interval [s]: {}, radio IP: {}, radio IP: {})",
				pm.getRadioSupplier(), radioID, radioRoutingTablePollingInterval, currentNode.getRadioIP(),
				radioMgtIP.getHostAddress());
	}

	public boolean isRadioInterfaceReady() {
		return radioInterfaceReady;
	}

	private void setCurrentRoutingTable(List<RouteEntry> routeTable) throws Exception {
		if (!isPrecisionTimerReady()) {
			LOGGER.warn("Army precision timer not yet ready! Routing table not set!");
			return;
		}

		Preferences pm = Preferences.getInstance();
		RoutingTable newRoutingTable = new RoutingTable(getCurrentDate());

		for (RouteEntry routeEntry : routeTable) {
			String destRadioID = "" + routeEntry.getDestinationNode();
			String nextRadioID = "" + routeEntry.getNextNode();
			Node destNode = pm.getNodeWithRadioID(destRadioID);
			Node nextNode = pm.getNodeWithRadioID(nextRadioID);
			if (destNode != null && nextNode != null) {
				String destNodeIP = destNode.getNodeIP();
				String nextNodeIP = nextNode.getNodeIP();
				// LOGGER.debug("setCurrentRoutingTable: adding entry (destRadioID, destNodeIP,
				// nextRadioID, nextNodeIP): ({}, {}, {}, {})", destRadioID, destNodeIP,
				// nextRadioID, nextNodeIP);
				if (TakeController.getInstance().getNodeName(destNodeIP) != null)
					newRoutingTable.addEntry(destNodeIP, nextNodeIP);
			} else
				LOGGER.debug("Set current routing table: No existing node with destination radio ID '" + destRadioID
						+ "' and next radio ID '" + nextRadioID + "'");
		}
		LOGGER.debug("Current routing table updated");

		this.currentRoutingTable = newRoutingTable;
	}

	@Override
	public RoutingTable getCurrentOLSRRoutingTable() throws Exception {
		return currentRoutingTable;
	}

	@Override
	public Geolocation getCurrentGeolocation() throws Exception {
		// LOGGER.info("gpsdEndpoint.poll(): " + gpsdEndpoint.poll());
		if (currentLongitude == 0. && currentLatitude == 0.)
			LOGGER.warn("Current geolocation (longitude, latitude) = ({}, {})", currentLongitude, currentLatitude);
		return new Geolocation(currentLongitude, currentLatitude);
	}

	@Override
	public long getCurrentTimeMillis() {
		if (!USE_ARMY_PRECISION_TIMER)
			return currentTimeMillisDelta + System.currentTimeMillis();

		/*
		 * Calendar cal = Calendar.getInstance(); int year = cal.get(Calendar.YEAR); int
		 * month = cal.get(Calendar.MONTH); int date = cal.get(Calendar.DATE);
		 * cal.clear(); cal.set(year, month, date); long currentDayInMillis =
		 * cal.getTimeInMillis();
		 */
		// Or with Java 8:
		long currentDayInMillis = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000l;
		long currentTimeFromMidnightInMillis = (long) (timer.GetTime() * 1000.0);
		return currentDayInMillis + currentTimeFromMidnightInMillis;
	}

	@Override
	public Date getCurrentDate() {
		if (!isPrecisionTimerReady()) {
			LOGGER.warn("Army precision timer not yet ready! Returned date will be null!");
			return null;
		}
		return new Date(getCurrentTimeMillis());
	}

}
