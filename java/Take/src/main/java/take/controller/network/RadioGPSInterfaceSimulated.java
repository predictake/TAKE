package main.java.take.controller.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

import main.java.take.controller.TakeController;
import main.java.take.model.Geolocation;
import main.java.take.model.RoutingTable;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class RadioGPSInterfaceSimulated implements RadioGPSInterface {

	public static final String NAME = "Simulated";

	private Geolocation currentGeolocation;

	@Override
	public RoutingTable getCurrentOLSRRoutingTable() throws Exception {
		RoutingTable newRoutingTable = new RoutingTable(getCurrentDate());

		Process p = Runtime.getRuntime().exec("route -n");
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = null;
		int i = 0;
		while ((line = br.readLine()) != null) {
			if (i < 2)
				i++;
			else if (line.length() > 0) {
				String[] lineContent = line.split(" ");
				String destNodeIP = lineContent[0];
				int j = 1;
				while (lineContent[j].length() == 0)
					j++;
				String nextNodeIP = lineContent[j];
				if (TakeController.getInstance().getNodeName(destNodeIP) != null)
					newRoutingTable.addEntry(destNodeIP, nextNodeIP);
			}
		}
		/* int exitValue = */p.waitFor();

		return newRoutingTable;
	}

	@Override
	public Geolocation getCurrentGeolocation() {
		this.currentGeolocation = this.currentGeolocation == null ? Geolocation.newRandomGeolocation()
				: Geolocation.newRandomlyMovedGeolocation(this.currentGeolocation);
		return this.currentGeolocation;
	}

	@Override
	public long getCurrentTimeMillis() {
		return System.currentTimeMillis();
	}

	@Override
	public Date getCurrentDate() {
		return new Date();
	}

}
