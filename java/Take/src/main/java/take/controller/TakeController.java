package main.java.take.controller;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.controller.network.RadioGPSInterface;
import main.java.take.controller.network.RadioGPSInterfaceReal;
import main.java.take.controller.network.RadioGPSInterfaceSimulated;
import main.java.take.controller.sendingstrategy.SendingStrategy;
import main.java.take.controller.sendingstrategy.SendingStrategyDirect;
import main.java.take.controller.sendingstrategy.SendingStrategyGetCloser;
import main.java.take.model.DataMessage;
import main.java.take.model.DataMessage.Type;
import main.java.take.model.Geolocation;
import main.java.take.model.Node;
import main.java.take.model.RoutingTable;
import main.java.take.model.TransmissionStatistics;
import main.java.take.model.UserMessage;
import main.java.take.model.UserMessage.Status;
import main.java.take.model.Message;
import main.java.take.model.MessageAcknowledgment;
import main.java.take.model.MessageTrace;
import main.java.take.model.MilitaryUnit;
import main.java.take.util.FileUtil;

/**
 * Main controller of the TAKE application.
 * 
 * @author Gisler Christophe
 *
 */
public class TakeController {
	
	public static final DateFormat DATE_FORMAT_FILE = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	public static final DateFormat DATE_FORMAT_LOG = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	public static final String SEP = ",";

	private static final String DATA_DIR_PATH = Preferences.getInstance().getDataDirPath();
	private static final String ROUTING_TABLE_EVOLUTION_FILE_PATH = DATA_DIR_PATH + "routing_table_evolution.csv";
	private static final String GEOLOCATIONS_FOLDER_PATH = DATA_DIR_PATH + "geolocations";
	// private static final String MILITARY_UNITS_FILE_PATH = DATA_DIR_PATH +
	// "military_units.ser";
	private static final String TCP = "TCP", UDP = "UDP";
	// private static final long SAVE_APP_DATA_PERIOD = 30; // In seconds [s]
	private static final int DATAGRAM_MAX_SIZE = Preferences.getInstance().getPacketMaxSize(); // In bytes [B]
	
	// IPv4 is required to embed specific QoS tags (diffserv/TOS) within USER, BFT and RFT messages
	private static final boolean SET_MSG_QOS_TAGS = false; 
	private static final int USER_MSG_IP_TOS = 0x10; 	// = IPTOS_LOWDELAY (see RFC 1349)
	private static final int BFT_RFT_MSG_IP_TOS = 0x04;	// = IPTOS_RELIABILITY (see RFC 1349)

	private static final Logger LOGGER = LoggerFactory.getLogger(TakeController.class);

	private final ThreadFactory threadFactory = Executors.defaultThreadFactory();
	private final ExecutorService executorService = Executors.newCachedThreadPool(threadFactory);
	private final ScheduledExecutorService scheduledExecutorService = Executors
			.newSingleThreadScheduledExecutor(threadFactory);
	private ScheduledFuture<?> bftMessageSendingSchedFuture, rftMessageSendingSchedFuture;
	private String transportProtocol;
	private int portNumber;
	/*
	 * Note that a node is an ally military unit with an IP address (in the TAKE
	 * system network)
	 */
	private Node currentNode;
	private Map<String, Node> nodes = Collections.synchronizedMap(new LinkedHashMap<String, Node>()); // Key = node
																										// name, value =
																										// node (i.e.
																										// ally military
																										// unit)
	private Map<String, MilitaryUnit> enemyUnits = Collections
			.synchronizedMap(new LinkedHashMap<String, MilitaryUnit>()); // Key = enemy military unit name, value =
																			// enemy military unit
	private RadioGPSInterface radioGPSInterface;
	private RoutingTable routingTable = null;
	private SendingStrategy sendingStrategy; // Of the TAKE application
	private boolean simulatedEnemiesCreated = false;

	private static volatile TakeController instance = new TakeController();

	private TakeController() {
		// IPv4 is required to embed QoS tags (diffserv/TOS) within USER, BFT and RFT messages
		if (SET_MSG_QOS_TAGS)
			System.setProperty("java.net.preferIPv4Stack" , "true");
		
		// Load settings properties from configuration file
		Preferences pm = Preferences.getInstance();

		LOGGER.info("TAKE Application Version: {}", pm.getAppVersion());
		LOGGER.info("TAKE Application Config:\n{}", pm.getFormattedAppConfig());

		this.transportProtocol = pm.getTransportProtocol();
		this.portNumber = pm.getMessagingPort();

		// Create data folder if not exist
		FileUtil.createFolderIfNotExist(DATA_DIR_PATH);
		FileUtil.createFolderIfNotExist(GEOLOCATIONS_FOLDER_PATH);

		// Create ally nodes
		this.nodes = pm.getNodes();

		// Restore message manager data if any
		// MessageManager.getInstance().restoreFromFile();

		// Restore data about ally nodes and enemy units from file
		// restoreMilitaryUnitsFromFile();

		// Set current (ally) node
		String currentNodeName = pm.getNodeName();
		setCurrentNode(currentNodeName);

		// Set TAKE communication strategy
		long responseTimeout = pm.getResponseTimeout();
		String sendingStrategyName = pm.getSendingStrategy();
		setSendingStrategy(sendingStrategyName, responseTimeout);

		// Set the radio/GPS interface
		long initialDelay = 3; // [s] to wait for the network interface to start
		radioGPSInterface = pm.isRadioGPSInterfaceSimulated() ? new RadioGPSInterfaceSimulated()
				: new RadioGPSInterfaceReal();
		if (!pm.isRadioGPSInterfaceSimulated()) {
			initialDelay = 30;

			ExecutorService singleExecutorService0 = Executors.newSingleThreadExecutor(threadFactory);
			singleExecutorService0.execute(new Runnable() {
				@Override
				public void run() {
					try {
						((RadioGPSInterfaceReal) radioGPSInterface).initGPSd();
					} catch (Exception e) {
						LOGGER.error("Init army GPSd interface - Exception", e);
					}
				}
			});

			ExecutorService singleExecutorService1 = Executors.newSingleThreadExecutor(threadFactory);
			singleExecutorService1.execute(new Runnable() {
				@Override
				public void run() {
					try {
						((RadioGPSInterfaceReal) radioGPSInterface).initPrecisionTimer();
					} catch (Exception e) {
						LOGGER.error("Init army precision timer interface - Exception", e);
					}
				}
			});

			ExecutorService singleExecutorService2 = Executors.newSingleThreadExecutor(threadFactory);
			singleExecutorService2.execute(new Runnable() {
				@Override
				public void run() {
					try {
						((RadioGPSInterfaceReal) radioGPSInterface).initRadioInterface();
					} catch (Exception e) {
						LOGGER.error("Init army radio interface - Exception", e);
					}
				}
			});
		}

		// Start receiving incoming messages, i.e. listening on port and waiting for
		// potential incoming messages or acknowledgments
		ExecutorService singleExecutorService = Executors.newSingleThreadExecutor(threadFactory);
		singleExecutorService.execute(new Runnable() {
			@Override
			public void run() {
				switch (transportProtocol) {
				case TCP:
					receiveTCP();
					break;
				case UDP:
					receiveUDP();
					break;
				}
				LOGGER.info("Message reception started on port {} ({})", portNumber, transportProtocol);
			}
		});

		// Update and save some data objects periodically (e.g. every 30 seconds)
		/*
		 * scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
		 * 
		 * @Override public void run() { //MessageManager.getInstance().saveToFile();
		 * saveMilitaryUnitsToFile(); saveTransmissionStatisticsToDisk(); } },
		 * SAVE_APP_DATA_PERIOD, SAVE_APP_DATA_PERIOD, SECONDS);
		 */
		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				updateRoutingTable(false);
			}
		}, initialDelay, pm.getRoutingTableUpdatePeriod(), SECONDS);
		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				updateCurrentNodeGeolocation(false);
			}
		}, initialDelay, pm.getNodeLocationUpdatePeriod(), SECONDS);

		// Set the data message sending period in [s] of the BFT and RFT service.
		setBFTMessageSendingPeriod(pm.getBFTMessageSendingPeriod(), initialDelay);
		setRFTMessageSendingPeriod(pm.getRFTMessageSendingPeriod(), initialDelay);
	}

	/**
	 * Set the data message sending period in [s] of the BFT service.
	 * 
	 * @param sendingPeriod
	 */
	public void setBFTMessageSendingPeriod(int sendingPeriod, long initialDelay) {
		if (bftMessageSendingSchedFuture != null)
			bftMessageSendingSchedFuture.cancel(true);
		if (sendingPeriod > 0) {
			this.bftMessageSendingSchedFuture = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					sendBFTorRFTDataMessages(true);
				}
			}, initialDelay, sendingPeriod, SECONDS);
		}
	}

	/**
	 * Set the data message sending period in [s] of the RFT service.
	 * 
	 * @param sendingPeriod
	 */
	public void setRFTMessageSendingPeriod(int sendingPeriod, long initialDelay) {
		if (rftMessageSendingSchedFuture != null)
			rftMessageSendingSchedFuture.cancel(true);
		if (sendingPeriod > 0) {
			this.rftMessageSendingSchedFuture = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					sendBFTorRFTDataMessages(false);
				}
			}, initialDelay, sendingPeriod, SECONDS);
		}
	}

	public void setSendingStrategy(String sendingStrategyName, long responseTimeout) {
		switch (sendingStrategyName) {
		case SendingStrategyGetCloser.NAME:
			boolean replaceTargetedNodesWithSameNextNodeByNextNode = false;
			boolean takeNextNodesOnly = false;
			this.sendingStrategy = new SendingStrategyGetCloser(responseTimeout,
					replaceTargetedNodesWithSameNextNodeByNextNode, takeNextNodesOnly);
			break;
		case SendingStrategyGetCloser.NAME2:
			replaceTargetedNodesWithSameNextNodeByNextNode = false;
			takeNextNodesOnly = true;
			this.sendingStrategy = new SendingStrategyGetCloser(responseTimeout,
					replaceTargetedNodesWithSameNextNodeByNextNode, takeNextNodesOnly);
			break;
		case SendingStrategyGetCloser.NAME3:
			replaceTargetedNodesWithSameNextNodeByNextNode = true;
			takeNextNodesOnly = false;
			this.sendingStrategy = new SendingStrategyGetCloser(responseTimeout,
					replaceTargetedNodesWithSameNextNodeByNextNode, takeNextNodesOnly);
			break;
		case SendingStrategyGetCloser.NAME4:
			replaceTargetedNodesWithSameNextNodeByNextNode = true;
			takeNextNodesOnly = true;
			this.sendingStrategy = new SendingStrategyGetCloser(responseTimeout,
					replaceTargetedNodesWithSameNextNodeByNextNode, takeNextNodesOnly);
			break;
		case SendingStrategyDirect.NAME:
		default:
			this.sendingStrategy = new SendingStrategyDirect(responseTimeout);
			break;
		}
	}

	public static TakeController getInstance() {
		return instance;
	}

	public String getTransportProtocol() {
		return transportProtocol;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public Node getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(String currentNodeName) {
		this.currentNode = nodes.get(currentNodeName);
	}

	/**
	 * 
	 * Note that a node is an ally military unit with an IP address (in the TAKE
	 * system network).
	 * 
	 * @return all nodes (i.e. ally military units, including this current node)
	 */
	public Map<String, Node> getNodes() {
		return nodes;
	}

	/**
	 * Return list of given nodes.
	 * 
	 * @param nodeNames
	 *            (if null, return list of all nodes)
	 * @return list of given related nodes
	 */
	public List<Node> getNodes(String... nodeNames) {
		if (nodeNames == null)
			return new ArrayList<Node>(nodes.values());
		return Arrays.stream(nodeNames).filter(nodes::containsKey).map(nodes::get).collect(Collectors.toList());
	}
	/*
	 * public Map<String, Node> getNodes(String... nodeNames) { return
	 * Arrays.stream(nodeNames).filter(nodes::containsKey).collect(Collectors.toMap(
	 * Function.identity(), nodes::get)); }
	 */

	public String getNodeIP(String nodeName) {
		if (nodeName == null)
			return null;
		return nodes.get(nodeName).getNodeIP();
	}

	public String getNodeName(String nodeIP) {
		for (String nodeName : nodes.keySet())
			if (nodes.get(nodeName).getNodeIP().equals(nodeIP))
				return nodeName;
		return null;
	}

	public Map<String, MilitaryUnit> getEnemyUnits() {
		return enemyUnits;
	}

	public RoutingTable getRoutingTable() {
		return routingTable;
	}

	private void updateRoutingTable(boolean forceWritingToDisk) {
		try {
			if (radioGPSInterface instanceof RadioGPSInterfaceReal
					&& !((RadioGPSInterfaceReal) radioGPSInterface).isRadioInterfaceReady()) {
				LOGGER.warn("Update routing table - Army radio interface not yet ready!");
				return;
			}

			// Get new updated routing table
			RoutingTable newRoutingTable = radioGPSInterface.getCurrentOLSRRoutingTable();

			if (newRoutingTable == null) {
				LOGGER.warn("Update routing table - New routing table is null!");
				return;
			}
			if (newRoutingTable.isEmpty()) {
				LOGGER.warn("Update routing table - New routing table is empty!");
				// return;
			}

			// Save routing table to file if it first time or if it has changed
			if (routingTable == null || forceWritingToDisk || !newRoutingTable.hasSameEntriesAs(this.routingTable)) {
				String currentTimestamp = "" + getCurrentTimeMillis();
				String currentNodeIP = currentNode.getNodeIP();
				Files.write(Paths.get(ROUTING_TABLE_EVOLUTION_FILE_PATH),
						newRoutingTable.getEntries().values().stream()
								.map(e -> currentTimestamp + SEP + currentNodeIP + SEP + e.getDestNodeIP() + SEP
										+ e.getNextNodeIP())
								.collect(Collectors.toList()),
						StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				LOGGER.debug("Updated routing table saved to file");
			}

			// Update routing table
			this.routingTable = newRoutingTable;
		} catch (Exception e) {
			LOGGER.error("Update routing table - Exception", e);
		}
	}

	private void updateCurrentNodeGeolocation(boolean forceWritingToDisk) {
		try {
			if (radioGPSInterface instanceof RadioGPSInterfaceReal
					&& !((RadioGPSInterfaceReal) radioGPSInterface).isGPSdReady()) {
				LOGGER.warn("Update routing table - Army GPSd interface not yet ready!");
				return;
			}

			// Get new updated node GPS geolocation
			Geolocation newGeolocation = radioGPSInterface.getCurrentGeolocation();

			if (newGeolocation == null) {
				LOGGER.warn("Update current node geolocation - New geolocation is null!");
				return;
			}

			// Save node GPS geolocation to file if it has changed
			boolean forceBFTRFTMsgSending = Preferences.getInstance().isForceBFTRFTMessageSending();
			if (!currentNode.hasLastGeolocation() || forceWritingToDisk || forceBFTRFTMsgSending
					|| !newGeolocation.hasSameLocationAs(currentNode.getLastGeolocation())) {
				Files.write(Paths.get(GEOLOCATIONS_FOLDER_PATH, currentNode.getName() + ".csv"),
						new String(newGeolocation.getLatitude() + SEP + newGeolocation.getLongitude() + SEP
								+ DATE_FORMAT_FILE.format(newGeolocation.getDate()) + "\n").getBytes(),
						StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				LOGGER.debug("Updated current node geolocation saved to file");
			}

			// Update current node GPS geolocation
			currentNode.updateGeolocation(newGeolocation);
		} catch (Exception e) {
			LOGGER.error("Update current node geolocation - Exception", e);
		}
	}

	private synchronized void resetAlliesAndEnemiesData() {
		enemyUnits.clear();
		for (Node node : nodes.values()) {
			node.resetGeolocationsHistory();
			// FileUtil.deleteFile(new File(MILITARY_UNITS_FILE_PATH, node.getName()));
		}
	}

	/*
	 * private void saveMilitaryUnitsToFile() { try (FileOutputStream fos = new
	 * FileOutputStream(MILITARY_UNITS_FILE_PATH); BufferedOutputStream bos = new
	 * BufferedOutputStream(fos); ObjectOutputStream out = new
	 * ObjectOutputStream(bos);) { Map<String, MilitaryUnit> allMilitaryUnits =
	 * Collections.synchronizedMap(new LinkedHashMap<String, MilitaryUnit>());
	 * allMilitaryUnits.putAll(nodes); allMilitaryUnits.putAll(enemyUnits);
	 * out.writeObject(allMilitaryUnits); } catch (FileNotFoundException e) {
	 * LOGGER.error("Save military units to file - File Not Found Exception", e); }
	 * catch (IOException e) {
	 * LOGGER.error("Save military units to file - I/O Exception", e); } }
	 */

	/*
	 * private synchronized void restoreMilitaryUnitsFromFile() { if (!new
	 * File(MILITARY_UNITS_FILE_PATH).exists()) return; try (FileInputStream fis =
	 * new FileInputStream(MILITARY_UNITS_FILE_PATH); BufferedInputStream bis = new
	 * BufferedInputStream(fis); ObjectInputStream in = new ObjectInputStream
	 * (bis);) {
	 * 
	 * @SuppressWarnings("unchecked") Map<String, MilitaryUnit> allMilitaryUnits =
	 * (Map<String, MilitaryUnit>) in.readObject(); for (MilitaryUnit mu:
	 * allMilitaryUnits.values()) { if (mu.getStatus() == MilitaryUnit.Status.ALLY)
	 * { if (nodes.containsKey(mu.getName())) { Node node = nodes.get(mu.getName());
	 * node.setGeolocationsHistory(mu.getGeolocationsHistory()); } } else
	 * enemyUnits.put(mu.getName(), mu); } } catch (FileNotFoundException e) {
	 * LOGGER.error("Restore military units from file - File Not Found Exception",
	 * e); } catch (IOException e) {
	 * LOGGER.error("Restore military units from file - I/O Exception", e); } catch
	 * (ClassNotFoundException e) {
	 * LOGGER.error("Restore military units from file - Class Not Found Exception",
	 * e); } }
	 */

	/*
	 * private void resetLoggers() { // reset the default context (which may already
	 * have been initialized) // since we want to reconfigure it LoggerContext lc =
	 * (LoggerContext)LoggerFactory.getILoggerFactory(); lc.reset();
	 * 
	 * JoranConfigurator config = new JoranConfigurator(); config.setContext(lc);
	 * 
	 * try {
	 * config.doConfigure(getClass().getClassLoader().getResource("logback.xml")); }
	 * catch (JoranException e) { e.printStackTrace(); } }
	 */

	public synchronized void quit(int delay) {
		ScheduledExecutorService singleExecutorService0 = Executors.newScheduledThreadPool(1, threadFactory);
		singleExecutorService0.schedule(new Runnable() {
			@Override
			public void run() {
				try {
					System.exit(0);
				} catch (Exception e) {
					LOGGER.error("Quit Take application - Exception", e);
				}
			}
		}, delay, TimeUnit.SECONDS);
	}

	public void saveTransmissionStatisticsToDisk() {
		try {
			TransmissionStatistics.getInstance().saveToDisk();
		} catch (IOException e) {
			LOGGER.error("Save transmission statistics to disk - IOException", e);
		}
	}

	/*
	 * private void saveMilitaryUnitsToFile() { try (FileOutputStream fos = new
	 * FileOutputStream(MILITARY_UNITS_FILE_PATH); BufferedOutputStream bos = new
	 * BufferedOutputStream(fos); ObjectOutputStream out = new
	 * ObjectOutputStream(bos);) { Map<String, MilitaryUnit> allMilitaryUnits =
	 * Collections.synchronizedMap(new LinkedHashMap<String, MilitaryUnit>());
	 * allMilitaryUnits.putAll(nodes); allMilitaryUnits.putAll(enemyUnits);
	 * out.writeObject(allMilitaryUnits); } catch (FileNotFoundException e) {
	 * LOGGER.error("Save military units to file - File Not Found Exception", e); }
	 * catch (IOException e) {
	 * LOGGER.error("Save military units to file - I/O Exception", e); } }
	 */
	
	/*
	 * private synchronized void restoreMilitaryUnitsFromFile() { if (!new
	 * File(MILITARY_UNITS_FILE_PATH).exists()) return; try (FileInputStream fis =
	 * new FileInputStream(MILITARY_UNITS_FILE_PATH); BufferedInputStream bis = new
	 * BufferedInputStream(fis); ObjectInputStream in = new ObjectInputStream
	 * (bis);) {
	 * 
	 * @SuppressWarnings("unchecked") Map<String, MilitaryUnit> allMilitaryUnits =
	 * (Map<String, MilitaryUnit>) in.readObject(); for (MilitaryUnit mu:
	 * allMilitaryUnits.values()) { if (mu.getStatus() == MilitaryUnit.Status.ALLY)
	 * { if (nodes.containsKey(mu.getName())) { Node node = nodes.get(mu.getName());
	 * node.setGeolocationsHistory(mu.getGeolocationsHistory()); } } else
	 * enemyUnits.put(mu.getName(), mu); } } catch (FileNotFoundException e) {
	 * LOGGER.error("Restore military units from file - File Not Found Exception",
	 * e); } catch (IOException e) {
	 * LOGGER.error("Restore military units from file - I/O Exception", e); } catch
	 * (ClassNotFoundException e) {
	 * LOGGER.error("Restore military units from file - Class Not Found Exception",
	 * e); } }
	 */
	
	/*
	 * private void resetLoggers() { // reset the default context (which may already
	 * have been initialized) // since we want to reconfigure it LoggerContext lc =
	 * (LoggerContext)LoggerFactory.getILoggerFactory(); lc.reset();
	 * 
	 * JoranConfigurator config = new JoranConfigurator(); config.setContext(lc);
	 * 
	 * try {
	 * config.doConfigure(getClass().getClassLoader().getResource("logback.xml")); }
	 * catch (JoranException e) { e.printStackTrace(); } }
	 */
	
	public synchronized void reset() {
		resetAlliesAndEnemiesData();
		TransmissionStatistics.getInstance().reset();
		MessageManager.getInstance().reset();
		FileUtil.deleteAllFilesInFolder(DATA_DIR_PATH);
		FileUtil.createFolderIfNotExist(GEOLOCATIONS_FOLDER_PATH);
		updateRoutingTable(true);
		updateCurrentNodeGeolocation(true);
		// resetLoggers();
		// FileUtil.deleteAllFilesInFolder("logs");
		// FileUtil.createFileInFolder("take.log", "logs");
	}

	/**
	 * Get node status of the given nodes. The status of node consists of a boolean
	 * value which is set to true if the node is present in the routing table and
	 * false if it is not.
	 * 
	 * @param nodeNames
	 * @return nodeStatus
	 */
	public boolean[] getNodeStatus(String[] nodeNames) {
		boolean[] nodeStatus = new boolean[nodeNames.length];
		if (routingTable != null)
			for (int i = 0; i < nodeNames.length; i++)
				nodeStatus[i] = routingTable.containsEntry(getNodeIP(nodeNames[i]));
		return nodeStatus;
	}

	public Set<String> getDestinationNodesWithSameNextNodeInRoutingTableAs(String nodeName) {
		Set<String> destNodeNames = new LinkedHashSet<String>();
		if (routingTable != null) {
			Set<String> destNodeIPs = routingTable.getDestNodeIPsWithSameNextNodeIPAs(getNodeIP(nodeName));
			for (String destNodeIP : destNodeIPs)
				destNodeNames.add(getNodeName(destNodeIP));
		}
		return destNodeNames;
	}

	public String getNextNodeInRoutingTableOf(String destNodeName) {
		if (routingTable == null)
			return null;
		String nextNodeIP = routingTable.getEntry(getNodeIP(destNodeName)).getNextNodeIP();
		return getNodeName(nextNodeIP);
	}

	public Set<String> getNeighborNodes() {
		Set<String> neighborNodeNames = new LinkedHashSet<String>();
		if (routingTable != null) {
			Set<String> neighborNodeIPs = routingTable.getNeighborNodeIPs(currentNode.getNodeIP());
			for (String nodeIP : neighborNodeIPs)
				neighborNodeNames.add(getNodeName(nodeIP));
		}
		return neighborNodeNames;
	}

	/**
	 * Try to send the given message to all its recipients. This method is called by
	 * the TAKE application services dedicated to users.
	 * 
	 * @param message
	 */
	public void send(Message message) {
		executorService.submit(new Runnable() {
			@Override
			public void run() {
				try {
					MessageManager mm = MessageManager.getInstance();
					TransmissionStatistics transmissionStats = TransmissionStatistics.getInstance();

					// Add the message recipient number to the sum of all recipients of all user
					// messages created by this sender
					transmissionStats.addUserMessageRecipients(message.getRecipientNames().size());

					// Add the new message to the message manager
					message.getTrace().addNodeHavingReceivedMsgFromNodeOnDate(currentNode.getName(),
							currentNode.getName(), message.getDate());
					mm.addMessage(message);

					// Send the message using the chosen TAKE strategy
					sendingStrategy.sendMessage(message);

					// If message is a user message, update its status from SENT to RECEIVED or
					// FAILED
					if (message instanceof UserMessage)
						((UserMessage) message).setSentStatusToReceivedOrFailedForRecipients();

					// Compute the message completion rate and add it to transmission statistics
					int[] msgCompletionInfos = mm.getMessageCompletionInfos(message.getID());
					int successfulRecipientNbr = msgCompletionInfos[0];
					int recipientNbr = msgCompletionInfos[1];
					transmissionStats.addUserMessageCompletionData(message.getID(), successfulRecipientNbr,
							recipientNbr);

					LOGGER.info("{} message '{}' sent on {} received by {} over {} recipients", getMsgType(message),
							message.getID(), DATE_FORMAT_LOG.format(message.getDate()), successfulRecipientNbr,
							recipientNbr);
				} catch (Exception e) {
					LOGGER.error("Send message - Exception", e);
				}
			}
		});
	}

	/*
	 * If BFT/RFT message sending is forced, each node will simulate its own
	 * movement.
	 */
	/*
	 * private void simulateCurrentNodeMovement() { if
	 * (PropertiesManager.getInstance().isBFTRFTMessageSendingForced()) {
	 * getCurrentNode().simulateMovement(); } }
	 */

	/*
	 * If specified in the properties, each node will simulate N enemy movement.
	 */
	private void simulateRFTEnemyMovement() {
		int simulatedEnemies = Preferences.getInstance().getRFTSimulatedEnemies();
		if (simulatedEnemies <= 0)
			return;
		if (!simulatedEnemiesCreated) {
			for (int i = 0; i < simulatedEnemies; i++) {
				String newEnemyName = "Enemy" + (i + 1) + MilitaryUnit.DETECTED_BY_NAME_SEP
						+ getCurrentNode().getName();
				MilitaryUnit enemyMU = new MilitaryUnit(MilitaryUnit.Status.ENNEMY, newEnemyName, "TypeX");
				enemyMU.updateGeolocation(Geolocation.newRandomGeolocation());
				enemyUnits.put(enemyMU.getName(), enemyMU);
			}
			simulatedEnemiesCreated = true;
		}
		for (MilitaryUnit mu : enemyUnits.values()) {
			if (mu.getName().endsWith(MilitaryUnit.DETECTED_BY_NAME_SEP + getCurrentNode().getName()))
				mu.simulateMovement();
		}
	}

	/*
	 * Each (ally) node automatically sends the required data (GPS dated locations,
	 * types,...) about itself and other allies to all (ally) neighbor nodes.
	 */
	private void sendBFTorRFTDataMessages(boolean bft) {
		boolean forceBFTRFT = Preferences.getInstance().isForceBFTRFTMessageSending();
		if (!bft)
			simulateRFTEnemyMovement();

		Collection<? extends MilitaryUnit> militaryUnits = bft ? nodes.values() : enemyUnits.values();
		// Create BFT or RFT data messages containing only the information
		// (geolocations,...)
		// which was not already received from or sent to each neighbor node
		List<Message> messages = new LinkedList<Message>();

		Set<String> neighborNodeNames = getNeighborNodes();
		// LOGGER.debug("SEND {} DATA MSG - Neighbor nodes ({}): {}", bft ? "BFT" :
		// "RFT", neighborNodeNames.size(), String.join(", ", neighborNodeNames));
		for (String neighborNodeName : neighborNodeNames) {
			List<String> arguments = new LinkedList<String>();
			for (MilitaryUnit mu : militaryUnits) {
				if (mu.hasLastGeolocation()
						&& (forceBFTRFT || !mu.getRelatedNodesHavingCurrentGeolocation().contains(neighborNodeName))) {
					Geolocation currentGeoloc = mu.getLastGeolocation();
					arguments.add(mu.getStatus() + SEP + mu.getName() + SEP + mu.getType() + SEP
							+ currentGeoloc.getLongitude() + SEP + currentGeoloc.getLatitude() + SEP
							+ currentGeoloc.getDate().getTime());
				}
			}
			// LOGGER.debug("SEND {} DATA MSG - Neighbor Node: {}, arguments ({}): {}", bft
			// ? "BFT" : "RFT", neighborNodeName, arguments.size(), String.join(" | ",
			// arguments));
			// Create message only if there are new data to send to neighbor node
			if (!arguments.isEmpty()) {
				Message bftOrRftMessage = new DataMessage(Type.BFT_RFT_DATA, arguments, currentNode.getName(),
						neighborNodeName);
				messages.add(bftOrRftMessage);
				// MessageManager.getInstance().addMessage(bftOrRftMessage);
			}
		}

		// Send messages to neighbor nodes if necessary
		if (messages.isEmpty())
			return;
		LOGGER.info("{} {} data message(s) sent on {} to neighbor nodes", messages.size(), bft ? "BFT" : "RFT",
				DATE_FORMAT_LOG.format(getCurrentDate()));
		Map<String, MessageAcknowledgment> msgACKsFromNodes = new LinkedHashMap<String, MessageAcknowledgment>();
		long timeoutForResponseInSecs = Preferences.getInstance().getResponseTimeout();
		float delayBetweenMessageSendingInSecs = Preferences.getInstance().getMultipleMessageSendingDelay();
		long messageSendingDelayInMillisecs = 0l,
				deltaDelayInMillisecs = (long) (1000l * delayBetweenMessageSendingInSecs);

		// ExecutorService executorService2 =
		// Executors.newCachedThreadPool(threadFactory);
		ScheduledExecutorService executorService2 = Executors.newScheduledThreadPool(messages.size(), threadFactory);
		for (Message message : messages) {
			for (String targetedNodeName : message.getRecipientNames()) {
				messageSendingDelayInMillisecs += deltaDelayInMillisecs;
				// Send message via an executor service on a new thread
				executorService2.schedule(new Runnable() {
					@Override
					public void run() {
						try {
							// Send message to node directly and get returned message acknowledgement
							MessageAcknowledgment messageACK = sendMessageToNodeDirectly(message, targetedNodeName,
									timeoutForResponseInSecs);

							// If message acknowledgement received from recipient
							if (messageACK != null)
								msgACKsFromNodes.put(targetedNodeName, messageACK);
						} catch (Exception e) {
							LOGGER.error("Send message directly - Exception", e);
						}
					}
				}, messageSendingDelayInMillisecs, TimeUnit.MILLISECONDS);
			}
		}

		// Wait for the eventual recipient responses
		try {
			executorService2.shutdown();
			executorService2.awaitTermination(timeoutForResponseInSecs + (1000l * messageSendingDelayInMillisecs),
					TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executorService2.shutdownNow();
		}

		// Add message completion data
		/*
		 * for (Message message: messages) { // N.B.: There is only 1 recipient neighbor
		 * node per message if
		 * (messageACKs.containsKey(message.getRecipientNames().iterator().next()))
		 * TransmissionStatistics.getInstance().addMessageCompletionData(message.getID()
		 * , 1, 1); else
		 * TransmissionStatistics.getInstance().addMessageCompletionData(message.getID()
		 * , 0, 1); }
		 */

		// If BFT/RFT message sending is forced, do not perform the following last part
		if (forceBFTRFT)
			return;

		// Add each neighbor node having received an ACK for the sent data message to
		// the list of related nodes
		// having received the last geolocations of all military units (allies for BFT,
		// enemies for RFT)
		for (String neighborNodeName : msgACKsFromNodes.keySet()) {
			for (MilitaryUnit mu : militaryUnits) {
				mu.getRelatedNodesHavingCurrentGeolocation().add(neighborNodeName);
			}
		}
	}

	/**
	 * Send a message to the IPs of the given targeted nodes directly (via standard
	 * IP routing, e.g. OLSR) and wait for related acknowledgments during the given
	 * time for response (in seconds). This method is called by the classes
	 * implementing the TAKE strategy interface.
	 * 
	 * @param message
	 * @param targetedNodeNames
	 * @param timeoutForResponseInSecs
	 * @return map of acknowledgments received from targeted nodes after the given
	 *         timeout for response
	 */
	public Map<String, MessageAcknowledgment> sendMessageDirectly(Message message, Set<String> targetedNodeNames,
			long timeoutForResponseInSecs) {
		Map<String, MessageAcknowledgment> msgACKsFromNodes = new LinkedHashMap<String, MessageAcknowledgment>(
				targetedNodeNames.size());
		float delayBetweenMessageSendingInSecs = Preferences.getInstance().getMultipleMessageSendingDelay();
		long messageSendingDelayInMillisecs = 0l,
				deltaDelayInMillisecs = (long) (1000l * delayBetweenMessageSendingInSecs);

		// ExecutorService executorService2 =
		// Executors.newCachedThreadPool(threadFactory);
		ScheduledExecutorService executorService2 = Executors.newScheduledThreadPool(targetedNodeNames.size(),
				threadFactory);
		for (String targetedNodeName : targetedNodeNames) {
			messageSendingDelayInMillisecs += deltaDelayInMillisecs;
			// Send message via an executor service on a new thread
			executorService2.schedule(new Runnable() {
				@Override
				public void run() {
					try {
						// Send message to node directly and get returned message acknowledgement
						MessageAcknowledgment messageACK = sendMessageToNodeDirectly(message, targetedNodeName,
								timeoutForResponseInSecs);

						// If message acknowledgement received from recipient
						if (messageACK != null)
							msgACKsFromNodes.put(targetedNodeName, messageACK);
					} catch (Exception e) {
						LOGGER.error("Send message directly - Exception", e);
					}
				}
			}, messageSendingDelayInMillisecs, TimeUnit.MILLISECONDS);
		}

		// Wait for the eventual recipient responses
		try {
			executorService2.shutdown();
			executorService2.awaitTermination(timeoutForResponseInSecs, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executorService2.shutdownNow();
		}

		return msgACKsFromNodes;
	}

	private MessageAcknowledgment sendMessageToNodeDirectly(Message message, String targetedNodeName,
			long timeoutForResponseInSecs) {
		// Send message to node directly and get returned message acknowledgement
		String destNodeIP = getNodeIP(targetedNodeName);
		Date msgDepartureDate = getCurrentDate();
		String msgType = getMsgType(message);
		LOGGER.debug("{} message '{}' sent via {} (port {}) to {} ({}) on {}", msgType, message.getID(),
				transportProtocol, portNumber, targetedNodeName, destNodeIP,
				DATE_FORMAT_LOG.format(msgDepartureDate)/* , message */);

		// Update message departure date
		message.setDepartureDate(msgDepartureDate);

		// Compute the size in [byte] of the un-/compressed message to send
		// int msgSize = sizeOf(message);
		int msgCompressedSize = sizeOfCompressed(message);

		// Effective sending of message
		MessageAcknowledgment messageACK = null;
		switch (transportProtocol) {
		case TCP:
			messageACK = sendMessageTCP(message, destNodeIP, (int) timeoutForResponseInSecs);
			break;
		case UDP:
			messageACK = sendMessageUDP(message, destNodeIP, (int) timeoutForResponseInSecs);
			break;
		}

		if (messageACK == null) {
			LOGGER.warn("No ACK received for sent {} message '{}'", msgType, message.getID());

			// Add transmission data for statistics
			TransmissionStatistics.getInstance().addSentIPMessageData_NoACK(message, currentNode.getName(),
					targetedNodeName, msgCompressedSize, msgDepartureDate);

			return null;
		}
		Date ackArrivalDate = getCurrentDate();
		LOGGER.debug("ACK for sent {} message '{}' received from {} ({}) on {}", msgType, message.getID(),
				targetedNodeName, destNodeIP, DATE_FORMAT_LOG.format(ackArrivalDate)/* , messageACK */);

		// Update message trace
		message.getTrace().addNodeHavingReceivedMsgFromNodeOnDate(targetedNodeName, currentNode.getName(),
				messageACK.getMessageArrivalDate());

		// Handle the received message ACK
		handleReceivedMessageAcknowledgment(messageACK, targetedNodeName);

		// Compute the size in [byte] of the un-/compressed message ACK to send back
		// int ackSize = sizeOf(messageACK);
		int ackCompressedSize = sizeOfCompressed(messageACK);

		// Add transmission data for statistics
		TransmissionStatistics.getInstance().addSentIPMessageData_ACK(message, currentNode.getName(), targetedNodeName,
				msgCompressedSize, msgDepartureDate, messageACK.getMessageArrivalDate(), ackCompressedSize,
				messageACK.getDepartureDate(), ackArrivalDate);

		return messageACK;
	}

	private MessageAcknowledgment sendMessageTCP(Message message, String host, int timeoutForResponseInSecs) {
		try (Socket socket = new Socket(host, portNumber);
				GZIPOutputStream gzos = new GZIPOutputStream(socket.getOutputStream(), DATAGRAM_MAX_SIZE);
				ObjectOutputStream out = new ObjectOutputStream(gzos);) {
			if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT messages 
				socket.setTrafficClass(message instanceof UserMessage ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);
			
			// Send message
			out.writeObject(message);
			gzos.finish();
			out.flush();

			// Set timeout in [ms] for response (if > 0)
			if (timeoutForResponseInSecs > 0)
				socket.setSoTimeout(timeoutForResponseInSecs * 1000);

			// Get message acknowledgment
			GZIPInputStream gzis = new GZIPInputStream(socket.getInputStream(), DATAGRAM_MAX_SIZE);
			ObjectInputStream in = new ObjectInputStream(gzis);
			MessageAcknowledgment messageACK = (MessageAcknowledgment) in.readObject();
			in.close();
			gzis.close();
			return messageACK;

		} catch (SocketTimeoutException e) {
			LOGGER.warn("TCP socket timeout {} seconds after sending message '{}'", timeoutForResponseInSecs,
					message.getID());
		} catch (UnknownHostException e) {
			LOGGER.error("Send message via TCP - Unknown Host Exception", e);
		} catch (IOException e) {
			LOGGER.error("Send message via TCP - I/O Exception", e);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Send message via TCP - Class Not Found Exception", e);
		}
		return null;
	}

	private MessageAcknowledgment sendMessageUDP(Message message, String host, int timeoutForResponseInSecs) {
		try (DatagramSocket socket = new DatagramSocket(/* portNumber, InetAddress.getByName(hostName) */);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				GZIPOutputStream gzos = new GZIPOutputStream(baos, DATAGRAM_MAX_SIZE);
				ObjectOutputStream out = new ObjectOutputStream(gzos);) {
			if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT messages 
				socket.setTrafficClass(message instanceof UserMessage ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);
			
			// Send message
			out.writeObject(message);
			gzos.finish();
			out.flush();
			byte[] buffer = baos.toByteArray();
			DatagramPacket outPacket = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(host), portNumber);
			socket.send(outPacket);

			// Set timeout in [ms] for response (if > 0)
			if (timeoutForResponseInSecs > 0)
				socket.setSoTimeout(timeoutForResponseInSecs * 1000);

			// Get messageACK
			byte[] inBuffer = new byte[DATAGRAM_MAX_SIZE];
			DatagramPacket inPacket = new DatagramPacket(inBuffer, inBuffer.length);
			socket.receive(inPacket);
			ByteArrayInputStream bais = new ByteArrayInputStream(inBuffer);
			GZIPInputStream gzis = new GZIPInputStream(bais, DATAGRAM_MAX_SIZE);
			ObjectInputStream in = new ObjectInputStream(gzis);
			MessageAcknowledgment messageACK = (MessageAcknowledgment) in.readObject(); 
						// If no GZIP, possible to use: new String(inPacket.getData(), 0, inPacket.getLength());
			in.close();
			gzis.close();
			bais.close();
			return messageACK;

		} catch (SocketTimeoutException e) {
			LOGGER.warn("UDP socket timeout {} seconds after sending message '{}'", timeoutForResponseInSecs,
					message.getID());
		} catch (UnknownHostException e) {
			LOGGER.error("Send message via UDP - Unknown Host Exception", e);
		} catch (IOException e) {
			LOGGER.error("Send message via UDP - I/O Exception", e);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Send message via UDP - Class of the response object not found", e);
		}
		return null;
	}

	public void sendMessageAcknowledgmentDirectly(MessageAcknowledgment messageACK, Set<String> targetedNodeNames) {
		long timeout = Preferences.getInstance().getResponseTimeout();
		ExecutorService executorService2 = Executors.newCachedThreadPool(threadFactory);
		for (String targetedNodeName : targetedNodeNames) {

			// Send message via an executor service on a new thread
			executorService2.submit(new Runnable() {
				@Override
				public void run() {
					// Send message to node directly and get returned message acknowledgement
					String targetedNodeIP = getNodeIP(targetedNodeName);
					Date msgDepartureDate = getCurrentDate();
					String msgType = getMsgType(MessageManager.getInstance().getMessage(messageACK.getMessageID()));
					LOGGER.debug("ACK for {} message '{}' sent via {} (port {}) to {} ({}) on {}", msgType,
							messageACK.getMessageID(), transportProtocol, portNumber, targetedNodeName, targetedNodeIP,
							DATE_FORMAT_LOG.format(msgDepartureDate)/* , message */);

					// Effective sending of message ACK
					switch (transportProtocol) {
					case TCP:
						sendAcknowledgmentTCP(messageACK, targetedNodeIP);
						break;
					case UDP:
						sendAcknowledgmentUDP(messageACK, targetedNodeIP);
						break;
					}
				}
			});
		}

		// Await termination at most the specified timeout
		try {
			executorService2.shutdown();
			executorService2.awaitTermination(timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executorService2.shutdownNow();
		}
	}

	private void sendAcknowledgmentTCP(MessageAcknowledgment messageACK, String host) {
		try (Socket socket = new Socket(host, portNumber);
				GZIPOutputStream gzos = new GZIPOutputStream(socket.getOutputStream(), DATAGRAM_MAX_SIZE);
				ObjectOutputStream out = new ObjectOutputStream(gzos);) {
			if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT message ACKs 
			socket.setTrafficClass(MessageManager.getInstance().containsMessage(messageACK.getMessageID()) ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);
						
			// Send message ACK
			out.writeObject(messageACK);
			gzos.finish();
			out.flush();

		} catch (UnknownHostException e) {
			LOGGER.error("Send message ACK via TCP - Unknown Host Exception", e);
		} catch (IOException e) {
			LOGGER.error("Send message ACK via TCP - I/O Exception", e);
		}
	}

	private void sendAcknowledgmentUDP(MessageAcknowledgment messageACK, String host) {
		try (DatagramSocket socket = new DatagramSocket(/* portNumber, InetAddress.getByName(hostName) */);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				GZIPOutputStream gzos = new GZIPOutputStream(baos, DATAGRAM_MAX_SIZE);
				ObjectOutputStream out = new ObjectOutputStream(gzos);) {
			if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT message ACKs 
				socket.setTrafficClass(MessageManager.getInstance().containsMessage(messageACK.getMessageID()) ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);
			
			// Send message ACK
			out.writeObject(messageACK);
			gzos.finish();
			out.flush();
			byte[] buffer = baos.toByteArray();
			DatagramPacket outPacket = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(host),
					portNumber);
			socket.send(outPacket);

		} catch (UnknownHostException e) {
			LOGGER.error("Send message ACK via UDP - Unknown Host Exception", e);
		} catch (IOException e) {
			LOGGER.error("Send message ACK via UDP - I/O Exception", e);
		}
	}

	private void receiveTCP() {
		try /* (ServerSocket serverSocket = new ServerSocket(portNumber)) */ {
			@SuppressWarnings("resource")
			ServerSocket serverSocket = new ServerSocket(portNumber);
			while (true) {
				Socket socket = serverSocket.accept();

				LOGGER.debug("TCP socket connection received on port {}", portNumber);

				executorService.submit(new Runnable() {
					public void run() {
						try (GZIPInputStream gzis = new GZIPInputStream(socket.getInputStream(), DATAGRAM_MAX_SIZE);
								ObjectInputStream in = new ObjectInputStream(gzis);
								GZIPOutputStream gzos = new GZIPOutputStream(socket.getOutputStream(), DATAGRAM_MAX_SIZE);
								ObjectOutputStream out = new ObjectOutputStream(gzos);) {

							// Receive object
							Object receivedObject = in.readObject();
							// if (Thread.currentThread().isInterrupted()) return;
							if (receivedObject != null) {
								String fromNodeIP = socket.getInetAddress().getHostAddress();
								if (receivedObject instanceof Message) {
									Message receivedMessage = (Message) receivedObject;

									// Get acknowledgment for received message
									MessageAcknowledgment messageACK = handleReceivedMessage(receivedMessage, fromNodeIP);
									
									if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT message ACKs 
										socket.setTrafficClass(receivedMessage instanceof UserMessage ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);

									// Send message acknowledgment
									out.writeObject(messageACK);
									gzos.finish();
									out.flush();
								} else if (receivedObject instanceof MessageAcknowledgment) {
									MessageAcknowledgment receivedMessageACK = (MessageAcknowledgment) receivedObject;

									// Handle the received message ACK
									handleReceivedMessageAcknowledgment(receivedMessageACK, getNodeName(fromNodeIP));
								}
							} else
								LOGGER.warn("Object received via TCP is null");

						} catch (ClassNotFoundException e) {
							LOGGER.error("Receive message via TCP - Class of the message object not found", e);
						} catch (IOException e) {
							LOGGER.error("Receive message via TCP - I/O Exception", e);
						} catch (Exception e) {
							LOGGER.error("Receive message via TCP - Exception", e);
						}
					}
				});
			}
		} catch (IOException e) {
			LOGGER.error("Receive message via TCP - Could not open the server socket on port " + portNumber, e);
		}
	}

	private void receiveUDP() {
		try /* (DatagramSocket serverSocket = new DatagramSocket(portNumber)) */ { // <- Causes a Socket closed exception
			@SuppressWarnings("resource")
			DatagramSocket serverSocket = new DatagramSocket(portNumber);
			while (true) {
				byte[] inBuffer = new byte[DATAGRAM_MAX_SIZE];
				DatagramPacket inPacket = new DatagramPacket(inBuffer, inBuffer.length);

				// The following method blocks until a datagram is received
				serverSocket.receive(inPacket);

				LOGGER.debug("UDP socket connection received on port {}", portNumber);

				// Create new thread to handle the received datagram
				executorService.submit(new Runnable() {
					@Override
					public void run() {
						try (ByteArrayInputStream bais = new ByteArrayInputStream(inPacket.getData()/* inBuffer */);
								GZIPInputStream gzis = new GZIPInputStream(bais, DATAGRAM_MAX_SIZE);
								ObjectInputStream in = new ObjectInputStream(gzis);
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								GZIPOutputStream gzos = new GZIPOutputStream(baos, DATAGRAM_MAX_SIZE);
								ObjectOutputStream out = new ObjectOutputStream(gzos);) {

							// Receive object
							Object receivedObject = in.readObject();
							// if (Thread.currentThread().isInterrupted()) return;
							if (receivedObject != null) {
								String fromNodeIP = inPacket.getAddress().getHostAddress();
								if (receivedObject instanceof Message) {
									Message receivedMessage = (Message) receivedObject;

									// Get acknowledgment for received message
									MessageAcknowledgment messageACK = handleReceivedMessage(receivedMessage, fromNodeIP);

									// Send message acknowledgment
									out.writeObject(messageACK);
									gzos.finish();
									out.flush();
									byte[] outBuffer = baos.toByteArray();
									DatagramPacket respPacket = new DatagramPacket(outBuffer, outBuffer.length, inPacket.getAddress(), inPacket.getPort());
									if (SET_MSG_QOS_TAGS) // Set specific QoS tags (diffserv/TOS) within USER, BFT and RFT message ACKs 
										serverSocket.setTrafficClass(receivedMessage instanceof UserMessage ? USER_MSG_IP_TOS : BFT_RFT_MSG_IP_TOS);
									serverSocket.send(respPacket);
								} else if (receivedObject instanceof MessageAcknowledgment) {
									MessageAcknowledgment receivedMessageACK = (MessageAcknowledgment) receivedObject;

									// Handle the received message ACK
									handleReceivedMessageAcknowledgment(receivedMessageACK, getNodeName(fromNodeIP));
								}
							} else
								LOGGER.warn("Object received via UDP is null");

						} catch (ClassNotFoundException e) {
							LOGGER.error("Receive message via UDP - Class Not Found Exception", e);
						} catch (IOException e) {
							LOGGER.error("Receive message via UDP - I/O Exception", e);
						} catch (Exception e) {
							LOGGER.error("Receive message via UDP - Exception", e);
						}
					}
				});
			}
		} catch (IOException e) {
			LOGGER.error("Receiving message via UDP - Could not bind the datagram socket to port " + portNumber, e);
		}
	}

	private MessageAcknowledgment handleReceivedMessage(Message receivedMessage, String fromNodeIP) throws Exception {
		Date recMsgArrivalDate = getCurrentDate();
		String fromNodeName = getNodeName(fromNodeIP);
		String msgType = getMsgType(receivedMessage);
		MessageManager mm = MessageManager.getInstance();

		LOGGER.debug("{} message '{}' received from {} ({}) on {}", msgType, receivedMessage.getID(), fromNodeName,
				fromNodeIP, DATE_FORMAT_LOG.format(recMsgArrivalDate)/* , receivedMessage */);

		// Update message trace
		receivedMessage.getTrace().addNodeHavingReceivedMsgFromNodeOnDate(currentNode.getName(), fromNodeName,
				recMsgArrivalDate);

		// Add the received message to the message manager (if it has not already been
		// received)
		MessageTrace msgTrace = new MessageTrace();
		if (receivedMessage instanceof UserMessage) {
			mm.addMessage(receivedMessage);

			// Get the node names having already received the message to include in ACK to
			// reply to sender node
			msgTrace = mm.getMessage(receivedMessage.getID()).getTrace().getDifferenceWith(receivedMessage.getTrace());
		}

		executorService.submit(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				// Handle the received message if current node is one of its recipients
				handleReceivedMessageIfRecipientNode(receivedMessage);

				// Forward the received message according to the chosen Take strategy
				sendingStrategy.forwardReceivedMessage(receivedMessage, fromNodeName);
				return null;
			}
		});

		// Create ACK to return for message received from given node IP
		MessageAcknowledgment recMsgACK = new MessageAcknowledgment(receivedMessage.getID(), recMsgArrivalDate,
				msgTrace);

		LOGGER.debug("ACK for received {} message '{}' sent to {} ({}) on {}", msgType, receivedMessage.getID(),
				fromNodeName, fromNodeIP, DATE_FORMAT_LOG.format(recMsgACK.getDepartureDate())/* , recMsgACK */);

		// Compute the sizes in [byte] of the un-/compressed received message and ACK to
		// send back
		// int recMsgSize = sizeOf(receivedMessage);
		int recMsgCompressedSize = sizeOfCompressed(receivedMessage);
		// int ackSize = sizeOf(recMsgACK);
		int ackCompressedSize = sizeOfCompressed(recMsgACK);

		// Add transmission data for statistics
		Date recMsgDepartureDate = receivedMessage.getDepartureDate();
		Date ackDepartureDate = getCurrentDate();
		TransmissionStatistics.getInstance().addReceivedIPMessageData(receivedMessage, fromNodeName,
				currentNode.getName(), recMsgCompressedSize, recMsgDepartureDate, recMsgArrivalDate, ackCompressedSize,
				ackDepartureDate);

		return recMsgACK;
	}

	private void handleReceivedMessageIfRecipientNode(Message receivedMessage) throws IOException {
		// Return if the received message has not arrived at destination,
		// i.e. if current node is not one of the recipients
		if (!receivedMessage.getRecipientNames().contains(currentNode.getName()))
			return;

		TransmissionStatistics transmissionStats = TransmissionStatistics.getInstance();

		if (receivedMessage instanceof UserMessage) {
			UserMessage receivedUserMessage = (UserMessage) receivedMessage;

			// Update received message status to RECEIVED (because it is the Take strategy
			// "direct")
			receivedUserMessage.updateStatus(currentNode.getName(), Status.RECEIVED, getCurrentDate());

			transmissionStats.incrementUserMessagesReceived();

			LOGGER.info("User message '{}' received from {}", receivedUserMessage.getID(),
					receivedUserMessage.getSenderName());

			// Create and send the data message containing the updated message status
			// N.B.: At last, the first RECEIVED status is set via the message ACKs
			// received, not via a new data message sent.
			/*
			 * String[] arguments = new String[]{ receivedUserMessage.getID(),
			 * updatedStatus.name() }; String senderName = currentNode.getName(); String
			 * recipientName = receivedUserMessage.getSenderName(); DataMessage dataMessage
			 * = new DataMessage(DataMessage.Type.MSG_STATUS, arguments, senderName,
			 * recipientName); send(dataMessage);
			 */

		} else { // receivedMessage is a DataMessage
			DataMessage receivedDataMessage = (DataMessage) receivedMessage;

			LOGGER.info("Data message '{}' of type '{}' received from {}", receivedDataMessage.getID(),
					receivedDataMessage.getType().name(), receivedDataMessage.getSenderName());

			switch (receivedDataMessage.getType()) {
			case MSG_STATUS:
				transmissionStats.incrementUserMessagesReceived();
				// Update user message status for received message ID and new status
				try {
					String messageID = receivedDataMessage.getArguments().get(0);
					String recipientName = receivedDataMessage.getArguments().get(1);
					Status newStatus = Status.valueOf(receivedDataMessage.getArguments().get(2));
					Date date = TakeController.DATE_FORMAT_LOG.parse(receivedDataMessage.getArguments().get(3));
					UserMessage userMessage = MessageManager.getInstance().getUserMessage(messageID);
					if (userMessage != null) {
						userMessage.updateStatus(recipientName, newStatus, date);
						LOGGER.info("Status of user message '{}' udated to {} for recipient {} on {}", messageID,
								newStatus.name(), recipientName, DATE_FORMAT_LOG.format(date));
					} else
						LOGGER.warn(
								"Status of user message '{}' not udated to {} for recipient {} on {}: no user message with this ID",
								messageID, newStatus.name());
				} catch (ParseException e) {
					LOGGER.error("Update user message status - Exception", e);
				}
				break;
			case BFT_RFT_DATA:
				// Create/update ally (node) / enemy entries with received data (description,
				// locations,...)
				// LOGGER.debug("DATA MSG RECEIVED: {}", receivedDataMessage.toString());
				List<String> arguments = receivedDataMessage.getArguments();
				for (String arg : arguments) {
					// Parse received BFT or RFT data
					String[] argData = arg.split(SEP);
					MilitaryUnit.Status status = MilitaryUnit.Status.valueOf(argData[0]);
					String name = argData[1];
					String type = argData[2];
					double longitude = Double.parseDouble(argData[3]);
					double latitude = Double.parseDouble(argData[4]);
					Date date = new Date(Long.parseLong(argData[5]));
					boolean forceBFTRFTMsgSending = Preferences.getInstance().isForceBFTRFTMessageSending();
					// Handle RFT (enemy) data
					if (status.equals(MilitaryUnit.Status.ENNEMY)) {
						if (!enemyUnits.containsKey(name))
							enemyUnits.put(name, new MilitaryUnit(status, name, type));
						boolean geolocWasUpdated = enemyUnits.get(name)
								.updateGeolocation(new Geolocation(longitude, latitude, date));
						if (!forceBFTRFTMsgSending)
							enemyUnits.get(name).getRelatedNodesHavingCurrentGeolocation()
									.add(receivedDataMessage.getSenderName());
						// Save enemy GPS geolocation to file if it has changed
						if (geolocWasUpdated) {
							// LOGGER.debug("RFT geolocation ({}, {}) on date {} updated for enemy {} {}",
							// longitude, latitude, DATE_FORMAT_LOG.format(date), type, name);
							Geolocation newGeolocation = enemyUnits.get(name).getLastGeolocation();
							Files.write(Paths.get(GEOLOCATIONS_FOLDER_PATH, name + ".csv"),
									new String(newGeolocation.getLongitude() + SEP + newGeolocation.getLatitude() + SEP
											+ DATE_FORMAT_FILE.format(newGeolocation.getDate()) + "\n").getBytes(),
									StandardOpenOption.CREATE, StandardOpenOption.APPEND);
						}
					} else { // Handle BFT (ally) data
						if (!name.equals(currentNode.getName()) && nodes.containsKey(name)) {
							boolean geolocWasUpdated = nodes.get(name)
									.updateGeolocation(new Geolocation(longitude, latitude, date));
							if (!forceBFTRFTMsgSending)
								nodes.get(name).getRelatedNodesHavingCurrentGeolocation()
										.add(receivedDataMessage.getSenderName());
							// Save node GPS geolocation history to file if it has changed
							if (geolocWasUpdated) {
								// LOGGER.debug("BFT geolocation ({}, {}) on date {} updated for ally {} {}",
								// longitude, latitude, DATE_FORMAT_LOG.format(date), type, name);
								Geolocation newGeolocation = nodes.get(name).getLastGeolocation();
								Files.write(Paths.get(GEOLOCATIONS_FOLDER_PATH, name + ".csv"),
										new String(newGeolocation.getLongitude() + SEP + newGeolocation.getLatitude()
												+ SEP + DATE_FORMAT_FILE.format(newGeolocation.getDate()) + "\n").getBytes(),
										StandardOpenOption.CREATE, StandardOpenOption.APPEND);
							}
						}
					}
				}
				break;
			}
		}
	}

	private void handleReceivedMessageAcknowledgment(MessageAcknowledgment receivedMessageACK, String fromNodeName) {
		try {
			Date ackArrivalDate = getCurrentDate();

			// Handling of the received message ACK by the message manager of the current
			// node
			MessageManager.getInstance().addMessageTrace(receivedMessageACK.getMessageID(),
					receivedMessageACK.getMessageTrace());

			// If message ID of received message ACK corresponds to a message originally
			// sent by this sender node,
			// and if the message was received by one or more of its recipients, set the
			// corresponding Round Trip
			// Time (RTT) if the latter has not already been set
			Message message = MessageManager.getInstance().getMessage(receivedMessageACK.getMessageID());
			if (message != null && message.hasSender(currentNode.getName())) {
				for (String recipientName : message.getRecipientNames()) {
					if (message.getTrace().wasMessageReceivedByNode(recipientName)) {
						message.setRoundTripTimeForRecipientIfNotSet(recipientName, ackArrivalDate);
					}
				}
			}

			// Forward the received message ACK according to the chosen Take strategy
			executorService.submit(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					sendingStrategy.forwardReceivedMessageAcknowledgment(receivedMessageACK, fromNodeName);
					return null;
				}
			});
		} catch (Exception e) {
			LOGGER.error("Handle received message acknowledgment - Exception", e);
		}
	}

	/**
	 * 
	 * @param serialObject
	 * @return size of (uncompressed) serialObject, i.e. number of valid bytes
	 */
	/*
	 * private static int sizeOf(Serializable serialObject) { try
	 * (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream
	 * oos = new ObjectOutputStream(baos);) {
	 * 
	 * oos.writeObject(serialObject); oos.close(); return baos.size();
	 * 
	 * } catch (IOException e) {
	 * LOGGER.error("Size of compressed (GZIP) serializable object - Exception", e);
	 * } return -1; }
	 */

	/**
	 * 
	 * @param serialObject
	 * @return size of compressed (with GZIP) serialObject, i.e. number of valid
	 *         bytes
	 */
	private static int sizeOfCompressed(Serializable serialObject) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				GZIPOutputStream gzos = new GZIPOutputStream(baos, DATAGRAM_MAX_SIZE);
				ObjectOutputStream oos = new ObjectOutputStream(gzos);) {

			oos.writeObject(serialObject);
			gzos.finish();
			oos.close();
			return baos.size();

		} catch (IOException e) {
			LOGGER.error("Size of compressed (GZIP) serializable object - Exception", e);
		}
		return -1;
	}

	private static String getMsgType(Message message) {
		if (message == null)
			return null;
		switch (message.getClass().getSimpleName()) {
		case "UserMessage":
			return "User";
		case "DataMessage":
			return "Data";
		}
		return null;
	}

	public Map<String, ? extends MilitaryUnit> getAllMilitaryUnits(boolean ally) {
		return ally ? nodes : enemyUnits;
	}

	public Map<String, MilitaryUnit> getMilitaryUnitsInCircleArea(boolean ally, float circleRadius) {
		Map<String, MilitaryUnit> selectedUnits = new LinkedHashMap<String, MilitaryUnit>();
		try {
			Geolocation circleCenter = radioGPSInterface.getCurrentGeolocation();
			Map<String, ? extends MilitaryUnit> allUnits = getAllMilitaryUnits(ally);
			for (String unitName : allUnits.keySet()) {
				MilitaryUnit unit = (MilitaryUnit) allUnits.get(unitName);
				if (unit.isInsideCircelArea(circleCenter, circleRadius))
					selectedUnits.put(unitName, unit);
			}
		} catch (Exception e) {
			LOGGER.error("Get military units in circle area - Exception", e);
		}
		return selectedUnits;
	}

	public Map<String, MilitaryUnit> getMilitaryUnitsInPolygonArea(boolean ally, List<Geolocation> polygonGPSPoints) {
		Map<String, MilitaryUnit> selectedUnits = new LinkedHashMap<String, MilitaryUnit>();
		try {
			Map<String, ? extends MilitaryUnit> allUnits = getAllMilitaryUnits(ally);
			for (String unitName : allUnits.keySet()) {
				MilitaryUnit unit = (MilitaryUnit) allUnits.get(unitName);
				if (unit.isInsidePolygonArea(polygonGPSPoints))
					selectedUnits.put(unitName, unit);
			}
		} catch (Exception e) {
			LOGGER.error("Get military units in circle area - Exception", e);
		}
		return selectedUnits;
	}

	public String setEnemyLocationAndType(String enemyID, String type, double longitude, double latitude, Date date) {
		if (enemyID == null) {
			int nodeIndex = 0;
			for (String nodeName : nodes.keySet()) {
				if (nodeName.equals(currentNode.getName()))
					break;
				nodeIndex++;
			}
			enemyID = "E" + type + "-" + nodeIndex + "-" + new String(date.getTime() + "").substring(3);
		}
		if (!enemyUnits.containsKey(enemyID))
			enemyUnits.put(enemyID, new MilitaryUnit(MilitaryUnit.Status.ENNEMY, enemyID, type));
		enemyUnits.get(enemyID).updateGeolocation(new Geolocation(longitude, latitude, date));
		return enemyID;
	}

	public long getCurrentTimeMillis() {
		return radioGPSInterface.getCurrentTimeMillis();
	}

	public Date getCurrentDate() {
		return radioGPSInterface.getCurrentDate();
	}

}
