package main.java.take.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.model.Node;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class Preferences {

	public static final String PATH_SEP = File.separator;

	private static final String CONFIG_FILE_NAME = "config.properties";
	private static final String NODE_SETTINGS_FILE_NAME = "node_settings.csv";

	private Properties appConfigProperties;
	/*
	 * Key = node name, value = node (node name, node type, node IP, radio ID, radio
	 * IP)
	 */
	private Map<String, Node> nodes = Collections.synchronizedMap(new LinkedHashMap<String, Node>());

	private static final Logger LOGGER = LoggerFactory.getLogger(Preferences.class);

	private static volatile Preferences instance = new Preferences();

	public static Preferences getInstance() {
		return instance;
	}

	private Preferences() {
		loadAppConfigProperties();
		loadNodesFromNodeSettingsFile();
	}

	private void loadAppConfigProperties() {
		appConfigProperties = new Properties();
		try (InputStream is = new FileInputStream(
				CONFIG_FILE_NAME)/* getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_NAME) */) {
			appConfigProperties.load(is);
		} catch (FileNotFoundException e) {
			LOGGER.error("Load application configuration properties - File not found", e);
		} catch (IOException e) {
			LOGGER.error("Load application configuration properties - I/O Exception", e);
		}
	}

	private void saveAppConfigProperties() {
		try (OutputStream os = new FileOutputStream(
				CONFIG_FILE_NAME)/* getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_NAME) */) {
			appConfigProperties.store(os, "Take application default configuration properties");
		} catch (FileNotFoundException e) {
			LOGGER.error("Save application configuration properties - File not found", e);
		} catch (IOException e) {
			LOGGER.error("Save application configuration properties - I/O Exception", e);
		}
	}

	private void loadNodesFromNodeSettingsFile() {
		boolean simulMode = isRadioGPSInterfaceSimulated();
		try (InputStream is = new FileInputStream(
				NODE_SETTINGS_FILE_NAME)/* getClass().getClassLoader().getResourceAsStream(NODE_SETTINGS_FILE_NAME) */;
				Scanner scanner = new Scanner(is);) {
			while (scanner.hasNextLine()) {
				String[] lineData = scanner.nextLine().split(",");
				String nodeName = lineData[0].trim();
				String nodeType = lineData[1].trim();
				String nodeIP = lineData[2].trim();
				String radioID = simulMode ? null : lineData[3].trim();
				String radioIP = simulMode ? null : lineData[4].trim();
				this.nodes.put(lineData[0], new Node(nodeName, nodeType, nodeIP, radioID, radioIP));
			}
		} catch (IOException e) {
			LOGGER.error("Load nodes from node settings file - I/O Exception", e);
		}
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public Node getCurrentNode() {
		return nodes.get(getNodeName());
	}

	public Node getNodeWithRadioID(String radioID) {
		for (Node node : nodes.values()) {
			if (node.getRadioID().equals(radioID))
				return node;
		}
		return null;
	}

	private String getAppConfigProperty(String propertyName) {
		return appConfigProperties.getProperty(propertyName);
	}

	private void setAppConfigProperty(String propertyName, String propertyValue, boolean writeFile) {
		appConfigProperties.setProperty(propertyName, propertyValue);
		if (writeFile)
			saveAppConfigProperties();
	}

	public String getNodeName() {
		return getAppConfigProperty("node_name");
	}

	public void setNodeName(String nodeName) {
		setAppConfigProperty("node_name", nodeName, true);
	}

	private static String appendPathSeparator(String path) {
		return path.endsWith(PATH_SEP) ? path : path + PATH_SEP;
	}

	public String getDataDirPath() {
		return appendPathSeparator(getAppConfigProperty("data_dir_path"));
	}

	public int getWebServicePort() {
		return Integer.parseInt(getAppConfigProperty("web_service_port"));
	}

	public int getMessagingPort() {
		return Integer.parseInt(getAppConfigProperty("messaging_port"));
	}

	public String getTransportProtocol() {
		return getAppConfigProperty("transport_protocol");
	}

	public int getPacketMaxSize() {
		return Integer.parseInt(getAppConfigProperty("packet_max_size"));
	}

	/**
	 * 
	 * @return node location update period in [s]
	 */
	public long getNodeLocationUpdatePeriod() {
		return Long.parseLong(getAppConfigProperty("node_location_update_period"));
	}

	/**
	 * 
	 * @return routing table update period in [s]
	 */
	public long getRoutingTableUpdatePeriod() {
		return Long.parseLong(getAppConfigProperty("routing_table_update_period"));
	}

	public String getSendingStrategy() {
		return getAppConfigProperty("sending_strategy");
	}

	/**
	 * 
	 * @return message response timeout in [s]
	 */
	public long getResponseTimeout() {
		return Long.parseLong(getAppConfigProperty("response_timeout"));
	}

	public void setSendingStrategy(String sendingStrategy, String responseTimeout) {
		setAppConfigProperty("sending_strategy", sendingStrategy, false);
		setAppConfigProperty("response_timeout", responseTimeout, true);
	}

	/**
	 * 
	 * @return BFT message sending period in [s]
	 */
	public int getBFTMessageSendingPeriod() {
		return Integer.parseInt(getAppConfigProperty("bft_message_sending_period"));
	}

	public void setBFTMessageSendingPeriod(int bftMessageSendingPeriod) {
		setAppConfigProperty("bft_message_sending_period", "" + bftMessageSendingPeriod, true);
	}

	/**
	 * 
	 * @return RFT message sending period in [s]
	 */
	public int getRFTMessageSendingPeriod() {
		return Integer.parseInt(getAppConfigProperty("rft_message_sending_period"));
	}

	public void setRFTMessageSendingPeriod(int rftMessageSendingPeriod) {
		setAppConfigProperty("rft_message_sending_period", "" + rftMessageSendingPeriod, true);
	}

	public float getMultipleMessageSendingDelay() {
		return Float.parseFloat(getAppConfigProperty("multiple_message_sending_delay"));
	}

	public void setMultipleMessageSendingDelay(float multipleMessageSendingDelay) {
		setAppConfigProperty("multiple_message_sending_delay", "" + multipleMessageSendingDelay, true);
	}

	public boolean isForceBFTRFTMessageSending() {
		return getAppConfigProperty("force_bft_rft_message_sending").toLowerCase().equals("true");
	}

	public void setForceBFTRFTMessageSending(boolean forced) {
		setAppConfigProperty("force_bft_rft_message_sending", "" + forced, true);
	}

	public int getRFTSimulatedEnemies() {
		return Integer.parseInt(getAppConfigProperty("rft_simulated_enemies"));
	}

	public boolean isRadioGPSInterfaceSimulated() {
		return getAppConfigProperty("radio_gps_interface").toLowerCase().equals("simulated");
	}

	public String getGPSdHost() {
		return getAppConfigProperty("gpsd_host");
	}

	public int getGPSdPort() {
		return Integer.parseInt(getAppConfigProperty("gpsd_port"));
	}

	public String getRadioSupplier() {
		return getAppConfigProperty("radio_supplier");
	}

	public String getRadioChannel() {
		return getAppConfigProperty("radio_channel");
	}

	public int getRadioRoutingTablePollingInterval() {
		return Integer.parseInt(getAppConfigProperty("radio_routing_table_polling_interval"));
	}

	public String getAppVersion() {
		String mavenPackage = "Take";
		String mavenArtifact = "Take";
		// Try to get version number from maven properties in jar's META-INF
		try (InputStream is = getClass()
				.getResourceAsStream("/META-INF/maven/" + mavenPackage + "/" + mavenArtifact + "/pom.properties")) {
			if (is != null) {
				Properties p = new Properties();
				p.load(is);
				String version = p.getProperty("version", "").trim();
				if (!version.isEmpty())
					return version;
			}
		} catch (IOException e) {
			LOGGER.error("Get app version - Resource not found", e);
		}
		return "N/A";
	}

	public String getFormattedAppConfig() {
		StringBuilder sb = new StringBuilder();

		sb.append("Node name: " + getNodeName() + "\n");
		sb.append("Data directory path: " + getDataDirPath() + "\n");
		sb.append("Web service port: " + getWebServicePort() + "\n");
		sb.append("Messaging port: " + getMessagingPort() + "\n");
		sb.append("Transport protocol: " + getTransportProtocol() + "\n");
		sb.append("Packet max size [B]: " + getPacketMaxSize() + "\n");
		sb.append("Node location update period [s]: " + getNodeLocationUpdatePeriod() + "\n");
		sb.append("Routing table update period [s]: " + getRoutingTableUpdatePeriod() + "\n");
		sb.append("Sending strategy: " + getSendingStrategy() + "\n");
		sb.append("Response timeout [s]: " + getResponseTimeout() + "\n");
		sb.append("Multiple message sending delay [s]: " + getMultipleMessageSendingDelay() + "\n");
		sb.append("BFT message sending period [s]: " + getBFTMessageSendingPeriod() + "\n");
		sb.append("RFT message sending period [s]: " + getRFTMessageSendingPeriod() + "\n");
		sb.append("BFT/RFT message sending forced: " + isForceBFTRFTMessageSending() + "\n");
		sb.append("RFT simulated enemies: " + getRFTSimulatedEnemies() + "\n");
		sb.append("Radio/GPS interface simulated: " + isRadioGPSInterfaceSimulated() + "\n");
		if (!isRadioGPSInterfaceSimulated()) {
			sb.append("GPSd host: " + getGPSdHost() + "\n");
			sb.append("GPSd port: " + getGPSdPort() + "\n");
			sb.append("Radio supplier: " + getRadioSupplier() + "\n");
			sb.append("Radio band: " + getRadioChannel() + "\n");
			sb.append("Radio routing table polling interval [s]: " + getRadioRoutingTablePollingInterval());
		}

		return sb.toString();
	}

	public String getFormattedNodeSettings() {
		boolean simulMode = isRadioGPSInterfaceSimulated();
		StringBuilder sb = new StringBuilder();

		for (Node ns : nodes.values())
			// nodeName, nodeType, nodeIP, radioID, radioIP
			sb.append(ns.getName() + " " + ns.getType() + " " + ns.getNodeIP()
					+ (simulMode ? "" : " " + ns.getRadioID() + " " + ns.getRadioIP()) + "\n");
		sb.delete(sb.length() - 1, sb.length());

		return sb.toString();
	}

}
