package main.java.take.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import main.java.take.model.Message;
import main.java.take.model.MessageTrace;
import main.java.take.model.UserMessage;
import main.java.take.model.dao.UserMessageDAO;
import main.java.take.model.dao.UserMessageFileDAO;

/**
 * 
 * Every user or data message which is sent or received (in transit or not) is
 * stored and managed by this class.
 * 
 * @author Gisler Christophes
 *
 */
public class MessageManager implements Serializable {

	private static final long serialVersionUID = 2381975476154114548L;

	// private static final Logger LOGGER =
	// LoggerFactory.getLogger(MessageManager.class);

	// private static final String PATH_SEP = File.separator;
	// private static final String FILE_PATH =
	// PropertiesManager.getInstance().getDataDirPath() + PATH_SEP +
	// "message_manager.ser";

	private String currentNodeName;
	/* Key = message ID, value = message */
	private Map<String, Message> messages = Collections.synchronizedMap(new LinkedHashMap<String, Message>());

	private final UserMessageDAO userMessageDAO = new UserMessageFileDAO();

	private static volatile MessageManager instance = new MessageManager();

	private MessageManager() {
		this.currentNodeName = TakeController.getInstance().getCurrentNode().getName();
	}

	public static MessageManager getInstance() {
		return instance;
	}

	public boolean containsMessage(String messageID) {
		return messages.containsKey(messageID);
	}

	public Message getMessage(String messageID) {
		return messages.get(messageID);
	}

	public UserMessage getUserMessage(String messageID) {
		Message message = getMessage(messageID);
		if (message != null && message instanceof UserMessage)
			return (UserMessage) message;
		return null;
	}

	/**
	 * Get the user messages sent by the given sender after given date
	 * 
	 * @param date
	 *            (if null, all received user messages are returned)
	 * @return sentUserMessages
	 */
	public SortedSet<UserMessage> getUserMessagesSentBySenderAfterDate(String senderName, Date date) {
		SortedSet<UserMessage> sentUserMessages = new TreeSet<UserMessage>();
		for (Message message : messages.values()) {
			if (message instanceof UserMessage && message.getSenderName().equals(senderName)
					&& (date == null || message.getDate().after(date))) {
				sentUserMessages.add((UserMessage) message);
			}
		}
		return sentUserMessages;
	}

	/**
	 * Get the user messages received by the given recipient after given date
	 * 
	 * @param date
	 *            (if null, all received user messages are returned)
	 * @return receivedUserMessages
	 */
	public SortedSet<UserMessage> getUserMessagesReceivedByRecipientAfterDate(String recipientName, Date date) {
		SortedSet<UserMessage> receivedUserMessages = new TreeSet<UserMessage>();
		for (Message message : messages.values()) {
			if (message instanceof UserMessage && message.getRecipientNames().contains(recipientName)
					&& (date == null || message.getDate().after(date))) {
				receivedUserMessages.add((UserMessage) message);
			}
		}
		return receivedUserMessages;
	}

	public synchronized void addMessage(Message message) throws IOException {
		String messageID = message.getID();
		if (!messages.containsKey(messageID)) {
			messages.put(messageID, message);
		} else {
			messages.get(messageID).getTrace().add(message.getTrace());
		}
		saveUserMessageIfNeeded(messageID);
	}

	public MessageTrace getMessageTrace(String messageID) {
		return messages.get(messageID).getTrace();
	}

	public synchronized void addMessageTrace(String messageID, MessageTrace messageTrace) throws IOException {
		if (messages.containsKey(messageID)) {
			messages.get(messageID).getTrace().add(messageTrace);
			saveUserMessageIfNeeded(messageID);
		}
	}

	public void removeMessage(String messageID) {
		messages.remove(messageID);
	}

	public Set<String> getNodesHavingMessage(String messageID) {
		if (messages.containsKey(messageID))
			return messages.get(messageID).getTrace().getNodesHavingReceivedMsg();
		return new TreeSet<String>();
	}

	public boolean hasNodeReceivedMessage(String nodeName, String messageID) {
		return messages.containsKey(messageID) && messages.get(messageID).getTrace().wasMessageReceivedByNode(nodeName);
	}

	public Set<String> getRecipientsHavingNotReceivedMessage(String messageID) {
		if (!containsMessage(messageID))
			return new TreeSet<String>();

		// Create set containing all recipients
		Set<String> recipientsHavingNotReceivedMsg = new TreeSet<String>(messages.get(messageID).getRecipientNames());

		// Remove from set the nodes having received the messages (nodes which are not
		// recipients are ignored -> asymmetric set difference)
		recipientsHavingNotReceivedMsg.removeAll(messages.get(messageID).getTrace().getNodesHavingReceivedMsg());

		return recipientsHavingNotReceivedMsg;
	}

	/**
	 * Return an array containing the number of successful recipients and the number
	 * of total recipients.
	 * 
	 * @param messageID
	 * @return new int[]{ successfulRecipientNbr, recipientNbr }
	 */
	public int[] getMessageCompletionInfos(String messageID) {
		if (!containsMessage(messageID))
			return null;

		// Create set containing all recipients
		Set<String> recipients = messages.get(messageID).getRecipientNames();
		Set<String> recipientsHavingNotReceivedMsg = new TreeSet<String>(recipients);

		// Remove from set the nodes having received the messages (nodes which are not
		// recipients are ignored -> asymmetric set difference)
		recipientsHavingNotReceivedMsg.removeAll(messages.get(messageID).getTrace().getNodesHavingReceivedMsg());

		// Compute the number of successful recipients
		final int recipientNbr = recipients.size();
		final int successfulRecipientNbr = recipientNbr - recipientsHavingNotReceivedMsg.size();
		return new int[] { successfulRecipientNbr, recipientNbr };
	}

	/**
	 * The mean of the RTT of all messages originally sent by this current node
	 * (sender) to other nodes (recipients). The RTT of a message originally sent
	 * from this sender node to another recipient node is defined by the time
	 * difference in [ms] between the arrival date of the message acknowledgment
	 * (ACK) containing the info that the given recipient node has received the
	 * message and the departure date of the message from this node. The RTT is
	 * computed for all types of messages (user messages, BFT/RFT data
	 * messages,...). When sending from a sender to recipients, a message may pass
	 * through other nodes in the sense of an applicative routing (targeting TAKE
	 * applications on intermediary nodes) and in the sense of a normal IP routing
	 * (here with OLSR).
	 * 
	 * @return mean round trip time in [ms] of all messages originally sent from
	 *         this sender to recipient nodes, -1 if no recipient has received any
	 *         messages
	 */
	public long getMeanRoundTripTimeForAllRecipients() {
		long recipientNbr = 0l;
		long meanRTTForRecipients = 0l;
		for (Message msg : messages.values()) {
			if (msg.hasSender(currentNodeName)) {
				recipientNbr += msg.getRoundTripTimesForRecipients().size();
				meanRTTForRecipients += msg.getRoundTripTimesForRecipients().values().stream().reduce(0l, Long::sum);
			}
		}
		return recipientNbr > 0l ? meanRTTForRecipients / recipientNbr : -1l;
	}

	public synchronized void reset() {
		messages.clear();
		userMessageDAO.deleteAllMessages();
	}

	/*
	 * Save message if it is a user message and if current node name is either the
	 * sender or amongst the recipients
	 */
	private void saveUserMessageIfNeeded(String msgID) throws IOException {
		Message message = messages.get(msgID);
		if (message != null && message instanceof UserMessage
				&& (message.hasSender(currentNodeName) || message.hasRecipient(currentNodeName)))
			userMessageDAO.saveMessage((UserMessage) message);
	}

	/*
	 * public synchronized void saveToFile() { if (messages.isEmpty()) return; try
	 * (FileOutputStream fos = new FileOutputStream(FILE_PATH); BufferedOutputStream
	 * bos = new BufferedOutputStream(fos); ObjectOutputStream out = new
	 * ObjectOutputStream(bos);) { out.writeObject(this); } catch
	 * (FileNotFoundException e) {
	 * LOGGER.error("Save To file - File Not Found Exception", e); } catch
	 * (IOException e) { LOGGER.error("Save To file - I/O Exception", e); } }
	 * 
	 * public synchronized void restoreFromFile() { if (!new
	 * File(FILE_PATH).exists()) return; try (FileInputStream fis = new
	 * FileInputStream(FILE_PATH); BufferedInputStream bis = new
	 * BufferedInputStream(fis); ObjectInputStream in = new ObjectInputStream
	 * (bis);) { MessageManager messageManager = (MessageManager) in.readObject();
	 * this.messages.putAll(messageManager.messages); } catch (FileNotFoundException
	 * e) { LOGGER.error("Restore from file - File Not Found Exception", e); } catch
	 * (IOException e) { LOGGER.error("Restore from file - I/O Exception", e); }
	 * catch (ClassNotFoundException e) {
	 * LOGGER.error("Restore from file - Class Not Found Exception", e); } }
	 */

}
