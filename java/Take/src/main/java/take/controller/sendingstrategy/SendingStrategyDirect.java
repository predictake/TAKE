package main.java.take.controller.sendingstrategy;

import java.util.Set;

import main.java.take.model.Message;
import main.java.take.model.MessageAcknowledgment;

/**
 * 
 * Take sending strategy "Direct" consisting in sending the message directly to
 * each recipient Ri (without any retransmission).
 * 
 * @author Gisler Christophe
 *
 */
public class SendingStrategyDirect implements SendingStrategy {

	public static final String NAME = "Direct";

	private long responseTimeout;

	public SendingStrategyDirect(long responseTimeout) {
		this.responseTimeout = responseTimeout;
	}

	@Override
	public void sendMessage(Message message) throws Exception {
		Set<String> recipientNames = message.getRecipientNames();

		// Try to send M to each recipient nodes directly (according to OLSR routing
		// table RT)
		mainController.sendMessageDirectly(message, recipientNames, responseTimeout);
	}

	@Override
	public void forwardReceivedMessage(Message receivedMessage, String fromNodeName) throws Exception {
		// No specific message forward strategy
	}

	@Override
	public void forwardReceivedMessageAcknowledgment(MessageAcknowledgment receivedMessageACK, String fromNodeName)
			throws Exception {
		// No specific message ACK forward strategy
	}

}
