package main.java.take.controller.sendingstrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.controller.TakeController;
import main.java.take.controller.MessageManager;
import main.java.take.model.Message;
import main.java.take.model.MessageAcknowledgment;

/**
 * 
 * Interface for the Take sending strategy.
 * 
 * A Take sending strategy must implement what to do when: 1. A message must be
 * send to all its recipients; 2. A message is received from another node; 3. A
 * message acknowledgment is received from another node.
 * 
 * @author Gisler Christophe
 *
 */
public interface SendingStrategy {

	public static final String SEP = TakeController.SEP;

	public static final Logger LOGGER = LoggerFactory.getLogger(TakeController.class);

	public TakeController mainController = TakeController.getInstance();
	public MessageManager messageManager = MessageManager.getInstance();

	/**
	 * Send the given message from this current node to all its recipient nodes
	 * according to the implemented Take retransmission strategy.
	 * 
	 * @param message
	 */
	public void sendMessage(Message message) throws Exception;

	/**
	 * Forward the given message received from the given node according to the
	 * implemented Take retransmission strategy.
	 * 
	 * @param received
	 *            message
	 * @param from
	 *            node name
	 */
	public void forwardReceivedMessage(Message receivedMessage, String fromNodeName) throws Exception;

	/**
	 * Forward the given message acknowledgement received from the given node
	 * according to the implemented Take retransmission strategy. N.B.: Names of
	 * nodes having got the message and included in the received ACK have been
	 * already added to the message manager.
	 * 
	 * @param received
	 *            message ACK
	 * @param fromNodeName
	 */
	public void forwardReceivedMessageAcknowledgment(MessageAcknowledgment receivedMessageACK, String fromNodeName)
			throws Exception;

}
