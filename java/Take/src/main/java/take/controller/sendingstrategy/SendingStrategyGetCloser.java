package main.java.take.controller.sendingstrategy;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import main.java.take.model.MessageAcknowledgment;
import main.java.take.model.MessageTrace;
import main.java.take.model.Message;

/**
 * Take sending strategy "GetCloser" consisting in sending the message: 1.
 * directly to each recipient Ri, and if it has failed 2. to N = 2 nodes closer
 * to Ri, i.e. whose next hop in the routing table is the same as the one of Ri,
 * or to N = 2 neighbor nodes of Ri, if there is no node closer to Ri
 * 
 * @author Gisler Christophe
 *
 */
public class SendingStrategyGetCloser implements SendingStrategy {

	public static final String NAME = "GetCloser", NAME2 = "GetCloser2", NAME3 = "GetCloser3", NAME4 = "GetCloser4";

	private static final int N = 2; // See comment above

	private long responseTimeout; // In seconds
	private boolean replaceTargetedNodesWithSameNextNodeByNextNode, takeNextNodesOnly,
			getNeighborNodesIfNoCloserNode = false;

	public SendingStrategyGetCloser(long responseTimeout, boolean replaceTargetedNodesWithSameNextNodeByNextNode,
			boolean takeNextNodesOnly) {
		this.responseTimeout = responseTimeout;
		this.replaceTargetedNodesWithSameNextNodeByNextNode = replaceTargetedNodesWithSameNextNodeByNextNode; // Instead
																												// of
																												// 1st
																												// direct
																												// sending
		this.takeNextNodesOnly = takeNextNodesOnly;
	}

	@Override
	public void sendMessage(Message message) throws Exception {
		/* ********************************************************************** */
		/* 1. I am a sender/intermediary node wanting to send/forward a message M */
		/* ********************************************************************** */
		sendToGetCloser(message);

		/*
		 * D. After 4T seconds, if I am the original sender, resend M according to the
		 * GetCloser strategy. N.B.: In each node, each sent/received message is stored
		 * with the list of node names having already got the the message, thus a
		 * message can not sent several times to a node which has already received it
		 * and at the end we can know which recipient(s) have received it.
		 */
		if (message.getSenderName().equals(mainController.getCurrentNode().getName()))
			sendToGetCloser(message);
	}

	@Override
	public void forwardReceivedMessage(Message receivedMessage, String fromNodeName) throws Exception {
		/* ********************************************************************** */
		/* 2. I am a intermediary/recipient node having received M from a node Ni */
		/* (therefore I'm closer to at least one recipient of M) */
		/* ********************************************************************** */

		/*
		 * A. Perform steps a. to c. of point 1
		 */
		sendToGetCloser(receivedMessage);
	}

	@Override
	public void forwardReceivedMessageAcknowledgment(MessageAcknowledgment receivedMessageACK, String fromNodeName)
			throws Exception {
		String messageID = receivedMessageACK.getMessageID();
		Set<String> targetedNodes = new LinkedHashSet<String>();
		Message message = messageManager.getMessage(messageID);
		if (message == null)
			return;

		// Get sender node and current node
		String currentNode = mainController.getCurrentNode().getName();
		String senderNode = message.getSenderName();
		if (!senderNode.equals(currentNode))
			targetedNodes.add(senderNode);

		// Get previous nodes from which the message was sent
		String firstPrecedingNode = message.getTrace().getFirstNodePrecedingNode(currentNode);
		if (firstPrecedingNode != null && !firstPrecedingNode.equals(currentNode))
			targetedNodes.add(firstPrecedingNode);

		if (targetedNodes.isEmpty())
			return;

		// Get a message trace simplified relatively to the recipients only
		MessageTrace msgRecipientsTrace = message.getRecipientsTrace();

		// Send a new message ACK to sender node and first preceding node (if not equal
		// to current node)
		MessageAcknowledgment messageACK = new MessageAcknowledgment(messageID,
				receivedMessageACK.getMessageArrivalDate(), msgRecipientsTrace);
		mainController.sendMessageAcknowledgmentDirectly(messageACK, targetedNodes);
	}

	private void sendToGetCloser(Message message) throws Exception {
		String messageID = message.getID();

		/*
		 * A. Try to send M directly (i.e. according to OLSR routing table RT) to each
		 * recipient nodes having not already received M and wait T seconds for incoming
		 * acknowledgments
		 */
		Set<String> targetedNodes = messageManager.getRecipientsHavingNotReceivedMessage(messageID);
		if (replaceTargetedNodesWithSameNextNodeByNextNode && targetedNodes.size() > 1)
			targetedNodes = replaceTargetedNodesWithSameNextNodeByNextNode(targetedNodes);
		if (targetedNodes.isEmpty())
			return;
		Map<String, MessageAcknowledgment> receivedMessageACKsFromNodes = mainController.sendMessageDirectly(message,
				targetedNodes, responseTimeout);
		targetedNodes.removeAll(receivedMessageACKsFromNodes.keySet());

		/*
		 * B. For each unsuccessful recipient node Ri, send M to (at most) N = 2
		 * intermediary nodes, which are located in the direction of (and potentially
		 * closer to) Ri (i.e. whose next hop in RT is the same as the one of the
		 * recipient), and wait T seconds for incoming acknowledgments
		 */
		targetedNodes = getUntargetedNodesInDirectionToRemainingTargetedNodes(targetedNodes, messageID);
		if (targetedNodes.isEmpty() && getNeighborNodesIfNoCloserNode)
			targetedNodes = getUntargetedNeighborNodes(messageID);
		if (targetedNodes.isEmpty())
			return;
		receivedMessageACKsFromNodes = mainController.sendMessageDirectly(message, targetedNodes, responseTimeout);
		targetedNodes.removeAll(receivedMessageACKsFromNodes.keySet());

		/*
		 * C. For each unsuccessful intermediary node Ni, resend M to (at most) N = 2
		 * intermediary nodes, which are closer to Ni and wait 2T seconds for incoming
		 * acknowledgments
		 */
		targetedNodes = getUntargetedNodesInDirectionToRemainingTargetedNodes(targetedNodes, messageID);
		if (targetedNodes.isEmpty() && getNeighborNodesIfNoCloserNode)
			targetedNodes = getUntargetedNeighborNodes(messageID);
		if (targetedNodes.isEmpty())
			return;
		receivedMessageACKsFromNodes = mainController.sendMessageDirectly(message, targetedNodes, 2 * responseTimeout);
	}

	private Set<String> replaceTargetedNodesWithSameNextNodeByNextNode(Set<String> targetedNodes) {
		if (targetedNodes.isEmpty())
			return targetedNodes;

		Set<String> gatheredTargetedNodes = new TreeSet<String>();

		Map<String, TreeSet<String>> nextNodeDestNodes = new LinkedHashMap<String, TreeSet<String>>();
		// For each targeted node, build a map whose keys are the existing next nodes
		// and values are the set of destination nodes with same next node
		for (String targetedNode : targetedNodes) {
			String nextNode = mainController.getNextNodeInRoutingTableOf(targetedNode);
			if (!nextNodeDestNodes.containsKey(nextNode))
				nextNodeDestNodes.put(nextNode, new TreeSet<String>());
			nextNodeDestNodes.get(nextNode).add(targetedNode);
		}
		// Build the new "gathered" targeted nodes as follows: add next node if the
		// number of
		// associated destination nodes is greater as 1 or the destination node itself
		// otherwise
		for (String nextNode : nextNodeDestNodes.keySet()) {
			if (nextNodeDestNodes.get(nextNode).size() > 1)
				gatheredTargetedNodes.add(nextNode);
			else
				gatheredTargetedNodes.add(nextNodeDestNodes.get(nextNode).first());
		}

		return gatheredTargetedNodes;
	}

	private Set<String> getUntargetedNodesInDirectionToRemainingTargetedNodes(Set<String> remainingTargetedNodes,
			String messageID) {
		Set<String> untargetedCloserNodes = new TreeSet<String>();

		for (String remainingTargetedNode : remainingTargetedNodes) {
			if (takeNextNodesOnly) {
				// Get next node of given node (which is closer)
				String nextNodeOfRemainingTargetedNode = mainController
						.getNextNodeInRoutingTableOf(remainingTargetedNode);

				// Add it to the untargeted closer nodes if it has not already received the
				// message
				if (!messageManager.hasNodeReceivedMessage(nextNodeOfRemainingTargetedNode, messageID))
					untargetedCloserNodes.add(nextNodeOfRemainingTargetedNode);
			} else {
				// Get list of nodes potentially closer to given node
				Set<String> nodesWithSameNextNodeAsRemainingTargetedNode = mainController
						.getDestinationNodesWithSameNextNodeInRoutingTableAs(remainingTargetedNode);

				// Add at most N of them to the set of untargeted closer nodes if they have not
				// already received the message
				for (String nodeWithSameNextNodeAsRemainingTargetedNode : nodesWithSameNextNodeAsRemainingTargetedNode) {
					if (untargetedCloserNodes.size() < N && !messageManager
							.hasNodeReceivedMessage(nodeWithSameNextNodeAsRemainingTargetedNode, messageID)) {
						untargetedCloserNodes.add(nodeWithSameNextNodeAsRemainingTargetedNode);
					}
				}
			}
		}

		return untargetedCloserNodes;
	}

	private Set<String> getUntargetedNeighborNodes(String messageID) {
		Set<String> untargetedNeighborNodes = new TreeSet<String>(mainController.getNeighborNodes());
		untargetedNeighborNodes.removeAll(messageManager.getNodesHavingMessage(messageID));
		int i = untargetedNeighborNodes.size();
		for (String node : untargetedNeighborNodes)
			if (i-- > N)
				untargetedNeighborNodes.remove(node);
		return untargetedNeighborNodes;
	}

}
