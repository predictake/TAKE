package main.java.take.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.DateFormat;
import java.util.Date;

import main.java.take.controller.TakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class MessageAcknowledgment implements Externalizable {

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private String messageID; // Optional message ID used to identify the message when special ACK is sent
								// independently
	private Date messageArrivalDate; // Date of arrival of the related message for which this ACK will be sent
	private Date departureDate; // Date of departure of this ACK
	private MessageTrace messageTrace; // The trace include nodes having already received the message on given date

	/**
	 * Constructor required by GSon and Externalizable
	 */
	public MessageAcknowledgment() {
	}

	public MessageAcknowledgment(String messageID, Date messageArrivalDate, MessageTrace messageTrace) {
		this.messageID = messageID;
		this.messageArrivalDate = messageArrivalDate;
		this.departureDate = TakeController.getInstance().getCurrentDate();
		this.messageTrace = messageTrace;
	}

	public String getMessageID() {
		return messageID;
	}

	public Date getMessageArrivalDate() {
		return messageArrivalDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public MessageTrace getMessageTrace() {
		return messageTrace;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Message acknowledgment (ACK):");
		if (messageID != null)
			sb.append("\n- MSG ID: \t" + messageID);
		sb.append("\n- MSG arrival date: \t" + DATE_FORMAT.format(messageArrivalDate));
		sb.append("\n- ACK departure date: \t" + DATE_FORMAT.format(departureDate));
		/*
		 * sb.append("\n- Nodes having received MSG: \t"); sb.append(String.join(", ",
		 * msgTrace.getNodesHavingReceivedMsg()));
		 */

		return sb.toString();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeUTF(messageID);
		out.writeLong(messageArrivalDate.getTime());
		out.writeLong(departureDate.getTime());
		out.writeObject(messageTrace);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		messageID = in.readUTF();
		messageArrivalDate = new Date(in.readLong());
		departureDate = new Date(in.readLong());
		messageTrace = (MessageTrace) in.readObject();
	}

}
