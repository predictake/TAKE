package main.java.take.model.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import main.java.take.controller.Preferences;
import main.java.take.model.UserMessage;
import main.java.take.util.FileUtil;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class UserMessageFileDAO implements UserMessageDAO {

	public static final String MAILBOX_PATH = Paths.get(Preferences.getInstance().getDataDirPath(), "mailbox")
			.toString() + File.separator;

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();;

	private static final String MSG_FILE_EXT = ".json";

	public UserMessageFileDAO() {
		createDirectoriesIfNeeded();
	}

	private void createDirectoriesIfNeeded() {
		File msgDir = new File(MAILBOX_PATH);
		if (!msgDir.isDirectory())
			msgDir.mkdirs();
	}

	@Override
	public void saveMessage(UserMessage message) throws IOException {
		FileUtil.createFolderIfNotExist(MAILBOX_PATH);
		String msgJSON = GSON.toJson(message);
		FileUtil.writeTextToFile(getMessageFilePath(message), msgJSON);
	}

	private static String getMessageFilePath(UserMessage message) {
		return MAILBOX_PATH + message.getID() + MSG_FILE_EXT;
	}

	@Override
	public void deleteMessage(UserMessage message) {
		FileUtil.deleteFile(getMessageFilePath(message));
	}

	@Override
	public SortedSet<UserMessage> getUserMessages() {
		SortedSet<UserMessage> messages = new TreeSet<UserMessage>();
		File[] msgFiles = FileUtil.getFilesEndingWith(MSG_FILE_EXT, MAILBOX_PATH);
		for (File msgf : msgFiles) {
			try (BufferedReader in = new BufferedReader(new FileReader(msgf));) {
				UserMessage message = GSON.fromJson(new JsonReader(in), UserMessage.class);
				messages.add(message);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return messages;
	}

	@Override
	public void deleteAllMessages() {
		FileUtil.deleteAllFilesInFolder(MAILBOX_PATH);
	}

}
