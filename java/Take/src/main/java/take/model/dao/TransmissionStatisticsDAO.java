package main.java.take.model.dao;

import java.io.IOException;

/**
 * 
 * @author Gisler Christophe
 *
 */
public interface TransmissionStatisticsDAO {

	/**
	 * Save transmission statistics to DB
	 * 
	 * @param message
	 * @throws IOException
	 */
	public void saveToDatabase() throws IOException;

	/**
	 * Clear DB, i.e. delete all saved transmission statistics
	 */
	public void clearDatabase();

}
