package main.java.take.model.dao;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import main.java.take.controller.TakeController;
import main.java.take.controller.Preferences;
import main.java.take.model.TransmissionData;
import main.java.take.model.TransmissionStatistics;
import main.java.take.model.TransmissionData.Type;
import main.java.take.util.FileUtil;

/**
 * Save transmission statistics to JSON file
 * 
 * @author Gisler Christophe
 *
 */
public class TransmissionStatisticsFileDAO implements TransmissionStatisticsDAO {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.#####");
	private static final String SEP = ",", SEP2 = " ", NA = "n/a";
	private static final boolean WRITE_TO_CSV = true;

	public static final String TS_FILE_NAME      = "transmission_stats";
	public static final String TS_JSON_FILE_PATH = Preferences.getInstance().getDataDirPath() + TS_FILE_NAME + ".json";
	public static final String TS_CSV_FILE_PATH  = Preferences.getInstance().getDataDirPath() + TS_FILE_NAME + ".csv";

	private TransmissionStatistics transmissionStatistics;

	public TransmissionStatisticsFileDAO(TransmissionStatistics transmissionStatistics) {
		this.transmissionStatistics = transmissionStatistics;
	}

	@Override
	public void saveToDatabase() throws IOException {
		JSONObject jsonObject = new JSONObject();
		List<String> lines = new LinkedList<String>();

		String currentNode = TakeController.getInstance().getCurrentNode().getName();
		jsonObject.put("Current Node", currentNode);

		// Statistics for user messages
		int userMessageRecipients = transmissionStatistics.getUserMessageRecipients();
		jsonObject.put("User Messages Recipients", userMessageRecipients);
		float userMessageCompletionRate = Float
				.parseFloat(DECIMAL_FORMAT.format(transmissionStatistics.getUserMessageCompletionRate()));
		jsonObject.put("User Message Completion Rate", userMessageCompletionRate);
		int userMessagesReceived = transmissionStatistics.getUserMessagesReceived();
		jsonObject.put("User Messages Received", userMessagesReceived); // As a recipient of the MSG
		int userMessageSent_NoACK = transmissionStatistics.getUserMessagesSent_NoACK();
		jsonObject.put("User Messages Sent - NO ACK Received", userMessageSent_NoACK); // As a sender of the MSG
		int userMessagesSent_ACK = transmissionStatistics.getUserMessagesSent_ACK();
		jsonObject.put("User Messages Sent - ACK Received", userMessagesSent_ACK); // As a sender of the MSG
		long userMessageMeanRTT = transmissionStatistics.getUserMessageMeanRoundTripTime();
		jsonObject.put("User Message Mean Round-Trip-Time", userMessageMeanRTT);

		// Statistics for IP messages
		float ipMessageCompletionRate = Float
				.parseFloat(DECIMAL_FORMAT.format(transmissionStatistics.getIPMessageCompletionRate())); // =
																											// IPMessagesSent_ACK
																											// /
																											// (IPMessagesSent_ACK
																											// +
																											// IPMessagesSent_NoACK)
		jsonObject.put("IP Message Completion Rate", ipMessageCompletionRate);
		int ipMessagesReceived = transmissionStatistics.getIPMessagesReceived();
		jsonObject.put("IP Messages Received", ipMessagesReceived); // As any node (sender, intermediary, recipient)
		int ipMessagesSent_NoACK = transmissionStatistics.getIPMessagesSent_NoACK();
		jsonObject.put("IP Messages Sent - NO ACK Received", ipMessagesSent_NoACK); // As any node (sender,
																					// intermediary, recipient)
		int ipMessagesSent_ACK = transmissionStatistics.getIPMessagesSent_ACK();
		jsonObject.put("IP Messages Sent - ACK Received", ipMessagesSent_ACK); // As any node (sender, intermediary,
																				// recipient)
		long ipMessageMeanRTT = transmissionStatistics.getIPMessageMeanRoundTripTime();
		jsonObject.put("IP Message Mean Round-Trip-Time", ipMessageMeanRTT);
		long ipMessagesReceivedMeanProcessingTime = transmissionStatistics.getIPMessagesReceivedMeanProcessingTime();
		jsonObject.put("IP Messages Received Mean Processing Time", ipMessagesReceivedMeanProcessingTime);

		// Statistics for BFT/RFT messages
		float bftKnownGeolocationRate = Float
				.parseFloat(DECIMAL_FORMAT.format(transmissionStatistics.getBFTKnownGeolocationRate()));
		jsonObject.put("BFT Known Geolocation Rate", bftKnownGeolocationRate);
		int[] meanTimeBetweenBFTGeolocationUpdatesPerNode = transmissionStatistics
				.getMeanTimesBetweenBFTGeolocationUpdatesReceivedPerNode();
		jsonObject.put("Mean Time Between BFT Geolocation Updates Per Node",
				meanTimeBetweenBFTGeolocationUpdatesPerNode);
		int[] meanTimeBetweenRFTGeolocationUpdatesPerNode = transmissionStatistics
				.getMeanTimesBetweenRFTGeolocationUpdatesReceivedPerNode();
		jsonObject.put("Mean Time Between RFT Geolocation Updates Per Node",
				meanTimeBetweenRFTGeolocationUpdatesPerNode);
		int[] meanBFTGeolocationsReceivedPerNode = transmissionStatistics.getNumbersOfBFTGeolocationsReceivedPerNode();
		jsonObject.put("BFT Geolocations Received Per Node", meanBFTGeolocationsReceivedPerNode);
		int[] meanRFTGeolocationsReceivedPerNode = transmissionStatistics.getNumbersOfRFTGeolocationsReceivedPerNode();
		jsonObject.put("RFT Geolocations Received Per Node", meanRFTGeolocationsReceivedPerNode);

		if (WRITE_TO_CSV) {
			lines.add(
					"Current Node,User Messages Recipients,User Message Completion Rate,User Messages Received,User Messages Sent - NO ACK Received,User Messages Sent - ACK Received,User Message Mean Round-Trip-Time,IP Message Completion Rate,IP Messages Received,IP Messages Sent - NO ACK Received,IP Messages Sent - ACK Received,IP Message Mean Round-Trip-Time,IP Messages Received Mean Processing Time,BFT Known Geolocation Rate,Mean Time Between BFT Geolocation Updates Per Node,Mean Time Between RFT Geolocation Updates Per Node,BFT Geolocations Received Per Node,RFT Geolocations Received Per Node");
			lines.add(currentNode + SEP + userMessageRecipients + SEP + userMessageCompletionRate + SEP
					+ userMessagesReceived + SEP + userMessageSent_NoACK + SEP + userMessagesSent_ACK + SEP
					+ userMessageMeanRTT + SEP + ipMessageCompletionRate + SEP + ipMessagesReceived + SEP
					+ ipMessagesSent_NoACK + SEP + ipMessagesSent_ACK + SEP + ipMessageMeanRTT + SEP
					+ ipMessagesReceivedMeanProcessingTime + SEP + bftKnownGeolocationRate + SEP
					+ toString(meanTimeBetweenBFTGeolocationUpdatesPerNode) + SEP
					+ toString(meanTimeBetweenRFTGeolocationUpdatesPerNode) + SEP
					+ toString(meanBFTGeolocationsReceivedPerNode) + SEP
					+ toString(meanRFTGeolocationsReceivedPerNode));
			lines.add(
					"#,MSG Type,MSG ID,MSG Departure Node,MSG Arrival Node,MSG Size,MSG Departure Date,MSG Arrival Date,ACK Size,ACK Departure Date,ACK Arrival Date");
		}
		List<TransmissionData> transmissionDatas = transmissionStatistics.getTransmissionDatas();
		synchronized (transmissionDatas) {
			int i = 0;
			for (TransmissionData td : transmissionDatas) {
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("MSG Type", getFormattedType(td.getType()));
				jsonObject2.put("MSG ID", td.getMsgID());
				jsonObject2.put("MSG Departure Node", td.getMsgDepartureNodeName());
				jsonObject2.put("MSG Arrival Node", td.getMsgArrivalNodeName());
				jsonObject2.put("MSG Size", td.getMsgSize());
				jsonObject2.put("MSG Departure Date", DATE_FORMAT.format(td.getMsgDepartureDate()));
				if (td.getMsgArrivalDate() != null)
					jsonObject2.put("MSG Arrival Date", DATE_FORMAT.format(td.getMsgArrivalDate()));
				if (td.getAckSize() > 0)
					jsonObject2.put("ACK Size", td.getAckSize());
				if (td.getAckDepartureDate() != null)
					jsonObject2.put("ACK Departure Date", DATE_FORMAT.format(td.getAckDepartureDate()));
				if (td.getAckArrivalDate() != null)
					jsonObject2.put("ACK Arrival Date", DATE_FORMAT.format(td.getAckArrivalDate()));
				jsonObject.put("Transmission Data " + (++i), jsonObject2);
				if (WRITE_TO_CSV)
					lines.add(i + SEP + getFormattedType(td.getType()) + SEP + td.getMsgID() + SEP
							+ td.getMsgDepartureNodeName() + SEP + td.getMsgArrivalNodeName() + SEP + td.getMsgSize()
							+ SEP + td.getMsgDepartureDate().getTime() + SEP
							+ (td.getMsgArrivalDate() == null ? NA : td.getMsgArrivalDate().getTime()) + SEP
							+ (td.getAckSize() < 1 ? NA : td.getAckSize()) + SEP
							+ (td.getAckDepartureDate() == null ? NA : td.getAckDepartureDate().getTime()) + SEP
							+ (td.getAckArrivalDate() == null ? NA : td.getAckArrivalDate().getTime()));
			}
		}

		// Write statistics to JSON and CSV files and back-up them (have intermediary statistics files)
		// Gilles Waeber, 2019-04-17
		FileUtil.writeTextToFile(TS_JSON_FILE_PATH, jsonObject.toString(3));
		if (WRITE_TO_CSV)
			FileUtil.writeLinesToFile(TS_CSV_FILE_PATH, lines);
		
		clearDatabase();
		
		FileUtil.writeTextToFile(TS_JSON_FILE_PATH, jsonObject.toString(3));
		if (WRITE_TO_CSV)
			FileUtil.writeLinesToFile(TS_CSV_FILE_PATH, lines);
		
	}

	private static String toString(int[] array) {
		return Arrays.stream(array).mapToObj(i -> "" + i).collect(Collectors.joining(SEP2));
	}

	private String getFormattedType(Type type) {
		switch (type) {
		case RECEIVED:
			return "Received";
		case SENT_NO_ACK:
			return "SentNoAck";
		case SENT_ACK:
			return "SentAck";
		}
		return null;
	}

	@Override
	public void clearDatabase() {
		/*
		 * FileUtil.deleteFile(TS_JSON_FILE_PATH); if (WRITE_TO_CSV)
		 * FileUtil.deleteFile(TS_CSV_FILE_PATH);
		 */
		long currentTime = TakeController.getInstance().getCurrentTimeMillis();
		FileUtil.renameFile(TS_JSON_FILE_PATH, TS_JSON_FILE_PATH + "_" + currentTime);
		if (WRITE_TO_CSV)
			FileUtil.renameFile(TS_CSV_FILE_PATH, TS_CSV_FILE_PATH + "_" + currentTime);
	}

}
