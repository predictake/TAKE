package main.java.take.model.dao;

import java.io.IOException;
import java.util.Set;

import main.java.take.model.UserMessage;

/**
 * 
 * @author Gisler Christophe
 *
 */
public interface UserMessageDAO {

	/**
	 * Save message to DB
	 * 
	 * @param message
	 * @throws IOException
	 */
	public void saveMessage(UserMessage message) throws IOException;

	/**
	 * Delete message from DB
	 * 
	 * @param message
	 */
	public void deleteMessage(UserMessage message);

	/**
	 * Get all messages from DB
	 * 
	 * @return Set of messages sorted by date
	 */
	public Set<UserMessage> getUserMessages();

	/**
	 * Delete all messages from DB
	 */
	public void deleteAllMessages();

}
