package main.java.take.model;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class RoutingTableEntry {

	private String destNodeIP;
	private String nextNodeIP;
	// private int hopCount; // Not available in the OLSR implemented in the
	// military radio
	// private String rssi;
	// private String snr;
	// private String mode;

	public RoutingTableEntry(String destNodeIP,
			String nextNodeIP/* , int hopCount *//* , String rssi, String snr, String mode */) {
		this.destNodeIP = destNodeIP;
		this.nextNodeIP = nextNodeIP;
		// this.hopCount = hopCount;
		// this.rssi = rssi;
		// this.snr = snr;
		// this.mode = mode;
	}

	public String getDestNodeIP() {
		return destNodeIP;
	}

	public String getNextNodeIP() {
		return nextNodeIP;
	}

	/*
	 * public int getHopCount() { return hopCount; }
	 */

	/*
	 * public String getRSSI() { return rssi; }
	 * 
	 * public String getSNR() { return snr; }
	 * 
	 * public String getMode() { return mode; }
	 */

	public boolean hasSameValuesAs(RoutingTableEntry that) {
		return this.destNodeIP.equals(that.destNodeIP) && this.nextNodeIP.equals(that.nextNodeIP)
		/* && this.hopCount == that.hopCount */
		/*
		 * && this.rssi.equals(that.rssi) && this.snr.equals(that.snr) &&
		 * this.mode.equals(that.mode)
		 */;
	}

}