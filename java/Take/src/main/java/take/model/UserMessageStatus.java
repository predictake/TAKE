package main.java.take.model;

import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;

import main.java.take.controller.TakeController;
import main.java.take.model.UserMessage.Status;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class UserMessageStatus {

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private String recipientName;
	private LinkedList<Status> statusHistory = new LinkedList<Status>();
	private LinkedList<Date> datesHistory = new LinkedList<Date>();

	/**
	 * Constructor required by GSon
	 */
	public UserMessageStatus() {
	}

	public UserMessageStatus(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public LinkedList<Status> getStatusHistory() {
		return statusHistory;
	}

	public LinkedList<Date> getDatesHistory() {
		return datesHistory;
	}

	public Status getCurrentStatus() {
		return statusHistory.getLast();
	}

	public Date getCurrentDate() {
		return datesHistory.getLast();
	}

	public synchronized void update(Status status, Date date) {
		statusHistory.add(status);
		datesHistory.add(date);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Message status: ");
		sb.append("\n- Recipient: \t" + recipientName);
		sb.append("\n- Current status: \t" + statusHistory.getLast());
		sb.append("\n- Current date: \t" + DATE_FORMAT.format(datesHistory.getLast()));

		return sb.toString();
	}

}