package main.java.take.model;

import java.io.Serializable;

/**
 * A node is an ALLY military unit with additional information, such as an IP
 * address. Notably represents the data structure of a line of the node settings
 * file.
 * 
 * @author Gisler Christophe
 *
 */
public class Node extends MilitaryUnit implements Comparable<Node>, Serializable {

	private static final long serialVersionUID = -2340027143859241500L;

	private String nodeIP; // Or: nodeRadioIP
	private String radioID;
	private String radioIP; // Or: radioMgtIP

	/**
	 * Constructor required by GSon
	 */
	public Node() {
	}

	public Node(String name, String type, String nodeIP, String radioID, String radioIP) {
		super(Status.ALLY, name, type);
		this.nodeIP = nodeIP;
		this.radioID = radioID;
		this.radioIP = radioIP;
	}

	public String getNodeIP() {
		return nodeIP;
	}

	public void setNodeIP(String nodeIP) {
		this.nodeIP = nodeIP;
	}

	public String getRadioID() {
		return radioID;
	}

	/**
	 * 
	 * @return radio IP (for management)
	 */
	public String getRadioIP() {
		return radioIP;
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Node && this.nodeIP.equals(((Node) obj).getNodeIP()));
	}

	@Override
	public int compareTo(Node that) {
		return this.getNodeIP().compareTo(that.getNodeIP());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Ally Node:");
		sb.append("\n- Name: \t" + name);
		sb.append("\n- Type: \t" + type);
		sb.append("\n- IP: \t" + nodeIP);
		sb.append("\n- Radio ID: \t" + radioID);
		sb.append("\n- Radio IP: \t" + radioIP);
		sb.append("\n- " + getLastGeolocation());

		return sb.toString();
	}

}
