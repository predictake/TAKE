package main.java.take.model;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class DataMessage extends Message {

	public enum Type {
		MSG_STATUS, BFT_RFT_DATA;
	}

	private Type type;
	private List<String> arguments = new ArrayList<String>();

	/**
	 * Constructor required by GSon and Externalizable
	 */
	public DataMessage() {
		super();
	}

	public DataMessage(Type type, Collection<String> arguments, String senderName, String... recipientNames) {
		super(senderName, recipientNames);
		this.type = type;
		this.arguments.addAll(arguments);
	}

	public Type getType() {
		return type;
	}

	public List<String> getArguments() {
		return arguments;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Data message:");
		sb.append("\n- ID: \t" + id);
		sb.append("\n- Date: \t" + DATE_FORMAT.format(date));
		sb.append("\n- From: \t" + senderName);
		sb.append("\n- To:   \t" + String.join(", ", recipientNames));
		sb.append("\n- Type: \t" + type);
		sb.append("\n- Arguments: \n" + String.join("\n  ", arguments));

		return sb.toString();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		super.writeExternal(out);
		out.writeBoolean(type == Type.MSG_STATUS ? false : true);
		out.writeInt(arguments.size());
		for (String argument : arguments)
			out.writeUTF(argument);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		super.readExternal(in);
		type = in.readBoolean() == false ? Type.MSG_STATUS : Type.BFT_RFT_DATA;
		int size = in.readInt();
		for (int i = 0; i < size; i++)
			arguments.add(in.readUTF());
	}

}
