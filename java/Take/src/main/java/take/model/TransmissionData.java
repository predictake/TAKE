package main.java.take.model;

import java.text.DateFormat;
import java.util.Date;

import main.java.take.controller.TakeController;

/**
 * Transmission data about sent and received messages and acknowledgments. Used
 * to compute transmission statistics.
 * 
 * @author Gisler Christophe
 *
 */
public class TransmissionData {

	public enum Type {
		RECEIVED, SENT_ACK, SENT_NO_ACK;
	}

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private Type type;
	private String msgID;
	private String msgDepartureNodeName;
	private String msgArrivalNodeName;
	private int msgSize; // Size in [byte]
	private Date msgDepartureDate;
	private Date msgArrivalDate;
	private int ackSize; // Size in [byte]
	private Date ackDepartureDate;
	private Date ackArrivalDate;

	/**
	 * Constructor required by GSon
	 */
	public TransmissionData() {
	}

	public TransmissionData(Type type, String msgID, String msgDepartureNodeName, String msgArrivalNodeName,
			int msgSize, Date msgDepartureDate, Date msgArrivalDate, int ackSize, Date ackDepartureDate,
			Date ackArrivalDate) {
		this.type = type;
		this.msgID = msgID;
		this.msgDepartureNodeName = msgDepartureNodeName;
		this.msgArrivalNodeName = msgArrivalNodeName;
		this.msgSize = msgSize;
		this.msgDepartureDate = msgDepartureDate;
		this.msgArrivalDate = msgArrivalDate;
		this.ackSize = ackSize;
		this.ackDepartureDate = ackDepartureDate;
		this.ackArrivalDate = ackArrivalDate;
	}

	public Type getType() {
		return type;
	}

	public String getMsgID() {
		return msgID;
	}

	public int getMsgSize() {
		return msgSize;
	}

	public String getMsgDepartureNodeName() {
		return msgDepartureNodeName;
	}

	public String getMsgArrivalNodeName() {
		return msgArrivalNodeName;
	}

	public Date getMsgDepartureDate() {
		return msgDepartureDate;
	}

	public Date getMsgArrivalDate() {
		return msgArrivalDate;
	}

	public int getAckSize() {
		return ackSize;
	}

	public String getAckDepartureNodeName() {
		return msgArrivalNodeName;
	}

	public String getAckArrivalNodeName() {
		return msgDepartureNodeName;
	}

	public Date getAckDepartureDate() {
		return ackDepartureDate;
	}

	public Date getAckArrivalDate() {
		return ackArrivalDate;
	}

	public boolean wasReceivedMessage() {
		return type == Type.RECEIVED;
	}

	public boolean wasSuccessfulSentMessage() {
		return type == Type.SENT_ACK;
	}

	public boolean wasFailedSentMessage() {
		return type == Type.SENT_NO_ACK;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Transmission data:");
		sb.append("\n- MSG type: " + type);
		sb.append("\n- MSG ID: " + msgID);
		sb.append("\n- MSG size: " + msgSize);
		sb.append("\n- MSG departure node: " + msgDepartureNodeName);
		sb.append("\n- MSG departure date: " + DATE_FORMAT.format(msgDepartureDate));
		sb.append("\n- MSG arrival node: " + msgArrivalNodeName);
		if (msgArrivalDate != null)
			sb.append("\n- MSG arrival date: " + DATE_FORMAT.format(msgArrivalDate));
		if (ackSize > 0)
			sb.append("\n- ACK size: " + ackSize);
		if (ackDepartureDate != null)
			sb.append("\n- ACK departure date: " + DATE_FORMAT.format(ackDepartureDate));
		if (ackArrivalDate != null)
			sb.append("\n- ACK arrival date: " + DATE_FORMAT.format(ackArrivalDate));

		return sb.toString();
	}

}
