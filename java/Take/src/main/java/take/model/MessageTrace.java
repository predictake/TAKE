package main.java.take.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import main.java.take.controller.TakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class MessageTrace implements Externalizable {

	// Key = node having got MSG, value = map whose key = node having sent MSG and
	// value = arrival date
	private Map<String, Map<String, Date>> nodesHavingReceivedMsgFromNodesOnDates = Collections
			.synchronizedMap(new LinkedHashMap<String, Map<String, Date>>());

	/**
	 * Constructor required by GSon and Externalizable
	 */
	public MessageTrace() {
	}

	public Map<String, Map<String, Date>> getNodesHavingReceivedMsgFromNodesOnDates() {
		return nodesHavingReceivedMsgFromNodesOnDates;
	}

	public void add(MessageTrace otherTrace) {
		synchronized (this.nodesHavingReceivedMsgFromNodesOnDates) {
			for (String nodeHavingMsg : otherTrace.nodesHavingReceivedMsgFromNodesOnDates.keySet()) {
				Map<String, Date> fromNodesOnDates = otherTrace.nodesHavingReceivedMsgFromNodesOnDates
						.get(nodeHavingMsg);
				if (!this.nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingMsg)) {
					this.nodesHavingReceivedMsgFromNodesOnDates.put(nodeHavingMsg, fromNodesOnDates);
				} else
					for (String fromNode : fromNodesOnDates.keySet()) {
						Date onDate = fromNodesOnDates.get(fromNode);
						if (!this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).containsKey(fromNode)
								|| this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).get(fromNode)
										.after(onDate)) {
							this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).put(fromNode, onDate);
						}
					}
			}
		}
	}

	/**
	 * Return the so-called difference trace, corresponding to the difference
	 * between this trace (A) and the other given trace (B), namely A - B
	 * 
	 * @param otherTrace
	 * @return difference trace
	 */
	public MessageTrace getDifferenceWith(MessageTrace otherTrace) {
		MessageTrace diffTrace = new MessageTrace();
		synchronized (this.nodesHavingReceivedMsgFromNodesOnDates) {
			for (String nodeHavingMsg : this.nodesHavingReceivedMsgFromNodesOnDates.keySet()) {
				Map<String, Date> fromNodesOnDates = this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg);
				if (!otherTrace.nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingMsg)) {
					diffTrace.nodesHavingReceivedMsgFromNodesOnDates.put(nodeHavingMsg, fromNodesOnDates);
				} else
					for (String fromNode : fromNodesOnDates.keySet()) {
						Date onDate = fromNodesOnDates.get(fromNode);
						if (!otherTrace.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).containsKey(fromNode)
								|| otherTrace.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).get(fromNode)
										.after(onDate)) {
							if (!diffTrace.nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingMsg))
								diffTrace.nodesHavingReceivedMsgFromNodesOnDates.put(nodeHavingMsg,
										Collections.synchronizedMap(new TreeMap<String, Date>()));
							diffTrace.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingMsg).put(fromNode, onDate);
						}
					}
			}
		}
		return diffTrace;
	}

	/**
	 * Return a simplified trace containing only the nodes (and dates) to which the
	 * message was first arrived to given nodes
	 * 
	 * @param nodeNames
	 * @return simplified trace
	 */
	public MessageTrace getSimplifiedTraceFor(Set<String> nodeNames) {
		MessageTrace simplifiedTrace = new MessageTrace();
		for (String nodeName : nodeNames) {
			Map<String, Date> fromNodesOnDates = this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeName);
			if (fromNodesOnDates != null) {
				String fromNode = null;
				Date firstArrivalDate = TakeController.getInstance().getCurrentDate();
				for (Entry<String, Date> entry : fromNodesOnDates.entrySet()) {
					if (entry.getValue().before(firstArrivalDate)) {
						fromNode = entry.getKey();
						firstArrivalDate = entry.getValue();
					}
				}
				simplifiedTrace.addNodeHavingReceivedMsgFromNodeOnDate(nodeName, fromNode, firstArrivalDate);
			}
		}
		return simplifiedTrace;
	}

	public void addNodeHavingReceivedMsgFromNodeOnDate(String nodeHavingReceivedMsg, String fromNode, Date onDate) {
		if (nodeHavingReceivedMsg == null || fromNode == null || onDate == null)
			return;
		synchronized (this.nodesHavingReceivedMsgFromNodesOnDates) {
			if (!nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingReceivedMsg))
				nodesHavingReceivedMsgFromNodesOnDates.put(nodeHavingReceivedMsg,
						Collections.synchronizedMap(new TreeMap<String, Date>()));
			if (!nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg).containsKey(fromNode)
					|| nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg).get(fromNode).after(onDate))
				nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg).put(fromNode, onDate);
		}
	}

	public Set<String> getNodesHavingReceivedMsg() {
		return nodesHavingReceivedMsgFromNodesOnDates.keySet();
	}

	/**
	 * Get nodes which sent the message to this node
	 * 
	 * @param nodeHavingReceivedMsg
	 * @return nodeHavingSentMsgToThisNode
	 */
	public Set<String> getNodesPrecedingNode(String nodeHavingReceivedMsg) {
		if (nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingReceivedMsg))
			return nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg).keySet();
		return null;
	}

	public String getFirstNodePrecedingNode(String nodeHavingReceivedMsg) {
		if (!nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeHavingReceivedMsg))
			return null;
		String firstNode = null;
		Date firstDate = null;
		synchronized (this.nodesHavingReceivedMsgFromNodesOnDates) {
			for (Entry<String, Date> entry : this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg)
					.entrySet()) {
				if (firstNode == null || entry.getValue().before(firstDate)) {
					firstNode = entry.getKey();
					firstDate = entry.getValue();
				}
			}
		}
		return firstNode;
	}

	public boolean wasMessageReceivedByNode(String nodeName) {
		return nodesHavingReceivedMsgFromNodesOnDates.containsKey(nodeName);
	}

	public Date getFirstArrivalDateToNode(String nodeName) {
		Map<String, Date> fromNodesOnDates = this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeName);
		if (fromNodesOnDates != null) {
			Date firstArrivalDate = TakeController.getInstance().getCurrentDate();
			for (Date date : fromNodesOnDates.values())
				if (date.before(firstArrivalDate))
					firstArrivalDate = date;
			return firstArrivalDate;
		}
		return null;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		synchronized (this.nodesHavingReceivedMsgFromNodesOnDates) {
			out.writeInt(nodesHavingReceivedMsgFromNodesOnDates.size());
			for (String nodeHavingReceivedMsg : nodesHavingReceivedMsgFromNodesOnDates.keySet()) {
				out.writeUTF(nodeHavingReceivedMsg);
				Map<String, Date> fromNodeArrivalDates = nodesHavingReceivedMsgFromNodesOnDates
						.get(nodeHavingReceivedMsg);
				out.writeInt(fromNodeArrivalDates.size());
				for (Entry<String, Date> entry : fromNodeArrivalDates.entrySet()) {
					out.writeUTF(entry.getKey());
					out.writeLong(entry.getValue().getTime());
				}
			}
		}
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		int size = in.readInt();
		for (int i = 0; i < size; i++) {
			String nodeHavingReceivedMsg = in.readUTF();
			this.nodesHavingReceivedMsgFromNodesOnDates.put(nodeHavingReceivedMsg,
					Collections.synchronizedMap(new TreeMap<String, Date>()));
			int size2 = in.readInt();
			for (int j = 0; j < size2; j++) {
				String fromNode = in.readUTF();
				Date arrivalDate = new Date(in.readLong());
				this.nodesHavingReceivedMsgFromNodesOnDates.get(nodeHavingReceivedMsg).put(fromNode, arrivalDate);
			}
		}
	}

}
