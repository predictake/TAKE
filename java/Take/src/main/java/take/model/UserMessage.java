package main.java.take.model;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import main.java.take.controller.TakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class UserMessage extends Message {

	public enum Type {
		STANDARD, ORDER;
	}

	public enum Status {
		SENT, RECEIVED, READ, ACCEPTED, REFUSED, EXECUTED, FAILED;
	}

	private Type type; // Standard or order
	private String subject; // Title (optional)
	private String content; // Body
	// private File attachment; // Attached file (optional)
	private Map<String, UserMessageStatus> recipientMessageStatus = Collections
			.synchronizedMap(new LinkedHashMap<String, UserMessageStatus>()); // Key = recipient name, value = message
																				// status

	/**
	 * Constructor required by GSon and Externalizable
	 */
	public UserMessage() {
		super();
	}

	public UserMessage(Type type, String subject, String content, File attachment, String senderName,
			String... recipientNames) {
		super(senderName, recipientNames);
		this.type = type;
		this.subject = subject;
		this.content = content;
		// this.attachment = attachment;
		updateStatusForAllRecipients(Status.SENT, this.date);
	}

	public Type getType() {
		return type;
	}

	public String getSubject() {
		return subject;
	}

	public String getContent() {
		return content;
	}

	/*
	 * public File getAttachment() { return attachment; }
	 */

	public List<UserMessageStatus> getRecipientMessageStatus() {
		return new ArrayList<UserMessageStatus>(recipientMessageStatus.values());
	}

	public Status getCurrentStatusForRecipient(String recipientName) {
		return recipientMessageStatus.get(recipientName).getStatusHistory().getLast();
	}

	public boolean hasGotStatusForRecipient(Status status, String recipientName) {
		return recipientMessageStatus.get(recipientName).getStatusHistory().contains(status);
	}

	public UserMessageStatus getMessageStatus(String recipientName) {
		return recipientMessageStatus.get(recipientName);
	}

	public void updateStatus(String recipientName, Status newStatus, Date date) {
		if (!recipientMessageStatus.containsKey(recipientName))
			recipientMessageStatus.put(recipientName, new UserMessageStatus(recipientName));
		recipientMessageStatus.get(recipientName).update(newStatus, date);
	}

	public void updateStatusForAllRecipients(Status status, Date date) {
		for (String recipientName : recipientNames)
			updateStatus(recipientName, status, date);
	}

	/**
	 * Set (update) SENT status for: - successful recipients to RECEIVED with trace
	 * first arrival date; - unsuccessful recipients to FAILED with current date.
	 */
	public void setSentStatusToReceivedOrFailedForRecipients() {
		Date failureDate = TakeController.getInstance().getCurrentDate();
		for (String recipientName : recipientNames) {
			if (getCurrentStatusForRecipient(recipientName) == Status.SENT) {
				if (trace.wasMessageReceivedByNode(recipientName)) {
					updateStatus(recipientName, Status.RECEIVED, trace.getFirstArrivalDateToNode(recipientName));
				} else {
					updateStatus(recipientName, Status.FAILED, failureDate);
				}
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("User message:");
		sb.append("\n- ID: \t" + id);
		sb.append("\n- Date: \t" + DATE_FORMAT.format(date));
		sb.append("\n- From: \t" + senderName);
		sb.append("\n- To:   \t" + String.join(", ", recipientNames));
		sb.append("\n- Type: \t" + type);
		sb.append("\n- Subject: \t " + subject);
		sb.append("\n- Content: \t" + content);
		// sb.append("\n- Attachment: \t " + (attachment != null ? attachment.getName()
		// : "None"));

		return sb.toString();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		super.writeExternal(out);
		out.writeBoolean(type == Type.STANDARD ? false : true);
		out.writeUTF(subject);
		out.writeUTF(content);
		// Not written: recipientMessageStatus
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		super.readExternal(in);
		type = in.readBoolean() == false ? Type.STANDARD : Type.ORDER;
		subject = in.readUTF();
		content = in.readUTF();
		// Not read: recipientMessageStatus
	}

}
