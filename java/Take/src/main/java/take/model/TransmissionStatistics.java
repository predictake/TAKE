package main.java.take.model;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import main.java.take.controller.MessageManager;
import main.java.take.controller.TakeController;
import main.java.take.model.TransmissionData.Type;
import main.java.take.model.dao.TransmissionStatisticsDAO;
import main.java.take.model.dao.TransmissionStatisticsFileDAO;

/**
 * Transmitted data statistics of the TAKE application running in a given node
 * 
 * Note: "User messages" include all messages except BFT/RFT data messages
 * 
 * @author Gisler Christophe
 *
 */
public class TransmissionStatistics implements Iterable<TransmissionData> {

	public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");
	public static final String SEP = "_";

	private final TransmissionStatisticsDAO transmissionStatisticsDAO = new TransmissionStatisticsFileDAO(this);

	private int userMessageRecipients = 0;
	private int userMessagesReceived = 0, userMessagesSent_NoACK = 0, userMessagesSent_ACK = 0;
	private int ipMessagesReceived = 0, ipMessagesSent_NoACK = 0, ipMessagesSent_ACK = 0;
	private List<TransmissionData> transmissionDatas = Collections.synchronizedList(new LinkedList<TransmissionData>());

	private static volatile TransmissionStatistics instance = new TransmissionStatistics();

	public static TransmissionStatistics getInstance() {
		return instance;
	}

	private TransmissionStatistics() {
	}

	/**
	 * Add the given number of message recipients to the sum of all recipients of
	 * all user messages created and sent by this sender node
	 * 
	 * @param userMessageRecipients
	 */
	public void addUserMessageRecipients(int userMessageRecipients) {
		this.userMessageRecipients += userMessageRecipients;
	}

	/**
	 * Get the sum of all user message recipients, i.e. the sum of all recipients of
	 * all user messages created by this sender node
	 * 
	 * @return userMessageRecipients
	 */
	public int getUserMessageRecipients() {
		return userMessageRecipients;
	}

	public synchronized void addUserMessageCompletionData(String messageID, int recipientAckNbr, int recipientNbr) {
		this.userMessagesSent_ACK += recipientAckNbr;
		this.userMessagesSent_NoACK += recipientNbr - recipientAckNbr;
	}

	public void incrementUserMessagesReceived() {
		this.userMessagesReceived++;
	}

	/**
	 * Get the user message completion rate which is equal to the number of user
	 * messages sent from this sender node to other recipient nodes and which were
	 * acknowledged by the latters over the total number of user messages sent from
	 * this sender node to other recipient nodes
	 * 
	 * @return userMessagesSent_ACK / (userMessagesSent_ACK +
	 *         userMessagesSent_NoACK)
	 */
	public synchronized float getUserMessageCompletionRate() {
		int denom = userMessagesSent_ACK + userMessagesSent_NoACK;
		return denom != 0 ? (float) userMessagesSent_ACK / (float) denom : 1.f;
	}

	/**
	 * Get the number of user messages received by this node when it was amongst the
	 * recipients of these messages
	 * 
	 * @return userMessagesReceived
	 */
	public int getUserMessagesReceived() {
		return userMessagesReceived;
	}

	/**
	 * Get the number of ACKs received by this sender node from all the recipients
	 * which it sent user messages to
	 * 
	 * @return userMessagesSent_ACK
	 */
	public int getUserMessagesSent_ACK() {
		return userMessagesSent_ACK;
	}

	/**
	 * Get the number of ACKs NOT received by this sender node from the recipients
	 * which it sent user messages to
	 * 
	 * @return userMessagesSent_NoACK
	 */
	public int getUserMessagesSent_NoACK() {
		return userMessagesSent_NoACK;
	}

	/**
	 * Get the mean round trip time (RTT) in [ms] of all user messages originally
	 * sent by this sender to their recipients.
	 * 
	 * The RTT of a message originally sent from this sender node to another
	 * recipient node is defined by the time difference in [ms] between the arrival
	 * date of the acknowledgment (ACK) informing that the recipient node has
	 * received the message and the departure date of the message from this sender
	 * node.
	 * 
	 * @return user message mean RTT, or -1 if no recipient has received and
	 *         aknowledged any message
	 */
	public synchronized long getUserMessageMeanRoundTripTime() {
		return MessageManager.getInstance().getMeanRoundTripTimeForAllRecipients();
	}

	public List<TransmissionData> getTransmissionDatas() {
		return transmissionDatas;
	}

	public synchronized void addReceivedIPMessageData(Message receivedMessage, String msgDepartureNodeName,
			String msgArrivalNodeName, int msgSize, Date msgDepartureDate, Date msgArrivalDate, int ackSize,
			Date ackDepartureDate) {
		TransmissionData td = new TransmissionData(Type.RECEIVED, receivedMessage.getID(), msgDepartureNodeName,
				msgArrivalNodeName, msgSize, msgDepartureDate, msgArrivalDate, ackSize, ackDepartureDate, null);
		transmissionDatas.add(td);
		ipMessagesReceived++;
		// if (receivedMessage.hasRecipient(msgArrivalNodeName)) // msgArrivalNodeName =
		// current node name
		// userMessagesReceived++;
	}

	public synchronized void addSentIPMessageData_NoACK(Message message, String msgDepartureNodeName,
			String msgArrivalNodeName, int msgSize, Date msgDepartureDate) {
		TransmissionData td = new TransmissionData(Type.SENT_NO_ACK, message.getID(), msgDepartureNodeName,
				msgArrivalNodeName, msgSize, msgDepartureDate, null, 0, null, null);
		transmissionDatas.add(td);
		ipMessagesSent_NoACK++;
		// if (message.hasSender(msgDepartureNodeName)) // msgDepartureNodeName =
		// current node name
		// userMessagesSent_NoACK++;
	}

	public synchronized void addSentIPMessageData_ACK(Message message, String msgDepartureNodeName,
			String msgArrivalNodeName, int msgSize, Date msgDepartureDate, Date msgArrivalDate, int ackSize,
			Date ackDepartureDate, Date ackArrivalDate) {
		TransmissionData td = new TransmissionData(Type.SENT_ACK, message.getID(), msgDepartureNodeName,
				msgArrivalNodeName, msgSize, msgDepartureDate, msgArrivalDate, ackSize, ackDepartureDate,
				ackArrivalDate);
		transmissionDatas.add(td);
		ipMessagesSent_ACK++;
		// if (message.hasSender(msgDepartureNodeName)) // msgDepartureNodeName =
		// current node name
		// userMessagesSent_ACK++;
	}

	/**
	 * Get the IP message completion rate which is equal to the number of messages
	 * sent from this node IP to other node IPs and which were directly acknowledged
	 * over the total number of messages sent from this node IP to other node IPs
	 * 
	 * @return ipMessagesSent_ACK / (ipMessagesSent_ACK + ipMessagesSent_NoACK)
	 */
	public synchronized float getIPMessageCompletionRate() {
		int denom = ipMessagesSent_ACK + ipMessagesSent_NoACK;
		return denom != 0 ? (float) ipMessagesSent_ACK / (float) denom : 1.f;
	}

	/**
	 * Get the number of messages received by this node (IP), which might be not
	 * necessarily a final recipient of these messages (which might just be in
	 * transit).
	 * 
	 * @return ipMessagesReceived
	 */
	public int getIPMessagesReceived() {
		return ipMessagesReceived;
	}

	/**
	 * Get the number of messages sent by this node (IP) to another nodes (IPs)
	 * directly but for which no ACK was received Thus, this node might be not
	 * necessarily the original sender node and the messages might just be in
	 * transit.
	 * 
	 * @return ipMessagesSent_NoACK
	 */
	public int getIPMessagesSent_NoACK() {
		return ipMessagesSent_NoACK;
	}

	/**
	 * Get the number of messages sent by this node (IP) to another nodes (IPs)
	 * directly and for which an ACK was received Thus, this node might be not
	 * necessarily the original sender node and the messages might just be in
	 * transit.
	 * 
	 * @return ipMessagesSent_ACK
	 */
	public int getIPMessagesSent_ACK() {
		return ipMessagesSent_ACK;
	}

	/**
	 * Get the mean round trip time (RTT) in [ms] of all messages sent from this
	 * node (IP) to other nodes (IPs).
	 * 
	 * The RTT of a message sent from this node to another node directly (i.e. from
	 * IP to IP) is defined by the time difference in [ms] between the arrival date
	 * of the acknowledgment (ACK) informing that the other node (IP) has received
	 * the message and the departure date of the message from this node (IP).
	 * 
	 * This RTT is computed for all messages (user messages and BFT/RFT data
	 * messages) sent from IP to IP directly. Note that these messages may have
	 * passed through other intermediary nodes in the sense of a normal IP routing,
	 * via OLSR in our case.
	 * 
	 * @return IP message mean RTT
	 */
	public synchronized long getIPMessageMeanRoundTripTime() {
		long msgNbr = 0l;
		long msgRTTSum = 0l;
		for (TransmissionData td : transmissionDatas) {
			if (td.wasSuccessfulSentMessage()) {
				msgRTTSum += td.getAckArrivalDate().getTime() - td.getMsgDepartureDate().getTime();
				msgNbr++;
			}
		}
		return msgNbr > 0l ? msgRTTSum / msgNbr : 0l;
	}

	/**
	 * Get the mean processing time of the messages received by this node.
	 * 
	 * For a message received, the processing time is defined by the time difference
	 * in [ms] between the message acknowledgment the departure date from this node
	 * and the message arrival date to this node.
	 * 
	 * @return sum of the processing times of all received messages / number of
	 *         received messages
	 */
	public synchronized long getIPMessagesReceivedMeanProcessingTime() {
		long receivedMsgNbr = 0l;
		long receivedMsgPTSum = 0l;
		for (TransmissionData td : transmissionDatas) {
			if (td.wasReceivedMessage()) {
				receivedMsgPTSum += td.getAckDepartureDate().getTime() - td.getMsgArrivalDate().getTime();
				receivedMsgNbr++;
			}
		}
		return receivedMsgNbr > 0l ? receivedMsgPTSum / receivedMsgNbr : 0l;
	}

	/**
	 * Get the rate of nodes (allies) whose geolocation is known over the total
	 * number of nodes.
	 * 
	 * @return nodeKnownGeolocationRate = # nodes with available geolocation / #
	 *         nodes
	 */
	public synchronized float getBFTKnownGeolocationRate() {
		Map<String, Node> nodes = TakeController.getInstance().getNodes();
		long nodeKnownGeolocationNbr = nodes.values().stream().filter(Node::hasLastGeolocation).count();
		return nodeKnownGeolocationNbr != 0 ? (float) nodeKnownGeolocationNbr / (float) nodes.size() : 0f;
	}

	/**
	 * Get an array with the mean time in [s] between ally (node) geolocation
	 * updates received for each node.
	 * 
	 * The returned values correspond to the nodes as listed in the
	 * node_settings.csv file.
	 * 
	 * @return mean times in [s] between ally (node) geolocation updates received
	 *         per node
	 */
	public synchronized int[] getMeanTimesBetweenBFTGeolocationUpdatesReceivedPerNode() {
		Map<String, Node> nodes = TakeController.getInstance().getNodes();
		int[] meanTimeBetweenBFTGeolocationsReceivedPerNode = new int[nodes.size()];
		int n = 0;
		for (Node node : nodes.values()) {
			List<Geolocation> nodeGeolocationsHisto = node.getGeolocationsHistory();
			if (nodeGeolocationsHisto.size() > 1) {
				long deltaTimesSum = 0, deltaTimesNbr = 0;
				for (int i = 1; i < nodeGeolocationsHisto.size(); i++) {
					deltaTimesSum += nodeGeolocationsHisto.get(i).getDate().getTime()
							- nodeGeolocationsHisto.get(i - 1).getDate().getTime();
					deltaTimesNbr++;
				}
				meanTimeBetweenBFTGeolocationsReceivedPerNode[n] = (int) Math
						.rint((double) deltaTimesSum / 1000. / deltaTimesNbr);
			} else {
				meanTimeBetweenBFTGeolocationsReceivedPerNode[n] = -1;
			}
			n++;
		}
		return meanTimeBetweenBFTGeolocationsReceivedPerNode;
	}

	/**
	 * Get an array with the mean time in [s] between enemy geolocation updates
	 * received for each node.
	 * 
	 * The returned values correspond to the nodes as listed in the
	 * node_settings.csv file.
	 * 
	 * @return mean times in [s] between enemy geolocation updates received per node
	 */
	public synchronized int[] getMeanTimesBetweenRFTGeolocationUpdatesReceivedPerNode() {
		Map<String, Node> nodes = TakeController.getInstance().getNodes();
		List<String> nodeNames = new ArrayList<>(nodes.keySet());
		Map<String, MilitaryUnit> enemies = TakeController.getInstance().getEnemyUnits();
		int[] meanTimeBetweenRFTGeolocationsReceivedPerNode = new int[nodes.size()];
		Arrays.fill(meanTimeBetweenRFTGeolocationsReceivedPerNode, -1);
		for (MilitaryUnit enemy : enemies.values()) {
			String detectedByNode = enemy.getName()
					.substring(enemy.getName().lastIndexOf(MilitaryUnit.DETECTED_BY_NAME_SEP)
							+ MilitaryUnit.DETECTED_BY_NAME_SEP.length());
			int nodeIndex = nodeNames.indexOf(detectedByNode);
			List<Geolocation> nodeGeolocationsHisto = enemy.getGeolocationsHistory();
			if (nodeGeolocationsHisto.size() > 1) {
				long deltaTimesSum = 0, deltaTimesNbr = 0;
				for (int i = 1; i < nodeGeolocationsHisto.size(); i++) {
					deltaTimesSum += nodeGeolocationsHisto.get(i).getDate().getTime()
							- nodeGeolocationsHisto.get(i - 1).getDate().getTime();
					deltaTimesNbr++;
				}
				meanTimeBetweenRFTGeolocationsReceivedPerNode[nodeIndex] = (int) Math
						.rint((double) deltaTimesSum / 1000. / deltaTimesNbr); // or += if we then divide by the number
																				// of simulated enemies as in comments
																				// below
			}
		}
		/*
		 * for (int i = 0; i < meanTimeBetweenRFTGeolocationsReceivedPerNode.length;
		 * i++) { if (meanTimeBetweenRFTGeolocationsReceivedPerNode[i] > 0)
		 * meanTimeBetweenRFTGeolocationsReceivedPerNode[i] /=
		 * Preferences.getInstance().getRFTSimulatedEnemies(); }
		 */
		return meanTimeBetweenRFTGeolocationsReceivedPerNode;
	}

	/**
	 * Get an array with the number of ally (node) geolocations received for each
	 * node.
	 * 
	 * The returned values correspond to the nodes as listed in the
	 * node_settings.csv file.
	 * 
	 * @return numbers of ally (node) geolocations received per node
	 */
	public synchronized int[] getNumbersOfBFTGeolocationsReceivedPerNode() {
		Map<String, Node> nodes = TakeController.getInstance().getNodes();
		int[] numberOfBFTGeolocationsReceivedPerNode = new int[nodes.size()];
		int n = 0;
		for (Node node : nodes.values())
			numberOfBFTGeolocationsReceivedPerNode[n++] = node.getGeolocationsHistory().size();
		return numberOfBFTGeolocationsReceivedPerNode;
	}

	/**
	 * Get an array with the number of enemy geolocations received for each node
	 * (independently of the number of enemies).
	 * 
	 * The returned values correspond to the nodes as listed in the
	 * node_settings.csv file.
	 * 
	 * @return numbers of enemy geolocations received per node
	 */
	public synchronized int[] getNumbersOfRFTGeolocationsReceivedPerNode() {
		Map<String, MilitaryUnit> enemies = TakeController.getInstance().getEnemyUnits();
		List<String> nodeNames = new ArrayList<>(TakeController.getInstance().getNodes().keySet());
		int[] numberOfRFTGeolocationsReceivedPerNode = new int[nodeNames.size()];
		for (MilitaryUnit enemy : enemies.values()) {
			String detectedByNode = enemy.getName()
					.substring(enemy.getName().lastIndexOf(MilitaryUnit.DETECTED_BY_NAME_SEP)
							+ MilitaryUnit.DETECTED_BY_NAME_SEP.length());
			int nodeIndex = nodeNames.indexOf(detectedByNode);
			numberOfRFTGeolocationsReceivedPerNode[nodeIndex] = enemy.getGeolocationsHistory().size(); // or += if we
																										// then divide
																										// by the number
																										// of simulated
																										// enemies as in
																										// comments
																										// below
		}
		/*
		 * for (int i = 0; i < numberOfRFTGeolocationsReceivedPerNode.length; i++) { if
		 * (numberOfRFTGeolocationsReceivedPerNode[i] > 0)
		 * numberOfRFTGeolocationsReceivedPerNode[i] /=
		 * Preferences.getInstance().getRFTSimulatedEnemies(); }
		 */
		return numberOfRFTGeolocationsReceivedPerNode;
	}

	public synchronized void saveToDisk() throws IOException {
		transmissionStatisticsDAO.saveToDatabase();
	}

	public synchronized void reset() {
		userMessageRecipients = 0;
		userMessagesReceived = 0;
		userMessagesSent_ACK = 0;
		userMessagesSent_NoACK = 0;
		ipMessagesReceived = 0;
		ipMessagesSent_ACK = 0;
		ipMessagesSent_NoACK = 0;
		transmissionDatas.clear();
		transmissionStatisticsDAO.clearDatabase();
	}

	public static String format(double value) {
		return DECIMAL_FORMAT.format(value);
	}

	@Override
	public Iterator<TransmissionData> iterator() {
		return transmissionDatas.iterator();
	}

}
