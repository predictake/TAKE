package main.java.take.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

import main.java.take.controller.TakeController;

/**
 * Class representing a location with a date (used by class Node)
 * 
 * @author Gisler Christophe
 *
 */
public class Geolocation implements Serializable {

	private static final long serialVersionUID = 1155279298909823900L;

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.####");

	public static final double LONGITUDE_DELTA = 0.4;
	public static final double LATITUDE_DELTA = 0.4;
	public static final double MOVING_SPEED_FACTOR = 1.0 / 400.0;

	private double longitude, latitude;
	private Date date;

	/**
	 * Create dated geo-location with given longitude, latitude and date
	 * 
	 * @param longitude
	 * @param latitude
	 * @param date
	 */
	public Geolocation(double longitude, double latitude, Date date) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.date = date;
	}

	/**
	 * Create dated geo-location with given longitude, latitude and current date
	 * 
	 * @param longitude
	 * @param latitude
	 */
	public Geolocation(double longitude, double latitude) {
		this(longitude, latitude, new Date());
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public Date getDate() {
		return date;
	}

	public boolean hasSameLocationAs(Geolocation that) {
		return this.latitude == that.latitude && this.longitude == that.longitude;
	}

	@Override
	public String toString() {
		return "Geolocation (latitude, longitude) on " + DATE_FORMAT.format(date) + ": " + latitude + ", " + longitude;
	}

	public static Geolocation newRandomGeolocation() {
		// Round number to 4 decimals
		double newLongitude = Double.parseDouble(DECIMAL_FORMAT.format(Math.random() * LONGITUDE_DELTA));
		double newLatitude = Double.parseDouble(DECIMAL_FORMAT.format(Math.random() * LATITUDE_DELTA));
		return new Geolocation(newLongitude, newLatitude);
	}

	public static Geolocation newRandomlyMovedGeolocation(Geolocation currentGeolocation) {
		double lonSign = Math.random() < 0.5 ? -1.0 : 1.0;
		double latSign = Math.random() < 0.5 ? -1.0 : 1.0;
		// Round number to 4 decimals
		double newLongitude = Double.parseDouble(DECIMAL_FORMAT
				.format(currentGeolocation.getLongitude() + lonSign * Math.random() * LONGITUDE_DELTA / 100.0));
		double newLatitude = Double.parseDouble(DECIMAL_FORMAT
				.format(currentGeolocation.getLatitude() + latSign * Math.random() * LATITUDE_DELTA / 100.0));
		return new Geolocation(newLongitude, newLatitude);
	}

}
