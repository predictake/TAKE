package main.java.take.model;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class RoutingTable {

	private Date time;
	/* Key = destination node IP, value = routing table entry */
	private SortedMap<String, RoutingTableEntry> entries = Collections
			.synchronizedSortedMap(new TreeMap<String, RoutingTableEntry>());

	public RoutingTable(Date time) {
		this.time = time;
	}

	public Date getTime() {
		return time;
	}

	public SortedMap<String, RoutingTableEntry> getEntries() {
		return entries;
	}

	public RoutingTableEntry getEntry(String destNodeIP) {
		return entries.get(destNodeIP);
	}

	public boolean containsEntry(String destNodeIP) {
		return entries.containsKey(destNodeIP);
	}

	public void addEntry(String destNodeIP, String nextNodeIP/* , int hopCount */) {
		entries.put(destNodeIP, new RoutingTableEntry(destNodeIP, nextNodeIP/* , hopCount */));
	}

	public void addEntry(RoutingTableEntry rtEntry) {
		entries.put(rtEntry.getDestNodeIP(), rtEntry);
	}

	public boolean hasSameEntriesAs(RoutingTable that) {
		if (this.entries.size() != that.entries.size())
			return false;
		for (String destNodeIP : this.entries.keySet()) {
			RoutingTableEntry thisRTE = this.entries.get(destNodeIP);
			RoutingTableEntry thatRTE = that.entries.get(destNodeIP);
			if (thatRTE == null || !thatRTE.hasSameValuesAs(thisRTE))
				return false;
		}
		return true;
	}

	public Set<String> getDestNodeIPsWithSameNextNodeIPAs(String destNodeIP) {
		Set<String> destNodeIPs = new LinkedHashSet<String>();
		if (entries.containsKey(destNodeIP)) {
			String nextNodeIP = entries.get(destNodeIP).getNextNodeIP();
			for (RoutingTableEntry rte : entries.values())
				if (!rte.getDestNodeIP().equals(destNodeIP) && rte.getNextNodeIP().equals(nextNodeIP))
					destNodeIPs.add(rte.getDestNodeIP());
		}
		return destNodeIPs;
	}

	public Set<String> getNeighborNodeIPs(String currentNodeIP) {
		Set<String> neighborNodeIPs = new LinkedHashSet<String>();
		for (RoutingTableEntry rte : entries.values())
			if (!rte.getDestNodeIP().equals(currentNodeIP) && rte.getDestNodeIP().equals(rte.getNextNodeIP()))
				neighborNodeIPs.add(rte.getDestNodeIP());
		return neighborNodeIPs;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Routing table: ");
		sb.append("- Date: \t" + time);
		// sb.append("- Node IP: \t" + currentNodeIP);
		sb.append("- Entries (" + entries.size() + "): \n");
		sb.append("  #.\t Dest. Node IP\t Next Node IP");// \t Hop Count\n");
		int i = 0;
		for (RoutingTableEntry rte : entries.values())
			sb.append("  " + (++i) + ".\t " + rte.getDestNodeIP() + "\t "
					+ rte.getNextNodeIP() /* + "\t " + rte.getHopCount() */);

		return sb.toString();
	}

	public int size() {
		return entries.size();
	}

	public boolean isEmpty() {
		return entries.isEmpty();
	}

}
