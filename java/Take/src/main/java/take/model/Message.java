package main.java.take.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import main.java.take.controller.TakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class Message implements Externalizable, Comparable<Message> {

	protected static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	protected String id; // String with <dateInMillis>_<senderName>
	protected Date date; // Creation date (just before first sending)
	protected String senderName; // Sender node name
	protected Set<String> recipientNames = new LinkedHashSet<String>(); // Recipient node name(s)
	protected Date departureDate; // Departure date from last visited node
	protected MessageTrace trace = new MessageTrace(); // Includes nodes having already received the message from other nodes on dates
	protected Map<String, Long> recipientRTTs = Collections.synchronizedMap(new LinkedHashMap<String, Long>()); 
													// Key = recipient name, value = message RTT for recipient

	/**
	 * Constructor required by GSon and Externalizable
	 */
	public Message() {
	}

	public Message(String senderName, String... recipientNames) {
		this.date = TakeController.getInstance().getCurrentDate(); // Current date and time
		this.senderName = senderName;
		this.id = getID(date, senderName);
		this.recipientNames.addAll(Arrays.asList(recipientNames));
	}

	/**
	 * Get a unique ID for this sending
	 * 
	 * @return String with <dateInMillis>_<senderName>
	 */
	public static String getID(Date date, String senderName) {
		return date.getTime() + "_" + senderName;
	}

	public String getID() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public String getSenderName() {
		return senderName;
	}

	public boolean hasSender(String nodeName) {
		return senderName.equals(nodeName);
	}

	public Set<String> getRecipientNames() {
		return recipientNames;
	}

	public boolean hasRecipient(String nodeName) {
		return recipientNames.contains(nodeName);
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public MessageTrace getTrace() {
		return trace;
	}

	public void setTrace(MessageTrace trace) {
		this.trace = trace;
	}

	public MessageTrace getRecipientsTrace() {
		return trace.getSimplifiedTraceFor(recipientNames);
	}

	public Map<String, Long> getRoundTripTimesForRecipients() {
		return recipientRTTs;
	}

	public long getRoundTripTimeForRecipient(String recipientName) {
		return recipientRTTs.get(recipientName);
	}

	public void setRoundTripTimeForRecipientIfNotSet(String recipientName, Date ackArrivalDate) {
		if (!recipientRTTs.containsKey(recipientName)) {
			long recipientRTT = ackArrivalDate.getTime() - date.getTime();
			recipientRTTs.put(recipientName, recipientRTT);
		}
	}

	@Override
	public int compareTo(Message that) {
		return this.getID().compareTo(that.getID());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Message:");
		sb.append("\n- ID: \t" + id);
		sb.append("\n- Date: \t" + DATE_FORMAT.format(date));
		sb.append("\n- From: \t" + senderName);
		sb.append("\n- To:   \t" + String.join(", ", recipientNames));
		// sb.append("\n- Nodes having received message: \t" + String.join(", ",
		// trace.getNodesHavingReceivedMsg()));

		return sb.toString();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(date.getTime());
		out.writeUTF(senderName);
		out.writeInt(recipientNames.size());
		for (String recipientName : recipientNames)
			out.writeUTF(recipientName);
		out.writeLong(departureDate.getTime());
		out.writeObject(trace);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		date = new Date(in.readLong());
		senderName = in.readUTF();
		this.id = getID(date, senderName);
		int size = in.readInt();
		for (int i = 0; i < size; i++)
			recipientNames.add(in.readUTF());
		departureDate = new Date(in.readLong());
		trace = (MessageTrace) in.readObject();
	}

}
