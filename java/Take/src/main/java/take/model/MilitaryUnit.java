package main.java.take.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class MilitaryUnit implements Serializable {

	private static final long serialVersionUID = 5606827781419936652L;

	public static final String DETECTED_BY_NAME_SEP = "_By_";

	public enum Status {
		ALLY, ENNEMY, UNKNOWN;
	}

	protected Status status; // See above
	protected String name; // Name (ID) displayed in the GUI => Must be the unique between all (ally) nodes
	protected String type; // Defined by the army, e.g.: "Soldier", "Section", "Jeep", "Tank",... ,
							// "Unknown"
	// protected double transmissionPower; // In a first time, all nodes are
	// considered to have a equal power
	protected List<Geolocation> geolocationsHistory = Collections.synchronizedList(new LinkedList<Geolocation>());
	protected Set<String> relatedNodesHavingCurrentGeolocation = new TreeSet<String>();

	/**
	 * Constructor required by GSon
	 */
	public MilitaryUnit() {
	}

	public MilitaryUnit(Status status, String name, String type) {
		this.status = status;
		this.name = name;
		this.type = type;
	}

	public Status getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Geolocation> getGeolocationsHistory() {
		return geolocationsHistory;
	}

	public void setGeolocationsHistory(List<Geolocation> list) {
		this.geolocationsHistory = list;
	}

	public void resetGeolocationsHistory() {
		geolocationsHistory.clear();
		relatedNodesHavingCurrentGeolocation.clear();
	}

	public synchronized Geolocation getLastGeolocation() {
		if (!geolocationsHistory.isEmpty())
			return geolocationsHistory.get(geolocationsHistory.size() - 1);
		return null;
	}

	public synchronized boolean hasLastGeolocation() {
		return getLastGeolocation() != null;
	}

	/**
	 * Update last geolocation if its date occurs after the date of current one (if
	 * any).
	 * 
	 * @param newGeolocation
	 * @retrun true if geolocation was updated, false otherwise
	 */
	public synchronized boolean updateGeolocation(Geolocation newGeolocation) {
		if (this.geolocationsHistory.isEmpty() || newGeolocation.getDate().after(this.getLastGeolocation().getDate())) {
			this.geolocationsHistory.add(newGeolocation);
			relatedNodesHavingCurrentGeolocation.clear();
			return true;
		}
		return false;
	}

	public Set<String> getRelatedNodesHavingCurrentGeolocation() {
		return relatedNodesHavingCurrentGeolocation;
	}

	/**
	 * Test if this unit is located inside the circle area (boundaries included)
	 * delimited by the given center and radius.
	 * 
	 * @param center
	 *            (geolocation, i.e. GPS point with latitude and longitude)
	 * @param radius
	 *            in [km]
	 * @return true if unit is inside the circle area, i.e. if the distance between
	 *         center and the current location is less or equal to the radius value,
	 *         or false otherwise.
	 */
	public boolean isInsideCircelArea(Geolocation center, float radius) {
		return getDistance(center, getLastGeolocation()) <= radius;
	}

	/**
	 * Test if this unit is located inside the area (boundaries included) delimited
	 * by the given list of points.
	 * 
	 * @see http://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
	 * 
	 *      This method return valid result if the polygon delimited by the given
	 *      GPS points is not too big, i.e. can be considered as flat on the surface
	 *      of the Earth.
	 * @see http://alienryderflex.com/polygon
	 * 
	 * @param list
	 *            of GPS locations (points of polygon) delimiting the map area
	 * @return true if unit is inside the map area delimited by the given GPS
	 *         locations, or false otherwise.
	 */
	public boolean isInsidePolygonArea(List<Geolocation> polygonGPSPoints) {
		Geolocation currentGPSPoint = getLastGeolocation();
		int i, j;
		boolean c = false;
		for (i = 0, j = polygonGPSPoints.size() - 1; i < polygonGPSPoints.size(); j = i++) {
			if (((polygonGPSPoints.get(i).getLatitude() >= currentGPSPoint.getLatitude()) != (polygonGPSPoints.get(j)
					.getLatitude() >= currentGPSPoint.getLatitude()))
					&& (currentGPSPoint.getLongitude() <= (polygonGPSPoints.get(j).getLongitude()
							- polygonGPSPoints.get(i).getLongitude())
							* (currentGPSPoint.getLatitude() - polygonGPSPoints.get(i).getLatitude())
							/ (polygonGPSPoints.get(j).getLatitude() - polygonGPSPoints.get(i).getLatitude())
							+ polygonGPSPoints.get(i).getLongitude()))
				c = !c;
		}
		return c;
	}

	/**
	 * Return the distance in [km} between two GPS points given by their longitude
	 * and latitude values.
	 * 
	 * @see Haversine formula: https://en.wikipedia.org/wiki/Haversine_formula
	 * 
	 * @param gpsPoint1
	 * @param gpsPoint2
	 * @return distance in [km] between the 2 given GPS points
	 */
	public int getDistance(Geolocation gpsPoint1, Geolocation gpsPoint2) {
		double lon1 = gpsPoint1.getLongitude();
		double lat1 = gpsPoint1.getLatitude();
		double lon2 = gpsPoint2.getLongitude();
		double lat2 = gpsPoint2.getLatitude();

		double lonDistance = Math.toRadians(lon1 - lon2);
		double latDistance = Math.toRadians(lat1 - lat2);

		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		// Average radius of the Earth in [km]: 6371
		return (int) (Math.round(6371 * c));
	}

	/**
	 * Simulate the movement/displacement, i.e. perform a random geolocation update
	 * of this unit.
	 */
	public void simulateMovement() {
		Geolocation lastGeolocation = getLastGeolocation();
		if (lastGeolocation == null)
			return;
		updateGeolocation(Geolocation.newRandomlyMovedGeolocation(lastGeolocation));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Military Unit:");
		sb.append("\n- Status: \t" + status);
		sb.append("\n- Name: \t" + name);
		sb.append("\n- Type: \t" + type);
		sb.append("\n- " + getLastGeolocation());

		return sb.toString();
	}

}
