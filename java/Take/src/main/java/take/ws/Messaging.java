package main.java.take.ws;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import main.java.take.controller.MessageManager;
import main.java.take.controller.TakeController;
import main.java.take.model.DataMessage;
import main.java.take.model.Message;
import main.java.take.model.UserMessage;
import main.java.take.model.UserMessage.Type;
import main.java.take.model.UserMessage.Status;
import main.java.take.model.UserMessageStatus;

/**
 * 
 * @author Gisler Christophe
 *
 */
@Path("/msg")
public class Messaging {

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private static final String SEP = ",";

	private static final Logger LOGGER = LoggerFactory.getLogger(Messaging.class);

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Get node status of either: - all nodes (if null 'nodeNames' parameter); -
	 * node(s) whose name(s) is (are) given (if set 'nodeNames' parameter).
	 * 
	 * @param nodeNames:
	 *            (optional) comma-separated list of node names: "node0,node1,..."
	 * @return JSON response containing the list of node name(s) and status
	 */
	@Path("/getnodestatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNodeStatus(@QueryParam("nodenames") String nodeNames) {
		TakeController tc = TakeController.getInstance();

		String[] nodeNamesArray = nodeNames == null ? tc.getNodes().keySet().toArray(new String[0])
				: nodeNames.split(SEP);
		boolean[] nodeStatus = tc.getNodeStatus(nodeNamesArray);

		JSONObject jsonObject2 = new JSONObject();
		for (int i = 0; i < nodeNamesArray.length; i++)
			jsonObject2.put(nodeNamesArray[i], nodeStatus[i]);
		JSONObject jsonObject1 = new JSONObject();
		jsonObject1.put("nodeStatus", jsonObject2);

		LOGGER.info("Get node status of nodes {}", String.join(SEP + " ", nodeNamesArray));

		String result = jsonObject1.toString(3);
		return Response.status(200).entity(result).build();
	}

	/**
	 * Send a standard/order message to either: - all nodes (if null 'nodeNames'
	 * parameter); - node(s) with the given ID(s) (if set 'nodeNames' parameter).
	 * 
	 * @param type:
	 *            standard (1) or order (2) message
	 * @param title:
	 *            subject of the message
	 * @param content:
	 *            body of the message
	 * @param nodeNames:
	 *            (optional) comma-separated list of node name(s): "node0,node1,..."
	 * @return JSON response containing the sent message including its ID
	 */
	@Path("/sendmessage")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendMessage(@FormParam("type") int type, @FormParam("title") String title,
			@FormParam("content") String content,
			@FormParam("nodenames") String nodeNames/* , @Suspended final AsyncResponse asyncResponse */) {
		TakeController tc = TakeController.getInstance();

		// Get required information to create the message
		String senderName = tc.getCurrentNode().getName();
		String[] recipientNames = null;
		if (nodeNames == null)
			recipientNames = tc.getNodes().keySet().stream().filter(x -> !x.equals(senderName))
					.toArray(size -> new String[size]);
		else
			recipientNames = nodeNames.split(SEP);
		Type msgType = type == 2 ? Type.ORDER : Type.STANDARD;

		// Create and send the message
		Message message = new UserMessage(msgType, title, content, null, senderName, recipientNames);
		tc.send(message);

		LOGGER.info("User message '{}' with subject '{}' sent from {} to {} on {}", message.getID(), title, senderName,
				String.join(SEP + " ", recipientNames), DATE_FORMAT.format(message.getDate()));

		String result = GSON.toJson(message);
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the received or sent standard/order messages either from: - the last
	 * check date and time (if null 'fromdate' parameter); - the given date and time
	 * (if set 'fromdate' parameter)
	 * 
	 * @param statusID:
	 *            (0) received, (1) sent
	 * @param fromdate:
	 *            (optional) date and time in standard format "yyyy-MM-dd_HH-mm-ss"
	 * @return JSON response containing the list of received or sent messages (with
	 *         type, date, sender node,...)
	 */
	@Path("/getmessages")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessages(@QueryParam("statusid") int statusID, @QueryParam("fromdate") String fromDate) {
		// Retrieve from the mailbox user messages received after the given date
		Date date = null;
		try {
			if (fromDate != null)
				date = TakeController.DATE_FORMAT_FILE.parse(fromDate);
		} catch (ParseException e) {
			LOGGER.error("Get messages: date parsing error", e);
			return Response.status(400).entity("Date parsing error: " + fromDate).build();
		}
		String currentNodeName = TakeController.getInstance().getCurrentNode().getName();
		SortedSet<UserMessage> receivedMessages = MessageManager.getInstance()
				.getUserMessagesReceivedByRecipientAfterDate(currentNodeName, date);

		LOGGER.info("Get messages: {} received messages", receivedMessages.size());

		String result = GSON.toJson(receivedMessages);
		return Response.status(200).entity(result).build();
	}

	/**
	 * Send the updated status (with the update date) of a received standard/order
	 * message to its sender node only (i.e. not to other recipients).
	 * 
	 * @param messageID:
	 *            ID of the sent message (it was previously returned by the
	 *            sendMessage method)
	 * @param status:
	 *            'RECEIVED', 'READ', 'ACCEPTED', 'REFUSED', 'EXECUTED', 'FAILED'
	 *            (the 4 last for are order messages only)
	 * @return JSON response containing the sent data message including its
	 *         arguments (message status, date,...)
	 */
	@Path("/sendmessagestatus")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendMessageStatus(@FormParam("messageid") String messageID,
			@FormParam("status") String status/* , @Suspended final AsyncResponse asyncResponse */) {
		TakeController tc = TakeController.getInstance();

		// Get required information to update the message status and create the message
		// to send
		String senderName = tc.getCurrentNode().getName();
		/*
		 * Date sendingDate = null; try { sendingDate =
		 * NetworkManager.DATE_FORMAT_FILE.parse(sentDate); } catch (ParseException e) {
		 * e.printStackTrace(); } String messageID = UserMessage.getID(sendingDate,
		 * senderName);
		 */
		UserMessage userMessage = MessageManager.getInstance().getUserMessage(messageID);
		if (userMessage == null)
			return Response.status(400).entity("No existing user message with ID: " + messageID).build();
		Status updatedStatus = Status.valueOf(status.toUpperCase());
		if (updatedStatus == null)
			return Response.status(400).entity("Unknown status: " + status).build();
		List<String> arguments = new ArrayList<String>(2);
		arguments.add(messageID);
		arguments.add(updatedStatus.name());
		String recipientName = userMessage.getSenderName();

		// Update status of the message stored in mailbox
		userMessage.updateStatus(senderName, updatedStatus, new Date());

		// Create and send the message
		DataMessage dataMessage = new DataMessage(DataMessage.Type.MSG_STATUS, arguments, senderName, recipientName);
		tc.send(dataMessage);

		LOGGER.info("Data message '{}' of type {} sent from {} to {} on {}", dataMessage.getID(), dataMessage.getType(),
				senderName, recipientName, DATE_FORMAT.format(dataMessage.getDate()));

		String result = GSON.toJson(dataMessage);
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the status of a sent standard/order message relatively to all its
	 * recipient node(s). As the status of a sent message is sent back to the sender
	 * (only) each time it is updated by each recipient, this method only asks for
	 * current message status locally, i.e. on current node.
	 * 
	 * @param messageID:
	 *            ID of the sent message (it was previously returned by the
	 *            sendMessage method)
	 * @return JSON response containing the message including its status
	 */
	@Path("/getmessagestatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessageStatus(@QueryParam("messageid") String messageID) {
		UserMessage message = MessageManager.getInstance().getUserMessage(messageID);

		// Get the message status for all its recipients
		List<UserMessageStatus> messageStatus = message.getRecipientMessageStatus();
		StringBuilder sb = new StringBuilder();
		for (UserMessageStatus ms : messageStatus)
			sb.append("\n - " + ms.getRecipientName() + ": " + ms.getCurrentStatus().ordinal());

		LOGGER.info("Recipients' message status for message '{}' sent on {}:{}", messageID,
				DATE_FORMAT.format(message.getDate()), sb.toString());

		String result = GSON.toJson(message);
		return Response.status(200).entity(result).build();
	}

	/* ************ */
	/* TEST METHODS */
	/* ************ */

	@Path("/sendmsgtest/{recipientname}/{subject}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendMessageTest(@PathParam("recipientname") String recipientName,
			@PathParam("subject") String subject/* , @Suspended final AsyncResponse asyncResponse */) {
		TakeController mc = TakeController.getInstance();

		// Get required information to create the message
		String senderName = mc.getCurrentNode().getName();
		String senderIP = mc.getCurrentNode().getNodeIP();
		String recipientIP = mc.getNodes().get(recipientName).getNodeIP();

		// Create and send the message
		Message message = new UserMessage(Type.STANDARD, subject, "Hello, this is a content. Bye!", null, senderName,
				recipientName);
		mc.send(message);

		LOGGER.info("(Test) User message '{}' with subject '{}' sent from {} ({}) to {} ({}) on {}", message.getID(),
				subject, senderName, senderIP, recipientName, recipientIP, DATE_FORMAT.format(message.getDate()));

		String result = GSON.toJson(message);
		return Response.status(200).entity(result).build();
	}

	/**
	 * Retrieve the received user messages from the mailbox
	 * 
	 * @return received messages in GSON format
	 */
	@Path("/getmsgtest")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessageTest() {
		String currentNodeName = TakeController.getInstance().getCurrentNode().getName();
		SortedSet<UserMessage> receivedMessages = MessageManager.getInstance()
				.getUserMessagesReceivedByRecipientAfterDate(currentNodeName, null);

		LOGGER.info("(Test) Get messages: {} received messages", receivedMessages.size());

		String result = GSON.toJson(receivedMessages);
		return Response.status(200).entity(result).build();
	}

}
