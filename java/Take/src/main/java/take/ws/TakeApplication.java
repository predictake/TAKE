package main.java.take.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.controller.Preferences;
import main.java.take.controller.TakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
@Path("/app")
public class TakeApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(TakeApplication.class);

	// private static final Gson GSON = new
	// GsonBuilder().setPrettyPrinting().create();

	/**
	 * Get the version of the Take application.
	 * 
	 * @return TEXT response containing the version of the Take application
	 */
	@Path("/getversion")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getVersion() {
		String version = Preferences.getInstance().getAppVersion();

		LOGGER.info("Get Take application version: {}", version);

		String result = "V" + version;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the configuration properties contained in the config.properties file of
	 * the Take application.
	 * 
	 * @return TEXT response containing the configuration properties of the Take
	 *         application
	 */
	@Path("/getconfig")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getConfig() {
		String appConfig = Preferences.getInstance().getFormattedAppConfig();

		LOGGER.info("Get Take application configuration properties:\n{}", appConfig);

		String result = "Take application configuration properties:\n" + appConfig;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get node settings, i.e. the node name (ID), type and IP address of all nodes,
	 * as well as the radio ID and radio IP address, when the application is run in
	 * real mode (see configuration file).
	 * 
	 * @return TEXT response containing the node settings of the Take application.
	 */
	@Path("/getnodesettings")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getNodeSettings() {
		String nodeSettings = Preferences.getInstance().getFormattedNodeSettings();

		LOGGER.info("Get Take application node settings:\n{}", nodeSettings);

		String result = "Take Application node settings:\n" + nodeSettings;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the current node name from the config.properties file of the Take
	 * application.
	 * 
	 * @return TEXT response containing the current node name of the Take
	 *         application
	 */
	@Path("/getnodename")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getNodeName() {
		String nodeName = Preferences.getInstance().getNodeName();

		LOGGER.info("Get current node name from the Take application: {}", nodeName);

		// String result = "Current node name from the Take application:" + nodeName;
		return Response.status(200).entity(nodeName/* result */).build();
	}

	/**
	 * Set the current node name of the Take application.
	 * 
	 * @param nodeName
	 * @return TEXT message with confirmation
	 */
	/*
	 * @Path("/setnodename")
	 * 
	 * @POST
	 * 
	 * @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	 * 
	 * @Produces(MediaType.TEXT_PLAIN) public Response
	 * setNodeName(@FormParam("nodename") String nodeName) {
	 * TakeController.getInstance().setCurrentNode(nodeName);
	 * Preferences.getInstance().setNodeName(nodeName);
	 * 
	 * LOGGER.info("Set Take application current node name to '{}'", nodeName);
	 * 
	 * String result = "Take Application node name:\n" + nodeName; return
	 * Response.status(200).entity(result).build(); }
	 */

	/**
	 * Set the message sending strategy to use by the Take application.
	 * 
	 * @param sendingStrategyName:
	 *            name of the message sending strategy to use
	 * @param responseTimeout:
	 *            timeout for response (ACK for direct sent message)
	 * @return TEXT message with confirmation
	 */
	@Path("/setsendingstrategy")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response setSendingStrategy(@FormParam("strategy") String sendingStrategyName,
			@FormParam("timeout") String responseTimeout) {
		Preferences.getInstance().setSendingStrategy(sendingStrategyName, responseTimeout);
		TakeController.getInstance().setSendingStrategy(sendingStrategyName, Integer.parseInt(responseTimeout));

		LOGGER.info("Set Take sending strategy to '{}' with response timeout of {} seconds", sendingStrategyName,
				responseTimeout);

		String result = "Take sending strategy set to: " + sendingStrategyName + "\nResponse timeout set to: "
				+ responseTimeout + " seconds";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Save the transmission statistics of the Take application to disk.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/savetransmissionstats")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response saveTransmissionStatistics() {
		TakeController.getInstance().saveTransmissionStatisticsToDisk();

		LOGGER.info("Save Take transmission statistics to disk");

		String result = "Take transmission statistics saved to disk";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Reset the Take application stored data.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/reset")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response reset() {
		TakeController.getInstance().reset();

		LOGGER.info("Reset Take application");

		String result = "Take Application reset";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Quit the Take application.
	 */
	@Path("/quit")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response quit() {
		TakeController.getInstance().quit(2);

		LOGGER.info("Quit Take application");

		String result = "Take Application quit";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Force the sending of BFT/RFT data messages to neighbor nodes even if no new
	 * or updated information is available. Thus, all information will be (re)sent
	 * to every neighbor nodes each BFT/RFT message sending period.
	 * 
	 * @param forced:
	 * @return TEXT message with confirmation
	 */
	@Path("/setforcebftrftmsgsending")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response setForceBFTRFTMessageSending(@FormParam("forced") boolean forced) {
		Preferences.getInstance().setForceBFTRFTMessageSending(forced);

		LOGGER.info("Force BFT/RFT message sending set to: {}", forced);

		String result = "BFT/RFT message sending " + (forced ? "" : "not ") + "forced";
		return Response.status(200).entity(result).build();
	}

}
