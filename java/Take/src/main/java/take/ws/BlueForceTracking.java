package main.java.take.ws;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.controller.TakeController;
import main.java.take.controller.Preferences;
import main.java.take.model.Geolocation;
import main.java.take.model.MilitaryUnit;

/**
 * 
 * @author Gisler Christophe
 *
 */
@Path("/bft")
public class BlueForceTracking {

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private static final Logger LOGGER = LoggerFactory.getLogger(BlueForceTracking.class);

	private static final String SEP1 = ",", SEP2 = "_";

	/**
	 * Set the data message sending period in [s] of the BFT service.
	 * 
	 * @param data
	 *            message sending period in [s]
	 * @return TEXT message with confirmation
	 */
	@Path("/setmessagesendingperiod")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response setMessageSendingPeriod(@FormParam("period") int period) {
		// Set the data message sending period in [s] of the BFT service.
		Preferences.getInstance().setBFTMessageSendingPeriod(period);
		TakeController.getInstance().setBFTMessageSendingPeriod(period, 0);

		LOGGER.info("Data message sending period of the BFT service set to {} seconds", period);

		String result = "Data message sending period of the BFT service set to " + period + " seconds";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the dated GPS locations and types of either: - all available allies (if
	 * null 'radius' and 'polygonPoints' parameters); - allies located in the given
	 * circle radius in [km] relatively to the current location (if set 'radius'
	 * parameter); - allies located in the given polygon area (if set
	 * 'polygonPoints' parameter).
	 * 
	 * N.B.: Each (ally) node automatically sends the required information (GPS
	 * dated locations, types,...) about itself and the known enemies to all (ally)
	 * nodes.
	 * 
	 * @param radius:
	 *            (optional) circle radius given in [km]
	 * @param points:
	 *            (optional) polygon points delimited by a comma-separated list of
	 *            GPS points: "lat0_lon0,lat1_lon1,..."
	 * @return JSON response containing the list of dated GPS locations and types of
	 *         all allies (nodes)
	 */
	@Path("/getallylocations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllyLocations(@QueryParam("radius") float circleRadius,
			@QueryParam("points") String polygonPoints) {
		TakeController mc = TakeController.getInstance();

		// Get requested ally military units
		Map<String, ? extends MilitaryUnit> allies = null;
		final boolean areAllies = true;
		if (circleRadius == 0f && polygonPoints == null) {
			LOGGER.info("Get all ally locations");
			allies = mc.getAllMilitaryUnits(true);
		} else if (circleRadius != 0f) {
			LOGGER.info("Get all ally locations in a radius of {} km", circleRadius);
			allies = mc.getMilitaryUnitsInCircleArea(areAllies, circleRadius);
		} else {
			LOGGER.info("Get all ally locations in the polygon delimited by: {}", polygonPoints);
			String[] pointsData = polygonPoints.split(SEP1);
			List<Geolocation> points = new ArrayList<Geolocation>(pointsData.length);
			for (String pd : pointsData) {
				String[] pointData = pd.split(SEP2);
				double longitude = Double.parseDouble(pointData[0]);
				double latitude = Double.parseDouble(pointData[1]);
				points.add(new Geolocation(longitude, latitude));
			}
			allies = mc.getMilitaryUnitsInPolygonArea(areAllies, points);
		}

		// Build JSON response containing the types and dated GPS locations of all
		// requested ally nodes
		JSONObject jsonObject = new JSONObject();
		for (String allyName : allies.keySet()) {
			MilitaryUnit ally = allies.get(allyName);
			Geolocation allyGeolocation = ally.getLastGeolocation();
			JSONObject jsonObject2 = new JSONObject();
			jsonObject2.put("Type", ally.getType());
			jsonObject2.put("Longitude", allyGeolocation.getLongitude());
			jsonObject2.put("Latitude", allyGeolocation.getLatitude());
			jsonObject2.put("Date", DATE_FORMAT.format(allyGeolocation.getDate()));
			jsonObject.put("Ally '" + ally.getName() + "'", jsonObject2);
		}

		String result = "" + jsonObject;
		return Response.status(200).entity(result).build();
	}

}
