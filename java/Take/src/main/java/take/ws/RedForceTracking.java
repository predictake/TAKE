package main.java.take.ws;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.take.controller.TakeController;
import main.java.take.controller.Preferences;
import main.java.take.model.Geolocation;
import main.java.take.model.MilitaryUnit;

/**
 * 
 * @author Gisler Christophe
 *
 */
@Path("/rft")
public class RedForceTracking {

	private static final DateFormat DATE_FORMAT = TakeController.DATE_FORMAT_LOG;

	private static final Logger LOGGER = LoggerFactory.getLogger(RedForceTracking.class);

	private static final String SEP1 = ",", SEP2 = "_";

	/**
	 * Set the data message sending period in [s] of the RFT service.
	 * 
	 * @param data
	 *            message sending period in [s]
	 * @return TEXT message with confirmation
	 */
	@Path("/setmessagesendingperiod")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response setMessageSendingPeriod(@FormParam("period") int period) {
		// Set the data message sending period in [s] of the RFT service.
		Preferences.getInstance().setRFTMessageSendingPeriod(period);
		TakeController.getInstance().setRFTMessageSendingPeriod(period, 0);

		LOGGER.info("Data message sending period of the RFT service set to {} seconds", period);

		String result = "Data message sending period of the RFT service set to " + period + " seconds";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the dated GPS locations and types of either: - all available enemies (if
	 * null 'radius' and 'polygonPoints' parameters); - enemies located in the given
	 * circle radius in [km] relatively to the current location (if set 'radius'
	 * parameter); - enemies located in the given polygon area (if set
	 * 'polygonPoints' parameter).
	 * 
	 * N.B.: Each (ally) node automatically sends the required information (GPS
	 * dated locations, types,...) about itself and the known enemies to all (ally)
	 * nodes.
	 * 
	 * @param radius:
	 *            (optional) circle radius given in [km]
	 * @param points:
	 *            (optional) polygon points delimited by a comma-separated list of
	 *            GPS points: "lat0_lon0,lat1_lon1,..."
	 * @return JSON response containing the list of dated GPS locations and types of
	 *         all known enemies
	 */
	@Path("/getenemylocations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEnemyLocations(@QueryParam("radius") float circleRadius,
			@QueryParam("points") String polygonPoints) {
		TakeController mc = TakeController.getInstance();

		// Get requested enemy military units
		Map<String, ? extends MilitaryUnit> enemies = null;
		final boolean areAllies = false;
		if (circleRadius == 0f && polygonPoints == null) {
			LOGGER.info("Get all enemy locations");
			enemies = mc.getAllMilitaryUnits(true);
		} else if (circleRadius != 0f) {
			LOGGER.info("Get all enemy locations in a radius of {} km", circleRadius);
			enemies = mc.getMilitaryUnitsInCircleArea(areAllies, circleRadius);
		} else {
			LOGGER.info("Get all enemy locations in the polygon delimited by: {}", polygonPoints);
			String[] pointsData = polygonPoints.split(SEP1);
			List<Geolocation> points = new ArrayList<Geolocation>(pointsData.length);
			for (String pd : pointsData) {
				String[] pointData = pd.split(SEP2);
				double longitude = Double.parseDouble(pointData[0]);
				double latitude = Double.parseDouble(pointData[1]);
				points.add(new Geolocation(longitude, latitude));
			}
			enemies = mc.getMilitaryUnitsInPolygonArea(areAllies, points);
		}

		// Build JSON response containing the types and dated GPS locations of all
		// requested enemy nodes
		JSONObject jsonObject = new JSONObject();
		for (String enemyName : enemies.keySet()) {
			MilitaryUnit enemy = enemies.get(enemyName);
			Geolocation enemyGeolocation = enemy.getLastGeolocation();
			JSONObject jsonObject2 = new JSONObject();
			jsonObject2.put("Type", enemy.getType());
			jsonObject2.put("Latitude", enemyGeolocation.getLatitude());
			jsonObject2.put("Longitude", enemyGeolocation.getLongitude());
			jsonObject2.put("Date", DATE_FORMAT.format(enemyGeolocation.getDate()));
			jsonObject.put("Enemy '" + enemy.getName() + "'", jsonObject2);
		}

		String result = "" + jsonObject;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Set a new or updated dated GPS location and type of an enemy into current
	 * node (with current date).
	 * 
	 * N.B.: Each (ally) node automatically sends the required information (dated
	 * GPS locations, types,...) about itself and the known enemies to all (ally)
	 * nodes.
	 * 
	 * @param enemyID:
	 *            (optional) ID of the enemy if it is an update (otherwise a new ID
	 *            will be created)
	 * @param typeID:
	 *            type of the enemy (the type list is provided by the Armasuisse)
	 * @param latitude:
	 *            GPS latitude of the enemy
	 * @param longitude:
	 *            GPS longitude of the enemy
	 * @return JSON response containing the enemy ID
	 */
	@Path("/setenemylocation")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setEnemyLocation(@FormParam("enemyid") String enemyID, @FormParam("type") String type,
			@FormParam("latitude") String latitude, @FormParam("longitude") String longitude) {
		TakeController mc = TakeController.getInstance();
		Date date = new Date(); // Current date

		// Set a new or updated enemy GPS dated location and type
		enemyID = mc.setEnemyLocationAndType(enemyID, type, Double.parseDouble(longitude), Double.parseDouble(latitude),
				date);

		LOGGER.info("Set enemy ({}) location ({}, {}) and type ({}) observed on {}", enemyID, latitude, longitude, type,
				date);

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Enemy ID", enemyID);
		String result = "" + jsonObject;
		return Response.status(200).entity(result).build();
	}

}
