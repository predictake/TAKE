TAKE_PATH="Take"
EMULTAKE_PATH="EmulTake"
GIT_PATH="/home/take/TAKE/java/"
GIT_PATH_DEPLOY="/home/take/TAKE/take/take/real/" 

rm -r $GIT_PATH_DEPLOY/Take
mkdir $GIT_PATH_DEPLOY/Take
cp $TAKE_PATH/take.jar $GIT_PATH_DEPLOY/Take/take.jar
cp $TAKE_PATH/run.sh $GIT_PATH_DEPLOY/Take/run.sh
cp $TAKE_PATH/readme.txt $GIT_PATH_DEPLOY/Take/readme.txt

rm -r $GIT_PATH_DEPLOY/EmulTake
mkdir $GIT_PATH_DEPLOY/EmulTake
cp $EMULTAKE_PATH/takemul.jar $GIT_PATH_DEPLOY/EmulTake/takemul.jar
cp $EMULTAKE_PATH/config.properties $GIT_PATH_DEPLOY/EmulTake/config.properties
cp $EMULTAKE_PATH/take_default_config.properties $GIT_PATH_DEPLOY/EmulTake/take_default_config.properties
cp $EMULTAKE_PATH/take_default_node_settings.csv $GIT_PATH_DEPLOY/EmulTake/take_default_node_settings.csv
cp $EMULTAKE_PATH/emultake_service $GIT_PATH_DEPLOY/emultake_service
cp $EMULTAKE_PATH/set_launch_at_startup.sh $GIT_PATH_DEPLOY/set_launch_at_startup.sh
cp $EMULTAKE_PATH/run.sh $GIT_PATH_DEPLOY/EmulTake/run.sh
cp $EMULTAKE_PATH/readme.txt $GIT_PATH_DEPLOY/EmulTake/readme.txt

