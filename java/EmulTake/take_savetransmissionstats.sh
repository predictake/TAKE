#!/bin/bash

# This script asks TAKE to exports its statistics through its REST interface. 
# Gilles Waeber, 2019-04-17

curl --connect-timeout 3 --max-time 3 http://localhost:8080/take/app/savetransmissionstats

