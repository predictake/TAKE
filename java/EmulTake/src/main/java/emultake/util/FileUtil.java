package main.java.emultake.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class FileUtil {

	public final static String PATH_SEP = File.separator;

	public static final Charset UTF_8 = StandardCharsets.UTF_8;

	private FileUtil() {
	}

	/**
	 * Return a list of all lines contained in the given text file.
	 * 
	 * @param filePath
	 * @return List of all lines contained in the given file
	 * @throws FileNotFoundException
	 */
	public static List<String> loadLinesFromFile(String filePath) throws FileNotFoundException {
		if (filePath == null)
			return null;
		File file = new File(filePath);
		if (file.exists()) {
			ArrayList<String> lines = new ArrayList<String>();
			// Use a Scanner to get each line
			try (Scanner scanner = new Scanner(file, UTF_8.name());) {
				while (scanner.hasNextLine())
					lines.add(scanner.nextLine());
				return lines;
			}
		}
		return null;
	}

	/**
	 * Write the given lines (i.e. a text) into the given file.
	 * 
	 * @param filePath
	 *            of the output file
	 * @param lines
	 *            of the text to write
	 */
	public static void writeLinesToFile(String filePath, List<String> lines) throws IOException {
		File file = new File(filePath);
		try (OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file), UTF_8);
				BufferedWriter bufWriter = new BufferedWriter(fileWriter);
				PrintWriter printWriter = new PrintWriter(bufWriter, true);) {
			for (String line : lines)
				printWriter.println(line);
			if (printWriter.checkError())
				System.err.println("Error while writing lines to file: '" + file.getPath() + "'!");
		}
	}

	/**
	 * Read the text of the file at the given filepath.
	 * 
	 * @param filePath
	 * @return text
	 */
	public static String readTextFromFile(Path filePath) throws IOException {
		return Files.lines(filePath).collect(Collectors.joining());
		// return new String(Files.readAllBytes(Paths.get(filePath)));
	}

	/**
	 * Read the text of the file at the given filepath.
	 * 
	 * @param filePath
	 * @return text
	 */
	public static String readTextFromFile(String filePath) throws IOException {
		Path path = Paths.get(filePath);
		return Files.lines(path).collect(Collectors.joining());
		// return new String(Files.readAllBytes(Paths.get(filePath)));
	}

	/**
	 * Write the text (content) into the given file.
	 * 
	 * @param filePath
	 *            of the output file
	 * @param text
	 *            (content) to write
	 */
	public static void writeTextToFile(String filePath, String content) throws IOException {
		File file = new File(filePath);
		try (OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file), UTF_8);
				BufferedWriter bufWriter = new BufferedWriter(fileWriter);
				PrintWriter printWriter = new PrintWriter(bufWriter, true);) {
			printWriter.println(content);
			if (printWriter.checkError())
				System.err.println("Error while writing text to file: '" + file.getPath() + "'!");
		}
	}

	/**
	 * Delete a file or an empty directory given by its path.
	 * 
	 * @param filePath
	 * @return true if operation succeeded.
	 */
	public static boolean deleteFile(String filePath) {
		return deleteFile(new File(filePath));
	}

	/**
	 * Delete a file or an empty directory.
	 * 
	 * @param file
	 * @return true if operation succeeded.
	 */
	public static boolean deleteFile(File file) {
		return !file.exists() || file.delete();
	}

	/**
	 * Rename (move) a file to another name (location).
	 * 
	 * @param current
	 *            file path
	 * @param new
	 *            file path
	 * @return true if operation succeeded.
	 */
	public static boolean renameFile(String currentFilePath, String newFilePath) {
		File file = new File(currentFilePath);
		File newFile = new File(newFilePath);
		return (file.renameTo(newFile));
	}

	public static void createFileInFolder(String fileName, String folderPath) throws IOException {
		new File(folderPath, fileName).createNewFile();
	}

	public static void createNewFileIfNotExist(String filePath) throws IOException {
		File file = new File(filePath);
		if (!file.exists() && !file.isDirectory())
			file.createNewFile();
	}

	/**
	 * Create the folder denoted by this path, if it doesn't exist yet.
	 */
	public static void createFolderIfNotExist(String folderPath) {
		File folder = new File(folderPath);
		if (!folder.exists() && !folder.isDirectory())
			folder.mkdirs();
	}

	public static void copyFile(File srcFile, File destFile) throws IOException {
		Files.copy(srcFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	public static void copyFilesStartingByPrefixInFolder(String prefixOfFilesToCopy, File srcFolder, File destFolder)
			throws IOException {
		File[] filesStartingByPrefix = srcFolder
				.listFiles((File dir, String name) -> name.startsWith(prefixOfFilesToCopy));
		for (File fileToCopy : filesStartingByPrefix)
			Files.copy(fileToCopy.toPath(), Paths.get(destFolder.getAbsolutePath(), fileToCopy.getName()),
					StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * Delete all files and subfolders under folder. If a deletion fails, the method
	 * stops attempting to delete and returns false.
	 * 
	 * @param folder
	 * @return true if all deletions were successful.
	 */
	public static boolean deleteFolder(File folder) {
		if (folder.isDirectory()) {
			String[] children = folder.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteFolder(new File(folder, children[i]));
				if (!success)
					return false;
			}
		}
		// The directory is now empty so delete it
		return folder.delete();
	}

	/**
	 * Delete all files and subfolders under folder given by its path. If a deletion
	 * fails, the method stops attempting to delete and returns false.
	 * 
	 * @param folderPath
	 * @return true if all deletions were successful.
	 */
	public static boolean deleteFolder(String folderPath) {
		File folder = new File(folderPath);
		return deleteFolder(folder);
	}

	/**
	 * Delete all files in the given folder.
	 * 
	 * @param folderPath
	 */
	public static void deleteAllFilesInFolder(String folderPath) {
		File folder = new File(folderPath);
		deleteAllFilesInFolder(folder);
	}

	/**
	 * Delete all files in the given folder.
	 * 
	 * @param folder
	 */
	public static void deleteAllFilesInFolder(File folder) {
		if (folder.isDirectory()) {
			String[] children = folder.list();
			for (int i = 0; i < children.length; i++) {
				File currentFile = new File(folder, children[i]);
				// System.out.println(currentFile.getAbsolutePath());
				if (currentFile.isDirectory())
					deleteAllFilesInFolder(currentFile);
				else
					currentFile.delete();
			}
		}
	}

	/**
	 * * Get all files starting with the given prefix in the given folder.
	 * 
	 * @param prefix
	 * @param folderPath
	 * @return all files matching the given prefix or null is an error occurs
	 */
	public static File[] getFilesStartingWith(String prefix, String folderPath) {
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles(new MyFilenameFilter(MyFilenameFilter.STARTS_WITH, prefix));
		return listOfFiles;
	}

	/**
	 * * Get all files ending with the given suffix in the given folder.
	 * 
	 * @param suffix
	 * @param folderPath
	 * @return all files matching the given suffix or null is an error occurs
	 */
	public static File[] getFilesEndingWith(String suffix, String folderPath) {
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles(new MyFilenameFilter(MyFilenameFilter.ENDS_WITH, suffix));
		return listOfFiles;
	}

	/**
	 * Delete all files starting with the given prefix in the given folder and all
	 * subfolders.
	 * 
	 * @param prefix
	 * @param folderPath
	 */
	public static void deleteFilesStartingWith(String prefix, String folderPath) {
		File folder = new File(folderPath);
		deleteFiles(MyFilenameFilter.STARTS_WITH, prefix, folder);
	}

	/**
	 * Delete all files containing the given infix in the given folder and all
	 * subfolders.
	 * 
	 * @param infix
	 * @param folderPath
	 */
	public static void deleteFilesContaining(String infix, String folderPath) {
		File folder = new File(folderPath);
		deleteFiles(MyFilenameFilter.CONTAINS, infix, folder);
	}

	/**
	 * Delete all files ending with the given suffix in the given folder and all
	 * subfolders.
	 * 
	 * @param suffix
	 * @param folderPath
	 */
	public static void deleteFilesEndingWith(String suffix, String folderPath) {
		File folder = new File(folderPath);
		deleteFiles(MyFilenameFilter.ENDS_WITH, suffix, folder);
	}

	/*
	 * This method is recursive! It deletes also the matching files in subfolders.
	 */
	private static void deleteFiles(int matchingMode, String suffix, File folder) {
		if (folder.isDirectory()) {
			String[] children = folder.list(new MyFilenameFilter(matchingMode, suffix));
			for (int i = 0; i < children.length; i++) {
				File currentFile = new File(folder, children[i]);
				// System.out.println(currentFile.getAbsolutePath());
				if (currentFile.isDirectory())
					deleteFiles(matchingMode, suffix, currentFile);
				else
					currentFile.delete();
			}
		}
	}

	private static class MyFilenameFilter implements FilenameFilter {

		private final static int STARTS_WITH = 1;
		private final static int ENDS_WITH = 2;
		private final static int CONTAINS = 3;
		private int matchingMode;
		private String expr;

		public MyFilenameFilter(int matchingMode, String expr) {
			this.matchingMode = matchingMode;
			this.expr = expr;
		}

		@Override
		public boolean accept(File dir, String name) {
			boolean returnedValue = false;
			switch (matchingMode) {
			case STARTS_WITH:
				if (name.startsWith(expr))
					returnedValue = true;
				break;
			case ENDS_WITH:
				if (name.endsWith(expr))
					returnedValue = true;
				break;
			case CONTAINS:
				if (name.contains(expr))
					returnedValue = true;
				break;
			}
			if (new File(dir + PATH_SEP + name).isDirectory())
				returnedValue = true;
			return returnedValue;
		}

	}

}
