package main.java.emultake.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TerminalUtil {

	/**
	 * Execute command in terminal
	 * 
	 * @param command
	 * @return terminal output
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String executeCommand(String dirPath, String... command) throws IOException, InterruptedException {
		StringBuilder sb = new StringBuilder();

		sb.append(String.join(" ", command) + "\n");

		ProcessBuilder pb = new ProcessBuilder(command);
		if (dirPath != null)
			pb.directory(new File(dirPath));
		Process p = pb.start();
		// Process p = Runtime.getRuntime().exec(command);

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
		}

		// p.waitFor();

		return sb.toString();
	}

	public static String copy(String srcFilePath, String dstFileOrFolderPath) throws IOException, InterruptedException {
		return executeCommand(null, "cp", srcFilePath, dstFileOrFolderPath);
	}

	public static String copyFolder(String srcFilePath, String dstFolderPath) throws IOException, InterruptedException {
		return executeCommand(null, "cp", "-R", srcFilePath, dstFolderPath);
	}

	public static String targz(String srcFolderPath, String optionalArchivePath)
			throws IOException, InterruptedException {
		String archivePath = optionalArchivePath == null ? srcFolderPath : optionalArchivePath;
		return executeCommand(null, "tar", "-zcvf", archivePath + ".tar.gz", srcFolderPath);
	}

	public static String runJavaApplication(String dirPath, String javaJarName)
			throws IOException, InterruptedException {
		return executeCommand(dirPath, "java", "-jar", javaJarName, "&");
	}

	public static String runJavaApplication(String dirPath, String javaJarName, int minRAMm, int maxRAMm)
			throws IOException, InterruptedException {
		return executeCommand(dirPath, "java", "-jar", "-Xms" + minRAMm + "m", "-Xmx" + maxRAMm + "m", javaJarName,
				"&");
	}

	public static String killJavaApplication(String javaAppName) throws IOException, InterruptedException {
		return executeCommand(null, "pkill", "-9", "-f", javaAppName);
	}

}
