package main.java.emultake.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class DateUtil {

	public static final DateFormat DATE_FORMAT_FILE = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	public static final DateFormat DATE_FORMAT_LOG = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	private DateUtil() {
	}

	public static Date parse(String formattedDate, DateFormat dateFormat) throws ParseException {
		return dateFormat.parse(formattedDate);
	}

	public static Date parseFromLogFormat(String formattedDate) throws ParseException {
		return parse(formattedDate, DATE_FORMAT_LOG);
	}

	public static Date parseFromFileFormat(String formattedDate) throws ParseException {
		return parse(formattedDate, DATE_FORMAT_FILE);
	}

	public static String format(Date date, DateFormat dateFormat) {
		String formattedDate = null;
		formattedDate = dateFormat.format(date);
		return formattedDate;
	}

	public static String formatForLog(Date date) {
		return format(date, DATE_FORMAT_LOG);
	}

	public static String formatForFile(Date date) {
		return format(date, DATE_FORMAT_FILE);
	}

}
