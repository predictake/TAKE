package main.java.emultake.ws;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.emultake.controller.Preferences;
import main.java.emultake.model.Scenario;
import main.java.emultake.model.ScenarioRun;
import main.java.emultake.model.ScenarioRun.Status;
import main.java.emultake.model.ScenarioRunResult;
import main.java.emultake.model.TakeAppConfiguration;
import main.java.emultake.util.DateUtil;
import main.java.emultake.util.FileUtil;
import main.java.emultake.controller.EmulTakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
@Path("/app")
public class EmulTakeApp {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmulTakeApp.class);

	/**
	 * Get the version of the EmulTake application.
	 * 
	 * @return TEXT response containing the version of the EmulTake application
	 */
	@Path("/getversion")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getVersion() {
		String emulTakeVersion = Preferences.getAppVersion();

		LOGGER.info("Get EmulTake application version: {}", emulTakeVersion);

		String result = "V" + emulTakeVersion;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the configuration properties contained in the config.properties file of
	 * the EmulTake application.
	 * 
	 * @return TEXT response containing the configuration properties of the EmulTake
	 *         application
	 */
	@Path("/getconfig")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getConfig() {
		String appConfig = Preferences.getFormattedAppConfig();

		LOGGER.info("Get EmulTake application configuration properties:\n{}", appConfig);

		String result = "EmulTake configuration properties:\n" + appConfig;
		return Response.status(200).entity(result).build();
	}

	/**
	 * Add (push, upload, deploy) a scenario, which can be run by the EmulTake
	 * application. If a scenario with same name (ID) already exists, the latter
	 * will be replaced by the new one and all previously scheduled run(s) will be
	 * cancelled and deleted.
	 * 
	 * @param scenario
	 *            in JSON format
	 * @return TEXT message with confirmation
	 */
	@Path("/addscenario")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addScenario(@FormParam("scenario") String scenarioJSON) {
		try {
			Scenario scenario = new Scenario(scenarioJSON);
			EmulTakeController.getInstance().addScenario(scenario);

			LOGGER.info("Scenario with ID {}, name '{}' and creation date {} received and added", scenario.getID(),
					scenario.getName(), DateUtil.formatForLog(scenario.getDate()));

			String result = "Scenario with ID " + scenario.getID() + ", name '" + scenario.getName()
					+ "' and creation date " + DateUtil.formatForLog(scenario.getDate()) + " received and added";
			return Response.status(200).entity(result).build();
		} catch (IOException | JSONException | ParseException e) {
			LOGGER.error("Add scenario - Exception", e);

			return Response.status(400).entity(e.getMessage()).build(); // 400 = Bad request error
		}
	}

	/**
	 * Get the status of the given scenario: 'NA', 'DEPLOYED'
	 * 
	 * @param scenarioID:
	 *            ID of the scenario
	 * @return JSON response containing the scenario status
	 */
	@Path("/getscenariostatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getScenarioStatus(@QueryParam("scenarioid") int scenarioID) {
		Scenario.Status scenarioStatus = EmulTakeController.getInstance().getScenarioStatus(scenarioID);
		JSONObject jo = new JSONObject();
		jo.put("Scenario ID", scenarioID);
		jo.put("Status", scenarioStatus.name());
		String result = jo.toString();

		LOGGER.info("Get status for scenario {}: {}", scenarioID, scenarioStatus.toString());

		return Response.status(200).entity(result).build();
	}

	/**
	 * Get information (name/ID, creation date) about the received scenarios and the
	 * potential runs scheduled by the EmulTake application.
	 * 
	 * @return JSON response containing the requested information.
	 */
	@Path("/getscenariosandrunsinfo")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getScenariosAndRunsInfo() {
		EmulTakeController etc = EmulTakeController.getInstance();

		JSONObject scenariosAnRunsJSON = new JSONObject();

		JSONArray scenariosJSON = new JSONArray();
		for (Scenario scenario : etc.getScenarios().values()) {
			JSONObject scenarioJSON = new JSONObject();
			scenarioJSON.put("Scenario ID", scenario.getID());
			scenarioJSON.put("Scenario Name", scenario.getName());
			scenarioJSON.put("Creation Date", DateUtil.formatForFile(scenario.getDate()));
			scenariosJSON.put(scenarioJSON);
		}
		scenariosAnRunsJSON.put("Scenarios", scenariosJSON);

		JSONArray scenarioRunsJSON = new JSONArray();
		for (ScenarioRun scenarioRun : etc.getScenarioRuns().values()) {
			JSONObject scenarioRunJSON = new JSONObject();
			scenarioRunJSON.put("Run ID", scenarioRun.getID());
			scenarioRunJSON.put("Scenario ID", scenarioRun.getScenario().getID());
			scenarioRunJSON.put("Start Date", DateUtil.formatForFile(scenarioRun.getStartDate()));
			scenarioRunJSON.put("End Date", DateUtil.formatForFile(scenarioRun.getEndDate()));
			scenarioRunsJSON.put(scenarioRunJSON);
		}
		scenariosAnRunsJSON.put("Scenario Runs", scenarioRunsJSON);

		LOGGER.info("Get information about the received scenarios and runs:\n{}", scenariosAnRunsJSON);

		String result = scenariosAnRunsJSON.toString();
		return Response.status(200).entity(result).build();
	}

	/**
	 * Schedule a new scenario run with given ID for the given scenario (ID) on the
	 * given start date. If a run with this ID has been already scheduled, it will
	 * be cancelled and replaced by this new one.
	 * 
	 * @param scenarioID:
	 *            ID of the scenario to be run on given start date
	 * @param startDate:
	 *            start date in format "yyyy-MM-dd_HH-mm-ss"
	 * @return TEXT message with confirmation
	 */
	@Path("/schedulescenariorun")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response scheduleScenarioRun(@FormParam("runid") int runID, @FormParam("scenarioid") int scenarioID,
			@FormParam("startdate") String startDate) {
		Date runStartDate = null;
		if (startDate == null) {
			runStartDate = new Date();
		} else
			try {
				runStartDate = DateUtil.parseFromFileFormat(startDate);
			} catch (ParseException e) {
				LOGGER.error("Schedule scenario run - Date parse exception", e);

				return Response.status(400).entity("Date parsing error: " + startDate).build();
			}

		try {
			long delayUntilRun = EmulTakeController.getInstance().scheduleScenarioRun(runID, scenarioID, runStartDate);
			if (delayUntilRun < 0l) {
				String errorMessage = "Scenario run refused - The given start date ("
						+ DateUtil.formatForLog(runStartDate) + ") is before current time ("
						+ DateUtil.formatForLog(new Date()) + ") + default delay for the Take app to start ("
						+ Preferences.getTakeAppLaunchDelayBeforeScenarioRun() + " [s]) => Delay until Take start: "
						+ delayUntilRun + " [s]";

				LOGGER.error(errorMessage);
				return Response.status(500).entity(errorMessage).build();
			} else {
				LOGGER.info("Run {} scheduled on {} for scenario {}", runID, DateUtil.formatForLog(runStartDate),
						scenarioID);

				String result = "Run " + scenarioID + " scheduled on " + DateUtil.formatForLog(runStartDate)
						+ " for scenario " + scenarioID;
				return Response.status(200).entity(result).build();
			}
		} catch (Exception e) {
			LOGGER.error("Schedule scenario run - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Cancel and delete the given scenario run (if it exists).
	 * 
	 * @param runID:
	 *            ID of a scenario run to cancel
	 * @return TEXT message with confirmation
	 */
	@Path("/cancelscenariorun")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response cancelScenarioRun(@QueryParam("runid") int runID) {
		EmulTakeController.getInstance().cancelScenarioRun(runID);

		LOGGER.info("Scenario run {} cancelled and deleted", runID);

		String result = "Scenario run " + runID + " cancelled and deleted";
		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the status of the given scenario run: 'NA', 'SCHEDULED', 'RUNNING',
	 * 'COMPLETED', 'CANCELLED', 'FAILED'
	 * 
	 * @param runID:
	 *            ID of the scenario run
	 * @return JSON response containing the scenario run status
	 */
	@Path("/getscenariorunstatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getScenarioRunStatus(@QueryParam("runid") int runID) {
		Status scenarioRunStatus = EmulTakeController.getInstance().getScenarioRunStatus(runID);
		JSONObject jo = new JSONObject();
		jo.put("Run ID", runID);
		jo.put("Status", scenarioRunStatus.name());
		String result = jo.toString();

		LOGGER.info("Get status for scenario run {}: {}", runID, scenarioRunStatus.toString());

		return Response.status(200).entity(result).build();
	}

	/**
	 * Get the result obtained at the end of a given scenario run.
	 * 
	 * @param runID:
	 *            ID of the scenario run
	 * @return JSON response containing the scenario run result
	 */
	@Path("/getscenariorunresult")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getScenarioRunResult(@QueryParam("runid") int runID) {
		String result = null;
		try {
			ScenarioRunResult scenarioRunResult = EmulTakeController.getInstance().getScenarioRunResult(runID);
			result = scenarioRunResult.toString(); // In JSON format

			LOGGER.info("Get result for scenario run {}", runID);

			return Response.status(200).entity(result).build();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Get result for scenario run " + runID + " - Exception", e);

			result = getJSONFormattedException("Get result for scenario run " + runID, e);

			return Response.status(500).entity(result + " " + e.getMessage()).build();
		}
	}

	/**
	 * Get a TAR.GZ archive of all the files (logs, data, Munin charts,...)
	 * generated during the given scenario run by both Take and EmulTake
	 * applications.
	 * 
	 * @param runID:
	 *            ID of the scenario run
	 * @return TAR.GZ archive including files generated by the Take application
	 */
	@Path("/getscenariorunfiles")
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getScenarioRunFiles(@QueryParam("runid") int runID) {
		ResponseBuilder response = null;
		try {
			File archiveFile = EmulTakeController.getInstance().getScenrarioRunGeneratedFilesArchive(runID);
			response = Response.ok((Object) archiveFile);
			response.header("Content-Disposition", "attachment; filename=" + archiveFile.getName());

			LOGGER.info("Get Take application files for scenario run " + runID);
		} catch (Exception e) {
			response = Response.serverError();

			LOGGER.error("Get Take application files for scenario run " + runID + " - Exception", e);
		}

		return response.build();
	}

	/**
	 * Reset both EmulTake and Take applications and delete all stored data.
	 * 
	 * N.B.: If the Take Java application is running, it will call the internal Take
	 * reset method, else it will externally delete all created folders and files in
	 * the Take application folder.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/reset")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response reset() {
		try {
			EmulTakeController etc = EmulTakeController.getInstance();
			etc.reset();
			etc.resetTakeJavaApp();

			LOGGER.info("Reset EmulTake and Take applications");

			String result = "EmulTake and Take applications reset";
			return Response.status(200).entity(result).build();
		} catch (IOException e) {
			LOGGER.error("Reset EmulTake and Take applications - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Get Take application version.
	 * 
	 * @return TEXT message with requested information
	 */
	@Path("/gettakeappversion")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getTakeAppVersion() {
		try {
			String takeAppVersion = EmulTakeController.getInstance().getTakeAppVersion();

			LOGGER.info("Get Take application version: {}", takeAppVersion);

			String result = "V" + takeAppVersion;
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Get Take application version - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Get running Take application information (the application must be running).
	 * 
	 * @return TEXT message with requested information
	 */
	@Path("/getrunningtakeappinfo")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getTakeAppInfo() {
		try {
			String runningTakeAppInfo = EmulTakeController.getInstance().getRunningTakeAppInformation();

			LOGGER.info("Get running Take application information:\n{}", runningTakeAppInfo);

			String result = "Running Take application information:\n" + runningTakeAppInfo;
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Get running Take application information - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	private static String getJSONFormattedException(String task, Exception e) {
		JSONObject jo = new JSONObject();
		jo.put("Task", task);
		jo.put("Exception", e.getMessage());
		return jo.toString();
	}

	/* ************ */
	/* TEST METHODS */
	/* ************ */

	/**
	 * Test if the Take app can be started with the default provided configuration
	 * file.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/teststarttakeapp")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response testStartTakeApp() {
		try {
			TakeAppConfiguration takeAppConfiguration = TakeAppConfiguration.createDefaultConfigurationFromFile();
			EmulTakeController.getInstance().startTakeJavaApp(takeAppConfiguration, 0);

			LOGGER.info("Test start Take application");

			String result = "Test start Take application";
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Test start Take application - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Test if the Take application can be stopped.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/teststoptakeapp")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response testStopTakeApp() {
		try {
			EmulTakeController.getInstance().stopTakeJavaApp();

			LOGGER.info("Test stop Take application");

			return Response.status(200).entity("Test stop Take application: OK").build();
		} catch (IOException | InterruptedException e) {
			LOGGER.error("Test stop Take application - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Test Take application message sending. The current node will send a message
	 * to itself, get the message and reset the Take application.
	 * 
	 * @return TEXT message with test result (including the sent message)
	 */
	@Path("/testtakemsgsending")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response testTakeAppMessageSending() {
		try {
			String takeAppMsgSendingTestResults = EmulTakeController.getInstance().testTakeAppMessageSending();

			LOGGER.info("Test Take application message sending:\n{}", takeAppMsgSendingTestResults);

			String result = "Test Take application message sending:\n" + takeAppMsgSendingTestResults;
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Test Take App Message Sending - Exception", e);

			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	/**
	 * Test add scenario.
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/testaddscenario")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response testAddScenario() {
		try {
			String scenarioJSON = FileUtil.readTextFromFile("scenarioNode1.json");

			LOGGER.info("Test add scenario");

			return addScenario(scenarioJSON);
		} catch (IOException e) {
			LOGGER.error("Add scenario - Exception", e);

			return Response.status(400).entity("JSON file reading error: " + e.getMessage()).build();
		}
	}

	/**
	 * Test run scenario as soon as possible (ASAP).
	 * 
	 * @return TEXT message with confirmation
	 */
	@Path("/testrunscenarioasap")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response testRunScenarioASAP() {
		int runID = 1;
		int scenarioID = 1;
		Date startDate = new Date(
				new Date().getTime() + 60000l + 1000l * (long) Preferences.getTakeAppLaunchDelayBeforeScenarioRun());
		String startDateF = DateUtil.formatForFile(startDate);

		LOGGER.info("Test run scenario as soon as possible:\n- Run ID: {}\n- Scenario ID: {}\n- Start date: {}", runID,
				scenarioID, startDateF);

		return scheduleScenarioRun(runID, scenarioID, startDateF);
	}

}
