package main.java.emultake.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import main.java.emultake.model.NodeAction.Type;
import main.java.emultake.model.distributions.Distribution;
import main.java.emultake.model.distributions.DistributionDeterministicPattern;
import main.java.emultake.model.distributions.DistributionDeterministicUniform;
import main.java.emultake.model.distributions.DistributionRandomExponential;
import main.java.emultake.model.distributions.DistributionRandomGaussian;
import main.java.emultake.model.distributions.DistributionRandomPoisson;
import main.java.emultake.model.distributions.DistributionRandomUniform;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class NodeProfile {

	private static final long SEED_FOR_MSG_CONTENT_AND_RECIPIENTS = 12345;
	private static final Random random = new Random(SEED_FOR_MSG_CONTENT_AND_RECIPIENTS);
	private static final String SEP = ",";

	private String name; // = ID
	private float msgSendingFrequency = 0;
	private Distribution msgSendingTimeDistribution = null;
	private int msgRecipientsMax = 1;
	private int[] msgContentSizes;

	public NodeProfile(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public float getMsgSendingFrequency() {
		return msgSendingFrequency;
	}

	public void setMsgSendingFrequency(float msgSendingFrequency) {
		this.msgSendingFrequency = msgSendingFrequency;
	}

	public Distribution getMsgSendingTimeDistribution() {
		return msgSendingTimeDistribution;
	}

	public void setMsgSendingTimeDistribution(Distribution distribution) {
		this.msgSendingTimeDistribution = distribution;
	}

	public void setMsgSendingTimeDistribution(String distributionName, String... parameters) {
		switch (distributionName) {
		case DistributionDeterministicUniform.NAME:
			// float frequency = parameters[0];
			this.msgSendingTimeDistribution = new DistributionDeterministicUniform(msgSendingFrequency);
			break;
		case DistributionDeterministicPattern.NAME:
			String pattern = parameters[0];
			this.msgSendingTimeDistribution = new DistributionDeterministicPattern(pattern);
			break;
		case DistributionRandomUniform.NAME:
			long seed = Long.parseLong(parameters[0]);
			this.msgSendingTimeDistribution = new DistributionRandomUniform(seed);
			break;
		case DistributionRandomExponential.NAME:
			seed = Long.parseLong(parameters[0]);
			float lambda = Float.parseFloat(parameters[1]);
			this.msgSendingTimeDistribution = new DistributionRandomExponential(seed, lambda);
			break;
		case DistributionRandomGaussian.NAME:
			seed = Long.parseLong(parameters[0]);
			float mu = Float.parseFloat(parameters[1]);
			float sigma = Float.parseFloat(parameters[2]);
			this.msgSendingTimeDistribution = new DistributionRandomGaussian(seed, mu, sigma);
			break;
		case DistributionRandomPoisson.NAME:
			seed = Long.parseLong(parameters[0]);
			mu = Float.parseFloat(parameters[1]);
			this.msgSendingTimeDistribution = new DistributionRandomPoisson(seed, mu);
			break;
		}
	}

	public int getMsgRecipientsMax() {
		return msgRecipientsMax;
	}

	public void setMsgRecipientsMax(int msgRecipientsMax) {
		this.msgRecipientsMax = msgRecipientsMax;
	}

	public int[] getMsgContentSizes() {
		return msgContentSizes;
	}

	public void setMsgContentSizes(int... msgContentSizes) {
		this.msgContentSizes = msgContentSizes;
	}

	public List<NodeAction> generateNodeActions(String currentNodeName, List<String> recipientNodeNames, long startTime,
			long endTime) {
		List<NodeAction> actions = new LinkedList<NodeAction>();

		// Generate actions for MSG sending
		if (msgSendingFrequency > 0) {
			final int nbrOfMessagesToSend = (int) (msgSendingFrequency * (endTime - startTime));
			final int msgType = 1; // "1" for standard messages, "2" for orders
			final String msgTitle = "Message Title";
			final String msgContent = getRandomStringOfSizeChosenAmongst(msgContentSizes, random);
			List<NodeAction> msgActions = new ArrayList<NodeAction>(nbrOfMessagesToSend);
			for (int i = 0; i < nbrOfMessagesToSend; i++) {
				long sendingTime = msgSendingTimeDistribution.nextValueBewteen(startTime, endTime);
				String[] selectedRecipientNodeNames = selectRandomNodes(recipientNodeNames, msgRecipientsMax, random);
				msgActions.add(new NodeAction(sendingTime, Type.MSG_SEND_MSG, msgType, msgTitle, msgContent,
						String.join(SEP, selectedRecipientNodeNames)));
			}
			msgActions.sort((NodeAction na1, NodeAction na2) -> Long.compare(na1.getDelay(), na2.getDelay()));
			actions.addAll(msgActions);
		}

		return actions;
	}

	private static String getRandomStringOfSizeChosenAmongst(int[] availableSizes, Random random) {
		final String subset = " abcdefghijklmnopqrstuvwxyz, ABCDEFGHIJKLMNOPQRSTUVWXYZ. 0123456789 ";
		StringBuilder sb = new StringBuilder();
		int size = availableSizes[random.nextInt(availableSizes.length)];
		for (int i = 0; i < size; i++)
			sb.append(subset.charAt(random.nextInt(subset.length())));
		return sb.toString();
	}

	private static String[] selectRandomNodes(List<String> nodes, int maxNodesToSelect, Random random) {
		Set<String> selectedNodes = new TreeSet<String>();
		final int nbrOfNodes = nodes.size();
		final int nbrOfNodesToSelect = random.nextInt(maxNodesToSelect) + 1;
		int i = 0;
		while (i < nbrOfNodesToSelect) {
			selectedNodes.add(nodes.get(random.nextInt(nbrOfNodes)));
			i++;
		}
		return selectedNodes.toArray(new String[0]);
	}

}
