package main.java.emultake.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import main.java.emultake.controller.Preferences;
import main.java.emultake.model.NodeAction.Type;
import main.java.emultake.util.DateUtil;
import main.java.emultake.util.FileUtil;

/**
 * This class is meant to describe the general scenario (ID, name, creation
 * date, invlolved nodes and profiles) as well as the specific phase profiles of
 * the current node.
 * 
 * @author Gisler Christophe
 *
 */
public class Scenario {

	public enum Status {
		NA, DEPLOYED;
	}

	public static final String SCENARIOS_FOLDER_NAME = "scenarios";
	public static final String SCENARIO_JSON_FILE_NAME = "scenario.json";
	private static final String CURRENT_NODE_ACTIONS_FILE_NAME = "current_node_actions.csv";

	private static final String SEP = ",";

	private int id;
	private String name;
	private Date date; // Creation/modification date
	private int bftDataSendingPeriod = 0;
	private int rftDataSendingPeriod = 0;
	private boolean forceBFTRFTMessageSending = true;
	private List<ScenarioPhase> phases = new LinkedList<ScenarioPhase>();
	private Map<String, Node> nodes = Collections.synchronizedMap(new LinkedHashMap<String, Node>()); // Key = node name, value = node
	private String currentNodeName;
	private Map<String, NodeProfile> currentNodePhaseProfiles = new LinkedHashMap<String, NodeProfile>(); // Key = phase name
	private List<NodeAction> currentNodeActions = new LinkedList<NodeAction>();

	private TakeAppConfiguration takeAppConfiguration;
	private Path path;

	/**
	 * Constructor for received input JSON String
	 * 
	 * @param scenarioJSON
	 * @throws IOException
	 * @throws ParseException
	 * @throws JSONException
	 */
	public Scenario(String scenarioJSON) throws IOException, JSONException, ParseException {
		JSONObject scenarJSON = new JSONObject(scenarioJSON);

		this.id = scenarJSON.getInt("id");
		this.name = scenarJSON.getString("name");
		this.date = DateUtil.parseFromFileFormat(scenarJSON.getString("date"));
		this.bftDataSendingPeriod = scenarJSON.getInt("bftDataSendingPeriod");
		this.rftDataSendingPeriod = scenarJSON.getInt("rftDataSendingPeriod");
		this.forceBFTRFTMessageSending = scenarJSON.getBoolean("forceBFTRFTMessageSending");

		JSONArray phasesJSON = scenarJSON.getJSONArray("phases");
		for (int i = 0; i < phasesJSON.length(); i++) {
			JSONObject phaseJSON = phasesJSON.getJSONObject(i);
			String phaseName = phaseJSON.getString("phaseName");
			int phaseDuration = phaseJSON.getInt("phaseDuration");
			this.phases.add(new ScenarioPhase(phaseName, phaseDuration));
		}

		JSONArray nodesJSON = scenarJSON.getJSONArray("nodes");
		for (int i = 0; i < nodesJSON.length(); i++) {
			JSONObject nodeJSON = nodesJSON.getJSONObject(i);
			String nodeName = nodeJSON.getString("nodeName");
			String nodeType = nodeJSON.getString("nodeType");
			String nodeIP = nodeJSON.getString("nodeIP");
			String radioIP = nodeJSON.getString("radioIP");
			String radioID = nodeJSON.getString("radioID");
			String radioSupplier = nodeJSON.getString("radioSupplier");
			String radioChannel = nodeJSON.getString("radioChannel");
			this.nodes.put(nodeName, new Node(nodeName, nodeType, nodeIP, radioIP, radioID, radioSupplier, radioChannel));
		}

		this.currentNodeName = scenarJSON.getString("nodeName");

		JSONArray currentNodePhaseProfilesJSON = scenarJSON.getJSONArray("currentNodePhaseProfiles");
		for (int i = 0; i < currentNodePhaseProfilesJSON.length(); i++) {
			JSONObject profileJSON = currentNodePhaseProfilesJSON.getJSONObject(i);
			String phaseName = profileJSON.getString("phaseName");
			String profileName = profileJSON.getString("profileName");
			NodeProfile nodePhaseProfile = new NodeProfile(profileName);
			float msgSendingFrequency = (float) profileJSON.getDouble("msgSendingFrequency");
			nodePhaseProfile.setMsgSendingFrequency(msgSendingFrequency);
			if (msgSendingFrequency > 0.f) {
				String distributionName = profileJSON.getString("msgSendingTimeDistributionName");
				JSONArray distributionParametersJSON = profileJSON.getJSONArray("msgSendingTimeDistributionParameters");
				String[] distributionParameters = new String[distributionParametersJSON.length()];
				for (int j = 0; j < distributionParameters.length; j++)
					distributionParameters[j] = distributionParametersJSON.getString(j);
				nodePhaseProfile.setMsgSendingTimeDistribution(distributionName, distributionParameters);
			}
			int msgRecipientsMax = profileJSON.getInt("msgRecipientsMax");
			nodePhaseProfile.setMsgRecipientsMax(msgRecipientsMax);
			JSONArray msgContentSizesJSON = profileJSON.getJSONArray("msgContentSizes");
			int[] msgContentSizes = new int[msgContentSizesJSON.length()];
			for (int j = 0; j < msgContentSizes.length; j++)
				msgContentSizes[j] = msgContentSizesJSON.getInt(j);
			nodePhaseProfile.setMsgContentSizes(msgContentSizes);
			this.currentNodePhaseProfiles.put(phaseName, nodePhaseProfile);
		}

		// Create/generate required folder, files,...
		this.path = Paths.get(Preferences.getDataDirPath(), "scenarios", "scenario_" + id);
		Files.createDirectories(path);
		saveToFile();
		createTakeAppConfiguration();
		generateCurrentNodeActions();
	}

	public int getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getDate() {
		return date;
	}

	public long getBFTDataSendingPeriod() {
		return bftDataSendingPeriod;
	}

	public long getRFTDataSendingPeriod() {
		return rftDataSendingPeriod;
	}

	public boolean isForceBFTRFTMessageSending() {
		return forceBFTRFTMessageSending;
	}

	public List<ScenarioPhase> getPhases() {
		return phases;
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public String getCurrentNodeName() {
		return currentNodeName;
	}

	public Map<String, NodeProfile> getCurrentNodePhaseProfiles() {
		return currentNodePhaseProfiles;
	}

	public List<NodeAction> getCurrentNodeActions() {
		return currentNodeActions;
	}

	private void generateCurrentNodeActions() throws IOException {
		long phaseStartTime = 0, phaseEndTime = 0;

		// Generate actions for setting BFT/RFT data message sending periods
		currentNodeActions
				.add(new NodeAction(phaseStartTime, Type.APP_FORCE_BFT_RFT_MSG_SENDING, forceBFTRFTMessageSending));
		currentNodeActions.add(new NodeAction(phaseStartTime, Type.BFT_SET_MSG_SEND_PERIOD, bftDataSendingPeriod));
		currentNodeActions.add(new NodeAction(phaseStartTime, Type.RFT_SET_MSG_SEND_PERIOD, rftDataSendingPeriod));

		// Generate node actions to perform for each phase
		for (int i = 0; i < phases.size(); i++) {
			ScenarioPhase phase = phases.get(i);
			NodeProfile nodeProfile = currentNodePhaseProfiles.get(phase.getName());
			phaseStartTime = phaseEndTime;
			phaseEndTime += phase.getDuration();
			List<String> recipientNodes = new ArrayList<String>(nodes.keySet());
			recipientNodes.remove(currentNodeName);
			currentNodeActions.addAll(
					nodeProfile.generateNodeActions(currentNodeName, recipientNodes, phaseStartTime, phaseEndTime));
		}

		// Generate actions for stopping BFT/RFT data message sendings
		currentNodeActions.add(new NodeAction(phaseEndTime, Type.APP_FORCE_BFT_RFT_MSG_SENDING, false));
		currentNodeActions.add(new NodeAction(phaseEndTime, Type.BFT_SET_MSG_SEND_PERIOD, 0));
		currentNodeActions.add(new NodeAction(phaseEndTime, Type.RFT_SET_MSG_SEND_PERIOD, 0));

		// Save generated actions to file
		saveCurrentNodeActionsToFile();
	}

	private void saveCurrentNodeActionsToFile() throws IOException {
		List<String> lines = new LinkedList<String>();
		for (NodeAction action : currentNodeActions)
			lines.add(action.getDelay() + SEP + action.getType() + SEP
					+ Arrays.stream(action.getArgs()).map(o -> o.toString()).collect(Collectors.joining(SEP)));// .reduce((o1,
																												// o2)
																												// -> o1
																												// + SEP
																												// +
																												// o2).get());
		FileUtil.writeLinesToFile(Paths.get(path.toString(), CURRENT_NODE_ACTIONS_FILE_NAME).toString(), lines);
	}

	private void createTakeAppConfiguration() throws IOException {
		// Create Take config.properties and node_settings.csv files
		this.takeAppConfiguration = TakeAppConfiguration.createDefaultConfigurationFromFile();
		takeAppConfiguration.setNodeName(currentNodeName);
		takeAppConfiguration.setNodes(nodes);
		takeAppConfiguration.setRadioSupplier(nodes.get(currentNodeName).getRadioSupplier());
		takeAppConfiguration.setRadioChannel(nodes.get(currentNodeName).getRadioChannel());
		// takeAppConfiguration.setSendingStrategy(sendingStrategy, responseTimeout); //
		// Set in default config.properties file
		// takeAppConfiguration.setBFTMessageSendingPeriod(bft_message_sending_period);
		// // Set in default config.properties file
		// takeAppConfiguration.setRFTMessageSendingPeriod(rft_message_sending_period);
		// // Set in default config.properties file
		takeAppConfiguration.saveConfigPropertiesTo(path.toString());
		takeAppConfiguration.saveNodeSettingsTo(path.toString());
	}

	public TakeAppConfiguration getTakeAppConfiguration() {
		return takeAppConfiguration;
	}

	public Path getPath() {
		return path;
	}

	public long getDuration() {
		long duration = 0;
		for (ScenarioPhase phase : phases)
			duration += phase.getDuration();
		return duration;
	}

	@Override
	public String toString() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", id);
		jsonObject.put("date", DateUtil.formatForFile(date));
		jsonObject.put("name", name);
		jsonObject.put("bftDataSendingPeriod", getBFTDataSendingPeriod());
		jsonObject.put("rftDataSendingPeriod", getRFTDataSendingPeriod());
		jsonObject.put("forceBFTRFTMessageSending", isForceBFTRFTMessageSending());

		JSONArray jsonPhasesArray = new JSONArray();
		for (int i = 0; i < phases.size(); i++) {
			JSONObject jsonPhaseObject = new JSONObject();
			ScenarioPhase phase = phases.get(i);
			jsonPhaseObject.put("phaseName", phase.getName());
			jsonPhaseObject.put("phaseDuration", phase.getDuration());
			jsonPhasesArray.put(i, jsonPhaseObject);
		}
		jsonObject.put("phases", jsonPhasesArray);

		JSONArray jsonNodesArray = new JSONArray();
		int n = 0;
		for (Node node : nodes.values()) {
			JSONObject jsonNodeObject = new JSONObject();
			jsonNodeObject.put("nodeName", node.getName());
			jsonNodeObject.put("nodeType", node.getType());
			jsonNodeObject.put("nodeIP", node.getNodeIP());
			jsonNodeObject.put("radioIP", node.getRadioIP());
			jsonNodeObject.put("radioID", node.getRadioID());
			jsonNodeObject.put("radioSupplier", node.getRadioSupplier());
			jsonNodeObject.put("radioChannel", node.getRadioChannel());
			jsonNodesArray.put(n++, jsonNodeObject);
		}
		jsonObject.put("nodes", jsonNodesArray);

		jsonObject.put("nodeName", currentNodeName);

		JSONArray jsonCurrentNodePhaseProfilesArray = new JSONArray();
		n = 0;
		for (String phaseName : currentNodePhaseProfiles.keySet()) {
			NodeProfile np = currentNodePhaseProfiles.get(phaseName);
			JSONObject jsonProfileObject = new JSONObject();
			jsonProfileObject.put("phaseName", phaseName);
			jsonProfileObject.put("profileName", np.getName());
			float msgSendingFrequency = np.getMsgSendingFrequency();
			jsonProfileObject.put("msgSendingFrequency", (double) msgSendingFrequency);
			if (msgSendingFrequency > 0.f) {
				jsonProfileObject.put("msgSendingTimeDistributionName", np.getMsgSendingTimeDistribution().getName());
				JSONArray jsonDistributionParametersArray = new JSONArray();
				String[] distributionParameters = np.getMsgSendingTimeDistribution().getParameters();
				for (int m = 0; m < distributionParameters.length; m++) {
					jsonDistributionParametersArray.put(m, distributionParameters[m]);
				}
				jsonProfileObject.put("msgSendingTimeDistributionParameters", jsonDistributionParametersArray);
			}
			jsonProfileObject.put("msgRecipientsMax", np.getMsgRecipientsMax());
			JSONArray jsonMsgContentSizesArray = new JSONArray();
			int[] msgContentSizes = np.getMsgContentSizes();
			for (int m = 0; m < msgContentSizes.length; m++) {
				jsonMsgContentSizesArray.put(m, msgContentSizes[m]);
			}
			jsonProfileObject.put("msgContentSizes", jsonMsgContentSizesArray);
			jsonCurrentNodePhaseProfilesArray.put(n++, jsonProfileObject);
		}
		jsonObject.put("currentNodePhaseProfiles", jsonCurrentNodePhaseProfilesArray);

		return jsonObject.toString(3);
	}

	/**
	 * Save scenario to JSON file in its scenario folder
	 * 
	 * @throws IOException
	 */
	public void saveToFile() throws IOException {
		String scenarioJSON = this.toString();
		FileUtil.writeTextToFile(Paths.get(path.toString(), SCENARIO_JSON_FILE_NAME).toString(), scenarioJSON);
	}

	/**
	 * Load scenario from the given JSON file
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public static Scenario loadFromFile(String filePath) throws IOException, JSONException, ParseException {
		String scenarioJSON = FileUtil.readTextFromFile(filePath);
		return new Scenario(scenarioJSON);
	}

}
