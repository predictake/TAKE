package main.java.emultake.model;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class ScenarioPhase {

	private String name; // = ID
	private long duration; // [s]

	public ScenarioPhase(String name, long duration) {
		this.name = name;
		this.duration = duration;
	}

	public String getName() {
		return name;
	}

	public long getDuration() {
		return duration;
	}

}
