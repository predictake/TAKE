package main.java.emultake.model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class ScenarioRunResult {

	private Scenario scenario;
	private ScenarioRun scenarioRun;
	private String nodeName;

	// Statistics for User messages
	private int userMessageRecipients;
	private float userMessageCompletionRate;
	private int userMessagesReceived;
	private int userMessagesSent_NoACK;
	private int userMessagesSent_ACK;
	private long userMessageMeanRTT;

	// Statistics for IP messages
	private float ipMessageCompletionRate;
	private int ipMessagesReceived;
	private int ipMessagesSent_NoACK;
	private int ipMessagesSent_ACK;
	private long ipMessageMeanRTT;
	private long ipMessagesReceivedMeanProcessingTime;

	// Statistics for BFT/RFT messages
	private float bftKnownGeolocationRate;
	private int[] meanTimeBetweenBFTGeolocationUpdatesPerNode;
	private int[] meanTimeBetweenRFTGeolocationUpdatesPerNode;
	private int[] meanBFTGeolocationsReceivedPerNode;
	private int[] meanRFTGeolocationsReceivedPerNode;

	public ScenarioRunResult(Scenario scenario, ScenarioRun scenarioRun, String nodeName, int userMessageRecipients,
			float userMessageCompletionRate, int userMessagesReceived, int userMessageSent_NoACK,
			int userMessagesSent_ACK, long userMessageMeanRTT, float ipMessageCompletionRate, int ipMessagesReceived,
			int ipMessagesSent_NoACK, int ipMessagesSent_ACK, long ipMessageMeanRTT,
			long ipMessagesReceivedMeanProcessingTime, float bftKnownGeolocationRate,
			int[] meanTimeBetweenBFTGeolocationUpdatesPerNode, int[] meanTimeBetweenRFTGeolocationUpdatesPerNode,
			int[] meanBFTGeolocationsReceivedPerNode, int[] meanRFTGeolocationsReceivedPerNode) {
		this.scenario = scenario;
		this.scenarioRun = scenarioRun;
		this.nodeName = nodeName;
		this.userMessageRecipients = userMessageRecipients;
		this.userMessageCompletionRate = userMessageCompletionRate;
		this.userMessagesReceived = userMessagesReceived;
		this.userMessagesSent_NoACK = userMessageSent_NoACK;
		this.userMessagesSent_ACK = userMessagesSent_ACK;
		this.userMessageMeanRTT = userMessageMeanRTT;
		this.ipMessageCompletionRate = ipMessageCompletionRate;
		this.ipMessagesReceived = ipMessagesReceived;
		this.ipMessagesSent_NoACK = ipMessagesSent_NoACK;
		this.ipMessagesSent_ACK = ipMessagesSent_ACK;
		this.ipMessageMeanRTT = ipMessageMeanRTT;
		this.ipMessagesReceivedMeanProcessingTime = ipMessagesReceivedMeanProcessingTime;
		this.bftKnownGeolocationRate = bftKnownGeolocationRate;
		this.meanTimeBetweenBFTGeolocationUpdatesPerNode = meanTimeBetweenBFTGeolocationUpdatesPerNode;
		this.meanTimeBetweenRFTGeolocationUpdatesPerNode = meanTimeBetweenRFTGeolocationUpdatesPerNode;
		this.meanBFTGeolocationsReceivedPerNode = meanBFTGeolocationsReceivedPerNode;
		this.meanRFTGeolocationsReceivedPerNode = meanRFTGeolocationsReceivedPerNode;
	}

	public ScenarioRunResult(String scenarioRunResultJSON, ScenarioRun scenarioRun) {
		JSONObject scenarRunResultJSON = new JSONObject(scenarioRunResultJSON); // scenarRunResultJSON
		this.scenario = scenarioRun.getScenario();
		this.scenarioRun = scenarioRun;
		this.nodeName = scenario.getCurrentNodeName();

		// Statistics for User messages
		this.userMessageRecipients = scenarRunResultJSON.getInt("User Messages Recipients");
		this.userMessageCompletionRate = (float) scenarRunResultJSON.getDouble("User Message Completion Rate");
		this.userMessagesReceived = scenarRunResultJSON.getInt("User Messages Received"); // As a recipient of the MSG
		this.userMessagesSent_NoACK = scenarRunResultJSON.getInt("User Messages Sent - NO ACK Received"); // As a sender
																											// of the
																											// MSG
		this.userMessagesSent_ACK = scenarRunResultJSON.getInt("User Messages Sent - ACK Received"); // As a sender of
																										// the MSG
		this.userMessageMeanRTT = scenarRunResultJSON.getLong("User Message Mean Round-Trip-Time");

		// Statistics for IP messages
		this.ipMessageCompletionRate = (float) scenarRunResultJSON.getDouble("IP Message Completion Rate");
		this.ipMessagesReceived = scenarRunResultJSON.getInt("IP Messages Received"); // As any node (sender,
																						// intermediary, recipient)
		this.ipMessagesSent_NoACK = scenarRunResultJSON.getInt("IP Messages Sent - NO ACK Received"); // As any node
																										// (sender,
																										// intermediary,
																										// recipient)
		this.ipMessagesSent_ACK = scenarRunResultJSON.getInt("IP Messages Sent - ACK Received"); // As any node (sender,
																									// intermediary,
																									// recipient)
		this.ipMessageMeanRTT = scenarRunResultJSON.getLong("IP Message Mean Round-Trip-Time");
		this.ipMessagesReceivedMeanProcessingTime = scenarRunResultJSON
				.getLong("IP Messages Received Mean Processing Time");

		// Statistics for BFT/RFT messages
		this.bftKnownGeolocationRate = (float) scenarRunResultJSON.getDouble("BFT Known Geolocation Rate");
		JSONArray meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON = scenarRunResultJSON
				.getJSONArray("Mean Time Between BFT Geolocation Updates Per Node");
		this.meanTimeBetweenBFTGeolocationUpdatesPerNode = new int[meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON
				.length()];
		for (int i = 0; i < meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON.length(); i++)
			this.meanTimeBetweenBFTGeolocationUpdatesPerNode[i] = meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON
					.getInt(i);
		JSONArray meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON = scenarRunResultJSON
				.getJSONArray("Mean Time Between RFT Geolocation Updates Per Node");
		this.meanTimeBetweenRFTGeolocationUpdatesPerNode = new int[meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON
				.length()];
		for (int i = 0; i < meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON.length(); i++)
			this.meanTimeBetweenRFTGeolocationUpdatesPerNode[i] = meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON
					.getInt(i);
		JSONArray meanBFTGeolocationsReceivedPerNodeJSON = scenarRunResultJSON
				.getJSONArray("BFT Geolocations Received Per Node");
		this.meanBFTGeolocationsReceivedPerNode = new int[meanBFTGeolocationsReceivedPerNodeJSON.length()];
		for (int i = 0; i < meanBFTGeolocationsReceivedPerNodeJSON.length(); i++)
			this.meanBFTGeolocationsReceivedPerNode[i] = meanBFTGeolocationsReceivedPerNodeJSON.getInt(i);
		JSONArray meanRFTGeolocationsReceivedPerNodeJSON = scenarRunResultJSON
				.getJSONArray("RFT Geolocations Received Per Node");
		this.meanRFTGeolocationsReceivedPerNode = new int[meanRFTGeolocationsReceivedPerNodeJSON.length()];
		for (int i = 0; i < meanRFTGeolocationsReceivedPerNodeJSON.length(); i++)
			this.meanRFTGeolocationsReceivedPerNode[i] = meanRFTGeolocationsReceivedPerNodeJSON.getInt(i);
	}

	public Scenario getScenario() {
		return scenario;
	}

	public ScenarioRun getScenarioRun() {
		return scenarioRun;
	}

	public String getNodeName() {
		return nodeName;
	}

	public JSONObject toJSON() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Node Name", nodeName);

		// Statistics for User messages
		jsonObject.put("User Messages Recipients", userMessageRecipients);
		jsonObject.put("User Message Completion Rate", userMessageCompletionRate);
		jsonObject.put("User Messages Received", userMessagesReceived);
		jsonObject.put("User Messages Sent - NO ACK Received", userMessagesSent_NoACK);
		jsonObject.put("User Messages Sent - ACK Received", userMessagesSent_ACK);
		jsonObject.put("User Message Mean Round-Trip-Time", userMessageMeanRTT);

		// Statistics for IP messages
		jsonObject.put("IP Message Completion Rate", ipMessageCompletionRate);
		jsonObject.put("IP Messages Received", ipMessagesReceived);
		jsonObject.put("IP Messages Sent - NO ACK Received", ipMessagesSent_NoACK);
		jsonObject.put("IP Messages Sent - ACK Received", ipMessagesSent_ACK);
		jsonObject.put("IP Message Mean Round-Trip-Time", ipMessageMeanRTT);
		jsonObject.put("IP Messages Received Mean Processing Time", ipMessagesReceivedMeanProcessingTime);

		// Statistics for BFT/RFT messages
		jsonObject.put("BFT Known Geolocation Rate", bftKnownGeolocationRate);
		jsonObject.put("Mean Time Between BFT Geolocation Updates Per Node",
				meanTimeBetweenBFTGeolocationUpdatesPerNode);
		jsonObject.put("Mean Time Between RFT Geolocation Updates Per Node",
				meanTimeBetweenRFTGeolocationUpdatesPerNode);
		jsonObject.put("BFT Geolocations Received Per Node", meanBFTGeolocationsReceivedPerNode);
		jsonObject.put("RFT Geolocations Received Per Node", meanRFTGeolocationsReceivedPerNode);

		return jsonObject;
	}

	@Override
	public String toString() {
		return toJSON().toString(3);
	}

}
