package main.java.emultake.model.distributions;

/**
 * 
 * @author Gisler Christophe
 *
 */
public interface Distribution {

	public String getName();

	public String[] getParameters();

	/**
	 * 
	 * @param min
	 *            value
	 * @param max
	 *            value
	 * @return random value between the given min and max values
	 */
	public long nextValueBewteen(long min, long max);

}
