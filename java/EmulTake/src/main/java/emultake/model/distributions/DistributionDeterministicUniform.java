package main.java.emultake.model.distributions;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionDeterministicUniform implements Distribution {

	public static final String NAME = "Deterministic-Uniform";

	private float frequency;
	private long i = 0;

	public DistributionDeterministicUniform(float frequency) {
		this.frequency = frequency;
	}

	public float getFrequency() {
		return frequency;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { "" + frequency };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		if (frequency <= 0.f)
			return 0l;
		// E.g. frequency = 1 [msg/s] => 0, 1, 2, 3, 4, 5, 6, 7,...
		// 0/1 -> 0, 1/1 -> 1, 2/1 -> 2, 3/1 -> 3, 4/1 -> 4, 5/1 -> 5, 6/1 -> 6, 7/1 ->
		// 7,...
		// E.g. frequency = 2 [msg/s] => 0, 0, 1, 1, 2, 2, 3, 3,...
		// 0/2 -> 0, 1/2 -> 0, 2/2 -> 1, 3/2 -> 1, 4/2 -> 2, 5/2 -> 2, 6/2 -> 3, 7/2 ->
		// 3,...
		// E.g. frequency = 3 [msg/s] => 0, 0, 0, 1, 1, 1, 2, 2, 2,...
		// 0/3 -> 0, 1/3 -> 0, 2/3 -> 0, 3/3 -> 1, 4/3 -> 1, 5/3 -> 1, 6/3 -> 2, 7/3 ->
		// 2,...
		// E.g. frequency = 0.5 [msg/s] => 0, 2, 4, 6, 8,...
		return (long) (i++ / frequency);
	}

}
