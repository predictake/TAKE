package main.java.emultake.model.distributions;

import java.util.Random;

/**
 * 
 * Poisson random distribution.
 * 
 * N.B.: For n sufficiently large (usually n ≥ 20), a Poisson distribution of
 * parameter µ (also often called 𝜆) can be approximated by a normal
 * distribution of parameters µ and σ = √µ.
 * 
 * @see https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
 * @see http://www.real-statistics.com/binomial-and-related-distributions/poisson-distribution
 * 
 * @see http://www.real-statistics.com/sampling-distributions/simulation
 * @see http://cyberzoide.developpez.com/tutoriels/java/api-java-random
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionRandomPoisson implements Distribution {

	public static final String NAME = "Random-Poisson";

	private final double STEP = 500, EXP_STEP = Math.exp(STEP);

	private long seed;
	private Random random;

	private float mu; // = mean = lambda

	public DistributionRandomPoisson(long seed, float mu) {
		this.seed = seed;
		this.random = new Random(seed);
		this.mu = mu;
	}

	public long getSeed() {
		return seed;
	}

	public float getMu() {
		return mu;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { "" + seed, "" + mu };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		long value = nextPoisson(mu);
		/*
		 * if (value < min) value = min; else if (value > max) value = max;
		 */
		while (value < min || value > max)
			value = nextPoisson(mu);
		return value;
	}

	private long nextPoisson(float lambda) {
		double p = 1.0;
		long k = 0;

		// Original Knuth's version
		/*
		 * double l = Math.exp(-lambda); do { k++; p *= random.nextDouble(); } while (p
		 * > l);
		 */

		// Junhao's version, based on Knuth's
		double lambdaLeft = lambda;
		do {
			k++;
			p *= random.nextDouble();
			if (p < Math.E && lambdaLeft > 0.) {
				if (lambdaLeft > STEP) {
					p *= EXP_STEP;
					lambdaLeft -= STEP;
				} else {
					p *= Math.exp(lambdaLeft);
					lambdaLeft = -1;
				}
			}
		} while (p > 1);

		return (k - 1);
	}

}
