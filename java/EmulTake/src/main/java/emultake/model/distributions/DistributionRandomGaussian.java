package main.java.emultake.model.distributions;

import java.util.Random;

/**
 * 
 * Gaussian (normal) random distribution.
 * 
 * Note: a normal distribution is theoretically infinite. Thus, the numbers
 * falling outside the given range [min, max] must be discarded. However, by
 * choosing a standard deviation equal to (max - min) / 6, we can ensure that
 * 99.7% of the generated numbers will fall within the range. Indeed, about
 * 99.7% of a distribution is within +/- 3 standard deviations.
 * 
 * @see http://www.real-statistics.com/normal-distribution/basic-characteristics-normal-distribution
 * @see https://en.wikipedia.org/wiki/68–95–99.7_rule
 * 
 * @see http://www.real-statistics.com/sampling-distributions/simulation
 * @see http://cyberzoide.developpez.com/tutoriels/java/api-java-random
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionRandomGaussian implements Distribution {

	public static final String NAME = "Random-Gaussian";

	private long seed;
	private Random random;

	private float mu, sigma;

	public DistributionRandomGaussian(long seed, float mu, float sigma) {
		this.seed = seed;
		this.random = new Random(seed);
		this.mu = mu;
		this.sigma = sigma;
	}

	public DistributionRandomGaussian(long seed, long min, long max) {
		this(seed, (float) (max + min) / 2.f, (float) (max - min) / 6.f);
	}

	public long getSeed() {
		return seed;
	}

	public float getMu() {
		return mu;
	}

	public float getSigma() {
		return sigma;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { "" + seed, "" + mu, "" + sigma };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		long value = nextGaussian(mu, sigma);
		/*
		 * if (value < min) value = min; else if (value > max) value = max;
		 */
		while (value < min || value > max)
			value = nextGaussian(mu, sigma);
		return value;
	}

	private long nextGaussian(float mu, float sigma) {
		return Math.round(mu + (sigma * random.nextGaussian()));
	}

}
