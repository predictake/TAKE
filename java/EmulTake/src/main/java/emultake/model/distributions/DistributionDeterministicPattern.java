package main.java.emultake.model.distributions;

import java.util.Arrays;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionDeterministicPattern implements Distribution {

	public static final String NAME = "Deterministic-Pattern";

	private String pattern;
	private int[] patternI;
	private int sum, n = 0;

	public DistributionDeterministicPattern(String pattern) {
		this.pattern = pattern;
		this.patternI = pattern.chars().map(c -> Character.getNumericValue(c)).toArray();
		this.sum = Arrays.stream(patternI).sum();
	}

	public String getPattern() {
		return pattern;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { pattern };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		int j = n / sum;
		int ni = n++ % sum;
		int sumi = 0;
		for (int i = 0; i < patternI.length; i++) {
			for (int v = 0; v < patternI[i]; v++) {
				if (sumi == ni) {
					return patternI.length * j + i;
				} else
					sumi++;
			}
		}
		return 0l;
	}

}
