package main.java.emultake.model.distributions;

import java.util.Random;

/**
 * 
 * Uniform random distribution.
 * 
 * @see http://www.real-statistics.com/sampling-distributions/simulation
 * @see http://cyberzoide.developpez.com/tutoriels/java/api-java-random
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionRandomUniform implements Distribution {

	public static final String NAME = "Random-Uniform";

	private long seed;
	private Random random;

	public DistributionRandomUniform(long seed) {
		this.seed = seed;
		this.random = new Random(seed);
	}

	public long getSeed() {
		return seed;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { "" + seed };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		return Math.round(min + (max - min) * random.nextDouble());
	}

}
