package main.java.emultake.model.distributions;

import java.util.Random;

/**
 * 
 * Exponential random distribution.
 * 
 * @see http://www.real-statistics.com/sampling-distributions/simulation
 * @see http://cyberzoide.developpez.com/tutoriels/java/api-java-random
 * 
 * @author Gisler Christophe
 *
 */
public class DistributionRandomExponential implements Distribution {

	public static final String NAME = "Random-Exponential";

	private long seed;
	private Random random;

	private float lambda; // The rate parameter λ is a measure of frequency, e.g. the average rate of
							// events per unit of time

	public DistributionRandomExponential(long seed, float lambda) {
		this.seed = seed;
		this.random = new Random(seed);
		this.lambda = lambda;
	}

	public long getSeed() {
		return seed;
	}

	public float getLambda() {
		return lambda;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getParameters() {
		return new String[] { "" + seed, "" + lambda };
	}

	@Override
	public long nextValueBewteen(long min, long max) {
		long value = min + nextExp();
		while (value > max)
			value = min + nextExp();
		return value;
	}

	private long nextExp() {
		return Math.round(-Math.log(1 - random.nextDouble()) / lambda);
	}

}
