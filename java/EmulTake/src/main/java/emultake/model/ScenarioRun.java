package main.java.emultake.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.emultake.controller.EmulTakeController;
import main.java.emultake.controller.Preferences;
import main.java.emultake.takeservices.TakeAPPService;
import main.java.emultake.util.DateUtil;
import main.java.emultake.util.FileUtil;
import main.java.emultake.util.TerminalUtil;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class ScenarioRun implements Callable<ScenarioRunResult> {

	public enum Status {
		NA, SCHEDULED, RUNNING, COMPLETED, CANCELLED, FAILED;
	}

	public static final String SCENARIO_RUN_JSON_FILE_NAME = "scenarioRun.json";

	private static final String EXECUTED_NODE_ACTIONS_FILE_NAME = "executed_node_actions.csv";

	private static final String SEP = ",";

	private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioRun.class);

	private int id;
	private Scenario scenario;
	private Date startDate, endDate;
	private Status status;
	private Path path;
	private ScenarioRunResult result = null;

	private Map<NodeAction, Boolean> nodeActionExecutions = Collections
			.synchronizedMap(new LinkedHashMap<NodeAction, Boolean>()); // Key = node action, value = successfully
																		// executed

	private ScheduledExecutorService scheduledExecutorService = Executors
			.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());

	public ScenarioRun(int id, Scenario scenario, Date startDate) throws IOException {
		this.id = id;
		this.scenario = scenario;
		this.startDate = startDate;
		this.endDate = new Date(
				startDate.getTime() + 1000 * (scenario.getDuration() + Preferences.getExtraTimeToFinishRun()));
		this.status = Status.SCHEDULED;
		this.path = Paths.get(scenario.getPath().toString(), "run_" + id);

		// Create/generate required folder, files,...
		Files.createDirectories(path);
		saveToFile();
		saveScenarioToFile();
	}

	/**
	 * Constructor used to restore saved runs at EmulTake application restart
	 * 
	 * @param scenarioRunJSON
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public ScenarioRun(String scenarioRunJSON) throws IOException, JSONException, ParseException {
		JSONObject scenarRunJSON = new JSONObject(scenarioRunJSON);
		this.id = scenarRunJSON.getInt("id");
		String scenarioID = scenarRunJSON.getString("Scenario ID");
		this.scenario = EmulTakeController.getInstance().getScenarios().get(scenarioID);
		this.startDate = DateUtil.parseFromFileFormat(scenarRunJSON.getString("Start Date"));
		this.endDate = new Date(
				startDate.getTime() + 1000 * (scenario.getDuration() + Preferences.getExtraTimeToFinishRun()));
		this.status = Status.SCHEDULED;
		this.path = Paths.get(scenario.getPath().toString(), "run_" + id);

		JSONObject runResultJSON = scenarRunJSON.optJSONObject("Run Result");
		if (runResultJSON != null)
			this.result = new ScenarioRunResult(runResultJSON.toString(), this);

		// Files are already saved on disk
	}

	public int getID() {
		return id;
	}

	public Scenario getScenario() {
		return scenario;
	}

	public Date getStartDate() {
		return startDate;
	}

	/**
	 * End date is computed as: scenario duration + extra time to finish run
	 * 
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	public Status getStatus() {
		return status;
	}

	public Path getPath() {
		return path;
	}

	public ScenarioRunResult getResult() {
		return result;
	}

	/**
	 * This method has no effect if this scenario run is already DONE, CANCELLED or
	 * FAILED
	 */
	public void cancel() {
		if (status == Status.SCHEDULED || status == Status.RUNNING) {
			status = Status.CANCELLED;
			scheduledExecutorService.shutdownNow();
		}
	}

	@Override
	public ScenarioRunResult call() throws Exception {
		final int takeAppLaunchDelayBeforeRun = Preferences.getTakeAppLaunchDelayBeforeScenarioRun();
		final int extraTimeToFinish = Preferences.getExtraTimeToFinishRun();

		LOGGER.info("Scenario run {} starting", id);

		// Stop, reset, configure and start Take application directly
		// This operation will have the duration [s] of the takeAppLaunchDelayBeforeRun
		// parameter to finish execution before the scenario run starts
		try {
			EmulTakeController.getInstance().stopTakeJavaApp();
			EmulTakeController.getInstance().resetTakeJavaApp();
			EmulTakeController.getInstance().startTakeJavaApp(scenario.getTakeAppConfiguration(),
					takeAppLaunchDelayBeforeRun);

			LOGGER.info("Take application started with configuration of run " + id);
		} catch (Exception e) {
			status = Status.FAILED;

			LOGGER.error("Run " + id + " aborted because Take application could not be started", e);
			return null;
		}
		status = Status.RUNNING;

		LOGGER.info("Scenario run {} started", id);

		// Run scenario, i.e. execute current node scenario actions
		List<NodeAction> currentNodeScenarioActions = scenario.getCurrentNodeActions();
		for (NodeAction currentNodeScenarioAction : currentNodeScenarioActions) {
			scheduledExecutorService.schedule(new Runnable() {
				@Override
				public void run() {
					try {
						currentNodeScenarioAction.execute();
						nodeActionExecutions.put(currentNodeScenarioAction, true);

						LOGGER.info("Node action of type {} executed after {} seconds",
								currentNodeScenarioAction.getType(), currentNodeScenarioAction.getDelay());
					} catch (Exception e) {
						status = Status.FAILED;
						nodeActionExecutions.put(currentNodeScenarioAction, false);

						LOGGER.error("Node action of type " + currentNodeScenarioAction.getType() + " scheduled after "
								+ currentNodeScenarioAction.getDelay() + " seconds could not be executed", e);
					}
				}
			}, currentNodeScenarioAction.getDelay(), TimeUnit.SECONDS);
		}
		scheduledExecutorService.shutdown();
		scheduledExecutorService.awaitTermination(scenario.getDuration() + extraTimeToFinish, TimeUnit.SECONDS);

		// Let extra time finish run (for ACKs of sent messages to arrive)
		Thread.sleep(extraTimeToFinish * 1000);

		// Ask Take application to save its transmission statistics
		try {
			TakeAPPService.getInstance().saveTransmissionStatistics();

			LOGGER.info("Take application saved transmission statistics");
		} catch (Exception e) {
			status = Status.FAILED;

			LOGGER.error("Take application could not save transmission statistics", e);
		}

		// Stop Take application
		try {
			EmulTakeController.getInstance().stopTakeJavaApp();

			LOGGER.info("Take application quitted");
		} catch (Exception e) {
			status = Status.FAILED;

			LOGGER.error("Take application could not be quitted", e);
		}

		// Copy Take application generated files to run data folder
		try {
			EmulTakeController.getInstance().copyTakeAppFiles(path.toString(), scenario.getTakeAppConfiguration());

			LOGGER.info("Take application generated files copied to run {} folder", id);
		} catch (Exception e) {
			status = Status.FAILED;

			LOGGER.error("Take application generated files could not be copied to run " + id + " folder", e);
		}

		// Build scenario run result from generated Take statistics JSON file
		try {
			Path takeStatsFilePath = Paths.get(path.toString(), EmulTakeController.TAKE_APP_FILES_FOLDER_NAME,
					scenario.getTakeAppConfiguration().getDataDirPath(), "transmission_stats.json");
			// if (takeStatsFilePath.toFile().exists()) {
			String takeStatsFileContent = FileUtil.readTextFromFile(takeStatsFilePath);
			this.result = getScenarioRunResultFromTakeStatsFileContent(takeStatsFileContent);
			saveToFile();
			/*
			 * } else { LOGGER.
			 * error("Scenario run statistics file was not generated by the Take application and thus not copied"
			 * ); }
			 */
			saveExecutedNodeActionsToFile();
			saveEmulTakeLogFile();

			LOGGER.info("Scenario run result generated and saved to files");
		} catch (Exception e) {
			status = Status.FAILED;

			LOGGER.error("Scenario run result could not be generated and/or not saved to files", e);
		}

		// If run has failed, return null, else run has completed so return scenario run
		// result
		if (status == Status.FAILED) {
			LOGGER.warn("Scenario run {} failed", id);
		} else {
			status = Status.COMPLETED;

			LOGGER.info("Scenario run {} completed ", id);
		}

		return this.result;
	}

	private ScenarioRunResult getScenarioRunResultFromTakeStatsFileContent(String takeStatsFileContent) {
		JSONObject takeStatsJSON = new JSONObject(takeStatsFileContent);
		// String currentNodeName = takeStatsJSON.getString("Current Node");

		// Statistics for User messages
		int userMessageRecipients = takeStatsJSON.getInt("User Messages Recipients");
		float userMessageCompletionRate = (float) takeStatsJSON.getDouble("User Message Completion Rate");
		int userMessagesReceived = takeStatsJSON.getInt("User Messages Received"); // As a recipient of the MSG
		int userMessagesSent_NoACK = takeStatsJSON.getInt("User Messages Sent - NO ACK Received"); // As a sender of the
																									// MSG
		int userMessagesSent_ACK = takeStatsJSON.getInt("User Messages Sent - ACK Received"); // As a sender of the MSG
		long userMessageMeanRTT = takeStatsJSON.getLong("User Message Mean Round-Trip-Time");

		// Statistics for IP messages
		float ipMessageCompletionRate = (float) takeStatsJSON.getDouble("IP Message Completion Rate");
		int ipMessagesReceived = takeStatsJSON.getInt("IP Messages Received"); // As any node (sender, intermediary,
																				// recipient)
		int ipMessagesSent_NoACK = takeStatsJSON.getInt("IP Messages Sent - NO ACK Received"); // As any node (sender,
																								// intermediary,
																								// recipient)
		int ipMessagesSent_ACK = takeStatsJSON.getInt("IP Messages Sent - ACK Received"); // As any node (sender,
																							// intermediary, recipient)
		long ipMessageMeanRTT = takeStatsJSON.getLong("IP Message Mean Round-Trip-Time");
		long ipMessagesReceivedMeanProcessingTime = takeStatsJSON.getLong("IP Messages Received Mean Processing Time");

		// Statistics for BFT/RFT messages
		float bftKnownGeolocationRate = (float) takeStatsJSON.getDouble("BFT Known Geolocation Rate");
		JSONArray meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON = takeStatsJSON
				.getJSONArray("Mean Time Between BFT Geolocation Updates Per Node");
		int[] meanTimeBetweenBFTGeolocationUpdatesPerNode = new int[meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON
				.length()];
		for (int i = 0; i < meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON.length(); i++)
			meanTimeBetweenBFTGeolocationUpdatesPerNode[i] = meanTimeBetweenBFTGeolocationUpdatesPerNodeJSON.getInt(i);
		JSONArray meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON = takeStatsJSON
				.getJSONArray("Mean Time Between RFT Geolocation Updates Per Node");
		int[] meanTimeBetweenRFTGeolocationUpdatesPerNode = new int[meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON
				.length()];
		for (int i = 0; i < meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON.length(); i++)
			meanTimeBetweenRFTGeolocationUpdatesPerNode[i] = meanTimeBetweenRFTGeolocationUpdatesPerNodeJSON.getInt(i);
		JSONArray meanBFTGeolocationsReceivedPerNodeJSON = takeStatsJSON
				.getJSONArray("BFT Geolocations Received Per Node");
		int[] meanBFTGeolocationsReceivedPerNode = new int[meanBFTGeolocationsReceivedPerNodeJSON.length()];
		for (int i = 0; i < meanBFTGeolocationsReceivedPerNodeJSON.length(); i++)
			meanBFTGeolocationsReceivedPerNode[i] = meanBFTGeolocationsReceivedPerNodeJSON.getInt(i);
		JSONArray meanRFTGeolocationsReceivedPerNodeJSON = takeStatsJSON
				.getJSONArray("RFT Geolocations Received Per Node");
		int[] meanRFTGeolocationsReceivedPerNode = new int[meanRFTGeolocationsReceivedPerNodeJSON.length()];
		for (int i = 0; i < meanRFTGeolocationsReceivedPerNodeJSON.length(); i++)
			meanRFTGeolocationsReceivedPerNode[i] = meanRFTGeolocationsReceivedPerNodeJSON.getInt(i);

		return new ScenarioRunResult(scenario, this, scenario.getCurrentNodeName(), userMessageRecipients,
				userMessageCompletionRate, userMessagesReceived, userMessagesSent_NoACK, userMessagesSent_ACK,
				userMessageMeanRTT, ipMessageCompletionRate, ipMessagesReceived, ipMessagesSent_NoACK,
				ipMessagesSent_ACK, ipMessageMeanRTT, ipMessagesReceivedMeanProcessingTime, bftKnownGeolocationRate,
				meanTimeBetweenBFTGeolocationUpdatesPerNode, meanTimeBetweenRFTGeolocationUpdatesPerNode,
				meanBFTGeolocationsReceivedPerNode, meanRFTGeolocationsReceivedPerNode);
	}

	@Override
	public String toString() {
		JSONObject jsonObject = new JSONObject();

		jsonObject.put("Run ID", id);
		jsonObject.put("Scenario ID", scenario.getID());
		jsonObject.put("Start Date", DateUtil.formatForFile(startDate));
		if (this.result != null)
			jsonObject.put("Run Result", result.toJSON());

		return jsonObject.toString(3);
	}

	/*
	 * Save scenario run to JSON file in its scenario run folder
	 * 
	 * @throws IOException
	 */
	private void saveToFile() throws IOException {
		String scenarioRunJSON = this.toString();
		FileUtil.writeTextToFile(Paths.get(path.toString(), SCENARIO_RUN_JSON_FILE_NAME).toString(), scenarioRunJSON);
	}

	/*
	 * Save scenario to JSON file in its scenario run folder
	 * 
	 * @throws IOException
	 */
	private void saveScenarioToFile() throws IOException {
		String scenarioJSON = this.scenario.toString();
		FileUtil.writeTextToFile(Paths.get(path.toString(), Scenario.SCENARIO_JSON_FILE_NAME).toString(), scenarioJSON);
	}

	private void saveExecutedNodeActionsToFile() throws IOException {
		List<String> lines = new LinkedList<String>();
		for (NodeAction action : nodeActionExecutions.keySet())
			lines.add((nodeActionExecutions.get(action) ? "Executed" : "Failed") + SEP + action.getDelay() + SEP
					+ action.getType() + SEP
					+ Arrays.stream(action.getArgs()).map(o -> o.toString()).collect(Collectors.joining(SEP)));// .reduce((o1,
																												// o2)
																												// -> o1
																												// + SEP
																												// +
																												// o2).get());
		FileUtil.writeLinesToFile(Paths.get(path.toString(), EXECUTED_NODE_ACTIONS_FILE_NAME).toString(), lines);
	}

	private void saveEmulTakeLogFile() throws IOException, InterruptedException {
		TerminalUtil.copy(Paths.get("logs", "emultake.log").toString(), path.toString());
	}

	/**
	 * Load scenario run from the given JSON file
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParseException
	 */
	public static ScenarioRun loadFromFile(String filePath) throws IOException, JSONException, ParseException {
		String scenarioRunJSON = FileUtil.readTextFromFile(filePath);
		return new ScenarioRun(scenarioRunJSON);
	}

}
