package main.java.emultake.model;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class Node {

	private String name; // = ID
	private String type;
	private String nodeIP;
	private String radioIP;
	private String radioID;
	private String radioSupplier;
	private String radioChannel;

	public Node(String name, String type, String nodeIP, String radioIP, String radioID, String radioSupplier,
			String radioChannel) {
		this.name = name;
		this.type = type;
		this.nodeIP = nodeIP;
		this.radioIP = radioIP;
		this.radioID = radioID;
		this.radioSupplier = radioSupplier;
		this.radioChannel = radioChannel;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getNodeIP() {
		return nodeIP;
	}

	public String getRadioIP() {
		return radioIP;
	}

	public String getRadioID() {
		return radioID;
	}

	public String getRadioSupplier() {
		return radioSupplier;
	}

	public String getRadioChannel() {
		return radioChannel;
	}

}
