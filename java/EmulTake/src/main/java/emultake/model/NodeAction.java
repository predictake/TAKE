package main.java.emultake.model;

import main.java.emultake.takeservices.TakeAPPService;
import main.java.emultake.takeservices.TakeBFTService;
import main.java.emultake.takeservices.TakeMSGService;
import main.java.emultake.takeservices.TakeRFTService;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class NodeAction {

	public enum Type {
		APP_SET_NODE_NAME, APP_SET_SEND_STRATEGY, APP_SAVE_STATS, APP_RESET, APP_FORCE_BFT_RFT_MSG_SENDING, MSG_NODE_STATUS, MSG_SEND_MSG, MSG_GET_MSGS, MSG_SEND_MSG_STATUS, MSG_GET_MSG_STATUS, BFT_SET_MSG_SEND_PERIOD, BFT_GET_ALLY_LOCATIONS, RFT_SET_MSG_SEND_PERIOD, RFT_GET_ENEMY_LOCATIONS, RFT_SET_ENEMY_LOCATION;
	}

	private static final TakeAPPService TAKE_APP_SERVICE = TakeAPPService.getInstance();
	private static final TakeMSGService TAKE_MSG_SERVICE = TakeMSGService.getInstance();
	private static final TakeBFTService TAKE_BFT_SERVICE = TakeBFTService.getInstance();
	private static final TakeRFTService TAKE_RFT_SERVICE = TakeRFTService.getInstance();

	private long delay; // [s]
	private Type type;
	private Object[] args;

	public NodeAction(long delay, Type type, Object... args) {
		this.delay = delay;
		this.type = type;
		this.args = args;
	}

	public long getDelay() {
		return delay;
	}

	public Type getType() {
		return type;
	}

	public Object[] getArgs() {
		return args;
	}

	public void execute() throws Exception {
		switch (type) {

		/* APP Service */
		case APP_SET_NODE_NAME:
			TAKE_APP_SERVICE.setNodeName((String) args[0]);
			break;
		case APP_SET_SEND_STRATEGY:
			TAKE_APP_SERVICE.setSendingStrategy((String) args[0], (String) args[1]);
			break;
		case APP_SAVE_STATS:
			TAKE_APP_SERVICE.saveTransmissionStatistics();
			break;
		case APP_RESET:
			TAKE_APP_SERVICE.reset();
			break;
		case APP_FORCE_BFT_RFT_MSG_SENDING:
			TAKE_APP_SERVICE.forceBFTRFTMessageSending((boolean) args[0]);
			break;

		/* MSG Service */
		case MSG_NODE_STATUS:
			TAKE_MSG_SERVICE.getNodeStatus((String) args[0]);
			break;
		case MSG_SEND_MSG:
			TAKE_MSG_SERVICE.sendMessage((int) args[0], (String) args[1], (String) args[2], (String) args[3]);
			break;
		case MSG_GET_MSGS:
			TAKE_MSG_SERVICE.getMessages((int) args[0], (String) args[1]);
			break;
		case MSG_SEND_MSG_STATUS:
			TAKE_MSG_SERVICE.sendMessageStatus((String) args[0], (String) args[1]);
			break;
		case MSG_GET_MSG_STATUS:
			TAKE_MSG_SERVICE.getMessageStatus((String) args[0]);
			break;

		/* BFT Service */
		case BFT_SET_MSG_SEND_PERIOD:
			TAKE_BFT_SERVICE.setMessageSendingPeriod((int) args[0]);
			break;
		case BFT_GET_ALLY_LOCATIONS:
			TAKE_BFT_SERVICE.getAllyLocations((float) args[0], (String) args[1]);
			break;

		/* RFT Service */
		case RFT_SET_MSG_SEND_PERIOD:
			TAKE_RFT_SERVICE.setMessageSendingPeriod((int) args[0]);
			break;
		case RFT_GET_ENEMY_LOCATIONS:
			TAKE_RFT_SERVICE.getEnemyLocations((float) args[0], (String) args[1]);
			break;
		case RFT_SET_ENEMY_LOCATION:
			TAKE_RFT_SERVICE.setEnemyLocation((String) args[0], (String) args[1], (String) args[2], (String) args[3]);
			break;
		}
	}

}
