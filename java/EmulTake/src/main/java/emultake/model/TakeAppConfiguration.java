package main.java.emultake.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TakeAppConfiguration {

	public static final String TAKE_DEFAULT_CONFIG_FILE_NAME = "take_default_config.properties";
	public static final String TAKE_DEFAULT_NODE_SETTINGS_FILE_NAME = "take_default_node_settings.csv";
	public static final String TAKE_CONFIG_FILE_NAME = "config.properties";
	public static final String TAKE_NODE_SETTINGS_FILE_NAME = "node_settings.csv";

	private static final String SEP = ",";

	private Properties appConfigProperties = new Properties();

	private Map<String, Node> nodes = Collections.synchronizedMap(new LinkedHashMap<String, Node>()); // Key = node
																										// name, value =
																										// node

	public TakeAppConfiguration() {
	}

	public static TakeAppConfiguration createDefaultConfigurationFromFile() throws FileNotFoundException, IOException {
		TakeAppConfiguration defaultTakeAppConfig = new TakeAppConfiguration();
		defaultTakeAppConfig.loadDefaultConfigProperties();
		defaultTakeAppConfig.loadDefaultNodeSettings();
		return defaultTakeAppConfig;
	}

	public void loadDefaultConfigProperties() throws FileNotFoundException, IOException {
		try (InputStream is = new FileInputStream(TAKE_DEFAULT_CONFIG_FILE_NAME)) {
			appConfigProperties.load(is);
		}
	}

	public void saveConfigPropertiesTo(String folderPath) throws FileNotFoundException, IOException {
		try (OutputStream os = new FileOutputStream(Paths.get(folderPath, TAKE_CONFIG_FILE_NAME).toString())) {
			appConfigProperties.store(os, "Take application configuration properties");
		}
	}

	public void loadDefaultNodeSettings() throws FileNotFoundException, IOException {
		if (!new File(TAKE_DEFAULT_NODE_SETTINGS_FILE_NAME).exists())
			return;
		boolean simulMode = isRadioGPSInterfaceSimulated();
		try (InputStream is = new FileInputStream(TAKE_DEFAULT_NODE_SETTINGS_FILE_NAME);
				Scanner scanner = new Scanner(is);) {
			while (scanner.hasNextLine()) {
				String[] lineData = scanner.nextLine().split(",");
				String nodeName = lineData[0].trim();
				String nodeType = lineData[1].trim();
				String nodeIP = lineData[2].trim();
				String radioID = simulMode ? null : lineData[3].trim();
				String radioIP = simulMode ? null : lineData[4].trim();
				String radioSupplier = simulMode ? null : lineData[5].trim();
				String radioChannel = simulMode ? null : lineData[6].trim();
				this.nodes.put(lineData[0],
						new Node(nodeName, nodeType, nodeIP, radioID, radioIP, radioSupplier, radioChannel));
			}
		}
	}

	public void saveNodeSettingsTo(String folderPath) throws IOException {
		List<String> lines = new LinkedList<String>();
		for (Node node : nodes.values())
			lines.add(node.getName() + SEP + node.getType() + SEP + node.getNodeIP() + SEP + node.getRadioID() + SEP
					+ node.getRadioIP());
		Files.write(Paths.get(folderPath, TAKE_NODE_SETTINGS_FILE_NAME), lines);
	}

	private String getAppConfigProperty(String propertyName) {
		return appConfigProperties.getProperty(propertyName);
	}

	private void setAppConfigProperty(String propertyName, String propertyValue) {
		appConfigProperties.setProperty(propertyName, propertyValue);
	}

	public String getNodeName() {
		return getAppConfigProperty("node_name");
	}

	public void setNodeName(String nodeName) {
		setAppConfigProperty("node_name", nodeName);
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String, Node> nodes) {
		this.nodes = nodes;
	}

	public String getDataDirPath() {
		return getAppConfigProperty("data_dir_path");
	}

	public int getWebServicePort() {
		return Integer.parseInt(getAppConfigProperty("web_service_port"));
	}

	public int getMessagingPort() {
		return Integer.parseInt(getAppConfigProperty("messaging_port"));
	}

	public String getTransportProtocol() {
		return getAppConfigProperty("transport_protocol");
	}

	/**
	 * 
	 * @return node location update period in [s]
	 */
	public long getNodeLocationUpdatePeriod() {
		return Long.parseLong(getAppConfigProperty("node_location_update_period"));
	}

	/**
	 * 
	 * @return routing table update period in [s]
	 */
	public long getRoutingTableUpdatePeriod() {
		return Long.parseLong(getAppConfigProperty("routing_table_update_period"));
	}

	public String getSendingStrategy() {
		return getAppConfigProperty("sending_strategy");
	}

	/**
	 * 
	 * @return message response timeout in [s]
	 */
	public int getResponseTimeout() {
		return Integer.parseInt(getAppConfigProperty("response_timeout"));
	}

	public void setSendingStrategy(String sendingStrategy, String responseTimeout) {
		setAppConfigProperty("sending_strategy", sendingStrategy);
		setAppConfigProperty("response_timeout", responseTimeout);
	}

	/**
	 * 
	 * @return BFT message sending period in [s]
	 */
	public int getBFTMessageSendingPeriod() {
		return Integer.parseInt(getAppConfigProperty("bft_message_sending_period"));
	}

	public void setBFTMessageSendingPeriod(int bftMessageSendingPeriod) {
		setAppConfigProperty("bft_message_sending_period", "" + bftMessageSendingPeriod);
	}

	/**
	 * 
	 * @return RFT message sending period in [s]
	 */
	public int getRFTMessageSendingPeriod() {
		return Integer.parseInt(getAppConfigProperty("rft_message_sending_period"));
	}

	public void setRFTMessageSendingPeriod(int rftMessageSendingPeriod) {
		setAppConfigProperty("rft_message_sending_period", "" + rftMessageSendingPeriod);
	}

	public boolean isBFTRFTMessageSendingForced() {
		return getAppConfigProperty("force_bft_rft_message_sending").toLowerCase().equals("true");
	}

	public int getRFTSimulatedEnemies() {
		return Integer.parseInt(getAppConfigProperty("rft_simulated_enemies"));
	}

	public boolean isRadioGPSInterfaceSimulated() {
		return getAppConfigProperty("radio_gps_interface").toLowerCase().equals("simulated");
	}

	public String getGPSdHost() {
		return getAppConfigProperty("gpsd_host");
	}

	public int getGPSdPort() {
		return Integer.parseInt(getAppConfigProperty("gpsd_port"));
	}

	public String getRadioSupplier() {
		return getAppConfigProperty("radio_supplier");
	}

	public void setRadioSupplier(String radioSupplier) {
		setAppConfigProperty("radio_supplier", radioSupplier);
	}

	public String getRadioChannel() {
		return getAppConfigProperty("radio_channel");
	}

	public void setRadioChannel(String radioChannel) {
		setAppConfigProperty("radio_channel", radioChannel);
	}

	public int getRadioRoutingTablePollingInterval() {
		return Integer.parseInt(getAppConfigProperty("radio_routing_table_polling_interval"));
	}

}
