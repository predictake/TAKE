package main.java.emultake.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Static class
 * 
 * @author Gisler Christophe
 *
 */
public class Preferences {

	public static final String PATH_SEP = File.separator;

	private static final Logger LOGGER = LoggerFactory.getLogger(Preferences.class);

	private static final String CONFIG_FILE_NAME = "config.properties";
	private static final Properties APP_CONFIG_PROPERTIES = new Properties();

	public static void load() {
		loadAppConfigProperties();
	}

	private static void loadAppConfigProperties() {
		try (InputStream is = new FileInputStream(CONFIG_FILE_NAME)) {
			APP_CONFIG_PROPERTIES.load(is);
		} catch (FileNotFoundException e) {
			LOGGER.error("Load application configuration properties - File not found", e);
		} catch (IOException e) {
			LOGGER.error("Load application configuration properties - I/O Exception", e);
		}
	}

	private static String getAppConfigProperty(String propertyName) {
		return APP_CONFIG_PROPERTIES.getProperty(propertyName);
	}

	private static String appendPathSeparator(String path) {
		return path.endsWith(PATH_SEP) ? path : path + PATH_SEP;
	}

	/*
	 * private static void saveAppConfigProperties() { try (OutputStream os = new
	 * FileOutputStream(CONFIG_FILE_NAME)) { appConfigProperties.store(os,
	 * "Take application default configuration properties"); } catch
	 * (FileNotFoundException e) {
	 * LOGGER.error("Save application configuration properties - File not found",
	 * e); } catch (IOException e) {
	 * LOGGER.error("Save application configuration properties - I/O Exception", e);
	 * } }
	 * 
	 * private void setAppConfigProperty(String propertyName, String propertyValue,
	 * boolean writeFile) { appConfigProperties.setProperty(propertyName,
	 * propertyValue); if (writeFile) saveAppConfigProperties(); }
	 */

	/**
	 * N.B.: Already includes the path separator at the end
	 * 
	 * @return
	 */
	public static String getDataDirPath() {
		return appendPathSeparator(getAppConfigProperty("data_dir_path"));
	}

	public static String getTakeAppDirPath() {
		return appendPathSeparator(getAppConfigProperty("take_app_dir_path"));
	}

	public static int getWebServicePort() {
		return Integer.parseInt(getAppConfigProperty("web_service_port"));
	}

	public static int getTakeAppWebServicePort() {
		return Integer.parseInt(getAppConfigProperty("take_app_ws_port"));
	}

	public static int getTakeAppLaunchDelayBeforeScenarioRun() {
		return Integer.parseInt(getAppConfigProperty("take_app_launch_delay_before_scenario_run"));
	}

	public static int getExtraTimeToFinishRun() {
		return Integer.parseInt(getAppConfigProperty("extra_time_to_finish_run"));
	}

	public static String getAppVersion() {
		String mavenPackage = "EmulTake";
		String mavenArtifact = "EmulTake";
		// Try to get version number from maven properties in jar's META-INF
		try (InputStream is = Preferences.class
				.getResourceAsStream("/META-INF/maven/" + mavenPackage + "/" + mavenArtifact + "/pom.properties")) {
			if (is != null) {
				Properties p = new Properties();
				p.load(is);
				String version = p.getProperty("version", "").trim();
				if (!version.isEmpty())
					return version;
			}
		} catch (IOException e) {
			LOGGER.error("Get app version - Resource not found", e);
		}
		return "N/A";
	}

	public static String getFormattedAppConfig() {
		StringBuilder sb = new StringBuilder();

		sb.append("Data directory path: " + getDataDirPath() + "\n");
		sb.append("Take application path: " + getTakeAppDirPath() + "\n");
		sb.append("Web service port: " + getWebServicePort() + "\n");
		sb.append("Take app launch delay before scenario run: " + getTakeAppLaunchDelayBeforeScenarioRun() + "\n");
		sb.append("Extra time to finish run: " + getExtraTimeToFinishRun() + "\n");

		return sb.toString();
	}

}
