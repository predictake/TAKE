package main.java.emultake.controller;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.emultake.model.Scenario;
import main.java.emultake.model.ScenarioRun;
import main.java.emultake.model.ScenarioRun.Status;
import main.java.emultake.model.ScenarioRunResult;
import main.java.emultake.model.TakeAppConfiguration;
import main.java.emultake.takeservices.TakeAPPService;
import main.java.emultake.takeservices.TakeMSGService;
import main.java.emultake.util.FileUtil;
import main.java.emultake.util.TerminalUtil;

/**
 * Main controller of the TAKE application.
 * 
 * @author Gisler Christophe
 *
 */
public class EmulTakeController {

	public static final String SEP = ",", PATH_SEP = File.separator, TAKE_APP_FILES_FOLDER_NAME = "take_files";

	private static final Logger LOGGER = LoggerFactory.getLogger(EmulTakeController.class);

	private final ThreadFactory threadFactory = Executors.defaultThreadFactory();
	private final ScheduledExecutorService scheduledExecutorService = Executors
			.newSingleThreadScheduledExecutor(threadFactory);
	private ExecutorService executorService = Executors.newSingleThreadExecutor(threadFactory);
	private Future<Void> takeJavaAppFuture;
	private boolean isTakeAppRunning = false;

	private Map<Integer, Scenario> scenarios = Collections.synchronizedMap(new LinkedHashMap<Integer, Scenario>()); // Key
																													// =
																													// scenario
																													// ID
	private Map<Integer, ScenarioRun> scenarioRuns = Collections
			.synchronizedMap(new LinkedHashMap<Integer, ScenarioRun>()); // Key = run ID
	private Map<Integer, Future<ScenarioRunResult>> futureScenarioRunResults = Collections
			.synchronizedMap(new LinkedHashMap<Integer, Future<ScenarioRunResult>>()); // Key = run ID

	private static volatile EmulTakeController instance = new EmulTakeController();

	private EmulTakeController() {
		// Load and display settings properties from configuration file
		LOGGER.info("TAKE Application Version: {}", Preferences.getAppVersion());
		LOGGER.info("EmulTAKE Application Config:\n{}", Preferences.getFormattedAppConfig());

		// Create data folder if not exist
		try {
			Files.createDirectories(Paths.get(Preferences.getDataDirPath()));
		} catch (IOException e) {
			LOGGER.error("Create data folder - Exception", e);
		}

		// Restore saved scenarios and scheduled runs
		if (Files.exists(Paths.get(Preferences.getDataDirPath() + Scenario.SCENARIOS_FOLDER_NAME))) {
			try {
				File[] scenarioFolders = FileUtil.getFilesStartingWith("scenario",
						Preferences.getDataDirPath() + Scenario.SCENARIOS_FOLDER_NAME);
				for (File scenarioFolder : scenarioFolders) {
					String scenarioJSON = FileUtil.readTextFromFile(
							Paths.get(scenarioFolder.getAbsolutePath(), Scenario.SCENARIO_JSON_FILE_NAME).toString());
					Scenario scenario = new Scenario(scenarioJSON);
					scenarios.put(scenario.getID(), scenario);
					File[] scenarioRunFolders = FileUtil.getFilesStartingWith("run", scenarioFolder.getAbsolutePath());
					for (File scenarioRunFolder : scenarioRunFolders) {
						String scenarioRunJSON = FileUtil.readTextFromFile(
								Paths.get(scenarioRunFolder.getAbsolutePath(), ScenarioRun.SCENARIO_RUN_JSON_FILE_NAME)
										.toString());
						ScenarioRun scenarioRun = new ScenarioRun(scenarioRunJSON);
						long delayUntilRun = scheduleScenarioRun(scenarioRun.getID(), scenarioRun.getScenario().getID(),
								scenarioRun.getStartDate());
						if (delayUntilRun < 0l)
							scenarioRuns.put(scenarioRun.getID(), scenarioRun);
						// else the run has already been added to scenarioRuns in
						// scheduleScenarioRun(...)
					}
				}
			} catch (Exception e) {
				LOGGER.error("Restore saved scenarios and scheduled runs - Exception", e);
			}
		}
	}

	public static EmulTakeController getInstance() {
		return instance;
	}

	public ThreadFactory getThreadFactory() {
		return threadFactory;
	}

	public Map<Integer, Scenario> getScenarios() {
		return scenarios;
	}

	public void addScenario(Scenario scenario) {
		// If scenario exists, cancel existing runs tied to it
		if (scenarios.containsKey(scenario.getID())) {
			Set<Integer> runIDs = getRunIDsTiedToScenarioID(scenario.getID());
			for (int runID : runIDs)
				cancelScenarioRun(runID);
		}
		// Add scenario
		scenarios.put(scenario.getID(), scenario);
	}

	private Set<Integer> getRunIDsTiedToScenarioID(int scenarioID) {
		Set<Integer> runIDs = new TreeSet<Integer>();
		for (ScenarioRun scenarioRun : scenarioRuns.values())
			if (scenarioRun.getScenario().getID() == scenarioID)
				runIDs.add(scenarioRun.getID());
		return runIDs;
	}

	public Scenario.Status getScenarioStatus(int scenarioID) {
		if (!scenarios.containsKey(scenarioID))
			return Scenario.Status.NA;
		return Scenario.Status.DEPLOYED;
	}

	public Map<Integer, ScenarioRun> getScenarioRuns() {
		return scenarioRuns;
	}

	public Map<Integer, Future<ScenarioRunResult>> getScenarioRunResults() {
		return futureScenarioRunResults;
	}

	public long scheduleScenarioRun(int runID, int scenarioID, Date startDate) throws Exception {
		cancelScenarioRun(runID); // Cancel run if already exists
		Date currentDate = new Date(); // Now
		long delayUntilRun = (startDate.getTime() - currentDate.getTime()) / 1000l; // [s] > 0
		delayUntilRun = delayUntilRun - (long) Preferences.getTakeAppLaunchDelayBeforeScenarioRun(); // Remove delay [s]
																										// to launch the
																										// Take
																										// application
		if (delayUntilRun >= 0l) {
			ScenarioRun scenarioRun = new ScenarioRun(runID, scenarios.get(scenarioID), startDate);
			scenarioRuns.put(runID, scenarioRun);
			futureScenarioRunResults.put(runID,
					scheduledExecutorService.schedule(scenarioRun, delayUntilRun, TimeUnit.SECONDS));
		}
		return delayUntilRun;
	}

	/**
	 * 
	 * @param runID
	 * @return true if a run with the given ID already existed and was cancelled,
	 *         false otherwise
	 */
	public boolean cancelScenarioRun(int runID) {
		if (scenarioRuns.containsKey(runID)) {
			scenarioRuns.get(runID).cancel();
			if (futureScenarioRunResults.containsKey(runID))
				futureScenarioRunResults.get(runID).cancel(true);
			return true;
		}
		return false;
	}

	public Status getScenarioRunStatus(int runID) {
		if (!scenarioRuns.containsKey(runID))
			return Status.NA;
		return scenarioRuns.get(runID).getStatus();
	}

	public ScenarioRunResult getScenarioRunResult(int runID) throws InterruptedException, ExecutionException {
		ScenarioRunResult result = scenarioRuns.get(runID).getResult();
		if (result == null)
			result = futureScenarioRunResults.get(runID).get();
		return result;
	}

	/**
	 * Compress the data folder (including the Take app files) of the given scenario
	 * run into a single file in tar.gz format and return it.
	 * 
	 * @param runID
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public File getScenrarioRunGeneratedFilesArchive(int runID) throws IOException, InterruptedException {
		ScenarioRun scenarioRun = scenarioRuns.get(runID);
		TerminalUtil.targz(Paths.get(scenarioRun.getPath().toString()).toString(), null);
		return new File(scenarioRun.getPath().toString() + ".tar.gz");
	}

	public void copyTakeAppFiles(String destPath, TakeAppConfiguration tac) throws IOException, InterruptedException {
		// Create destination folder in EmulTake data folder if it doesn't exist
		File destFolder = new File(destPath, TAKE_APP_FILES_FOLDER_NAME);
		destFolder.mkdirs();

		// Copy Take app created files (config files, logs, data, Munin,...) into a
		// single result folder
		File takeConfigProperties = new File(Preferences.getTakeAppDirPath(), "config.properties");
		TerminalUtil.copy(takeConfigProperties.getAbsolutePath(), destFolder.getAbsolutePath());
		File takeNodeSettings = new File(Preferences.getTakeAppDirPath(), "node_settings.csv");
		TerminalUtil.copy(takeNodeSettings.getAbsolutePath(), destFolder.getAbsolutePath());
		File takeLogFile = Paths.get(Preferences.getTakeAppDirPath(), "logs", "take.log").toFile();
		TerminalUtil.copy(takeLogFile.getAbsolutePath(), destFolder.getAbsolutePath());
		File takeDataFolder = new File(Preferences.getTakeAppDirPath(), tac.getDataDirPath());
		TerminalUtil.copyFolder(takeDataFolder.getAbsolutePath(), destFolder.getAbsolutePath());
		// FileUtil.deleteFolder(Paths.get(destFolder.getAbsolutePath(),
		// takeDataFolder.getName(), "mailbox").toString());
		File muninChartsFolder = Paths
				.get(File.separator + "var", "www", "html", "munin", "localdomain", "localhost.localdomain").toFile();
		if (muninChartsFolder.exists()) {
			File destMuninFolder = Paths.get(destFolder.getAbsolutePath(), "munin").toFile();
			destMuninFolder.mkdirs();
			FileUtil.copyFilesStartingByPrefixInFolder("if_enp", muninChartsFolder, destMuninFolder);
		}
	}

	public boolean isTakeAppRunning() {
		return isTakeAppRunning;
	}

	/**
	 * Reset the EmulTake application. All created and received files will be
	 * deleted.
	 */
	public void reset() {
		scenarios.clear();
		for (int runID : scenarioRuns.keySet())
			cancelScenarioRun(runID);
		futureScenarioRunResults.clear();
		scenarioRuns.clear();
		FileUtil.deleteAllFilesInFolder(Preferences.getDataDirPath());
	}

	/**
	 * Reset the Take application. All created and received files will be deleted.
	 * N.B.: If Take Java app is running, it will call the internal Take app reset
	 * method, else it will externally delete all created folders and files in the
	 * Take app folder
	 * 
	 * @throws IOException
	 */
	public void resetTakeJavaApp() throws IOException {
		// If Take Java app is running, it will call the internal Take app reset method
		if (takeJavaAppFuture != null) {
			TakeAPPService.getInstance().reset();
		} else { // Else it will externally delete all created folders and files in the Take app
					// folder
			TakeAppConfiguration tac = TakeAppConfiguration.createDefaultConfigurationFromFile();
			FileUtil.deleteFolder(Preferences.getTakeAppDirPath() + tac.getDataDirPath());
			FileUtil.deleteFolder(Preferences.getTakeAppDirPath() + "logs");
			FileUtil.deleteFolder(Preferences.getTakeAppDirPath() + "tomcat." + tac.getWebServicePort());
		}
	}

	public void stopTakeJavaApp() throws IOException, InterruptedException {
		if (takeJavaAppFuture != null) {
			TakeAPPService.getInstance().quit();
			takeJavaAppFuture.cancel(true);
			takeJavaAppFuture = null;
		}
		TerminalUtil.killJavaApplication("take.jar");
		TerminalUtil.killJavaApplication("take.jar &");
		isTakeAppRunning = false;
	}

	public void startTakeJavaApp(TakeAppConfiguration takeAppConfiguration, int secondsToWaitForStart)
			throws Exception {
		if (isTakeAppRunning) {
			stopTakeJavaApp();
		}
		if (!isTakeAppRunning) {
			takeAppConfiguration.saveConfigPropertiesTo(Preferences.getTakeAppDirPath());
			takeAppConfiguration.saveNodeSettingsTo(Preferences.getTakeAppDirPath());
			takeJavaAppFuture = executorService.submit(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					TerminalUtil.runJavaApplication(Preferences.getTakeAppDirPath(), "take.jar");
					isTakeAppRunning = true;
					return null;
				}
			});
			Thread.sleep(secondsToWaitForStart * 1000);
		}
	}

	/**
	 * Try to get version number from maven properties in jar's META-INF
	 * 
	 * @return version
	 * @throws Exception
	 */
	public String getTakeAppVersion() throws Exception {
		String mavenPackage = "Take";
		String mavenArtifact = "Take";
		String pomFileContent = TerminalUtil.executeCommand(null, "unzip", "-p",
				Preferences.getTakeAppDirPath() + "take.jar",
				"META-INF/maven/" + mavenPackage + "/" + mavenArtifact + "/pom.properties");
		Properties p = new Properties();
		p.load(new StringReader(pomFileContent));
		String version = p.getProperty("version", "").trim();
		if (!version.isEmpty())
			return version;
		return "N/A";
	}

	public String getRunningTakeAppInformation() throws Exception {
		if (!isTakeAppRunning)
			return "Take application not running\nNo information available";

		TakeAPPService takeAppService = TakeAPPService.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("1. Take application version: " + takeAppService.getVersion() + "\n");
		sb.append("2. " + takeAppService.getConfig() + "\n");
		sb.append("3. " + takeAppService.getNodeSettings() + "\n");

		return sb.toString();
	}

	public String testTakeAppMessageSending() throws Exception {
		TakeAppConfiguration takeAppConfiguration = TakeAppConfiguration.createDefaultConfigurationFromFile();
		boolean takeWasRunning = isTakeAppRunning;
		if (!takeWasRunning)
			startTakeJavaApp(takeAppConfiguration, 3);

		TakeAPPService takeAPPService = TakeAPPService.getInstance();
		TakeMSGService takeMSGService = TakeMSGService.getInstance();
		StringBuilder sb = new StringBuilder();
		try {
			String nodeName = takeAPPService.getNodeName();
			sb.append("1. Send test message to: " + nodeName + "\n"
					+ takeMSGService.sendMessageTest(nodeName, "HelloWorld") + "\n");
			sb.append("2. Get received message(s) for: " + nodeName + "\n" + takeMSGService.getMessageTest() + "\n");
			// sb.append("3. " + takeAPPService.reset() + "\n");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Test Take app message sending - Exception", e);
		}

		if (takeWasRunning)
			stopTakeJavaApp();
		return sb.toString();
	}

}
