package main.java.emultake.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.Context;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.File;

import javax.servlet.ServletException;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class Main {

	private static final String CONTEXT_PATH = "/emultake";
	private static final String DOC_BASE = "";
	private static final String RESOURCE_PACKAGE = "main.java.emultake.ws";
	private static final String SERVLET_MAPPING = "/*";

	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		new Main().start();
	}

	public void start() {
		// Load application preferences
		Preferences.load();

		// Create Tomcat server
		Tomcat tomcat = new Tomcat();

		// Define port number for the web application
		/*
		 * String webPort = System.getenv("PORT"); if (webPort == null ||
		 * webPort.isEmpty()) { webPort = "8081"; }
		 */

		// Bind the port to Tomcat server
		tomcat.setPort(Preferences.getWebServicePort()/* Integer.valueOf(webPort) */);
		tomcat.getHost().setAppBase(".");

		try {
			// Define a web application context
			Context context = tomcat.addWebapp(CONTEXT_PATH, new File(DOC_BASE).getAbsolutePath());

			// Add servlet that will register Jersey REST resources
			/*
			 * final Set<Class<?>> classes = new HashSet<Class<?>>(); // register root
			 * resource classes.add(Messaging.class); classes.add(BlueForceTracking.class);
			 * classes.add(RedForceTracking.class); classes.add(Test.class); ResourceConfig
			 * resourceConfig = new ResourceConfig(classes);
			 */
			ResourceConfig resourceConfig = new ResourceConfig();
			resourceConfig.packages(RESOURCE_PACKAGE);

			Tomcat.addServlet(context, "jersey-container-servlet", new ServletContainer(resourceConfig));// setAsyncSupported(true);
			context.addServletMappingDecoded(SERVLET_MAPPING, "jersey-container-servlet");

			// Launch Tomcat server
			tomcat.start();
			LOGGER.info("Embedded Tomcat server started");
			// Start the Take application controller which waits for incoming messages
			EmulTakeController.getInstance();
			tomcat.getServer().await();
		} catch (ServletException e) {
			LOGGER.error("Could not define the web application context", e);
		} catch (LifecycleException e) {
			LOGGER.error("Could not start the tomcat server", e);
		}
	}

}