package main.java.emultake.takeservices;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import main.java.emultake.controller.Preferences;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TakeRFTService extends HTTPService {

	private static volatile TakeRFTService instance = new TakeRFTService();

	private TakeRFTService() {
		super("http://localhost:" + Preferences.getTakeAppWebServicePort() + "/take/rft/", 0);
	}

	public static TakeRFTService getInstance() {
		return instance;
	}

	public String setMessageSendingPeriod(int period) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("period", period);
		return performHTTPRequest("setmessagesendingperiod", HttpMethod.POST, MediaType.TEXT_PLAIN,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String getEnemyLocations(float circleRadius, String polygonPoints) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("radius", circleRadius);
		parameters.put("points", polygonPoints);
		return performHTTPRequest("getenemylocations", HttpMethod.GET, MediaType.APPLICATION_JSON, null, parameters);
	}

	public String setEnemyLocation(String enemyID, String type, String latitude, String longitude) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("enemyid", enemyID);
		parameters.put("type", type);
		parameters.put("latitude", latitude);
		parameters.put("longitude", longitude);
		return performHTTPRequest("setenemylocation", HttpMethod.POST, MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

}
