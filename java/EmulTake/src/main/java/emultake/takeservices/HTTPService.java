package main.java.emultake.takeservices;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.emultake.controller.EmulTakeController;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class HTTPService {

	protected static final Logger LOGGER = LoggerFactory.getLogger(EmulTakeController.class);

	protected static final Charset UTF_8 = StandardCharsets.UTF_8;

	protected String serviceURL;
	protected int connectionTimeout; // [ms]

	public HTTPService(String serviceURL, int connectionTimeout) {
		this.serviceURL = serviceURL.endsWith("/") ? serviceURL : serviceURL + "/";
		this.connectionTimeout = connectionTimeout > 0 ? connectionTimeout : 0;
	}

	public String getServiceURL() {
		return serviceURL;
	}

	/**
	 * Get the connection timeout. A timeout of 0 is interpreted as an infinite
	 * timeout.
	 * 
	 * @return connectionTimeout in [ms]
	 */
	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	private static String convertParametersToString(Map<String, Object> parameters)
			throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Entry<String, Object> entry : parameters.entrySet()) {
			if (first)
				first = false;
			else
				result.append("&");
			result.append(URLEncoder.encode(entry.getKey(), UTF_8.name()));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue().toString(), UTF_8.name()));
		}
		return result.toString();
	}

	public String performHTTPRequest(String restMethod, String httpMethod, String acceptMediaType,
			String contentMediaType, Map<String, Object> parameters) throws IOException {
		String response = null;

		URL requestURL = new URL(serviceURL + restMethod);
		HttpURLConnection connection = (HttpURLConnection) requestURL.openConnection();
		connection.setConnectTimeout(connectionTimeout);
		// connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestMethod(httpMethod);

		if (acceptMediaType != null)
			connection.setRequestProperty("Accept", acceptMediaType);
		if (contentMediaType != null)
			connection.setRequestProperty("Content-Type", contentMediaType);

		if (parameters != null && !parameters.isEmpty()) {
			try (OutputStream os = connection.getOutputStream();
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));) {
				writer.write(convertParametersToString(parameters));
				writer.flush();
			}
		}

		if (connection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
			StringBuilder sb = new StringBuilder();
			if (parameters != null) {
				sb.append("\nParameters:");
				for (String paramName : parameters.keySet())
					sb.append("\n" + paramName + ": " + parameters.get(paramName));
			}
			throw new RuntimeException("HTTP " + httpMethod + " request with URL '" + requestURL.toExternalForm()
					+ "' failed - HTTP error code: " + connection.getResponseCode() + sb.toString());
		}
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream(), UTF_8))) {
			response = buffer.lines().collect(Collectors.joining("\n"));
		}

		connection.disconnect();

		return response;
	}

}
