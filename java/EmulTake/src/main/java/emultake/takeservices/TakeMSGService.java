package main.java.emultake.takeservices;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import main.java.emultake.controller.Preferences;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TakeMSGService extends HTTPService {

	private static volatile TakeMSGService instance = new TakeMSGService();

	private TakeMSGService() {
		super("http://localhost:" + Preferences.getTakeAppWebServicePort() + "/take/msg/", 0);
	}

	public static TakeMSGService getInstance() {
		return instance;
	}

	public String getNodeStatus(String nodeNames) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("nodenames", nodeNames);
		return performHTTPRequest("getnodestatus", HttpMethod.GET, MediaType.APPLICATION_JSON, null, parameters);
	}

	public String sendMessage(int type, String title, String content, String nodeNames) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("type", type);
		parameters.put("title", title);
		parameters.put("content", content);
		parameters.put("nodenames", nodeNames);
		return performHTTPRequest("sendmessage", HttpMethod.POST, MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String getMessages(int statusID, String fromDate) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("statusid", statusID);
		parameters.put("fromdate", fromDate);
		return performHTTPRequest("getmessages", HttpMethod.GET, MediaType.APPLICATION_JSON, null, parameters);
	}

	public String sendMessageStatus(String messageID, String status) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("messageid", messageID);
		parameters.put("status", status);
		return performHTTPRequest("sendmessagestatus", HttpMethod.POST, MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String getMessageStatus(String messageID) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("messageid", messageID);
		return performHTTPRequest("getmessagestatus", HttpMethod.GET, MediaType.APPLICATION_JSON, null, parameters);
	}

	/* ************ */
	/* TEST METHODS */
	/* ************ */

	public String sendMessageTest(String recipientName, String subject) throws IOException {
		return performHTTPRequest(
				"sendmsgtest/" + URLEncoder.encode(recipientName, UTF_8.name()) + "/"
						+ URLEncoder.encode(subject, UTF_8.name()),
				HttpMethod.GET, MediaType.APPLICATION_JSON, null, null);
	}

	public String getMessageTest() throws IOException {
		return performHTTPRequest("getmsgtest", HttpMethod.GET, MediaType.APPLICATION_JSON, null, null);
	}

}
