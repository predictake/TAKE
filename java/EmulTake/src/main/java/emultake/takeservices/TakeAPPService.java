package main.java.emultake.takeservices;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import main.java.emultake.controller.Preferences;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TakeAPPService extends HTTPService {

	private static volatile TakeAPPService instance = new TakeAPPService();

	private TakeAPPService() {
		super("http://localhost:" + Preferences.getTakeAppWebServicePort() + "/take/app/", 0);
	}

	public static TakeAPPService getInstance() {
		return instance;
	}

	public String getVersion() throws IOException {
		return performHTTPRequest("getversion", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String getNodeName() throws IOException {
		return performHTTPRequest("getnodename", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String getConfig() throws IOException {
		return performHTTPRequest("getconfig", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String getNodeSettings() throws IOException {
		return performHTTPRequest("getnodesettings", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String setNodeName(String nodeName) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("nodename", nodeName);
		return performHTTPRequest("setnodename", HttpMethod.POST, MediaType.TEXT_PLAIN,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String setSendingStrategy(String sendingStrategyName, String responseTimeout) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("strategy", sendingStrategyName);
		parameters.put("timeout", responseTimeout);
		return performHTTPRequest("setsendingstrategy", HttpMethod.POST, MediaType.TEXT_PLAIN,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String saveTransmissionStatistics() throws IOException {
		return performHTTPRequest("savetransmissionstats", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String reset() throws IOException {
		return performHTTPRequest("reset", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String quit() throws IOException {
		return performHTTPRequest("quit", HttpMethod.GET, MediaType.TEXT_PLAIN, null, null);
	}

	public String forceBFTRFTMessageSending(boolean forced) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("forced", forced);
		return performHTTPRequest("setforcebftrftmsgsending", HttpMethod.POST, MediaType.TEXT_PLAIN,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

}
