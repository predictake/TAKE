package main.java.emultake.takeservices;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import main.java.emultake.controller.Preferences;

/**
 * 
 * @author Gisler Christophe
 *
 */
public class TakeBFTService extends HTTPService {

	private static volatile TakeBFTService instance = new TakeBFTService();

	private TakeBFTService() {
		super("http://localhost:" + Preferences.getTakeAppWebServicePort() + "/take/bft/", 0);
	}

	public static TakeBFTService getInstance() {
		return instance;
	}

	public String setMessageSendingPeriod(int period) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("period", period);
		return performHTTPRequest("setmessagesendingperiod", HttpMethod.POST, MediaType.TEXT_PLAIN,
				MediaType.APPLICATION_FORM_URLENCODED, parameters);
	}

	public String getAllyLocations(float circleRadius, String polygonPoints) throws IOException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("radius", circleRadius);
		parameters.put("points", polygonPoints);
		return performHTTPRequest("getallylocations", HttpMethod.GET, MediaType.APPLICATION_JSON, null, parameters);
	}

}
