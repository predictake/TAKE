// g++ -std=c++11 -o configs configs.cc; ./configs

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string> 

/*

Algorithm 0 -> Direct message
Algorithm 1 -> Get Closer
Algorithm 2 -> Section based

Topology 0 -> 4 nodes
Topology 1 -> 4 nodes (fully-meshed)
Topology 2 -> 10 nodes
Topology 3 -> 10 nodes (fully-meshed)
Topology 4 -> 60 nodes
Topology 5 -> 60 nodes (fully-meshed)

Throughput 0 -> 2 kb/s
Throughput 1 -> 50 kb/s
Throughput 2 -> 200 kb/s

Latency 0 -> 0 second
Latency 1 -> 1.5 seconds
Latency 2 -> 25 seconds

*/

int main(int argc, char const *argv[]) {
	
	//------------------------------------------------------------------------
	/* input parameters */

	int algorithm=1;	// number of algorithm (3)

	int topology=1;		// number of topology (6)

	int throughput=3;	// number of throughput (3)
	std::string throughputs[] = {"2", "50", "200"}; // different throughputs

	int latency=3;		// number of latency (3)
	std::string latencies[] = {"0", "1.5", "25"};	// different latencies
	
	//------------------------------------------------------------------------

	int config = 1;		// initial number of the config

	/* iterate the algorithm */
	for (int algo = 0; algo < algorithm; ++algo)	{

		/* iterate the topology */
		for (int topo = 0; topo < topology; ++topo) {

			/* iterate the throughput */
			for (int through = 0; through < throughput; ++through) {
				
				/* iterate the latency */
				for (int lat = 0; lat < latency; ++lat)	{

					// generate the config file
					std::ofstream file;
					std::string filename = "tc" + std::to_string(config) + ".cfg";
 					file.open (filename);
				  	file << "algorithm=" + std::to_string(algo)	<< std::endl;
					file << "topology=" + std::to_string(topo) << std::endl;
					file << "throughput=" + throughputs[through] << std::endl;
					file << "latency=" + latencies[lat];

				  	file.close();
					
					std::cout << "[+] Generated : " << filename << std::endl;
					config++; // next config
				}
			}
		}
	}

	return 0;
}