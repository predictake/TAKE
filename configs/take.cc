/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/********************************************************************************/
/* take.cc */
/*

Change the configuration of the waf to use c11

CXXFLAGS="-std=c++0x" ./waf configure
./waf --run "scratch/take --config=tc1.cfg"

*/

// ns3 includes
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"  

#include "ns3/wifi-module.h"
#include "ns3/olsr-module.h"

#include "ns3/flow-monitor-module.h"  // needed to monitor network 
#include "ns3/flow-monitor-helper.h"  // needed to monitor network 
#include "ns3/gnuplot.h"              // needed to generate graphics with gnuplot

#include "ns3/mobility-module.h"      // needed to move nodes

#include "ns3/netanim-module.h"       // needed to generate NetAnim simulation
#include "ns3/animation-interface.h"

#include "algo-0.h"                // import algo 0
//#include "algo-1.h"                // import algo 1 (not implemented yet)
//#include "algo-2.h"                // import algo 2 (not implemented yet)

// cpp includes
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <unordered_map>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/reader.h"
#include "rapidjson/filereadstream.h"

#include <cstdio>
#include <iostream>

NS_LOG_COMPONENT_DEFINE ("take");

using namespace ns3;
using namespace rapidjson;

// -----------------------------------------------------------------------------------
// Topology 

//Vector(x,y,z)
std::vector<std::vector<Vector>> topologies {
    // topology 0 : 4 Nodes
    {
        Vector(0, 0, 0), // node 0 
        Vector(0, 300, 0), // node 1 
        Vector(150, 150, 0), // node 2
        Vector(300, 150, 0) // node 3 
    },

    // topology 1 : 4 Nodes full meshed
    {
        Vector(0, 0, 0),
        Vector(0, 150, 0),
        Vector(150,0,0),
        Vector(150,150,0)
    },
    // topology 2 : 60 Nodes Random position
    {
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),   
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),   
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0),
        Vector(0, 0, 0)
    },
    // topology 3 : 60 Nodes full meshed
    {
        Vector(0, 0, 0),
        Vector(10, 0, 0),
        Vector(20, 0, 0),
        Vector(30, 0, 0),
        Vector(40, 0, 0),
        Vector(50, 0, 0),
        Vector(60, 0, 0),
        Vector(70, 0, 0),
        Vector(80, 0, 0),
        Vector(90, 0, 0),
        Vector(0, 10, 0),
        Vector(10, 10, 0),
        Vector(20, 10, 0),
        Vector(30, 10, 0),
        Vector(40, 10, 0),
        Vector(50, 10, 0),
        Vector(60, 10, 0),
        Vector(70, 10, 0),
        Vector(80, 10, 0),
        Vector(90, 10, 0),   
        Vector(0, 20, 0),
        Vector(10, 20, 0),
        Vector(20, 20, 0),
        Vector(30, 20, 0),
        Vector(40, 20, 0),
        Vector(50, 20, 0),
        Vector(60, 20, 0),
        Vector(70, 20, 0),
        Vector(80, 20, 0),
        Vector(90, 20, 0),
        Vector(0, 30, 0),
        Vector(10, 30, 0),
        Vector(20, 30, 0),
        Vector(30, 30, 0),
        Vector(40, 30, 0),
        Vector(50, 30, 0),
        Vector(60, 30, 0),
        Vector(70, 30, 0),
        Vector(80, 30, 0),
        Vector(90, 30, 0),
        Vector(0, 40, 0),
        Vector(10, 40, 0),
        Vector(20, 40, 0),
        Vector(30, 40, 0),
        Vector(40, 40, 0),
        Vector(50, 40, 0),
        Vector(60, 40, 0),
        Vector(70, 40, 0),
        Vector(80, 40, 0),
        Vector(90, 40, 0),   
        Vector(0, 50, 0),
        Vector(10, 50, 0),
        Vector(20, 50, 0),
        Vector(30, 50, 0),
        Vector(40, 50, 0),
        Vector(50, 50, 0),
        Vector(60, 50, 0),
        Vector(70, 50, 0),
        Vector(80, 50, 0),
        Vector(90, 50, 0)
    }
};

// structure of messages
struct Distribution {
  int size;
  int time;
  int src;
  int dst;
  //std::vector<int> dsts;
};

// -----------------------------------------------------------------------------------

// Print on stdout the time and the size of the packet received
/*void ReceivePacket(Ptr<const Packet> p, const Address & addr) {

  std::cout << Simulator::Now ().GetSeconds () << "\t" << p->GetSize() <<"\n";
}*/

 // Monitor trafic and prepare dataset to gnuplot
void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, std::string prefix) {
  double localThrou=0;
  std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
  Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier());
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats) {
    Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
    std::cout<<"Flow ID     : " << stats->first <<" ; "<< fiveTuple.sourceAddress <<" -----> "<<fiveTuple.destinationAddress<<std::endl;
    std::cout<<"Tx Packets = " << stats->second.txPackets<<std::endl;
    std::cout<<"Rx Packets = " << stats->second.rxPackets<<std::endl;
    //std::cout<<"Duration    : "<<(stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds())<<std::endl;
    std::cout<<"Last Received Packet  : "<< stats->second.timeLastRxPacket.GetSeconds()<<" Seconds"<<std::endl;
    std::cout<<"Throughput: " << stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds())/1024/1024  << " Mbps"<<std::endl;
    
    localThrou=(stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds())/1024/1024);
    // update gnuplot data
    //DataSet.Add((double)Simulator::Now().GetSeconds(),(double) localThrou);
    std::cout<<"---------------------------------------------------------------------------"<<std::endl;
  }

  flowMon->SerializeToXmlFile (prefix + "-flowmon.xml", true, true);
 
}

// Method to change the position of the node
 static void SetPosition (Ptr<Node> node, double x, double y = 0.0) {

  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  Vector pos = mobility->GetPosition();
  pos.x = x;
  pos.y = y;
  mobility->SetPosition(pos);
 }

typedef std::unordered_map<std::string, std::string> config_type;
static config_type configuration; // stock the configuration

// check the existence of the file
bool file_exists(const std::string& name){
    std::ifstream f(name.c_str());

    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }
}

// load the configuration file
bool load_configuration(const std::string& path, config_type& configuration){
    std::cout << "[+] Loading configuration from file " << path.c_str() << std::endl;

    // check if the file exists
    if(file_exists(path)){

        // open the file for reading operations
        std::ifstream file(path.c_str());

        // parse the file if opened and exists
        if(file.is_open() && file.good()){
            std::string line;
            while(file.good() && getline(file, line)){
                auto first = line.find('=');

                if(first == std::string::npos || line.rfind('=') != first){
                    std::cout << "[-] The configuration file " << path << " is invalid only supports entries in form of key=value" << std::endl;

                    return false;
                }
                
                auto key = line.substr(0, first);
                auto value = line.substr(first + 1, line.size());

                configuration[key] = value;
            }
        }
    }
    else {
      std::cout << "[-] The configuration file does not exist" << std::endl;
      return false;
    }

    return true;
}

// -----------------------------------------------------------------------------------
int main (int argc, char *argv[]) {

  bool enableFlowMonitor = false; // don't use flow monitor by default
  bool enableRateError = false; // don't activate rate error by default
  bool enablePcap = false; // don't activate pcap capture by defautl

  std::string phyMode ("DsssRate1Mbps"); // default physic mode
  int stopTime = 50; // default stop time of the simulation
  //std::string config ("tc1.cfg"); // default config to load
  int startConfig = 1;
  int stopConfig = 1;

  int nbPacket = 1; // default number of packet by message
  //CostFunc costFunc;
  
  
  // add attributes for the user input
  CommandLine cmd;
  cmd.AddValue ("enableMonitor", "Enable flow monitor", enableFlowMonitor);
  cmd.AddValue ("enablePcap", "Enable pcap capture", enablePcap);
  cmd.AddValue ("enableRateError", "Enable rate error", enableRateError);

  cmd.AddValue ("phyMode", "Wifi Physical mode", phyMode);
  cmd.AddValue ("stopTime", "Time in seconds to stop the simulation", stopTime);
  cmd.AddValue ("startConfig", "First configuration file to load", startConfig);
  cmd.AddValue ("stopConfig", "Last configuration file to load", stopConfig);

  // user input
  cmd.Parse (argc, argv);

  std::cout << "---------------------------------------------------" << std::endl;
  std::cout << "TAKE v0.2" << std::endl;
  std::cout << "" << std::endl;

  std::cout << "###################################################" << std::endl;
  std::cout << "" << std::endl;

  for (int tc = startConfig; tc <= stopConfig; tc++) {
    myCostFunc.StartRTTLog(tc);
    const clock_t begin_time = clock(); // calculate time to run simulation

    std::string config = "tc" + std::to_string(tc) + ".cfg";

    // configuration import from config file
    bool load = load_configuration("scratch/"+config, configuration);
    if (load) {
      std::cout << "[+] Configuration successfully loaded" << std::endl;
    } else {
      std::cout << "[-] Error while loading config" << std::endl;
    }
    
    std::cout << "" << std::endl;
    std::cout << "---------------------------------------------------" << std::endl;
    std::cout << "Algorithm : " << configuration["algorithm"] << std::endl;
    std::cout << "Topology : " << configuration["topology"] << std::endl;
    std::cout << "Throughput : " << configuration["throughput"] << " [kb/s]" << std::endl;
    std::cout << "Latency : " << configuration["latency"] << " [sec]" << std::endl;
    std::cout << "---------------------------------------------------" << std::endl;

    // cast the strings parameters in integer
    int topology = std::stoi(configuration["topology"]);
    int throughput = std::stoi(configuration["throughput"]);
    int latency = std::stoi(configuration["latency"]);
    int algorithm = std::stoi(configuration["algorithm"]);

    // cast throughput to the right format
    std::string throughput_str = "";
    switch(throughput) {
      case 2: 
        throughput_str = "2Kbps";
        break;
      case 50: 
        throughput_str = "50Kbps";
        break;
      case 200: 
        throughput_str = "200Kbps";
        break;
      default : 
        std::cout << "[-] No valid throughput found, use default value 2kbps" << std::endl;
        throughput_str = "2Kbps";    
    }

    std::string prefix (config.substr(0, config.find("."))); // take prefix
    
    int nbNodes = topologies[topology].size(); // number of nodes of the topology

    // -----------------------------------------------------------------------------------
    // Loading the distribution configuration from JSON file
    
    std::cout << "" << std::endl;
    std::string distribution_filename = "scratch/distribution-" + std::to_string(nbNodes) + ".json";

    std::cout << "[+] Loading distribution from file " << distribution_filename << std::endl;
    FILE* fp = fopen(distribution_filename.c_str(), "r");
    char readBuffer[65536];
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));
    
    Document document;
    document.ParseStream(is);
    fclose(fp);

    assert(document.IsObject()); 
    assert(document.HasMember("nbMessage"));

    // getting values from JSON
    int nbMessage = document["nbMessage"].GetInt(); // save nbMessage value

    const Value& m = document["messages"]; // Using a reference for consecutive access is handy and faster.
    assert(m.IsArray());

    Distribution distribution[nbMessage]; // todo dynamic

    for (SizeType i = 0; i < m.Size(); i++) {
      distribution[i].size = (m[i]["size"].GetInt()); // save size value
      distribution[i].time = (m[i]["time"].GetInt()); // save time value
      distribution[i].src = (m[i]["src"].GetInt()); // save src value
      
      distribution[i].dst = (m[i]["dst"].GetInt()); // save dst value

      /*
      const Value& d = m[i]["dst"]; // load all the dst

      // save the destinations
      for (int i = 0; i < d.Size(); ++i) {
        distribution[i].dsts.push_back(d[i].GetInt()); // save dst value
      }
      */
    }

    std::cout << "[+] Distribution successfully loaded with " << nbMessage << " messages" << std::endl;

    // -----------------------------------------------------------------------------------
    // Explicitly create the nodes required by the topology (shown above).
    NS_LOG_INFO ("Create nodes.");
    NodeContainer c; // ALL Nodes
    c.Create(nbNodes); // create nbNodes nodes

    // Set up WiFi
    WifiHelper wifi;
    YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
    wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

    // add error rate
    // 1 Mbps mode is based on DBPSK. BER is from equation 5.2-69 from 
    // John G. Proakis Digitial Communications, 2001 editio
    if (enableRateError) wifiPhy.SetErrorRateModel ("ns3::YansErrorRateModel");

    // Add propagation delay and propagation loss
    YansWifiChannelHelper wifiChannel ;
    wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
    wifiChannel.AddPropagationLoss ("ns3::TwoRayGroundPropagationLossModel",
      "SystemLoss", DoubleValue(1),
      "HeightAboveZ", DoubleValue(1.5));

    // For range near 250m
    wifiPhy.Set ("TxPowerStart", DoubleValue(33));
    wifiPhy.Set ("TxPowerEnd", DoubleValue(33));
    wifiPhy.Set ("TxPowerLevels", UintegerValue(1));
    wifiPhy.Set ("TxGain", DoubleValue(0));
    wifiPhy.Set ("RxGain", DoubleValue(0));
    wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue(-61.8));
    wifiPhy.Set ("CcaMode1Threshold", DoubleValue(-64.8));

    wifiPhy.SetChannel (wifiChannel.Create ());

    // Add a non-QoS upper mac
    NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
    wifiMac.SetType ("ns3::AdhocWifiMac");

    // Set 802.11b standard
    wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

    wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
      "DataMode",StringValue(phyMode),
      "ControlMode",StringValue(phyMode));

    // creates devices on the nodes in the node container c
    NetDeviceContainer devices;
    devices = wifi.Install (wifiPhy, wifiMac, c);

    //  Enable OLSR
    OlsrHelper olsr;

    // Install the routing protocol OLSR
    Ipv4ListRoutingHelper list;
    list.Add (olsr, 10); // add olsr with a priority of 10

    // Set up internet stack on the nodes container
    InternetStackHelper internet;
    internet.SetRoutingHelper (list);
    internet.Install (c);

    // Set up Addresses
    Ipv4AddressHelper ipv4;
    NS_LOG_INFO ("Assign IP Addresses.");
    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer ifcont = ipv4.Assign (devices);
    
    // -----------------------------------------------------------------------------------
    // Set Mobility for all nodes
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject <ListPositionAllocator>();
    
    std::cout<< "" << std::endl;
    std::cout << "[+] Creating topology " << topology << std::endl;
    // create simple topology
    for (int i = 0; i < nbNodes; i++) {
      positionAlloc ->Add(topologies[topology][i]); // node i
      std::cout << "[+] Node " << i << " created at : " << topologies[topology][i] << std::endl;

    }
    std::cout<< "" << std::endl;

    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(c); // install the mobility on the nodes

    // -----------------------------------------------------------------------------------
    NS_LOG_INFO ("Creating applications.");

    // Create the different messages from the fixed distribution
    std::cout << "[+] Creating " << nbMessage << " messages using algorithm " << algorithm << std::endl;

    ApplicationContainer sinkApps[nbMessage]; // container for all the destination
    uint16_t dstPort = 666; // destination port


    Ptr<Socket> ns3UdpServerSockets[nbMessage];      // all different socket connexion
    Ptr<Socket> ns3UdpClientSockets[nbMessage];

    Ptr<Algo0> app0_serv[nbMessage]; // algorithm 0 : Direct message server side
    Ptr<Algo0> app0_cli[nbMessage];  // algotithm 0 : Direct message client side
    // TODO ALGO1
    Ptr<Algo0> app1[nbMessage]; // algorithm 1 : Direct wave
    // TODO ALGO2
    Ptr<Algo0> app2[nbMessage]; // algorithm 2 : Military based

/*
    Ptr<Socket> ns3UdpSocket[nbMessage];      // all different socket connexion

    Ptr<Algo0> app0[nbMessage]; // algorithm 0 : Direct message
    // TODO ALGO1
    Ptr<Algo0> app1[nbMessage]; // algorithm 1 : Get Closer
    // TODO ALGO2
    Ptr<Algo0> app2[nbMessage]; // algorithm 2 : Section based
*/
    int size = 0;
    int time = 0;
    int src = 0;
    int dst = 0;

    for (int i = 0; i < nbMessage; i++) {
      
      size = distribution[i].size; // get the size parameter from distribution (bytes)
      time = distribution[i].time; // get the time parameter from distribution (second)
      src = distribution[i].src; // get the src parameter from distribution (node ip)
      dst = distribution[i].dst; // get the dst parameter from distribution (node ip)
      
      // handle multicast as multiple unicast
      //int multicast = distribution[i].dst.size();
      //int dst[distribution[i].dst.size();
      //std::cout << "[+] Multicast : " << multicast << std::endl;
      //dst = distribution[i].dsts[0]; // get the dst parameter from distribution (node ip)

      Address dstAddress (InetSocketAddress (ifcont.GetAddress (dst), dstPort));
      // destination of the message  
      ns3UdpServerSockets[i]=Socket::CreateSocket (c.Get (dst), UdpSocketFactory::GetTypeId ());  
      app0_serv[i] = CreateObject<Algo0> ();
      app0_serv[i]->Setup (ns3UdpServerSockets[i], dstAddress, size, nbPacket, DataRate (throughput_str),1,dst);/*,costFunc);*/
      c.Get (dst)->AddApplication (app0_serv[i]);
      app0_serv[i]->SetStartTime (Seconds (0.));
      app0_serv[i]->SetStopTime (Seconds (stopTime));
      
      //Address dstAddress (InetSocketAddress (ifcont.GetAddress (dst), dstPort)); // get destination address
      /*PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dstPort)); // create packet helper

      sinkApps[i] = packetSinkHelper.Install (c.Get (dst)); // install the node as destination
      sinkApps[i].Start (Seconds (0.));                     // start to listen at begining
      sinkApps[i].Stop (Seconds (stopTime));                // stop to listen at stopTime
      */
      ns3UdpClientSockets[i] = Socket::CreateSocket (c.Get (src), UdpSocketFactory::GetTypeId ()); // create socket
      // source of the message with specified algorithm
      // Algorithm 0 : Direct message
      if (algorithm==0) {
        app0_cli[i] = CreateObject<Algo0> ();
        app0_cli[i]->Setup (ns3UdpClientSockets[i], dstAddress, size, nbPacket, DataRate (throughput_str),0,dst);/*,costFunc);*/
        c.Get (src)->AddApplication (app0_cli[i]); // add the application to the source node
        app0_cli[i]->SetStartTime (Seconds (time));
        app0_cli[i]->SetStopTime (Seconds (stopTime)); 
      }
      // Algorithm 1 : Direct wave
      else if (algorithm==1) {
        app1[i] = CreateObject<Algo0> ();
        app1[i]->Setup (ns3UdpClientSockets[i], dstAddress, size, nbPacket, DataRate (throughput_str),0,dst);/*,costFunc);*/
        c.Get (src)->AddApplication (app1[i]); // add the application to the source node
        app1[i]->SetStartTime (Seconds (time));
        app1[i]->SetStopTime (Seconds (stopTime));  
      }
      // Algorithm 2 : Military based
      else if (algorithm==2) {
        app2[i] = CreateObject<Algo0> ();
        app2[i]->Setup (ns3UdpClientSockets[i], dstAddress, size, nbPacket, DataRate (throughput_str),0,dst);/*,costFunc);*/
        c.Get (src)->AddApplication (app2[i]); // add the application to the source node
        app2[i]->SetStartTime (Seconds (time));
        app2[i]->SetStopTime (Seconds (stopTime));     
      }
      else std::cout << "[-] Algorithm not valid" << std::endl;

      std::cout << "[+] Message " << i << " : At " << time
                << " from " << src << " to " << dst << std::endl;
    }
    
    NS_LOG_INFO ("applications ok");

    // TODO add movement in the simulation ...

    // node x move y meter on the right at z sec
    //Simulator::Schedule (Seconds (15.0), &SetPosition, c.Get (5), 500, 150);

    // -----------------------------------------------------------------------------------
    
    // Trace Received Packets
    //Config::ConnectWithoutContext("/NodeList/*/applicationList/*/$ns3::PacketSink/Rx", MakeCallback (&ReceivePacket));
    
    if (enableFlowMonitor) {
      //flowMonitor declaration
      FlowMonitorHelper fmHelper;
      Ptr<FlowMonitor> allMon = fmHelper.InstallAll();
      // call the flow monitor function
      ThroughputMonitor(&fmHelper, allMon, prefix);   
    }
    
    // Trace devices (pcap) if enabled
    if(enablePcap) wifiPhy.EnablePcap (prefix, devices);

    // Generate the animation file to import in NetAnim
    AnimationInterface anim (prefix + "-animation.xml");
    anim.EnableIpv4RouteTracking (prefix + "-routingtable.xml", Seconds(0), Seconds(stopTime), Seconds(1)); // generate xml file with all routing tables
    anim.EnablePacketMetadata (true); // add usefull information to NetAnim
    
    for (int i = 0; i < nbNodes; i++) { // set the size of all nodes at factor 5
      anim.UpdateNodeSize (i, 10, 10);
    }

    // Do the actual simulation.
    NS_LOG_INFO ("Run Simulation.");
    Simulator::Stop (Seconds(stopTime)); // stop the simulation at stopTime
    Simulator::Run (); // launch the simulation

    NS_LOG_INFO ("Done.");
    Simulator::Destroy (); // release all the ressources for this simulation

    // calculate time to run simulation
    const float end_time = float( clock () - begin_time ) /  CLOCKS_PER_SEC; 
    std::cout << "" << std::endl;
    std::cout << "[+] Simulation of " << prefix << " done in " << end_time << " seconds ..." << std::endl;
    std::cout << "" << std::endl;
    std::cout << "###################################################" << std::endl;
    std::cout << "" << std::endl;
    myCostFunc.StopRTTLog();
    myCostFunc.WriteInRTTinCSV(tc);
  }
}
